﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi.PostgreSQL
{

    /// <summary>
    /// Formulář pro dump large objektů tabulky
    /// </summary>
    public partial class FormDumpLargeObjects : Form
    {

        #region Private Fields

        /// <summary>
        /// Připojení k databázi
        /// </summary>
        private PostgreSQLWrapper database;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// Konstruktor formuláře
        /// </summary>
        public FormDumpLargeObjects()
        {
            InitializeComponent();
            Initialize(database: null);
        }

        /// <summary>
        /// Konstruktor formuláře
        /// </summary>
        /// <param name="database">Připojení k databázi</param>
        public FormDumpLargeObjects(PostgreSQLWrapper database)
        {
            InitializeComponent();
            Initialize(database: database);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// Připojení k databázi
        /// </summary>
        private PostgreSQLWrapper Database
        {
            get
            {
                return database;
            }
            set
            {
                database = value;
            }
        }

        #endregion Properties


        #region Methods

        /// <summary>
        /// Inicializace formuláře
        /// </summary>
        /// <param name="database">Připojení k databázi</param>
        public void Initialize(PostgreSQLWrapper database)
        {
            Database = database;
            Database.Catalog.Load();

            lblDatabaseValue.Text = String.Concat(
                $"{Database.Host}: ",
                $"{Database.Database}");

            SetComboBoxSchema();

            cboSchema.SelectedIndexChanged +=
                new EventHandler(
                (sender, e) => { SetComboBoxTable(); });

            cboTable.SelectedIndexChanged +=
                new EventHandler(
                (sender, e) => { SetComboBoxColumn(); });

            btnOpenFolder.Click +=
                new EventHandler(
                (sender, e) => { OpenFolder(); });

            btnDump.Click +=
                new EventHandler(
                (sender, e) =>
                {
                    Dump();
                    Close();
                });

            btnRestore.Click +=
                new EventHandler(
                (sender, e) =>
                {
                    Restore();
                    Close();
                });

            btnClose.Click += new EventHandler(
                (sender, e) => { Close(); });
        }

        /// <summary>
        /// Nastaví položky v ComboBox pro schéma
        /// </summary>
        private void SetComboBoxSchema()
        {
            cboSchema.DataSource = null;

            if ((Database == null) ||
                (!Database.Initialized) ||
                (!Database.Catalog.Initialized)) { return; }

            cboSchema.DataSource =
                database.Catalog.Schemas.Items
                    .OrderBy(a => a.Name)
                    .ToList();
            cboSchema.DisplayMember = "Name";
            cboSchema.ValueMember = "Id";
            if (cboSchema.Items.Count > 0)
            {
                cboSchema.SelectedIndex = 0;
            }
        }

        /// <summary>
        /// Nastaví položky v ComboBox pro tabulku
        /// </summary>
        private void SetComboBoxTable()
        {
            cboTable.DataSource = null;

            if ((Database == null) ||
                (!Database.Initialized) ||
                (!Database.Catalog.Initialized)) { return; }

            if (cboSchema.SelectedItem == null) { return; }

            DBSchema schema = (DBSchema)cboSchema.SelectedItem;

            cboTable.DataSource =
                schema.Tables.Items
                    .Where(a => a.Columns.Items
                        .Where(b => b.TypeInfo == "oid")
                        .Any())
                    .OrderBy(a => a.Name)
                    .ToList();
            cboTable.DisplayMember = "Name";
            cboTable.ValueMember = "Id";
        }

        /// <summary>
        /// Nastaví položky v ComboBox pro sloupec
        /// </summary>
        private void SetComboBoxColumn()
        {
            cboColumn.DataSource = null;

            if ((Database == null) ||
                (!Database.Initialized) ||
                (!Database.Catalog.Initialized)) { return; }

            if (cboTable.SelectedItem == null) { return; }

            DBTable table = (DBTable)cboTable.SelectedItem;

            cboColumn.DataSource =
                table.Columns.Items
                    .Where(a => a.TypeInfo == "oid")
                    .OrderBy(a => a.Name)
                    .ToList();
            cboColumn.DisplayMember = "Name";
            cboColumn.ValueMember = "Id";
        }

        /// <summary>
        /// Otevře složku pro large objekty
        /// </summary>
        private void OpenFolder()
        {
            FolderBrowserDialog dlg = new();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                lblFolderPathValue.Text = dlg.SelectedPath;
            }
        }

        /// <summary>
        /// Spustí dump large objektů
        /// </summary>
        private void Dump()
        {
            if ((Database == null) ||
                        (!Database.Initialized) ||
                        (!Database.Catalog.Initialized)) { return; }
            if (cboTable.SelectedItem == null) { return; }
            if (cboColumn.SelectedItem == null) { return; }
            if (String.IsNullOrEmpty(value: lblFolderPathValue.Text)) { return; }

            DBTable table = (DBTable)cboTable.SelectedItem;
            DBColumn column = (DBColumn)cboColumn.SelectedItem;

            Database.LoDump(
                table: table,
                column: column,
                folder: lblFolderPathValue.Text);

            MessageBox.Show(
                text: "Hotovo",
                caption: String.Empty,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);
        }

        /// <summary>
        /// Spustí restore large objektů
        /// </summary>
        private void Restore()
        {
            if ((Database == null) ||
                (!Database.Initialized) ||
                (!Database.Catalog.Initialized)) { return; }
            if (cboTable.SelectedItem == null) { return; }
            if (cboColumn.SelectedItem == null) { return; }
            if (String.IsNullOrEmpty(value: lblFolderPathValue.Text)) { return; }

            DBTable table = (DBTable)cboTable.SelectedItem;
            DBColumn column = (DBColumn)cboColumn.SelectedItem;

            Database.LoRestore(
                folder: lblFolderPathValue.Text,
                table: table,
                column: column);

            MessageBox.Show(
                text: "Hotovo",
                caption: String.Empty,
                buttons: MessageBoxButtons.OK,
                icon: MessageBoxIcon.Information);
        }

        #endregion Methods

    }

}
