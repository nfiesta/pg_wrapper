﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Linq;
using System.Windows.Forms;

namespace ZaJi.PostgreSQL.DataModel
{

    /// <summary>
    /// Form to display data of the database schema
    /// </summary>
    public partial class FormSchemaData
        : Form
    {

        #region Private Fields

        /// <summary>
        /// Language version
        /// </summary>
        private LanguageVersion languageVersion;

        /// <summary>
        /// Database schema
        /// </summary>
        private IDatabaseSchema databaseSchema;

        #endregion Private Fields


        #region Contructor

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="databaseSchema">Database schema</param>
        /// <param name="languageVersion">Language version</param>
        public FormSchemaData(
            IDatabaseSchema databaseSchema,
            LanguageVersion languageVersion)
        {
            InitializeComponent();
            Initialize(
                databaseSchema: databaseSchema,
                languageVersion: languageVersion);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// Database schema (read-only)
        /// </summary>
        public IDatabaseSchema DatabaseSchema
        {
            get
            {
                return databaseSchema;
            }
            private set
            {
                databaseSchema = value;
            }
        }

        /// <summary>
        /// Language version
        /// </summary>
        public LanguageVersion LanguageVersion
        {
            get
            {
                return languageVersion;
            }
            set
            {
                languageVersion = value;
                InitializeLabels();
            }
        }

        /// <summary>
        /// Number of rows in data table
        /// </summary>
        public Nullable<int> RowLimit { get; set; }

        #endregion Properties


        #region Methods

        /// <summary>
        /// Initialize form
        /// </summary>
        /// <param name="languageVersion">Language version</param>
        /// <param name="databaseSchema">Database schema</param>
        private void Initialize(
            IDatabaseSchema databaseSchema,
            LanguageVersion languageVersion)
        {
            DatabaseSchema = databaseSchema;
            LanguageVersion = languageVersion;
            RowLimit = 100;

            lblTableName.Text = String.Empty;

            btnClose.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            tsmiLookupTables.Visible = DatabaseSchema.LookupTables.Count != 0;
            foreach (ILookupTable table in DatabaseSchema.LookupTables
                .OrderBy(a => a.TableName))
            {
                ToolStripMenuItem item = new()
                {
                    Tag = table,
                    Text = table.TableName
                };

                switch (table.TableName.ToUpper()[2])
                {
                    case 'A':
                    case 'B':
                    case 'C':
                    case 'D':
                        tsmiLookupTables1.DropDownItems.Add(value: item);
                        break;
                    case 'E':
                    case 'F':
                    case 'G':
                    case 'H':
                        tsmiLookupTables2.DropDownItems.Add(value: item);
                        break;
                    case 'I':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'M':
                        tsmiLookupTables3.DropDownItems.Add(value: item);
                        break;
                    case 'N':
                    case 'O':
                    case 'P':
                    case 'Q':
                    case 'R':
                        tsmiLookupTables4.DropDownItems.Add(value: item);
                        break;
                    case 'S':
                        tsmiLookupTables5.DropDownItems.Add(value: item);
                        break;
                    default:
                        tsmiLookupTables6.DropDownItems.Add(value: item);
                        break;
                }

                item.Click += new EventHandler(
                    (sender, e) =>
                    {
                        pnlData.Controls.Clear();
                        DataGridView dgvData = new() { Dock = DockStyle.Fill };
                        dgvData.SelectionChanged += new EventHandler(
                            (a, b) => { dgvData.ClearSelection(); });
                        dgvData.CreateControl();
                        pnlData.Controls.Add(value: dgvData);

                        table.ReLoad(condition: null, limit: RowLimit, extended: false);
                        foreach (ColumnMetadata col in table.Columns.Values)
                        {
                            col.Visible = true;
                        };
                        table.SetDataGridViewDataSource(dataGridView: dgvData);
                        table.FormatDataGridView(dataGridView: dgvData);
                        lblTableName.Text = table.Caption;
                        txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
                        Functions.HighlightSQLSyntax(box: txtSQL);
                    });
            }

            tsmiMappingTables.Visible = DatabaseSchema.MappingTables.Count != 0;
            foreach (IMappingTable table in
                DatabaseSchema.MappingTables
                .OrderBy(a => a.TableName))
            {
                ToolStripMenuItem item = new()
                {
                    Tag = table,
                    Text = table.TableName
                };

                item.Click += new EventHandler(
                    (sender, e) =>
                    {
                        pnlData.Controls.Clear();
                        DataGridView dgvData = new() { Dock = DockStyle.Fill };
                        dgvData.SelectionChanged += new EventHandler(
                            (a, b) => { dgvData.ClearSelection(); });
                        dgvData.CreateControl();
                        pnlData.Controls.Add(value: dgvData);

                        table.ReLoad(condition: null, limit: RowLimit);
                        foreach (ColumnMetadata col in table.Columns.Values)
                        {
                            col.Visible = true;
                        };
                        table.SetDataGridViewDataSource(dataGridView: dgvData);
                        table.FormatDataGridView(dataGridView: dgvData);
                        lblTableName.Text = table.Caption;
                        txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
                        Functions.HighlightSQLSyntax(box: txtSQL);
                    });

                tsmiSpatialTables.DropDownItems.Add(value: item);
            }

            tsmiDataTables.Visible = DatabaseSchema.DataTables.Count != 0;
            foreach (IDataTable table in
                DatabaseSchema.DataTables
                .OrderBy(a => a.TableName))
            {
                ToolStripMenuItem item = new()
                {
                    Tag = table,
                    Text = table.TableName
                };

                switch (table.TableName.ToUpper()[2])
                {
                    case 'A':
                    case 'B':
                    case 'C':
                    case 'D':
                        tsmiDataTables1.DropDownItems.Add(value: item);
                        break;
                    case 'E':
                    case 'F':
                    case 'G':
                    case 'H':
                        tsmiDataTables2.DropDownItems.Add(value: item);
                        break;
                    case 'I':
                    case 'J':
                    case 'K':
                    case 'L':
                    case 'M':
                        tsmiDataTables3.DropDownItems.Add(value: item);
                        break;
                    case 'N':
                    case 'O':
                    case 'P':
                    case 'Q':
                    case 'R':
                        tsmiDataTables4.DropDownItems.Add(value: item);
                        break;
                    case 'S':
                        tsmiDataTables5.DropDownItems.Add(value: item);
                        break;
                    default:
                        tsmiDataTables6.DropDownItems.Add(value: item);
                        break;
                }

                item.Click += new EventHandler(
                    (sender, e) =>
                    {
                        pnlData.Controls.Clear();
                        DataGridView dgvData = new() { Dock = DockStyle.Fill };
                        dgvData.SelectionChanged += new EventHandler(
                            (a, b) => { dgvData.ClearSelection(); });
                        dgvData.CreateControl();
                        pnlData.Controls.Add(value: dgvData);

                        table.ReLoad(condition: null, limit: RowLimit);
                        foreach (ColumnMetadata col in table.Columns.Values)
                        {
                            col.Visible = true;
                        };
                        table.SetDataGridViewDataSource(dataGridView: dgvData);
                        table.FormatDataGridView(dataGridView: dgvData);
                        lblTableName.Text = table.Caption;
                        txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
                        Functions.HighlightSQLSyntax(box: txtSQL);
                    });
            }

            tsmiSpatialTables.Visible = DatabaseSchema.SpatialTables.Count != 0;
            foreach (ISpatialTable table in
                DatabaseSchema.SpatialTables
                .OrderBy(a => a.TableName))
            {
                ToolStripMenuItem item = new()
                {
                    Tag = table,
                    Text = table.TableName
                };

                item.Click += new EventHandler(
                    (sender, e) =>
                    {
                        pnlData.Controls.Clear();
                        DataGridView dgvData = new() { Dock = DockStyle.Fill };
                        dgvData.SelectionChanged += new EventHandler(
                            (a, b) => { dgvData.ClearSelection(); });
                        dgvData.CreateControl();
                        pnlData.Controls.Add(value: dgvData);

                        table.ReLoad(condition: null, limit: RowLimit, withGeom: true);
                        foreach (ColumnMetadata col in table.Columns.Values)
                        {
                            col.Visible = true;
                        };
                        table.SetDataGridViewDataSource(dataGridView: dgvData);
                        table.FormatDataGridView(dataGridView: dgvData);
                        lblTableName.Text = table.Caption;
                        txtSQL.Text = $"{Environment.NewLine}{table.SQL}{Environment.NewLine}";
                        Functions.HighlightSQLSyntax(box: txtSQL);
                    });

                tsmiSpatialTables.DropDownItems.Add(value: item);
            }
        }

        /// <summary>
        /// Initialize labels
        /// </summary>
        private void InitializeLabels()
        {
            switch (LanguageVersion)
            {
                case LanguageVersion.National:

                    Text = $"{DatabaseSchema?.SchemaName ?? String.Empty}";

                    tsmiLookupTables.Text = "Číselníky";
                    tsmiMappingTables.Text = "Mapovací tabulky";
                    tsmiSpatialTables.Text = "Prostorové tabulky";
                    tsmiDataTables.Text = "Datové tabulky";

                    btnClose.Text = "Zavřít";

                    break;

                case LanguageVersion.International:

                    Text = $"{DatabaseSchema?.SchemaName ?? String.Empty}";

                    tsmiLookupTables.Text = "Lookup tables";
                    tsmiMappingTables.Text = "Mapping tables";
                    tsmiSpatialTables.Text = "Spatial tables";
                    tsmiDataTables.Text = "Data tables";

                    btnClose.Text = "Close";
                    break;

                default:

                    Text = Name;

                    tsmiLookupTables.Text = tsmiLookupTables.Name;
                    tsmiMappingTables.Text = tsmiMappingTables.Name;
                    tsmiSpatialTables.Text = tsmiSpatialTables.Name;
                    tsmiDataTables.Text = tsmiDataTables.Name;

                    btnClose.Text = btnClose.Name;

                    break;
            }
        }

        #endregion Methods

    }

}
