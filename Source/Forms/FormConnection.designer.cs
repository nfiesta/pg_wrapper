﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.PostgreSQL
{
    partial class FormConnection
    {

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConnection));
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnCancel = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnOK = new System.Windows.Forms.Button();
            tabConnectionSettings = new System.Windows.Forms.TabControl();
            tpBasicSettings = new System.Windows.Forms.TabPage();
            tlpBasicSettings = new System.Windows.Forms.TableLayoutPanel();
            lblServer = new System.Windows.Forms.Label();
            lblPort = new System.Windows.Forms.Label();
            lblDatabase = new System.Windows.Forms.Label();
            lblUid = new System.Windows.Forms.Label();
            lblPsw = new System.Windows.Forms.Label();
            pnlServer = new System.Windows.Forms.Panel();
            cboServer = new System.Windows.Forms.ComboBox();
            pnPort = new System.Windows.Forms.Panel();
            txtPort = new System.Windows.Forms.ComboBox();
            pnlDatabase = new System.Windows.Forms.Panel();
            txtDatabase = new System.Windows.Forms.ComboBox();
            pnlUid = new System.Windows.Forms.Panel();
            txtUid = new System.Windows.Forms.ComboBox();
            pnlPsw = new System.Windows.Forms.Panel();
            txtPsw = new System.Windows.Forms.TextBox();
            tpExtendedSettings = new System.Windows.Forms.TabPage();
            tlpExtendedSettings = new System.Windows.Forms.TableLayoutPanel();
            lblCommandTimeout = new System.Windows.Forms.Label();
            lblKeepAlive = new System.Windows.Forms.Label();
            pnlCommandTimeout = new System.Windows.Forms.Panel();
            numCommandTimeout = new System.Windows.Forms.NumericUpDown();
            pnlKeepAlive = new System.Windows.Forms.Panel();
            numKeepAlive = new System.Windows.Forms.NumericUpDown();
            lblTcpKeepAlive = new System.Windows.Forms.Label();
            lblTcpKeepAliveTime = new System.Windows.Forms.Label();
            pnlTcpKeepAlive = new System.Windows.Forms.Panel();
            chkTcpKeepAlive = new System.Windows.Forms.CheckBox();
            lblTcpKeepAliveInterval = new System.Windows.Forms.Label();
            pnlTcpKeepAliveTime = new System.Windows.Forms.Panel();
            numTcpKeepAliveTime = new System.Windows.Forms.NumericUpDown();
            pnlTcpKeepAliveInterval = new System.Windows.Forms.Panel();
            numTcpKeepAliveInterval = new System.Windows.Forms.NumericUpDown();
            tlpMain.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlOK.SuspendLayout();
            tabConnectionSettings.SuspendLayout();
            tpBasicSettings.SuspendLayout();
            tlpBasicSettings.SuspendLayout();
            pnlServer.SuspendLayout();
            pnPort.SuspendLayout();
            pnlDatabase.SuspendLayout();
            pnlUid.SuspendLayout();
            pnlPsw.SuspendLayout();
            tpExtendedSettings.SuspendLayout();
            tlpExtendedSettings.SuspendLayout();
            pnlCommandTimeout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numCommandTimeout).BeginInit();
            pnlKeepAlive.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numKeepAlive).BeginInit();
            pnlTcpKeepAlive.SuspendLayout();
            pnlTcpKeepAliveTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numTcpKeepAliveTime).BeginInit();
            pnlTcpKeepAliveInterval.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)numTcpKeepAliveInterval).BeginInit();
            SuspendLayout();
            // 
            // tlpMain
            // 
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Controls.Add(tabConnectionSettings, 0, 0);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.Size = new System.Drawing.Size(611, 370);
            tlpMain.TabIndex = 0;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 3;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlCancel, 2, 0);
            tlpButtons.Controls.Add(pnlOK, 1, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 324);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(611, 46);
            tlpButtons.TabIndex = 27;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnCancel);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(424, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(6);
            pnlCancel.Size = new System.Drawing.Size(187, 46);
            pnlCancel.TabIndex = 12;
            // 
            // btnCancel
            // 
            btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            btnCancel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnCancel.Location = new System.Drawing.Point(6, 6);
            btnCancel.Margin = new System.Windows.Forms.Padding(0);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(175, 34);
            btnCancel.TabIndex = 13;
            btnCancel.Text = "btnCancel";
            btnCancel.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnOK);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(237, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(6);
            pnlOK.Size = new System.Drawing.Size(187, 46);
            pnlOK.TabIndex = 13;
            // 
            // btnOK
            // 
            btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnOK.Dock = System.Windows.Forms.DockStyle.Fill;
            btnOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnOK.Location = new System.Drawing.Point(6, 6);
            btnOK.Margin = new System.Windows.Forms.Padding(0);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(175, 34);
            btnOK.TabIndex = 12;
            btnOK.Text = "btnOK";
            btnOK.UseVisualStyleBackColor = true;
            // 
            // tabConnectionSettings
            // 
            tabConnectionSettings.Controls.Add(tpBasicSettings);
            tabConnectionSettings.Controls.Add(tpExtendedSettings);
            tabConnectionSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            tabConnectionSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            tabConnectionSettings.Location = new System.Drawing.Point(0, 0);
            tabConnectionSettings.Margin = new System.Windows.Forms.Padding(0);
            tabConnectionSettings.Name = "tabConnectionSettings";
            tabConnectionSettings.SelectedIndex = 0;
            tabConnectionSettings.Size = new System.Drawing.Size(611, 324);
            tabConnectionSettings.TabIndex = 26;
            // 
            // tpBasicSettings
            // 
            tpBasicSettings.Controls.Add(tlpBasicSettings);
            tpBasicSettings.Location = new System.Drawing.Point(4, 24);
            tpBasicSettings.Margin = new System.Windows.Forms.Padding(0);
            tpBasicSettings.Name = "tpBasicSettings";
            tpBasicSettings.Size = new System.Drawing.Size(603, 296);
            tpBasicSettings.TabIndex = 0;
            tpBasicSettings.Text = "pageBasicSettings";
            tpBasicSettings.UseVisualStyleBackColor = true;
            // 
            // tlpBasicSettings
            // 
            tlpBasicSettings.ColumnCount = 2;
            tlpBasicSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 117F));
            tlpBasicSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpBasicSettings.Controls.Add(lblServer, 0, 0);
            tlpBasicSettings.Controls.Add(lblPort, 0, 1);
            tlpBasicSettings.Controls.Add(lblDatabase, 0, 2);
            tlpBasicSettings.Controls.Add(lblUid, 0, 3);
            tlpBasicSettings.Controls.Add(lblPsw, 0, 4);
            tlpBasicSettings.Controls.Add(pnlServer, 1, 0);
            tlpBasicSettings.Controls.Add(pnPort, 1, 1);
            tlpBasicSettings.Controls.Add(pnlDatabase, 1, 2);
            tlpBasicSettings.Controls.Add(pnlUid, 1, 3);
            tlpBasicSettings.Controls.Add(pnlPsw, 1, 4);
            tlpBasicSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpBasicSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            tlpBasicSettings.Location = new System.Drawing.Point(0, 0);
            tlpBasicSettings.Margin = new System.Windows.Forms.Padding(0);
            tlpBasicSettings.Name = "tlpBasicSettings";
            tlpBasicSettings.RowCount = 6;
            tlpBasicSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpBasicSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpBasicSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpBasicSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpBasicSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpBasicSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpBasicSettings.Size = new System.Drawing.Size(603, 296);
            tlpBasicSettings.TabIndex = 27;
            // 
            // lblServer
            // 
            lblServer.AutoSize = true;
            lblServer.Dock = System.Windows.Forms.DockStyle.Fill;
            lblServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblServer.Location = new System.Drawing.Point(0, 0);
            lblServer.Margin = new System.Windows.Forms.Padding(0);
            lblServer.Name = "lblServer";
            lblServer.Size = new System.Drawing.Size(117, 35);
            lblServer.TabIndex = 14;
            lblServer.Text = "lblServer";
            lblServer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPort
            // 
            lblPort.AutoSize = true;
            lblPort.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblPort.Location = new System.Drawing.Point(0, 35);
            lblPort.Margin = new System.Windows.Forms.Padding(0);
            lblPort.Name = "lblPort";
            lblPort.Size = new System.Drawing.Size(117, 35);
            lblPort.TabIndex = 19;
            lblPort.Text = "lblPort";
            lblPort.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblDatabase
            // 
            lblDatabase.AutoSize = true;
            lblDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDatabase.Location = new System.Drawing.Point(0, 70);
            lblDatabase.Margin = new System.Windows.Forms.Padding(0);
            lblDatabase.Name = "lblDatabase";
            lblDatabase.Size = new System.Drawing.Size(117, 35);
            lblDatabase.TabIndex = 20;
            lblDatabase.Text = "lblDatabase";
            lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblUid
            // 
            lblUid.AutoSize = true;
            lblUid.Dock = System.Windows.Forms.DockStyle.Fill;
            lblUid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblUid.Location = new System.Drawing.Point(0, 105);
            lblUid.Margin = new System.Windows.Forms.Padding(0);
            lblUid.Name = "lblUid";
            lblUid.Size = new System.Drawing.Size(117, 35);
            lblUid.TabIndex = 21;
            lblUid.Text = "lblUid";
            lblUid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblPsw
            // 
            lblPsw.AutoSize = true;
            lblPsw.Dock = System.Windows.Forms.DockStyle.Fill;
            lblPsw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblPsw.Location = new System.Drawing.Point(0, 140);
            lblPsw.Margin = new System.Windows.Forms.Padding(0);
            lblPsw.Name = "lblPsw";
            lblPsw.Size = new System.Drawing.Size(117, 35);
            lblPsw.TabIndex = 22;
            lblPsw.Text = "lblPsw";
            lblPsw.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlServer
            // 
            pnlServer.Controls.Add(cboServer);
            pnlServer.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlServer.Location = new System.Drawing.Point(117, 0);
            pnlServer.Margin = new System.Windows.Forms.Padding(0);
            pnlServer.Name = "pnlServer";
            pnlServer.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            pnlServer.Size = new System.Drawing.Size(486, 35);
            pnlServer.TabIndex = 23;
            // 
            // cboServer
            // 
            cboServer.Dock = System.Windows.Forms.DockStyle.Fill;
            cboServer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            cboServer.FormattingEnabled = true;
            cboServer.Location = new System.Drawing.Point(0, 5);
            cboServer.Margin = new System.Windows.Forms.Padding(0);
            cboServer.Name = "cboServer";
            cboServer.Size = new System.Drawing.Size(486, 23);
            cboServer.TabIndex = 2;
            // 
            // pnPort
            // 
            pnPort.Controls.Add(txtPort);
            pnPort.Dock = System.Windows.Forms.DockStyle.Fill;
            pnPort.Location = new System.Drawing.Point(117, 35);
            pnPort.Margin = new System.Windows.Forms.Padding(0);
            pnPort.Name = "pnPort";
            pnPort.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            pnPort.Size = new System.Drawing.Size(486, 35);
            pnPort.TabIndex = 24;
            // 
            // txtPort
            // 
            txtPort.Dock = System.Windows.Forms.DockStyle.Fill;
            txtPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtPort.FormattingEnabled = true;
            txtPort.Location = new System.Drawing.Point(0, 5);
            txtPort.Margin = new System.Windows.Forms.Padding(0);
            txtPort.Name = "txtPort";
            txtPort.Size = new System.Drawing.Size(486, 23);
            txtPort.TabIndex = 4;
            // 
            // pnlDatabase
            // 
            pnlDatabase.Controls.Add(txtDatabase);
            pnlDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlDatabase.Location = new System.Drawing.Point(117, 70);
            pnlDatabase.Margin = new System.Windows.Forms.Padding(0);
            pnlDatabase.Name = "pnlDatabase";
            pnlDatabase.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            pnlDatabase.Size = new System.Drawing.Size(486, 35);
            pnlDatabase.TabIndex = 25;
            // 
            // txtDatabase
            // 
            txtDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            txtDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtDatabase.FormattingEnabled = true;
            txtDatabase.Location = new System.Drawing.Point(0, 5);
            txtDatabase.Margin = new System.Windows.Forms.Padding(0);
            txtDatabase.Name = "txtDatabase";
            txtDatabase.Size = new System.Drawing.Size(486, 23);
            txtDatabase.TabIndex = 5;
            // 
            // pnlUid
            // 
            pnlUid.Controls.Add(txtUid);
            pnlUid.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlUid.Location = new System.Drawing.Point(117, 105);
            pnlUid.Margin = new System.Windows.Forms.Padding(0);
            pnlUid.Name = "pnlUid";
            pnlUid.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            pnlUid.Size = new System.Drawing.Size(486, 35);
            pnlUid.TabIndex = 26;
            // 
            // txtUid
            // 
            txtUid.Dock = System.Windows.Forms.DockStyle.Fill;
            txtUid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtUid.FormattingEnabled = true;
            txtUid.Location = new System.Drawing.Point(0, 5);
            txtUid.Margin = new System.Windows.Forms.Padding(0);
            txtUid.Name = "txtUid";
            txtUid.Size = new System.Drawing.Size(486, 23);
            txtUid.TabIndex = 7;
            // 
            // pnlPsw
            // 
            pnlPsw.Controls.Add(txtPsw);
            pnlPsw.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlPsw.Location = new System.Drawing.Point(117, 140);
            pnlPsw.Margin = new System.Windows.Forms.Padding(0);
            pnlPsw.Name = "pnlPsw";
            pnlPsw.Padding = new System.Windows.Forms.Padding(0, 5, 0, 0);
            pnlPsw.Size = new System.Drawing.Size(486, 35);
            pnlPsw.TabIndex = 27;
            // 
            // txtPsw
            // 
            txtPsw.Dock = System.Windows.Forms.DockStyle.Fill;
            txtPsw.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            txtPsw.Location = new System.Drawing.Point(0, 5);
            txtPsw.Margin = new System.Windows.Forms.Padding(0);
            txtPsw.Name = "txtPsw";
            txtPsw.Size = new System.Drawing.Size(486, 21);
            txtPsw.TabIndex = 2;
            txtPsw.UseSystemPasswordChar = true;
            // 
            // tpExtendedSettings
            // 
            tpExtendedSettings.Controls.Add(tlpExtendedSettings);
            tpExtendedSettings.Location = new System.Drawing.Point(4, 24);
            tpExtendedSettings.Margin = new System.Windows.Forms.Padding(0);
            tpExtendedSettings.Name = "tpExtendedSettings";
            tpExtendedSettings.Size = new System.Drawing.Size(603, 296);
            tpExtendedSettings.TabIndex = 1;
            tpExtendedSettings.Text = "pageExtendedSettings";
            tpExtendedSettings.UseVisualStyleBackColor = true;
            // 
            // tlpExtendedSettings
            // 
            tlpExtendedSettings.ColumnCount = 2;
            tlpExtendedSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 467F));
            tlpExtendedSettings.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpExtendedSettings.Controls.Add(lblCommandTimeout, 0, 0);
            tlpExtendedSettings.Controls.Add(lblKeepAlive, 0, 1);
            tlpExtendedSettings.Controls.Add(pnlCommandTimeout, 1, 0);
            tlpExtendedSettings.Controls.Add(pnlKeepAlive, 1, 1);
            tlpExtendedSettings.Controls.Add(lblTcpKeepAlive, 0, 2);
            tlpExtendedSettings.Controls.Add(lblTcpKeepAliveTime, 0, 3);
            tlpExtendedSettings.Controls.Add(pnlTcpKeepAlive, 1, 2);
            tlpExtendedSettings.Controls.Add(lblTcpKeepAliveInterval, 0, 4);
            tlpExtendedSettings.Controls.Add(pnlTcpKeepAliveTime, 1, 3);
            tlpExtendedSettings.Controls.Add(pnlTcpKeepAliveInterval, 1, 4);
            tlpExtendedSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpExtendedSettings.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F);
            tlpExtendedSettings.Location = new System.Drawing.Point(0, 0);
            tlpExtendedSettings.Margin = new System.Windows.Forms.Padding(0);
            tlpExtendedSettings.Name = "tlpExtendedSettings";
            tlpExtendedSettings.RowCount = 6;
            tlpExtendedSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpExtendedSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpExtendedSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpExtendedSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpExtendedSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpExtendedSettings.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpExtendedSettings.Size = new System.Drawing.Size(603, 296);
            tlpExtendedSettings.TabIndex = 28;
            // 
            // lblCommandTimeout
            // 
            lblCommandTimeout.AutoSize = true;
            lblCommandTimeout.Dock = System.Windows.Forms.DockStyle.Fill;
            lblCommandTimeout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblCommandTimeout.Location = new System.Drawing.Point(0, 0);
            lblCommandTimeout.Margin = new System.Windows.Forms.Padding(0);
            lblCommandTimeout.Name = "lblCommandTimeout";
            lblCommandTimeout.Size = new System.Drawing.Size(467, 35);
            lblCommandTimeout.TabIndex = 14;
            lblCommandTimeout.Text = "lblCommandTimeout";
            lblCommandTimeout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblKeepAlive
            // 
            lblKeepAlive.AutoSize = true;
            lblKeepAlive.Dock = System.Windows.Forms.DockStyle.Fill;
            lblKeepAlive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblKeepAlive.Location = new System.Drawing.Point(0, 35);
            lblKeepAlive.Margin = new System.Windows.Forms.Padding(0);
            lblKeepAlive.Name = "lblKeepAlive";
            lblKeepAlive.Size = new System.Drawing.Size(467, 35);
            lblKeepAlive.TabIndex = 19;
            lblKeepAlive.Text = "lblKeepAlive";
            lblKeepAlive.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlCommandTimeout
            // 
            pnlCommandTimeout.Controls.Add(numCommandTimeout);
            pnlCommandTimeout.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCommandTimeout.Location = new System.Drawing.Point(467, 0);
            pnlCommandTimeout.Margin = new System.Windows.Forms.Padding(0);
            pnlCommandTimeout.Name = "pnlCommandTimeout";
            pnlCommandTimeout.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            pnlCommandTimeout.Size = new System.Drawing.Size(136, 35);
            pnlCommandTimeout.TabIndex = 22;
            // 
            // numCommandTimeout
            // 
            numCommandTimeout.Dock = System.Windows.Forms.DockStyle.Fill;
            numCommandTimeout.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            numCommandTimeout.Location = new System.Drawing.Point(0, 6);
            numCommandTimeout.Margin = new System.Windows.Forms.Padding(0);
            numCommandTimeout.Maximum = new decimal(new int[] { 3600, 0, 0, 0 });
            numCommandTimeout.Name = "numCommandTimeout";
            numCommandTimeout.Size = new System.Drawing.Size(136, 21);
            numCommandTimeout.TabIndex = 21;
            numCommandTimeout.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnlKeepAlive
            // 
            pnlKeepAlive.Controls.Add(numKeepAlive);
            pnlKeepAlive.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlKeepAlive.Location = new System.Drawing.Point(467, 35);
            pnlKeepAlive.Margin = new System.Windows.Forms.Padding(0);
            pnlKeepAlive.Name = "pnlKeepAlive";
            pnlKeepAlive.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            pnlKeepAlive.Size = new System.Drawing.Size(136, 35);
            pnlKeepAlive.TabIndex = 23;
            // 
            // numKeepAlive
            // 
            numKeepAlive.Dock = System.Windows.Forms.DockStyle.Fill;
            numKeepAlive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            numKeepAlive.Location = new System.Drawing.Point(0, 6);
            numKeepAlive.Margin = new System.Windows.Forms.Padding(0);
            numKeepAlive.Maximum = new decimal(new int[] { 3600, 0, 0, 0 });
            numKeepAlive.Name = "numKeepAlive";
            numKeepAlive.Size = new System.Drawing.Size(136, 21);
            numKeepAlive.TabIndex = 22;
            numKeepAlive.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // lblTcpKeepAlive
            // 
            lblTcpKeepAlive.AutoSize = true;
            lblTcpKeepAlive.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTcpKeepAlive.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblTcpKeepAlive.Location = new System.Drawing.Point(0, 70);
            lblTcpKeepAlive.Margin = new System.Windows.Forms.Padding(0);
            lblTcpKeepAlive.Name = "lblTcpKeepAlive";
            lblTcpKeepAlive.Size = new System.Drawing.Size(467, 35);
            lblTcpKeepAlive.TabIndex = 24;
            lblTcpKeepAlive.Text = "lblTcpKeepAlive";
            lblTcpKeepAlive.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblTcpKeepAliveTime
            // 
            lblTcpKeepAliveTime.AutoSize = true;
            lblTcpKeepAliveTime.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTcpKeepAliveTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblTcpKeepAliveTime.Location = new System.Drawing.Point(0, 105);
            lblTcpKeepAliveTime.Margin = new System.Windows.Forms.Padding(0);
            lblTcpKeepAliveTime.Name = "lblTcpKeepAliveTime";
            lblTcpKeepAliveTime.Size = new System.Drawing.Size(467, 35);
            lblTcpKeepAliveTime.TabIndex = 25;
            lblTcpKeepAliveTime.Text = "lblTcpKeepAliveTime";
            lblTcpKeepAliveTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlTcpKeepAlive
            // 
            pnlTcpKeepAlive.Controls.Add(chkTcpKeepAlive);
            pnlTcpKeepAlive.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlTcpKeepAlive.Location = new System.Drawing.Point(467, 70);
            pnlTcpKeepAlive.Margin = new System.Windows.Forms.Padding(0);
            pnlTcpKeepAlive.Name = "pnlTcpKeepAlive";
            pnlTcpKeepAlive.Size = new System.Drawing.Size(136, 35);
            pnlTcpKeepAlive.TabIndex = 28;
            // 
            // chkTcpKeepAlive
            // 
            chkTcpKeepAlive.AutoSize = true;
            chkTcpKeepAlive.Dock = System.Windows.Forms.DockStyle.Fill;
            chkTcpKeepAlive.Location = new System.Drawing.Point(0, 0);
            chkTcpKeepAlive.Margin = new System.Windows.Forms.Padding(0);
            chkTcpKeepAlive.Name = "chkTcpKeepAlive";
            chkTcpKeepAlive.Padding = new System.Windows.Forms.Padding(52, 0, 0, 0);
            chkTcpKeepAlive.Size = new System.Drawing.Size(136, 35);
            chkTcpKeepAlive.TabIndex = 0;
            chkTcpKeepAlive.UseVisualStyleBackColor = true;
            // 
            // lblTcpKeepAliveInterval
            // 
            lblTcpKeepAliveInterval.AutoSize = true;
            lblTcpKeepAliveInterval.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTcpKeepAliveInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblTcpKeepAliveInterval.Location = new System.Drawing.Point(0, 140);
            lblTcpKeepAliveInterval.Margin = new System.Windows.Forms.Padding(0);
            lblTcpKeepAliveInterval.Name = "lblTcpKeepAliveInterval";
            lblTcpKeepAliveInterval.Size = new System.Drawing.Size(467, 35);
            lblTcpKeepAliveInterval.TabIndex = 29;
            lblTcpKeepAliveInterval.Text = "lblTcpKeepAliveInterval";
            lblTcpKeepAliveInterval.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pnlTcpKeepAliveTime
            // 
            pnlTcpKeepAliveTime.Controls.Add(numTcpKeepAliveTime);
            pnlTcpKeepAliveTime.Location = new System.Drawing.Point(467, 105);
            pnlTcpKeepAliveTime.Margin = new System.Windows.Forms.Padding(0);
            pnlTcpKeepAliveTime.Name = "pnlTcpKeepAliveTime";
            pnlTcpKeepAliveTime.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            pnlTcpKeepAliveTime.Size = new System.Drawing.Size(136, 27);
            pnlTcpKeepAliveTime.TabIndex = 26;
            // 
            // numTcpKeepAliveTime
            // 
            numTcpKeepAliveTime.Dock = System.Windows.Forms.DockStyle.Fill;
            numTcpKeepAliveTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            numTcpKeepAliveTime.Location = new System.Drawing.Point(0, 6);
            numTcpKeepAliveTime.Margin = new System.Windows.Forms.Padding(0);
            numTcpKeepAliveTime.Maximum = new decimal(new int[] { 3600, 0, 0, 0 });
            numTcpKeepAliveTime.Name = "numTcpKeepAliveTime";
            numTcpKeepAliveTime.Size = new System.Drawing.Size(136, 21);
            numTcpKeepAliveTime.TabIndex = 22;
            numTcpKeepAliveTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // pnlTcpKeepAliveInterval
            // 
            pnlTcpKeepAliveInterval.Controls.Add(numTcpKeepAliveInterval);
            pnlTcpKeepAliveInterval.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlTcpKeepAliveInterval.Location = new System.Drawing.Point(467, 140);
            pnlTcpKeepAliveInterval.Margin = new System.Windows.Forms.Padding(0);
            pnlTcpKeepAliveInterval.Name = "pnlTcpKeepAliveInterval";
            pnlTcpKeepAliveInterval.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            pnlTcpKeepAliveInterval.Size = new System.Drawing.Size(136, 35);
            pnlTcpKeepAliveInterval.TabIndex = 30;
            // 
            // numTcpKeepAliveInterval
            // 
            numTcpKeepAliveInterval.Dock = System.Windows.Forms.DockStyle.Fill;
            numTcpKeepAliveInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            numTcpKeepAliveInterval.Location = new System.Drawing.Point(0, 6);
            numTcpKeepAliveInterval.Margin = new System.Windows.Forms.Padding(0);
            numTcpKeepAliveInterval.Maximum = new decimal(new int[] { 3600, 0, 0, 0 });
            numTcpKeepAliveInterval.Name = "numTcpKeepAliveInterval";
            numTcpKeepAliveInterval.Size = new System.Drawing.Size(136, 21);
            numTcpKeepAliveInterval.TabIndex = 22;
            numTcpKeepAliveInterval.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FormConnection
            // 
            AcceptButton = btnOK;
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            CancelButton = btnCancel;
            ClientSize = new System.Drawing.Size(611, 370);
            Controls.Add(tlpMain);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            KeyPreview = true;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormConnection";
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            tlpMain.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            tabConnectionSettings.ResumeLayout(false);
            tpBasicSettings.ResumeLayout(false);
            tlpBasicSettings.ResumeLayout(false);
            tlpBasicSettings.PerformLayout();
            pnlServer.ResumeLayout(false);
            pnPort.ResumeLayout(false);
            pnlDatabase.ResumeLayout(false);
            pnlUid.ResumeLayout(false);
            pnlPsw.ResumeLayout(false);
            pnlPsw.PerformLayout();
            tpExtendedSettings.ResumeLayout(false);
            tlpExtendedSettings.ResumeLayout(false);
            tlpExtendedSettings.PerformLayout();
            pnlCommandTimeout.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)numCommandTimeout).EndInit();
            pnlKeepAlive.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)numKeepAlive).EndInit();
            pnlTcpKeepAlive.ResumeLayout(false);
            pnlTcpKeepAlive.PerformLayout();
            pnlTcpKeepAliveTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)numTcpKeepAliveTime).EndInit();
            pnlTcpKeepAliveInterval.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)numTcpKeepAliveInterval).EndInit();
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TabControl tabConnectionSettings;
        private System.Windows.Forms.TabPage tpBasicSettings;
        private System.Windows.Forms.TableLayoutPanel tlpBasicSettings;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.Label lblUid;
        private System.Windows.Forms.ComboBox cboServer;
        private System.Windows.Forms.Label lblPsw;
        private System.Windows.Forms.TabPage tpExtendedSettings;
        private System.Windows.Forms.TableLayoutPanel tlpExtendedSettings;
        private System.Windows.Forms.Label lblCommandTimeout;
        private System.Windows.Forms.Label lblKeepAlive;
        private System.Windows.Forms.Panel pnlServer;
        private System.Windows.Forms.Panel pnPort;
        private System.Windows.Forms.ComboBox txtPort;
        private System.Windows.Forms.Panel pnlDatabase;
        private System.Windows.Forms.ComboBox txtDatabase;
        private System.Windows.Forms.Panel pnlUid;
        private System.Windows.Forms.ComboBox txtUid;
        private System.Windows.Forms.Panel pnlPsw;
        private System.Windows.Forms.TextBox txtPsw;
        private System.Windows.Forms.Panel pnlCommandTimeout;
        private System.Windows.Forms.NumericUpDown numCommandTimeout;
        private System.Windows.Forms.Panel pnlKeepAlive;
        private System.Windows.Forms.NumericUpDown numKeepAlive;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Panel pnlTcpKeepAliveTime;
        private System.Windows.Forms.NumericUpDown numTcpKeepAliveTime;
        private System.Windows.Forms.Label lblTcpKeepAlive;
        private System.Windows.Forms.Label lblTcpKeepAliveTime;
        private System.Windows.Forms.Panel pnlTcpKeepAlive;
        private System.Windows.Forms.CheckBox chkTcpKeepAlive;
        private System.Windows.Forms.Label lblTcpKeepAliveInterval;
        private System.Windows.Forms.Panel pnlTcpKeepAliveInterval;
        private System.Windows.Forms.NumericUpDown numTcpKeepAliveInterval;
    }

}