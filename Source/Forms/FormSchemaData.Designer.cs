﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.PostgreSQL.DataModel
{

    partial class FormSchemaData
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.msMain = new System.Windows.Forms.MenuStrip();
            this.tsmiLookupTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLookupTables1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLookupTables2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLookupTables3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLookupTables4 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLookupTables5 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiLookupTables6 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables2 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables3 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables4 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables5 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiDataTables6 = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmiSpatialTables = new System.Windows.Forms.ToolStripMenuItem();
            this.tlpMain = new System.Windows.Forms.TableLayoutPanel();
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.pnlData = new System.Windows.Forms.Panel();
            this.txtSQL = new System.Windows.Forms.RichTextBox();
            this.tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            this.pnlClose = new System.Windows.Forms.Panel();
            this.btnClose = new System.Windows.Forms.Button();
            this.lblTableName = new System.Windows.Forms.Label();
            this.tsmiMappingTables = new System.Windows.Forms.ToolStripMenuItem();
            this.msMain.SuspendLayout();
            this.tlpMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.tlpButtons.SuspendLayout();
            this.pnlClose.SuspendLayout();
            this.SuspendLayout();
            // 
            // msMain
            // 
            this.msMain.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.msMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLookupTables,
            this.tsmiMappingTables,
            this.tsmiDataTables,
            this.tsmiSpatialTables});
            this.msMain.Location = new System.Drawing.Point(0, 0);
            this.msMain.Name = "msMain";
            this.msMain.Size = new System.Drawing.Size(944, 24);
            this.msMain.TabIndex = 0;
            this.msMain.Text = "menuStrip1";
            // 
            // tsmiLookupTables
            // 
            this.tsmiLookupTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiLookupTables1,
            this.tsmiLookupTables2,
            this.tsmiLookupTables3,
            this.tsmiLookupTables4,
            this.tsmiLookupTables5,
            this.tsmiLookupTables6});
            this.tsmiLookupTables.Name = "tsmiLookupTables";
            this.tsmiLookupTables.Size = new System.Drawing.Size(114, 20);
            this.tsmiLookupTables.Text = "tsmiLookupTables";
            // 
            // tsmiLookupTables1
            // 
            this.tsmiLookupTables1.Name = "tsmiLookupTables1";
            this.tsmiLookupTables1.Size = new System.Drawing.Size(96, 22);
            this.tsmiLookupTables1.Text = "A..D";
            // 
            // tsmiLookupTables2
            // 
            this.tsmiLookupTables2.Name = "tsmiLookupTables2";
            this.tsmiLookupTables2.Size = new System.Drawing.Size(96, 22);
            this.tsmiLookupTables2.Text = "E..H";
            // 
            // tsmiLookupTables3
            // 
            this.tsmiLookupTables3.Name = "tsmiLookupTables3";
            this.tsmiLookupTables3.Size = new System.Drawing.Size(96, 22);
            this.tsmiLookupTables3.Text = "I..M";
            // 
            // tsmiLookupTables4
            // 
            this.tsmiLookupTables4.Name = "tsmiLookupTables4";
            this.tsmiLookupTables4.Size = new System.Drawing.Size(96, 22);
            this.tsmiLookupTables4.Text = "N..R";
            // 
            // tsmiLookupTables5
            // 
            this.tsmiLookupTables5.Name = "tsmiLookupTables5";
            this.tsmiLookupTables5.Size = new System.Drawing.Size(96, 22);
            this.tsmiLookupTables5.Text = "S";
            // 
            // tsmiLookupTables6
            // 
            this.tsmiLookupTables6.Name = "tsmiLookupTables6";
            this.tsmiLookupTables6.Size = new System.Drawing.Size(96, 22);
            this.tsmiLookupTables6.Text = "T..Z";
            // 
            // tsmiDataTables
            // 
            this.tsmiDataTables.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmiDataTables1,
            this.tsmiDataTables2,
            this.tsmiDataTables3,
            this.tsmiDataTables4,
            this.tsmiDataTables5,
            this.tsmiDataTables6});
            this.tsmiDataTables.Name = "tsmiDataTables";
            this.tsmiDataTables.Size = new System.Drawing.Size(98, 20);
            this.tsmiDataTables.Text = "tsmiDataTables";
            // 
            // tsmiDataTables1
            // 
            this.tsmiDataTables1.Name = "tsmiDataTables1";
            this.tsmiDataTables1.Size = new System.Drawing.Size(96, 22);
            this.tsmiDataTables1.Text = "A..D";
            // 
            // tsmiDataTables2
            // 
            this.tsmiDataTables2.Name = "tsmiDataTables2";
            this.tsmiDataTables2.Size = new System.Drawing.Size(96, 22);
            this.tsmiDataTables2.Text = "E..H";
            // 
            // tsmiDataTables3
            // 
            this.tsmiDataTables3.Name = "tsmiDataTables3";
            this.tsmiDataTables3.Size = new System.Drawing.Size(96, 22);
            this.tsmiDataTables3.Text = "I..M";
            // 
            // tsmiDataTables4
            // 
            this.tsmiDataTables4.Name = "tsmiDataTables4";
            this.tsmiDataTables4.Size = new System.Drawing.Size(96, 22);
            this.tsmiDataTables4.Text = "N..R";
            // 
            // tsmiDataTables5
            // 
            this.tsmiDataTables5.Name = "tsmiDataTables5";
            this.tsmiDataTables5.Size = new System.Drawing.Size(96, 22);
            this.tsmiDataTables5.Text = "S";
            // 
            // tsmiDataTables6
            // 
            this.tsmiDataTables6.Name = "tsmiDataTables6";
            this.tsmiDataTables6.Size = new System.Drawing.Size(96, 22);
            this.tsmiDataTables6.Text = "T..Z";
            // 
            // tsmiSpatialTables
            // 
            this.tsmiSpatialTables.Name = "tsmiSpatialTables";
            this.tsmiSpatialTables.Size = new System.Drawing.Size(109, 20);
            this.tsmiSpatialTables.Text = "tsmiSpatialTables";
            // 
            // tlpMain
            // 
            this.tlpMain.BackColor = System.Drawing.Color.White;
            this.tlpMain.ColumnCount = 1;
            this.tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.Controls.Add(this.splitMain, 0, 1);
            this.tlpMain.Controls.Add(this.tlpButtons, 0, 2);
            this.tlpMain.Controls.Add(this.lblTableName, 0, 0);
            this.tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpMain.Location = new System.Drawing.Point(0, 24);
            this.tlpMain.Margin = new System.Windows.Forms.Padding(0);
            this.tlpMain.Name = "tlpMain";
            this.tlpMain.RowCount = 3;
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tlpMain.Size = new System.Drawing.Size(944, 477);
            this.tlpMain.TabIndex = 6;
            // 
            // splitMain
            // 
            this.splitMain.BackColor = System.Drawing.Color.White;
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.Location = new System.Drawing.Point(0, 20);
            this.splitMain.Margin = new System.Windows.Forms.Padding(0);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.pnlData);
            this.splitMain.Panel1.Padding = new System.Windows.Forms.Padding(10);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.txtSQL);
            this.splitMain.Panel2.Padding = new System.Windows.Forms.Padding(10);
            this.splitMain.Size = new System.Drawing.Size(944, 417);
            this.splitMain.SplitterDistance = 193;
            this.splitMain.SplitterWidth = 1;
            this.splitMain.TabIndex = 30;
            // 
            // pnlData
            // 
            this.pnlData.AutoScroll = true;
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(10, 10);
            this.pnlData.Margin = new System.Windows.Forms.Padding(0);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(924, 173);
            this.pnlData.TabIndex = 3;
            // 
            // txtSQL
            // 
            this.txtSQL.BackColor = System.Drawing.SystemColors.Window;
            this.txtSQL.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtSQL.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSQL.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtSQL.Location = new System.Drawing.Point(10, 10);
            this.txtSQL.Margin = new System.Windows.Forms.Padding(0);
            this.txtSQL.Name = "txtSQL";
            this.txtSQL.ReadOnly = true;
            this.txtSQL.Size = new System.Drawing.Size(924, 203);
            this.txtSQL.TabIndex = 0;
            this.txtSQL.Text = "";
            // 
            // tlpButtons
            // 
            this.tlpButtons.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.tlpButtons.ColumnCount = 2;
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tlpButtons.Controls.Add(this.pnlClose, 1, 0);
            this.tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tlpButtons.Location = new System.Drawing.Point(0, 437);
            this.tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            this.tlpButtons.Name = "tlpButtons";
            this.tlpButtons.RowCount = 1;
            this.tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tlpButtons.Size = new System.Drawing.Size(944, 40);
            this.tlpButtons.TabIndex = 28;
            // 
            // pnlClose
            // 
            this.pnlClose.Controls.Add(this.btnClose);
            this.pnlClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlClose.Location = new System.Drawing.Point(784, 0);
            this.pnlClose.Margin = new System.Windows.Forms.Padding(0);
            this.pnlClose.Name = "pnlClose";
            this.pnlClose.Padding = new System.Windows.Forms.Padding(5);
            this.pnlClose.Size = new System.Drawing.Size(160, 40);
            this.pnlClose.TabIndex = 12;
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.White;
            this.btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.btnClose.Location = new System.Drawing.Point(5, 5);
            this.btnClose.Margin = new System.Windows.Forms.Padding(0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(150, 30);
            this.btnClose.TabIndex = 13;
            this.btnClose.Text = "btnClose";
            this.btnClose.UseVisualStyleBackColor = false;
            // 
            // lblTableName
            // 
            this.lblTableName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTableName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTableName.Location = new System.Drawing.Point(10, 0);
            this.lblTableName.Margin = new System.Windows.Forms.Padding(10, 0, 0, 0);
            this.lblTableName.Name = "lblTableName";
            this.lblTableName.Size = new System.Drawing.Size(934, 20);
            this.lblTableName.TabIndex = 31;
            this.lblTableName.Text = "lblTableName";
            this.lblTableName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tsmiMappingTables
            // 
            this.tsmiMappingTables.Name = "tsmiMappingTables";
            this.tsmiMappingTables.Size = new System.Drawing.Size(122, 20);
            this.tsmiMappingTables.Text = "tsmiMappingTables";
            // 
            // FormSchemaData
            // 
            this.AcceptButton = this.btnClose;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnClose;
            this.ClientSize = new System.Drawing.Size(944, 501);
            this.ControlBox = false;
            this.Controls.Add(this.tlpMain);
            this.Controls.Add(this.msMain);
            this.MainMenuStrip = this.msMain;
            this.Name = "FormSchemaData";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.msMain.ResumeLayout(false);
            this.msMain.PerformLayout();
            this.tlpMain.ResumeLayout(false);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.tlpButtons.ResumeLayout(false);
            this.pnlClose.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip msMain;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlClose;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.RichTextBox txtSQL;
        private System.Windows.Forms.Label lblTableName;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiSpatialTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables1;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables2;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables3;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables4;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables5;
        private System.Windows.Forms.ToolStripMenuItem tsmiLookupTables6;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables1;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables2;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables3;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables4;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables5;
        private System.Windows.Forms.ToolStripMenuItem tsmiDataTables6;
        private System.Windows.Forms.ToolStripMenuItem tsmiMappingTables;
    }

}