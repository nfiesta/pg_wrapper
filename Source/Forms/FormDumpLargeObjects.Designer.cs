﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi.PostgreSQL
{

    partial class FormDumpLargeObjects
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDumpLargeObjects));
            cboSchema = new System.Windows.Forms.ComboBox();
            cboTable = new System.Windows.Forms.ComboBox();
            cboColumn = new System.Windows.Forms.ComboBox();
            tlpMain = new System.Windows.Forms.TableLayoutPanel();
            tlpWorkSpace = new System.Windows.Forms.TableLayoutPanel();
            lblSchema = new System.Windows.Forms.Label();
            lblTable = new System.Windows.Forms.Label();
            lblColumn = new System.Windows.Forms.Label();
            lblFolder = new System.Windows.Forms.Label();
            lblDatabase = new System.Windows.Forms.Label();
            lblDatabaseValue = new System.Windows.Forms.Label();
            pnlFolder = new System.Windows.Forms.Panel();
            lblFolderPathValue = new System.Windows.Forms.Label();
            btnOpenFolder = new System.Windows.Forms.Button();
            tlpButtons = new System.Windows.Forms.TableLayoutPanel();
            pnlCancel = new System.Windows.Forms.Panel();
            btnClose = new System.Windows.Forms.Button();
            pnlDump = new System.Windows.Forms.Panel();
            btnDump = new System.Windows.Forms.Button();
            pnlOK = new System.Windows.Forms.Panel();
            btnRestore = new System.Windows.Forms.Button();
            tlpMain.SuspendLayout();
            tlpWorkSpace.SuspendLayout();
            pnlFolder.SuspendLayout();
            tlpButtons.SuspendLayout();
            pnlCancel.SuspendLayout();
            pnlDump.SuspendLayout();
            pnlOK.SuspendLayout();
            SuspendLayout();
            // 
            // cboSchema
            // 
            cboSchema.Dock = System.Windows.Forms.DockStyle.Fill;
            cboSchema.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboSchema.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            cboSchema.FormattingEnabled = true;
            cboSchema.Location = new System.Drawing.Point(179, 73);
            cboSchema.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            cboSchema.Name = "cboSchema";
            cboSchema.Size = new System.Drawing.Size(377, 23);
            cboSchema.TabIndex = 0;
            // 
            // cboTable
            // 
            cboTable.Dock = System.Windows.Forms.DockStyle.Fill;
            cboTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            cboTable.FormattingEnabled = true;
            cboTable.Location = new System.Drawing.Point(179, 108);
            cboTable.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            cboTable.Name = "cboTable";
            cboTable.Size = new System.Drawing.Size(377, 23);
            cboTable.TabIndex = 1;
            // 
            // cboColumn
            // 
            cboColumn.Dock = System.Windows.Forms.DockStyle.Fill;
            cboColumn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            cboColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            cboColumn.FormattingEnabled = true;
            cboColumn.Location = new System.Drawing.Point(179, 143);
            cboColumn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            cboColumn.Name = "cboColumn";
            cboColumn.Size = new System.Drawing.Size(377, 23);
            cboColumn.TabIndex = 2;
            // 
            // tlpMain
            // 
            tlpMain.BackColor = System.Drawing.SystemColors.Control;
            tlpMain.ColumnCount = 1;
            tlpMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.Controls.Add(tlpWorkSpace, 0, 0);
            tlpMain.Controls.Add(tlpButtons, 0, 1);
            tlpMain.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpMain.Location = new System.Drawing.Point(0, 0);
            tlpMain.Margin = new System.Windows.Forms.Padding(0);
            tlpMain.Name = "tlpMain";
            tlpMain.RowCount = 2;
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            tlpMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpMain.Size = new System.Drawing.Size(565, 301);
            tlpMain.TabIndex = 6;
            // 
            // tlpWorkSpace
            // 
            tlpWorkSpace.BackColor = System.Drawing.SystemColors.Control;
            tlpWorkSpace.ColumnCount = 3;
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 175F));
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 385F));
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            tlpWorkSpace.Controls.Add(cboSchema, 1, 2);
            tlpWorkSpace.Controls.Add(cboTable, 1, 3);
            tlpWorkSpace.Controls.Add(cboColumn, 1, 4);
            tlpWorkSpace.Controls.Add(lblSchema, 0, 2);
            tlpWorkSpace.Controls.Add(lblTable, 0, 3);
            tlpWorkSpace.Controls.Add(lblColumn, 0, 4);
            tlpWorkSpace.Controls.Add(lblFolder, 0, 1);
            tlpWorkSpace.Controls.Add(lblDatabase, 0, 0);
            tlpWorkSpace.Controls.Add(lblDatabaseValue, 1, 0);
            tlpWorkSpace.Controls.Add(pnlFolder, 1, 1);
            tlpWorkSpace.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpWorkSpace.Location = new System.Drawing.Point(0, 0);
            tlpWorkSpace.Margin = new System.Windows.Forms.Padding(0);
            tlpWorkSpace.Name = "tlpWorkSpace";
            tlpWorkSpace.RowCount = 6;
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            tlpWorkSpace.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpWorkSpace.Size = new System.Drawing.Size(565, 255);
            tlpWorkSpace.TabIndex = 7;
            // 
            // lblSchema
            // 
            lblSchema.Dock = System.Windows.Forms.DockStyle.Fill;
            lblSchema.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblSchema.Location = new System.Drawing.Point(4, 70);
            lblSchema.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblSchema.Name = "lblSchema";
            lblSchema.Size = new System.Drawing.Size(167, 35);
            lblSchema.TabIndex = 3;
            lblSchema.Text = "Databázové schéma:";
            lblSchema.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTable
            // 
            lblTable.Dock = System.Windows.Forms.DockStyle.Fill;
            lblTable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblTable.Location = new System.Drawing.Point(4, 105);
            lblTable.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblTable.Name = "lblTable";
            lblTable.Size = new System.Drawing.Size(167, 35);
            lblTable.TabIndex = 4;
            lblTable.Text = "Tabulka s large objekty:";
            lblTable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblColumn
            // 
            lblColumn.Dock = System.Windows.Forms.DockStyle.Fill;
            lblColumn.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblColumn.Location = new System.Drawing.Point(4, 140);
            lblColumn.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblColumn.Name = "lblColumn";
            lblColumn.Size = new System.Drawing.Size(167, 35);
            lblColumn.TabIndex = 5;
            lblColumn.Text = "Sloupec s large objekty:";
            lblColumn.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblFolder
            // 
            lblFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            lblFolder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblFolder.Location = new System.Drawing.Point(4, 35);
            lblFolder.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblFolder.Name = "lblFolder";
            lblFolder.Size = new System.Drawing.Size(167, 35);
            lblFolder.TabIndex = 6;
            lblFolder.Text = "Složka s large objekty:";
            lblFolder.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDatabase
            // 
            lblDatabase.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDatabase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, 0);
            lblDatabase.Location = new System.Drawing.Point(4, 0);
            lblDatabase.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblDatabase.Name = "lblDatabase";
            lblDatabase.Size = new System.Drawing.Size(167, 35);
            lblDatabase.TabIndex = 9;
            lblDatabase.Text = "Databáze:";
            lblDatabase.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDatabaseValue
            // 
            lblDatabaseValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblDatabaseValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, 0);
            lblDatabaseValue.Location = new System.Drawing.Point(179, 0);
            lblDatabaseValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblDatabaseValue.Name = "lblDatabaseValue";
            lblDatabaseValue.Size = new System.Drawing.Size(377, 35);
            lblDatabaseValue.TabIndex = 10;
            lblDatabaseValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlFolder
            // 
            pnlFolder.Controls.Add(lblFolderPathValue);
            pnlFolder.Controls.Add(btnOpenFolder);
            pnlFolder.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlFolder.Location = new System.Drawing.Point(175, 35);
            pnlFolder.Margin = new System.Windows.Forms.Padding(0);
            pnlFolder.Name = "pnlFolder";
            pnlFolder.Size = new System.Drawing.Size(385, 35);
            pnlFolder.TabIndex = 13;
            // 
            // lblFolderPathValue
            // 
            lblFolderPathValue.Dock = System.Windows.Forms.DockStyle.Fill;
            lblFolderPathValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, 0);
            lblFolderPathValue.Location = new System.Drawing.Point(0, 0);
            lblFolderPathValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            lblFolderPathValue.Name = "lblFolderPathValue";
            lblFolderPathValue.Size = new System.Drawing.Size(350, 35);
            lblFolderPathValue.TabIndex = 13;
            lblFolderPathValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOpenFolder
            // 
            btnOpenFolder.BackgroundImage = (System.Drawing.Image)resources.GetObject("btnOpenFolder.BackgroundImage");
            btnOpenFolder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            btnOpenFolder.Dock = System.Windows.Forms.DockStyle.Right;
            btnOpenFolder.FlatAppearance.BorderSize = 0;
            btnOpenFolder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            btnOpenFolder.Location = new System.Drawing.Point(350, 0);
            btnOpenFolder.Margin = new System.Windows.Forms.Padding(0);
            btnOpenFolder.Name = "btnOpenFolder";
            btnOpenFolder.Size = new System.Drawing.Size(35, 35);
            btnOpenFolder.TabIndex = 12;
            btnOpenFolder.UseVisualStyleBackColor = true;
            // 
            // tlpButtons
            // 
            tlpButtons.BackColor = System.Drawing.SystemColors.Control;
            tlpButtons.ColumnCount = 4;
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            tlpButtons.Controls.Add(pnlCancel, 3, 0);
            tlpButtons.Controls.Add(pnlDump, 1, 0);
            tlpButtons.Controls.Add(pnlOK, 2, 0);
            tlpButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            tlpButtons.Location = new System.Drawing.Point(0, 255);
            tlpButtons.Margin = new System.Windows.Forms.Padding(0);
            tlpButtons.Name = "tlpButtons";
            tlpButtons.RowCount = 1;
            tlpButtons.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            tlpButtons.Size = new System.Drawing.Size(565, 46);
            tlpButtons.TabIndex = 12;
            // 
            // pnlCancel
            // 
            pnlCancel.Controls.Add(btnClose);
            pnlCancel.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlCancel.Location = new System.Drawing.Point(378, 0);
            pnlCancel.Margin = new System.Windows.Forms.Padding(0);
            pnlCancel.Name = "pnlCancel";
            pnlCancel.Padding = new System.Windows.Forms.Padding(6);
            pnlCancel.Size = new System.Drawing.Size(187, 46);
            pnlCancel.TabIndex = 12;
            // 
            // btnClose
            // 
            btnClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            btnClose.Dock = System.Windows.Forms.DockStyle.Fill;
            btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnClose.Location = new System.Drawing.Point(6, 6);
            btnClose.Margin = new System.Windows.Forms.Padding(0);
            btnClose.Name = "btnClose";
            btnClose.Size = new System.Drawing.Size(175, 34);
            btnClose.TabIndex = 13;
            btnClose.Text = "Close";
            btnClose.UseVisualStyleBackColor = true;
            // 
            // pnlDump
            // 
            pnlDump.Controls.Add(btnDump);
            pnlDump.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlDump.Location = new System.Drawing.Point(4, 0);
            pnlDump.Margin = new System.Windows.Forms.Padding(0);
            pnlDump.Name = "pnlDump";
            pnlDump.Padding = new System.Windows.Forms.Padding(6);
            pnlDump.Size = new System.Drawing.Size(187, 46);
            pnlDump.TabIndex = 14;
            // 
            // btnDump
            // 
            btnDump.Dock = System.Windows.Forms.DockStyle.Fill;
            btnDump.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnDump.Location = new System.Drawing.Point(6, 6);
            btnDump.Margin = new System.Windows.Forms.Padding(0);
            btnDump.Name = "btnDump";
            btnDump.Size = new System.Drawing.Size(175, 34);
            btnDump.TabIndex = 12;
            btnDump.Text = "Dump";
            btnDump.UseVisualStyleBackColor = true;
            // 
            // pnlOK
            // 
            pnlOK.Controls.Add(btnRestore);
            pnlOK.Dock = System.Windows.Forms.DockStyle.Fill;
            pnlOK.Location = new System.Drawing.Point(191, 0);
            pnlOK.Margin = new System.Windows.Forms.Padding(0);
            pnlOK.Name = "pnlOK";
            pnlOK.Padding = new System.Windows.Forms.Padding(6);
            pnlOK.Size = new System.Drawing.Size(187, 46);
            pnlOK.TabIndex = 13;
            // 
            // btnRestore
            // 
            btnRestore.Dock = System.Windows.Forms.DockStyle.Fill;
            btnRestore.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            btnRestore.Location = new System.Drawing.Point(6, 6);
            btnRestore.Margin = new System.Windows.Forms.Padding(0);
            btnRestore.Name = "btnRestore";
            btnRestore.Size = new System.Drawing.Size(175, 34);
            btnRestore.TabIndex = 12;
            btnRestore.Text = "Restore";
            btnRestore.UseVisualStyleBackColor = true;
            // 
            // FormDumpLargeObjects
            // 
            AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            ClientSize = new System.Drawing.Size(565, 301);
            Controls.Add(tlpMain);
            FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            MaximizeBox = false;
            MinimizeBox = false;
            Name = "FormDumpLargeObjects";
            ShowIcon = false;
            ShowInTaskbar = false;
            StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            Text = "Dump and restore large objects";
            tlpMain.ResumeLayout(false);
            tlpWorkSpace.ResumeLayout(false);
            pnlFolder.ResumeLayout(false);
            tlpButtons.ResumeLayout(false);
            pnlCancel.ResumeLayout(false);
            pnlDump.ResumeLayout(false);
            pnlOK.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private System.Windows.Forms.ComboBox cboSchema;
        private System.Windows.Forms.ComboBox cboTable;
        private System.Windows.Forms.ComboBox cboColumn;
        private System.Windows.Forms.TableLayoutPanel tlpMain;
        private System.Windows.Forms.TableLayoutPanel tlpButtons;
        private System.Windows.Forms.Panel pnlCancel;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Panel pnlOK;
        private System.Windows.Forms.Button btnRestore;
        private System.Windows.Forms.TableLayoutPanel tlpWorkSpace;
        private System.Windows.Forms.Label lblSchema;
        private System.Windows.Forms.Label lblTable;
        private System.Windows.Forms.Label lblColumn;
        private System.Windows.Forms.Panel pnlDump;
        private System.Windows.Forms.Button btnDump;
        private System.Windows.Forms.Label lblFolder;
        private System.Windows.Forms.Label lblDatabase;
        private System.Windows.Forms.Label lblDatabaseValue;
        private System.Windows.Forms.Panel pnlFolder;
        private System.Windows.Forms.Label lblFolderPathValue;
        private System.Windows.Forms.Button btnOpenFolder;
    }

}