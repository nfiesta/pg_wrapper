﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Windows.Forms;

namespace ZaJi.PostgreSQL
{

    /// <summary>
    /// <para lang="cs">Formulář pro připojení k databázi</para>
    /// <para lang="en">Database connection form</para>
    /// </summary>
    public partial class FormConnection
        : Form
    {

        #region Private Fields

        /// <summary>
        /// <para lang="cs">Připojení k databázi PostgreSQL</para>
        /// <para lang="en">Connection to PostgreSQL database</para>
        /// </summary>
        private PostgreSQLWrapper postgres;

        /// <summary>
        /// <para lang="cs">IP adresa nebo jméno databázového serveru PostgreSQL</para>
        /// <para lang="en">IP address or name of the PostgreSQL database server</para>
        /// </summary>
        private string host;

        /// <summary>
        /// <para lang="cs">TCP port databázového serveru PostgreSQL</para>
        /// <para lang="en">TCP port of the PostgreSQL database server</para>
        /// </summary>
        private int port;

        /// <summary>
        /// <para lang="cs">Jméno databáze</para>
        /// <para lang="en">Database name</para>
        /// </summary>
        private string database;

        /// <summary>
        /// <para lang="cs">Jméno uživatele</para>
        /// <para lang="en">User name</para>
        /// </summary>
        private string userName;

        /// <summary>
        /// <para lang="cs">Heslo uživatele</para>
        /// <para lang="en">User password</para>
        /// </summary>
        private string password;

        /// <summary>
        /// <para lang="cs">Doba čekání na vykonání příkazu v sekundách</para>
        /// <para lang="en">Waiting time for command execution in seconds</para>
        /// </summary>
        private int commandTimeout;

        /// <summary>
        /// <para lang="cs">Doba v sekundách, po které je obnoveno připojení</para>
        /// <para lang="en">Time in seconds after which the connection is restored</para>
        /// </summary>
        private int keepAlive;

        /// <summary>
        /// <para lang="cs">Posílat TCP keep alive podle defaultního nastavení systému</para>
        /// <para lang="en">Whether to use TCP keepalive with system defaults if overrides isn't specified</para>
        /// </summary>
        private bool tcpKeepAlive;

        /// <summary>
        /// <para lang="cs">Počet sekund neaktivního připojení než je poslán TCP keepalive dotaz</para>
        /// <para lang="en">The number of seconds of connection inactivity before a TCP keepalive query is sent.</para>
        /// </summary>
        private int tcpKeepAliveTime;

        /// <summary>
        /// The interval, in seconds, between when successive keep-alive packets are sent if no acknowledgement is received.
        /// </summary>
        private int tcpKeepAliveInterval;

        #endregion Private Fields


        #region Constructor

        /// <summary>
        /// <para lang="cs">Konstruktor formuláře</para>
        /// <para lang="en">Form constructor</para>
        /// </summary>
        /// <param name="postgres">
        /// <para lang="cs">Připojení k databázi PostgreSQL</para>
        /// <para lang="en">Connection to PostgreSQL database</para>
        /// </param>
        public FormConnection(PostgreSQLWrapper postgres)
        {
            InitializeComponent();
            Initialize(postgres: postgres);
        }

        #endregion Constructor


        #region Properties

        /// <summary>
        /// <para lang="cs">Připojení k databázi PostgreSQL</para>
        /// <para lang="en">Connection to PostgreSQL database</para>
        /// </summary>
        private PostgreSQLWrapper Postgres
        {
            get
            {
                if (postgres == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument postgres must not be null.",
                        paramName: "postgres");
                }

                if (postgres.Setting == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument postgres.Setting must not be null.",
                        paramName: "setting");
                }

                return postgres;
            }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument postgres must not be null.",
                        paramName: nameof(Postgres));
                }

                if (value.Setting == null)
                {
                    throw new ArgumentNullException(
                        message: "Argument postgres.Setting must not be null.",
                        paramName: nameof(Postgres));
                }

                postgres = value;
            }
        }

        /// <summary>
        /// <para lang="cs">IP adresa nebo jméno databázového serveru PostgreSQL (read-only)</para>
        /// <para lang="en">IP address or name of the PostgreSQL database server (read-only)</para>
        /// </summary>
        public string Host
        {
            get
            {
                return host;
            }
        }

        /// <summary>
        /// <para lang="cs">TCP port databázového serveru PostgreSQL (read-only)</para>
        /// <para lang="en">TCP port of the PostgreSQL database server (read-only)</para>
        /// </summary>
        public int Port
        {
            get
            {
                return port;
            }
        }

        /// <summary>
        /// <para lang="cs">Jméno databáze (read-only)</para>
        /// <para lang="en">Database name (read-only)</para>
        /// </summary>
        public string Database
        {
            get
            {
                return database;
            }
        }

        /// <summary>
        /// <para lang="cs">Jméno uživatele (read-only)</para>
        /// <para lang="en">User name (read-only)</para>
        /// </summary>
        public string UserName
        {
            get
            {
                return userName;
            }
        }

        /// <summary>
        /// <para lang="cs">Heslo uživatele (read-only)</para>
        /// <para lang="en">User password (read-only)</para>
        /// </summary>
        public string Password
        {
            get
            {
                return password;
            }
        }

        /// <summary>
        /// <para lang="cs">Doba čekání na vykonání příkazu v sekundách (read-only)</para>
        /// <para lang="en">Waiting time for command execution in seconds (read-only)</para>
        /// </summary>
        public int CommandTimeout
        {
            get
            {
                return commandTimeout;
            }
        }

        /// <summary>
        /// <para lang="cs">Doba v sekundách, po které je obnoveno připojení (read-only)</para>
        /// <para lang="en">Time in seconds after which the connection is restored (read-only)</para>
        /// </summary>
        public int KeepAlive
        {
            get
            {
                return keepAlive;
            }
        }

        /// <summary>
        /// <para lang="cs">Posílat TCP keep alive podle defaultního nastavení systému</para>
        /// <para lang="en">Whether to use TCP keepalive with system defaults if overrides isn't specified</para>
        /// </summary>
        public bool TcpKeepAlive
        {
            get
            {
                return tcpKeepAlive;
            }
        }

        /// <summary>
        /// <para lang="cs">Počet sekund neaktivního připojení než je poslán TCP keepalive dotaz</para>
        /// <para lang="en">The number of seconds of connection inactivity before a TCP keepalive query is sent.</para>
        /// </summary>
        public int TcpKeepAliveTime
        {
            get
            {
                return tcpKeepAliveTime;
            }
        }

        /// <summary>
        /// The interval, in seconds, between when successive keep-alive packets are sent if no acknowledgement is received.
        /// </summary>
        public int TcpKeepAliveInterval
        {
            get
            {
                return tcpKeepAliveInterval;
            }
        }

        #endregion Properties


        #region Methods

        /// <summary>
        /// <para lang="cs">Inicializace formuláře</para>
        /// <para lang="en">Initializing the form</para>
        /// </summary>
        /// <param name="postgres">
        /// <para lang="cs">Připojení k databázi PostgreSQL</para>
        /// <para lang="en">Connection to PostgreSQL database</para>
        /// </param>
        private void Initialize(PostgreSQLWrapper postgres)
        {
            Postgres = postgres;

            foreach (string host in Postgres.Setting.Hosts)
            {
                cboServer.Items.Add(item: host);
            }
            host = Postgres.Setting.Host;
            cboServer.Text = host;

            foreach (int port in Postgres.Setting.Ports)
            {
                txtPort.Items.Add(item: port.ToString());
            }
            port = Postgres.Setting.Port;
            txtPort.Text = port.ToString();

            foreach (string database in Postgres.Setting.Databases)
            {
                txtDatabase.Items.Add(item: database);
            }
            database = Postgres.Setting.Database;
            txtDatabase.Text = database;

            foreach (string userName in Postgres.Setting.UserNames)
            {
                txtUid.Items.Add(item: userName);
            }
            userName = Postgres.Setting.UserName;
            txtUid.Text = userName;

            password = String.Empty;
            txtPsw.Text = password;

            numCommandTimeout.Value =
                Postgres.Setting.CommandTimeout;

            numKeepAlive.Value =
                Postgres.Setting.KeepAlive;

            chkTcpKeepAlive.Checked =
                Postgres.Setting.TcpKeepAlive;

            numTcpKeepAliveTime.Value =
                Postgres.Setting.TcpKeepAliveTime;

            numTcpKeepAliveInterval.Value =
                Postgres.Setting.TcpKeepAliveInterval;

            InitializeLabels();

            btnOK.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.OK;
                    Close();
                });

            btnCancel.Click += new EventHandler(
                (sender, e) =>
                {
                    DialogResult = DialogResult.Cancel;
                    Close();
                });

            FormClosing += new FormClosingEventHandler(
                (sender, e) =>
                {
                    if (DialogResult == DialogResult.OK)
                    {
                        e.Cancel = !SetConnectionParams();
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                });
        }

        /// <summary>
        /// <para lang="cs">Inicializace popisků formuláře</para>
        /// <para lang="en">Initializing form labels</para>
        /// </summary>
        private void InitializeLabels()
        {
            switch (Postgres.Setting.LanguageVersion)
            {
                case LanguageVersion.International:
                    Text = "Connection to database PostgreSQL";

                    tpBasicSettings.Text = "Basic settings";
                    tpExtendedSettings.Text = "Extended settings";

                    btnCancel.Text = "Cancel";
                    btnOK.Text = "OK";

                    lblServer.Text = "Server:";
                    lblPort.Text = "Port:";
                    lblDatabase.Text = "Database:";
                    lblUid.Text = "User:";
                    lblPsw.Text = "Password:";

                    lblCommandTimeout.Text = "Command timeout (s):";
                    lblKeepAlive.Text = "Keep alive (s):";
                    lblTcpKeepAlive.Text = "Tcp keep alive (on/off):";
                    lblTcpKeepAliveTime.Text = "Tcp keep alive (s):";
                    lblTcpKeepAliveInterval.Text = "Tcp keep alive interval (s):";

                    break;

                case LanguageVersion.National:
                    Text = "Připojení k databázi PostgreSQL";

                    tpBasicSettings.Text = "Základní nastavení";
                    tpExtendedSettings.Text = "Rozšířená nastavení";

                    btnCancel.Text = "Zrušit";
                    btnOK.Text = "OK";

                    lblServer.Text = "Server:";
                    lblPort.Text = "Port:";
                    lblDatabase.Text = "Databáze:";
                    lblUid.Text = "Uživatel:";
                    lblPsw.Text = "Heslo:";

                    lblCommandTimeout.Text = "Maximální doba vykonání příkazu (s):";
                    lblKeepAlive.Text = "Doba pro obnovení připojení (s):";
                    lblTcpKeepAlive.Text = "Tcp udržovat připojení (zapnuto/vypnuto):";
                    lblTcpKeepAliveTime.Text = "Doba pro obnovení Tcp připojení (s):";
                    lblTcpKeepAliveInterval.Text = "Interval pro obnovení Tcp připojení (s):";

                    break;

                default:
                    Text = Name;

                    tpBasicSettings.Text = tpBasicSettings.Name;
                    tpExtendedSettings.Text = tpExtendedSettings.Name;

                    btnCancel.Text = btnCancel.Name;
                    btnOK.Text = btnOK.Name;

                    lblServer.Text = lblServer.Name;
                    lblPort.Text = lblPort.Name;
                    lblDatabase.Text = lblDatabase.Name;
                    lblUid.Text = lblUid.Name;
                    lblPsw.Text = lblPsw.Name;

                    lblCommandTimeout.Text = lblCommandTimeout.Name;
                    lblKeepAlive.Text = lblKeepAlive.Name;
                    lblTcpKeepAlive.Text = lblTcpKeepAlive.Name;
                    lblTcpKeepAliveTime.Text = lblTcpKeepAliveTime.Name;
                    lblTcpKeepAliveInterval.Text = lblTcpKeepAliveInterval.Name;

                    break;
            }
        }

        /// <summary>
        /// <para lang="cs">Nastaví parametry připojení k databázi</para>
        /// <para lang="en">Sets database connection parameters</para>
        /// </summary>
        /// <returns>
        /// <para lang="cs">Vrací true, pokud parametry byly nastaveny, jinak false</para>
        /// <para lang="en">Returns true if the parameters have been set, false otherwise</para>
        /// </returns>
        private bool SetConnectionParams()
        {
            host = cboServer.Text.Trim();

            port = Functions.ConvertToInt(
                text: txtPort.Text.Trim(),
                defaultValue: port);

            database = txtDatabase.Text.Trim();

            userName = txtUid.Text.Trim();

            password = txtPsw.Text.Trim();

            commandTimeout = (int)numCommandTimeout.Value;

            keepAlive = (int)numKeepAlive.Value;

            tcpKeepAlive = chkTcpKeepAlive.Checked;

            tcpKeepAliveTime = (int)numTcpKeepAliveTime.Value;

            tcpKeepAliveInterval = (int)numTcpKeepAliveInterval.Value;

            return true;
        }

        #endregion Methods


    }

}
