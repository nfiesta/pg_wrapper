﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System.Collections.Generic;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Interface for database table
            /// </summary>
            public interface IDatabaseTable
            {

                #region Properties

                /// <summary>
                /// Schema name (read-only)
                /// </summary>
                string SchemaName { get; }

                /// <summary>
                /// Table name (read-only)
                /// </summary>
                string TableName { get; }

                /// <summary>
                /// Table alias (read-only)
                /// </summary>
                string TableAlias { get; }

                /// <summary>
                /// Short description (read-only)
                /// </summary>
                string Caption { get; }

                /// <summary>
                /// Language version (read-only)
                /// </summary>
                PostgreSQL.LanguageVersion LanguageVersion { get; }

                /// <summary>
                /// Columns metadata (read-only)
                /// </summary>
                System.Collections.Generic.Dictionary<string, ColumnMetadata> Columns { get; }

                /// <summary>
                /// Database connection (read-only)
                /// </summary>
                IDatabase Database { get; }

                /// <summary>
                /// Seznam identifikátorů
                /// </summary>
                public List<int> Identifiers { get; }

                /// <summary>
                /// Data (read-only)
                /// </summary>
                System.Data.DataTable Data { get; }

                /// <summary>
                /// Condition (read-only)
                /// </summary>
                string Condition { get; }

                /// <summary>
                /// Data View Row Filter
                /// </summary>
                string RowFilter { get; set; }

                /// <summary>
                /// Rows count limit (read-only)
                /// </summary>
                System.Nullable<int> Limit { get; }

                /// <summary>
                /// Rows count (read-only)
                /// </summary>
                int RowsCount { get; }

                /// <summary>
                /// Data was loaded from database - true|false (read-only)
                /// </summary>
                bool Loaded { get; }

                /// <summary>
                /// Time used for loading data from database (read-only)
                /// </summary>
                System.Nullable<double> LoadingTime { get; }

                /// <summary>
                /// Database stored procedure that has provided data for current object (read-only)
                /// </summary>
                string StoredProcedure { get; }

                /// <summary>
                /// SQL Query (read-only)
                /// </summary>
                string SQL { get; }

                /// <summary>
                /// Xml data file (read-only)
                /// </summary>
                string XML { get; }

                /// <summary>
                /// Insert Command (read-only)
                /// </summary>
                string InsertCommand { get; }

                /// <summary>
                /// Insert All Command (read-only)
                /// </summary>
                string InsertAllCommand { get; }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Exports data of the table into file
                /// </summary>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="folder">Folder for writing xml file with data</param>
                /// <param name="page">Number of file, if divided</param>
                void ExportData(ExportFileFormat fileFormat, string folder, string page);

                /// <summary>
                /// Imports data of the table from xml file
                /// (file name is equal to the name of the table)
                /// </summary>
                /// <param name="folder">Folder for reading xml file with data</param>
                void ImportDataFromXml(string folder);

                /// <summary>
                /// Sets DataGridView DataSource
                /// </summary>
                /// <param name="dataGridView">DataGridView</param>
                void SetDataGridViewDataSource(System.Windows.Forms.DataGridView dataGridView);

                /// <summary>
                /// Sets header, visibility, formats by data type
                /// and alignment in columns for given DataGridView
                /// </summary>
                /// <param name="dataGridView">DataGridView</param>
                void FormatDataGridView(System.Windows.Forms.DataGridView dataGridView);

                #endregion Methods

            }


            /// <summary>
            /// Interface for row in database table
            /// </summary>
            public interface IDatabaseTableEntry
            {

                #region Properties

                /// <summary>
                /// DataRow with etry data (read-only)
                /// </summary>
                System.Data.DataRow Data { get; }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                string ToString();

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                bool Equals(object obj);

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                int GetHashCode();

                #endregion Methods

            }


            /// <summary>
            /// Generic interface for row in database table
            /// </summary>
            public interface IDatabaseTableEntry<T>
                : IDatabaseTableEntry
                where T : IDatabaseTable
            {

                #region Properties

                /// <summary>
                /// List of entry data (read-only)
                /// </summary>
                T Composite { get; }

                #endregion Properties

            }

        }
    }
}
