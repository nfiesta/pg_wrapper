﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Abstract lookup table
            /// </summary>
            public abstract class ALookupTable
            : ADatabaseTable, ILookupTable
            {

                #region Constants

                /// <summary>
                /// Altogether category - National label
                /// </summary>
                public const string AltogetherCs = "bez rozlišení";

                /// <summary>
                /// Altogether category - English label
                /// </summary>
                public const string AltogetherEn = "altogether";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Altogether category is included - true|false
                /// </summary>
                private bool extended;

                #endregion Private Fields


                #region Constructors

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ALookupTable(
                    IDatabase database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                         database: database,
                         data: data,
                         condition: condition,
                         limit: limit,
                         loadingTime: loadingTime,
                         storedProcedure: storedProcedure)
                {
                    Extended = extended;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ALookupTable(
                    IDatabase database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                         database: database,
                         rows: rows,
                         condition: condition,
                         limit: limit,
                         loadingTime: loadingTime,
                         storedProcedure: storedProcedure)
                {
                    Extended = extended;
                }

                #endregion Constructors


                #region Properties

                /// <summary>
                /// Altogether category is included - true|false (read-only)
                /// </summary>
                public bool Extended
                {
                    get
                    {
                        return extended;
                    }
                    protected set
                    {
                        extended = value;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public void Load(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            condition: condition,
                            limit: limit,
                            extended: extended);
                    }
                }

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                public void ReLoad(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool extended = false)
                {
                    if (Database.Postgres.Catalog.Schemas[SchemaName] == null)
                    {
                        // Database schema does not exist
                        Data =
                            ALookupTable.EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                        Condition = String.Empty;
                        Limit = null;
                        Extended = false;
                        LoadingTime = null;
                        return;
                    }

                    if (
                        (Database.Postgres.Catalog.Schemas[SchemaName].Tables[TableName] == null) &&
                        (Database.Postgres.Catalog.Schemas[SchemaName].ForeignTables[TableName] == null))
                    {
                        // Database table does not exits
                        Data =
                            ALookupTable.EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                        Condition = String.Empty;
                        Limit = null;
                        Extended = false;
                        LoadingTime = null;
                        return;
                    }

                    Condition = condition;
                    Limit = limit;
                    Extended = extended;

                    System.Diagnostics.Stopwatch stopWatch = new();

                    stopWatch.Start();
                    Data = Database.Postgres.ExecuteQuery(sqlCommand: SQL);

                    // It adds lookup table category "0 - altogether"
                    // It is possible only for lookup tables that contains columns
                    // id, label_cs, description_cs, label_en, description_en
                    Dictionary<string, ColumnMetadata> columns = Columns;
                    if (
                        columns.TryGetValue(key: "id", out ColumnMetadata colId) &&
                        columns.TryGetValue(key: "label_cs", out ColumnMetadata colLabelCs) &&
                        columns.TryGetValue(key: "description_cs", out ColumnMetadata colDescriptionCs) &&
                        columns.TryGetValue(key: "label_en", out ColumnMetadata colLabelEn) &&
                        columns.TryGetValue(key: "description_en", out ColumnMetadata colDescriptionEn))
                    {
                        if (
                            Extended &&
                            !Data.AsEnumerable().Where(a => a.Field<int>(colId.Name) == 0).Any()
                        )
                        {
                            DataRow row = Data.NewRow();
                            Functions.SetIntArg(row: row, name: colId.Name, val: 0);
                            Functions.SetStringArg(row: row, name: colLabelCs.Name, val: ALookupTable.AltogetherCs);
                            Functions.SetStringArg(row: row, name: colDescriptionCs.Name, val: ALookupTable.AltogetherCs);
                            Functions.SetStringArg(row: row, name: colLabelEn.Name, val: ALookupTable.AltogetherEn);
                            Functions.SetStringArg(row: row, name: colDescriptionEn.Name, val: ALookupTable.AltogetherEn);
                            Data.Rows.Add(row: row);
                        }
                    }
                    stopWatch.Stop();

                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                /// <summary>
                /// Loads list of identifiers from database table
                /// </summary>
                public void LoadIdentifiers()
                {
                    if (Database.Postgres.Catalog.Schemas[SchemaName] == null)
                    {
                        // Database schema does not exist
                        Identifiers = [];
                        return;
                    }

                    if (
                        (Database.Postgres.Catalog.Schemas[SchemaName].Tables[TableName] == null) &&
                        (Database.Postgres.Catalog.Schemas[SchemaName].ForeignTables[TableName] == null))
                    {
                        // Database table does not exist
                        Identifiers = [];
                        return;
                    }

                    ColumnMetadata pkey = Columns.Values.Where(a => a.PKey).FirstOrDefault<ColumnMetadata>();

                    if (pkey == null)
                    {
                        // Primary key column does not exist
                        Identifiers = [];
                        return;
                    }

                    if (!new List<string> { "int2", "int4", "oid" }.Contains(item: pkey.DbDataType))
                    {
                        // Primary key must be integer type
                        Identifiers = [];
                        return;
                    }

                    DataTable dt = Database.Postgres.ExecuteQuery(
                        sqlCommand: $"SELECT {pkey.DbName}::int4 AS pkey FROM {SchemaName}.{TableName};");

                    Identifiers =
                        (dt == null)
                        ? []
                        : [..
                            dt.AsEnumerable()
                                .Where(a => a.Field<Nullable<int>>(columnName: "pkey") != null)
                                .Select(a => a.Field<int>(columnName: "pkey"))
                                .OrderBy(a => a)
                          ];
                }

                #endregion Methods

            }


            /// <summary>
            /// Abstract row in lookup table
            /// </summary>
            /// <typeparam name="T">Abstract lookup table</typeparam>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public abstract class ALookupTableEntry<T>(T composite = null, DataRow data = null)
                : ADatabaseTableEntry<T>(composite: composite, data: data),
                ILookupTableEntry<T>
                where T : ALookupTable
            {

                #region Derived Properties

                /// <summary>
                /// Label in national language
                /// </summary>
                public virtual string LabelCs { get; set; }

                /// <summary>
                /// Description in national language
                /// </summary>
                public virtual string DescriptionCs { get; set; }

                /// <summary>
                /// Extended label in national language (read-only)
                /// </summary>
                public virtual string ExtendedLabelCs { get; }

                /// <summary>
                /// Label in English
                /// </summary>
                public virtual string LabelEn { get; set; }

                /// <summary>
                /// Description in English
                /// </summary>
                public virtual string DescriptionEn { get; set; }

                /// <summary>
                /// Extended label in English (read-only)
                /// </summary>
                public virtual string ExtendedLabelEn { get; }

                #endregion Derived Properties

            }

        }
    }
}
