﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Extension version
            /// </summary>
            public class ExtensionVersion
            {

                #region Private Fields

                /// <summary>
                /// Major version
                /// </summary>
                private int major;

                /// <summary>
                /// Minor version
                /// </summary>
                private Nullable<int> minor;

                /// <summary>
                /// Patch version
                /// </summary>
                private Nullable<int> patch;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="version">Extension version string</param>
                public ExtensionVersion(string version)
                {
                    if (!String.IsNullOrEmpty(value: version))
                    {
                        char[] separators = [(char)46];
                        string[] s = version.Split(
                            separator: separators,
                            options: StringSplitOptions.RemoveEmptyEntries);

                        int m, n, p;
                        switch (s.Length)
                        {
                            case 1:
                                if (!Int32.TryParse(s: s[0], result: out m))
                                {
                                    ThrowException();
                                }
                                Major = m;
                                Minor = null;
                                Patch = null;
                                break;

                            case 2:
                                if (!Int32.TryParse(s: s[0], result: out m))
                                {
                                    ThrowException();
                                }
                                if (!Int32.TryParse(s: s[1], result: out n))
                                {
                                    ThrowException();
                                }
                                Major = m;
                                Minor = n;
                                Patch = null;
                                break;

                            case 3:
                                if (!Int32.TryParse(s: s[0], result: out m))
                                {
                                    ThrowException();
                                }
                                if (!Int32.TryParse(s: s[1], result: out n))
                                {
                                    ThrowException();
                                }
                                if (!Int32.TryParse(s: s[2], result: out p))
                                {
                                    ThrowException();
                                }
                                Major = m;
                                Minor = n;
                                Patch = p;
                                break;

                            default:
                                ThrowException();
                                break;
                        }
                    }
                    else
                    {
                        ThrowException();
                    }
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Major version
                /// </summary>
                public int Major
                {
                    get
                    {
                        return major;
                    }
                    private set
                    {
                        major = value;
                    }
                }

                /// <summary>
                /// Minor version
                /// </summary>
                public Nullable<int> Minor
                {
                    get
                    {
                        return minor;
                    }
                    private set
                    {
                        minor = value;
                    }
                }

                /// <summary>
                /// Patch version
                /// </summary>
                public Nullable<int> Patch
                {
                    get
                    {
                        return patch;
                    }
                    private set
                    {
                        patch = value;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Throw new argument version exception
                /// </summary>
                private static void ThrowException()
                {
                    throw new ArgumentException(
                            message: "Invalid extension version format.",
                            paramName: "version");
                }

                /// <summary>
                /// Returns a string that represents current object
                /// </summary>
                /// <returns>Returns a string that represents current object</returns>
                public override string ToString()
                {
                    if (Minor == null && Patch == null)
                    {
                        return $"{Major}";
                    }
                    else if (Minor != null && Patch == null)
                    {
                        return $"{Major}.{Minor}";
                    }
                    else if (Minor == null && Patch != null)
                    {
                        return $"{Major}.{String.Empty}.{Patch}";
                    }
                    else
                    {
                        return $"{Major}.{Minor}.{Patch}";
                    }
                }

                /// <summary>
                /// Determines whether specified object
                /// major version is equal to the current object major version
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public bool MajorEquals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ExtensionVersion Type, return False
                    if (obj is not ExtensionVersion)
                    {
                        return false;
                    }

                    return
                        Major == ((ExtensionVersion)obj).Major;
                }

                /// <summary>
                /// Determines whether specified object
                /// minor version is equal to the current object minor version
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public bool MinorEquals(object obj)
                {
                    if (MajorEquals(obj: obj))
                    {
                        if ((Minor == null) && (((ExtensionVersion)obj).Minor == null))
                        {
                            return true;
                        }
                        else if ((Minor == null) && (((ExtensionVersion)obj).Minor != null))
                        {
                            return false;
                        }
                        else if ((Minor != null) && (((ExtensionVersion)obj).Minor == null))
                        {
                            return false;
                        }
                        else
                        {
                            return ((int)Minor == (int)((ExtensionVersion)obj).Minor);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    if (MinorEquals(obj: obj))
                    {
                        if ((Patch == null) && (((ExtensionVersion)obj).Patch == null))
                        {
                            return true;
                        }
                        else if ((Patch == null) && (((ExtensionVersion)obj).Patch != null))
                        {
                            return false;
                        }
                        else if ((Patch != null) && (((ExtensionVersion)obj).Patch == null))
                        {
                            return false;
                        }
                        else
                        {
                            return ((int)Patch == (int)((ExtensionVersion)obj).Patch);
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Major.GetHashCode() ^
                        ((Minor == null) ? 0 : Minor.GetHashCode()) ^
                        ((Patch == null) ? 0 : Patch.GetHashCode());
                }

                #endregion Methodss

            }

        }
    }
}
