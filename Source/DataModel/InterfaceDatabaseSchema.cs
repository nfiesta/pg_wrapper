﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Interface for database schema
            /// </summary>
            public interface IDatabaseSchema
            {

                #region Properties

                /// <summary>
                /// Database (not-null) (read-only)
                /// </summary>
                IDatabase Database { get; }

                /// <summary>
                /// Schema name (read-only)
                /// </summary>
                string SchemaName { get; }

                /// <summary>
                /// List of lookup tables (not-null) (read-only)
                /// </summary>
                System.Collections.Generic.List<ILookupTable> LookupTables { get; }

                /// <summary>
                /// List of mapping tables (not-null) (read-only)
                /// </summary>
                System.Collections.Generic.List<IMappingTable> MappingTables { get; }

                /// <summary>
                /// List of spatial data tables (not-null) (read-only)
                /// </summary>
                System.Collections.Generic.List<ISpatialTable> SpatialTables { get; }

                /// <summary>
                /// List of data tables (not-null) (read-only)
                /// </summary>
                System.Collections.Generic.List<IDataTable> DataTables { get; }

                /// <summary>
                /// List of all tables (not-null) (read-only)
                /// </summary>
                System.Collections.Generic.List<IDatabaseTable> Tables { get; }

                /// <summary>
                /// Differences in database schema (not-null) (read-only)
                /// </summary>
                System.Collections.Generic.List<string> Differences { get; }

                #endregion Properties

            }

        }
    }
}
