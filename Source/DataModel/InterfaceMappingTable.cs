﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Interface for mapping table
            /// </summary>
            public interface IMappingTable
            : IDatabaseTable
            {

                #region Methods

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                void Load(string condition, System.Nullable<int> limit);

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                void ReLoad(string condition, System.Nullable<int> limit);

                /// <summary>
                /// Loads list of identifiers from database table
                /// </summary>
                void LoadIdentifiers();

                #endregion Methods

            }


            /// <summary>
            /// Interface for row in mapping table
            /// </summary>
            public interface IMappingTableEntry
                : IDatabaseTableEntry
            {
            }


            /// <summary>
            /// Generic interface for row in mapping table
            /// </summary>
            public interface IMappingTableEntry<T>
                : IMappingTableEntry, IDatabaseTableEntry<T>
                where T : IMappingTable
            {
            }

        }
    }
}