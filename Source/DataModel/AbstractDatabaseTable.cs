﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Abstract database table
            /// </summary>
            public abstract class ADatabaseTable
                : IDatabaseTable
            {

                #region Static Methods

                /// <summary>
                /// Empty data table
                /// </summary>
                /// <param name="tableName">Table name</param>
                /// <param name="columns">Columns metadata</param>
                /// <returns>Empty data table</returns>
                public static DataTable EmptyDataTable(
                    string tableName,
                    Dictionary<string, ColumnMetadata> columns)
                {
                    DataTable result = new(tableName: tableName)
                    {
                        TableName = tableName
                    };
                    foreach (ColumnMetadata column in columns.Values)
                    {
                        result.Columns.Add(
                            column: new DataColumn(
                                columnName: column.Name,
                                dataType: Type.GetType(typeName: column.DataType)));
                    }
                    return result;
                }

                /// <summary>
                /// Sets header for the data column
                /// </summary>
                /// <param name="columns">Columns metadata</param>
                /// <param name="columnName">Column name</param>
                /// <param name="headerTextCs">Column header text in national language</param>
                /// <param name="headerTextEn">Column header text in English</param>
                /// <param name="toolTipTextCs">Column tool tip text in national language</param>
                /// <param name="toolTipTextEn">Column tool tip text in English</param>
                public static void SetColumnHeader(
                    Dictionary<string, ColumnMetadata> columns,
                    string columnName,
                    string headerTextCs = null,
                    string headerTextEn = null,
                    string toolTipTextCs = null,
                    string toolTipTextEn = null)
                {
                    if (columns.TryGetValue(key: columnName, out ColumnMetadata value))
                    {
                        value.HeaderTextCs =
                            String.IsNullOrEmpty(value: headerTextCs) ? value.HeaderTextCs :
                            headerTextCs;
                        value.HeaderTextEn =
                            String.IsNullOrEmpty(value: headerTextEn) ? value.HeaderTextEn :
                            headerTextEn;
                        value.ToolTipTextCs =
                            String.IsNullOrEmpty(value: toolTipTextCs) ? value.ToolTipTextCs :
                            toolTipTextCs;
                        value.ToolTipTextEn =
                            String.IsNullOrEmpty(value: toolTipTextEn) ? value.ToolTipTextEn :
                            toolTipTextEn;
                    }
                }

                /// <summary>
                /// Sets columns visibility and order in DataGridView
                /// </summary>
                /// <param name="columns">Columns metadata</param>
                /// <param name="columnNames">Visible columns names in required order</param>
                public static void SetColumnsVisibility(
                    Dictionary<string, ColumnMetadata> columns,
                    params string[] columnNames)
                {
                    int i = -1;
                    List<ColumnMetadata> metadatas =
                        [.. columns.Values.OrderBy(a => a.DisplayIndex)];

                    if ((columnNames == null) || (columnNames.Length == 0))
                    {
                        // If no visible columns were selected, then all columns are visible
                        foreach (ColumnMetadata metadata in metadatas)
                        {
                            metadata.Visible = true;
                            metadata.DisplayIndex = ++i;
                        }
                    }
                    else
                    {
                        // If some visible columns were selected,
                        // then first set all columns to invisible and reset display order
                        // and then set selected columns visible
                        foreach (ColumnMetadata metadata in metadatas)
                        {
                            metadata.Visible = false;
                            metadata.DisplayIndex = (-1);
                        }

                        foreach (string name in columnNames)
                        {
                            if (columns.TryGetValue(key: name, out ColumnMetadata value))
                            {
                                value.Visible = true;
                                value.DisplayIndex = ++i;
                            }
                        }

                        foreach (ColumnMetadata metadata in metadatas)
                        {
                            if (metadata.DisplayIndex < 0)
                            {
                                metadata.Visible = false;
                                metadata.DisplayIndex = ++i;
                            }
                        }
                    }
                }


                /// <summary>
                /// Sets DataGridView DataSource
                /// </summary>
                /// <param name="data">Data table</param>
                /// <param name="dataGridView">Data grid view</param>
                public static void SetDataGridViewDataSource(
                    DataTable data,
                    System.Windows.Forms.DataGridView dataGridView)
                {
                    dataGridView.DataSource = null;
                    dataGridView.DataSource = data.DefaultView;
                }

                /// <summary>
                /// Sets header, visibility, formats by data type
                /// and alignment in columns for given DataGridView
                /// </summary>
                /// <param name="columns">Columns metadata</param>
                /// <param name="dataGridView">Data grid view</param>
                /// <param name="languageVersion">Language version</param>
                public static void FormatDataGridView(
                    Dictionary<string, ColumnMetadata> columns,
                    System.Windows.Forms.DataGridView dataGridView,
                    LanguageVersion languageVersion)
                {
                    // General:
                    dataGridView.AllowUserToAddRows = false;
                    dataGridView.AllowUserToDeleteRows = false;
                    dataGridView.AllowUserToResizeRows = false;

                    dataGridView.AlternatingRowsDefaultCellStyle =
                        new System.Windows.Forms.DataGridViewCellStyle()
                        {
                            BackColor = System.Drawing.Color.LightCyan
                        };

                    dataGridView.AutoGenerateColumns = false;
                    dataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.None;
                    dataGridView.BackgroundColor = System.Drawing.SystemColors.Window;
                    dataGridView.BorderStyle = System.Windows.Forms.BorderStyle.None;
                    dataGridView.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
                    dataGridView.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;

                    dataGridView.ColumnHeadersDefaultCellStyle =
                        new System.Windows.Forms.DataGridViewCellStyle()
                        {
                            Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                            BackColor = System.Drawing.Color.DarkBlue,
                            Font = new System.Drawing.Font(
                                familyName: "Microsoft Sans Serif",
                                emSize: 9.75F,
                                style: System.Drawing.FontStyle.Regular,
                                unit: System.Drawing.GraphicsUnit.Point,
                                gdiCharSet: (byte)238),
                            ForeColor = System.Drawing.Color.White,
                            SelectionBackColor = System.Drawing.Color.DarkBlue,
                            SelectionForeColor = System.Drawing.Color.White,
                            WrapMode = System.Windows.Forms.DataGridViewTriState.True
                        };

                    dataGridView.ColumnHeadersHeightSizeMode =
                        System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;

                    dataGridView.DefaultCellStyle =
                        new System.Windows.Forms.DataGridViewCellStyle()
                        {
                            Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft,
                            BackColor = System.Drawing.Color.White,
                            Font = new System.Drawing.Font(
                                familyName: "Microsoft Sans Serif",
                                emSize: 9.75F,
                                style: System.Drawing.FontStyle.Regular,
                                unit: System.Drawing.GraphicsUnit.Point,
                                gdiCharSet: (byte)238),
                            ForeColor = System.Drawing.SystemColors.ControlText,
                            Format = "N3",
                            NullValue = "NULL",
                            SelectionBackColor = System.Drawing.Color.Bisque,
                            SelectionForeColor = System.Drawing.SystemColors.ControlText,
                            WrapMode = System.Windows.Forms.DataGridViewTriState.False
                        };

                    dataGridView.EnableHeadersVisualStyles = false;
                    dataGridView.MultiSelect = false;
                    dataGridView.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
                    dataGridView.RowHeadersVisible = false;
                    dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;

                    // Columns:
                    foreach (ColumnMetadata column in columns.Values.OrderBy(a => a.DisplayIndex))
                    {
                        if (dataGridView.Columns.Contains(columnName: column.Name))
                        {
                            dataGridView.Columns[column.Name].ReadOnly =
                                column.ReadOnly;

                            dataGridView.Columns[column.Name].Visible =
                                column.Visible;

                            dataGridView.Columns[column.Name].HeaderText =
                                (languageVersion == LanguageVersion.National) ?
                                    (String.IsNullOrEmpty(value: column.HeaderTextCs) ?
                                        column.Name : column.HeaderTextCs) :
                                    (String.IsNullOrEmpty(value: column.HeaderTextEn) ?
                                        column.Name : column.HeaderTextEn);

                            dataGridView.Columns[column.Name].ToolTipText =
                                (languageVersion == LanguageVersion.National) ?
                                    (String.IsNullOrEmpty(value: column.ToolTipTextCs) ?
                                        dataGridView.Columns[column.Name].ToolTipText : column.ToolTipTextCs) :
                                    (String.IsNullOrEmpty(value: column.ToolTipTextEn) ?
                                        dataGridView.Columns[column.Name].ToolTipText : column.ToolTipTextEn);

                            dataGridView.Columns[column.Name].DefaultCellStyle.Format =
                                (String.IsNullOrEmpty(value: column.NumericFormat) ?
                                    dataGridView.Columns[column.Name].DefaultCellStyle.Format :
                                    column.NumericFormat);

                            dataGridView.Columns[column.Name].DefaultCellStyle.Alignment =
                               (System.Windows.Forms.DataGridViewContentAlignment)(int)column.Alignment;

                            dataGridView.Columns[column.Name].Width =
                                column.Width;

                            dataGridView.Columns[column.Name].DisplayIndex =
                                column.DisplayIndex;
                        }
                    }
                }

                #endregion Static Methods


                #region Private Fields

                /// <summary>
                /// Database connection
                /// </summary>
                private IDatabase database;

                /// <summary>
                /// Seznam identifikátorů
                /// </summary>
                private List<int> identifiers;

                /// <summary>
                /// Data
                /// </summary>
                private DataTable data;

                /// <summary>
                /// Condition
                /// </summary>
                private string condition;

                /// <summary>
                /// Rows count limit
                /// </summary>
                private Nullable<int> limit;

                /// <summary>
                /// Time used for loading data from database
                /// </summary>
                private Nullable<double> loadingTime;

                /// <summary>
                /// Database stored procedure that has provided data for current object
                /// </summary>
                private string storedProcedure;

                #endregion Private Fields


                #region Constructors

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ADatabaseTable(
                    IDatabase database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                {
                    Database = database;

                    Identifiers = [];

                    Data = data;

                    Condition = condition;

                    Limit = limit;

                    LoadingTime = loadingTime;

                    StoredProcedure = storedProcedure;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ADatabaseTable(
                    IDatabase database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                {
                    Database = database;

                    Identifiers = [];

                    Data = ((rows == null) || (!rows.Any())) ?
                           EmptyDataTable(
                               tableName: TableName,
                               columns: Columns) :
                            rows.CopyToDataTable();

                    Condition = condition;

                    Limit = limit;

                    LoadingTime = loadingTime;

                    StoredProcedure = storedProcedure;
                }

                #endregion Constructors


                #region Properties

                /// <summary>
                /// Schema name (read-only)
                /// </summary>
                public string SchemaName
                {
                    get
                    {
                        return
                            (string)GetType()
                                .GetField(name: "Schema")
                                .GetValue(obj: null);
                    }
                }

                /// <summary>
                /// Table name (read-only)
                /// </summary>
                public string TableName
                {
                    get
                    {
                        return
                             (string)GetType()
                                 .GetField(name: "Name")
                                 .GetValue(obj: null);
                    }
                }

                /// <summary>
                /// Table alias (read-only)
                /// </summary>
                public string TableAlias
                {
                    get
                    {
                        return
                             (string)GetType()
                                 .GetField(name: "Alias")
                                 .GetValue(obj: null);
                    }
                }

                /// <summary>
                /// Short description (read-only)
                /// </summary>
                public string Caption
                {
                    get
                    {
                        return LanguageVersion switch
                        {
                            LanguageVersion.National => (string)GetType()
                                                                    .GetField(name: "CaptionCs")
                                                                    .GetValue(obj: null),
                            LanguageVersion.International => (string)GetType()
                                                                    .GetField(name: "CaptionEn")
                                                                    .GetValue(obj: null),
                            _ => String.Empty,
                        };
                    }
                }

                /// <summary>
                /// Language version (read-only)
                /// </summary>
                public LanguageVersion LanguageVersion
                {
                    get
                    {
                        if (Database == null)
                            return LanguageVersion.International;

                        if (Database.Postgres == null)
                            return LanguageVersion.International;

                        if (Database.Postgres.Setting == null)
                            return LanguageVersion.International;

                        return Database.Postgres.Setting.LanguageVersion;
                    }
                }

                /// <summary>
                /// Columns metadata (read-only)
                /// </summary>
                public Dictionary<string, ColumnMetadata> Columns
                {
                    get
                    {
                        return
                            (Dictionary<string, ColumnMetadata>)
                            GetType()
                                .GetField(name: "Cols")
                                .GetValue(obj: null);
                    }
                }

                /// <summary>
                /// Database connection (read-only)
                /// </summary>
                public IDatabase Database
                {
                    get
                    {
                        return database ??
                             throw new ArgumentException(
                                message: "Argument database must not be null.",
                                paramName: nameof(Database));
                    }
                    protected set
                    {
                        database = value ??
                            throw new ArgumentException(
                                message: "Argument database must not be null.",
                                paramName: nameof(Database));
                    }
                }

                /// <summary>
                /// Seznam identifikátorů
                /// </summary>
                public List<int> Identifiers
                {
                    get
                    {
                        return identifiers ?? [];
                    }
                    protected set
                    {
                        identifiers = value ?? [];
                    }
                }

                /// <summary>
                /// Data (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        return data ??
                            EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    }
                    protected set
                    {
                        data = value ??
                            EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                    }
                }

                /// <summary>
                ///  Condition (read-only)
                /// </summary>
                public string Condition
                {
                    get
                    {
                        return condition ??
                            String.Empty;
                    }
                    protected set
                    {
                        condition = value ??
                            String.Empty;
                    }
                }

                /// <summary>
                /// Data View Row Filter
                /// </summary>
                public string RowFilter
                {
                    get
                    {
                        return
                            (data == null) ? String.Empty :
                            (data.DefaultView == null) ? String.Empty :
                            data.DefaultView.RowFilter;
                    }
                    set
                    {
                        if ((data != null) && (data.DefaultView != null))
                        {
                            try
                            {
                                data.DefaultView.RowFilter = value;
                            }
                            catch
                            {
                                data.DefaultView.RowFilter = String.Empty;
                            }
                        }
                    }
                }

                /// <summary>
                /// Rows count limit (read-only)
                /// </summary>
                public Nullable<int> Limit
                {
                    get
                    {
                        return limit;
                    }
                    protected set
                    {
                        if (limit < 0)
                        {
                            limit = null;
                        }
                        limit = value;
                    }
                }

                /// <summary>
                /// Rows count (read-only)
                /// </summary>
                public int RowsCount
                {
                    get
                    {
                        return
                            (data == null) ? 0 :
                            (data.Rows == null) ? 0 :
                            data.Rows.Count;
                    }
                }

                /// <summary>
                /// Data was loaded from database - true|false (read-only)
                /// </summary>
                public bool Loaded
                {
                    get
                    {
                        return LoadingTime != null;
                    }
                }

                /// <summary>
                /// Time used for loading data from database (read-only)
                /// (Null value indicates not loaded)
                /// </summary>
                public Nullable<double> LoadingTime
                {
                    get
                    {
                        return loadingTime;
                    }
                    protected set
                    {
                        loadingTime = value;
                    }
                }

                /// <summary>
                /// Database stored procedure that has provided data for current object (read-only)
                /// (Empty string if data were loaded directly from database table)
                /// </summary>
                public string StoredProcedure
                {
                    get
                    {
                        return storedProcedure ??
                            String.Empty;
                    }
                    set
                    {
                        storedProcedure = value ??
                            String.Empty;
                    }
                }

                /// <summary>
                /// SQL Query (read-only)
                /// </summary>
                public virtual string SQL
                {
                    get
                    {
                        // Table was loaded from stored procedure
                        if (!String.IsNullOrEmpty(value: StoredProcedure))
                        { return StoredProcedure; }

                        // SQL for direct access to database table
                        Dictionary<string, ColumnMetadata> columns = Columns;
                        if (Columns == null) { return String.Empty; }
                        if (Columns.Values.Count <= 0) { return String.Empty; }
                        if (String.IsNullOrEmpty(value: SchemaName)) { return String.Empty; }
                        if (String.IsNullOrEmpty(value: TableName)) { return String.Empty; }
                        if (String.IsNullOrEmpty(value: TableAlias)) { return String.Empty; }

                        string indentation = "    ";

                        string exprSelect = $"SELECT{Environment.NewLine}";

                        int i = 0;
                        int cnt = columns.Values.Count;
                        System.Text.StringBuilder sb = new();
                        foreach (ColumnMetadata column in columns.Values)
                        {
                            sb.Append(
                                value: String.Concat(
                                    indentation,
                                    column.SQL(
                                        cols: columns,
                                        alias: TableAlias,
                                        isLastOne: (++i == cnt),
                                        setToNull: !column.Elemental)));
                        }
                        string exprColumns = sb.ToString();

                        string exprFrom = String.Concat(
                            $"FROM{Environment.NewLine}",
                            indentation,
                            $"{SchemaName}.{TableName} AS {TableAlias}{Environment.NewLine}");

                        string exprWhere = String.IsNullOrEmpty(value: Condition) ?
                            String.Empty :
                            String.Concat(
                                $"WHERE{Environment.NewLine}",
                                indentation,
                                $"{Condition}{Environment.NewLine}");

                        string exprOrderBy =
                            String.Concat(
                                $"ORDER BY{Environment.NewLine}",
                                indentation,
                                $"{TableAlias}.{columns.First().Value.DbName}");

                        string exprLimit =
                            (limit == null) ? $";" :
                            (limit <= 0) ? $"{Environment.NewLine}LIMIT 0;" :
                            $"{Environment.NewLine}LIMIT {(int)limit};";

                        return String.Concat(
                            exprSelect,
                            exprColumns,
                            exprFrom,
                            exprWhere,
                            exprOrderBy,
                            exprLimit);
                    }
                }

                /// <summary>
                /// Xml data file (read-only)
                /// </summary>
                public string XML
                {
                    get
                    {
                        DataSet ds = new(dataSetName: SchemaName);
                        ds.Tables.Add(table: Data);
                        string xml = ds.GetXml();
                        ds.Tables.Remove(table: Data);
                        return xml;
                    }
                }

                /// <summary>
                /// Insert Command (read-only)
                /// </summary>
                public string InsertCommand
                {
                    get
                    {
                        return GenerateInsert(
                            allInOne: false);
                    }
                }

                /// <summary>
                /// Insert All Command (read-only)
                /// </summary>
                public string InsertAllCommand
                {
                    get
                    {
                        return GenerateInsert(
                            allInOne: true);
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Exports data of the table into file
                /// </summary>
                /// <param name="fileFormat">Format of the file with exported data</param>
                /// <param name="folder">Folder for writing xml file with data</param>
                /// <param name="page">Number of file, if divided</param>
                public void ExportData(
                    ExportFileFormat fileFormat,
                    string folder = null,
                    string page = null)
                {
                    switch (fileFormat)
                    {
                        case ExportFileFormat.Xml:
                            ExportDataToXml(
                                folder: folder,
                                page: page);
                            break;

                        case ExportFileFormat.SqlInsert:
                            ExportDataToInsert(
                                folder: folder,
                                page: page,
                                allInOne: false);
                            break;

                        case ExportFileFormat.SqlInsertAll:
                            ExportDataToInsert(
                                folder: folder,
                                page: page,
                                allInOne: true);
                            break;

                        default:
                            throw new ArgumentException(
                                message: $"Argument {nameof(fileFormat)} unknown value.",
                                paramName: nameof(fileFormat));
                    }
                }

                /// <summary>
                /// Exports data of the table into xml file
                /// (file name is equal to the name of the table)
                /// </summary>
                /// <param name="folder">Folder for writing xml file with data</param>
                /// <param name="page">Number of file, if divided</param>
                private void ExportDataToXml(
                    string folder = null,
                    string page = null)
                {
                    if (String.IsNullOrEmpty(value: folder))
                    {
                        System.Windows.Forms.FolderBrowserDialog dlg = new();

                        if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            folder = dlg.SelectedPath;
                        }
                    }

                    if ( String.IsNullOrEmpty(value: folder) ||
                         (!System.IO.Directory.Exists(path: folder)))
                    {
                        string message =
                            (LanguageVersion == LanguageVersion.National) ?
                                $"Složka {folder} neexistuje." :
                            (LanguageVersion == LanguageVersion.International) ?
                                $"Path {folder} does not exist." :
                                $"Path {folder} does not exist.";

                        System.Windows.Forms.MessageBox.Show(
                            text: message,
                            caption: String.Empty,
                            buttons: System.Windows.Forms.MessageBoxButtons.OK,
                            icon: System.Windows.Forms.MessageBoxIcon.Information);

                        return;
                    }

                    string fileName = String.IsNullOrEmpty(value: page)
                        ? System.IO.Path.Combine(folder, $"{TableName}.xml")
                        : System.IO.Path.Combine(folder, $"{TableName}_{page}.xml");

                    if (System.IO.File.Exists(path: fileName))
                    {
                        // File already exists,
                        // then delete and create new one.
                        System.IO.File.Delete(path: fileName);
                    }

                    try
                    {
                        DataSet ds = new(dataSetName: SchemaName);
                        ds.Tables.Add(table: Data);
                        ds.WriteXml(fileName: fileName);
                        ds.Tables.Remove(table: Data);
                    }
                    catch (Exception e)
                    {
                        System.Windows.Forms.MessageBox.Show(
                            text: e.Message,
                            caption: String.Empty,
                            buttons: System.Windows.Forms.MessageBoxButtons.OK,
                            icon: System.Windows.Forms.MessageBoxIcon.Exclamation);
                    }
                }

                /// <summary>
                /// Exports data of the table into text file with insert commands
                /// (file name is equal to the name of the table)
                /// </summary>
                /// <param name="folder">Folder for writing sql file with insert commands</param>
                /// <param name="page">Number of file, if divided</param>
                /// <param name="allInOne">All data in one INSERT command</param>
                private void ExportDataToInsert(
                    string folder = null,
                    string page = null,
                    bool allInOne = true)
                {
                    if (String.IsNullOrEmpty(value: folder))
                    {
                        System.Windows.Forms.FolderBrowserDialog dlg = new();

                        if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            folder = dlg.SelectedPath;
                        }
                    }

                    if (String.IsNullOrEmpty(value: folder) ||
                          (!System.IO.Directory.Exists(path: folder)))
                    {
                        string message =
                            (LanguageVersion == LanguageVersion.National) ?
                                $"Složka {folder} neexistuje." :
                            (LanguageVersion == LanguageVersion.International) ?
                                $"Path {folder} does not exist." :
                                $"Path {folder} does not exist.";

                        System.Windows.Forms.MessageBox.Show(
                            text: message,
                            caption: String.Empty,
                            buttons: System.Windows.Forms.MessageBoxButtons.OK,
                            icon: System.Windows.Forms.MessageBoxIcon.Information);

                        return;
                    }

                    string fileName = (String.IsNullOrEmpty(value: page))
                        ? System.IO.Path.Combine(folder, $"{TableName}.sql")
                        : System.IO.Path.Combine(folder, $"{TableName}_{page}.sql");

                    if (System.IO.File.Exists(path: fileName))
                    {
                        // File already exists,
                        // then delete and create new one.
                        System.IO.File.Delete(path: fileName);
                    }

                    string contents = GenerateInsert(allInOne: allInOne);

                    try
                    {
                        System.IO.File.WriteAllText(
                            path: fileName,
                            contents: contents);
                    }
                    catch (Exception e)
                    {
                        System.Windows.Forms.MessageBox.Show(
                            text: e.Message,
                            caption: String.Empty,
                            buttons: System.Windows.Forms.MessageBoxButtons.OK,
                            icon: System.Windows.Forms.MessageBoxIcon.Exclamation);
                    }
                }

                /// <summary>
                /// Generate INSERT command text
                /// </summary>
                /// <param name="allInOne">All data in one INSERT command</param>
                /// <returns>INSERT command text</returns>
                private string GenerateInsert(bool allInOne = true)
                {
                    if ((Data == null) || (RowsCount < 1))
                    {
                        return String.Empty;
                    }

                    const char comma = (char)44;
                    const char semicolon = (char)59;
                    const char leftBracket = (char)40;
                    const char rightBracket = (char)41;
                    const string indentation = "    ";

                    System.Text.StringBuilder sbDataRow = new();

                    System.Text.StringBuilder sbDataTable = new();

                    string exprColumns = Columns.Values
                            .Where(a => a.Elemental)
                            .Select(a => a.DbName)
                            .Aggregate((a, b) => $"{a}{comma}{b}");

                    string exprInsert = String.Concat(
                        $"INSERT INTO {SchemaName}.{TableName}{Environment.NewLine}",
                        $"{indentation}{leftBracket}{exprColumns}{rightBracket}{Environment.NewLine}",
                        $"VALUES{Environment.NewLine}");

                    if (allInOne)
                    {
                        sbDataTable.Append(value: exprInsert);
                    }

                    int i = 0;
                    foreach (DataRow row in data.Rows)
                    {
                        i++;
                        sbDataRow.Append(value: indentation);
                        sbDataRow.Append(value: leftBracket);

                        int j = 0;
                        foreach (
                            ColumnMetadata col in Columns.Values
                            .Where(a => a.Elemental))
                        {
                            j++;
                            switch (col.DbDataType)
                            {
                                case "bool":
                                    sbDataRow.Append(
                                        value: Functions.PrepNBoolArg(
                                            arg: Functions.GetNBoolArg(
                                                row: row,
                                                name: col.Name,
                                                defaultValue: null)));
                                    break;

                                case "int2":
                                    sbDataRow.Append(
                                        value: Functions.PrepNShortArg(
                                            arg: Functions.GetNShortArg(
                                                row: row,
                                                name: col.Name,
                                                defaultValue: null)));
                                    break;

                                case "int4":
                                    sbDataRow.Append(
                                        value: Functions.PrepNIntArg(
                                            arg: Functions.GetNIntArg(
                                                row: row,
                                                name: col.Name,
                                                defaultValue: null)));
                                    break;

                                case "int8":
                                case "oid":
                                    sbDataRow.Append(
                                        value: Functions.PrepNLongArg(
                                            arg: Functions.GetNLongArg(
                                                row: row,
                                                name: col.Name,
                                                defaultValue: null)));
                                    break;

                                case "float4":
                                    sbDataRow.Append(
                                        value: Functions.PrepNFloatArg(
                                            arg: Functions.GetNFloatArg(
                                                row: row,
                                                name: col.Name,
                                                defaultValue: null)));
                                    break;

                                case "float8":
                                    sbDataRow.Append(
                                        value: Functions.PrepNDoubleArg(
                                            arg: Functions.GetNDoubleArg(
                                                row: row,
                                                name: col.Name,
                                                defaultValue: null)));
                                    break;

                                case "money":
                                case "numeric":
                                    sbDataRow.Append(
                                        value: Functions.PrepNDecimalArg(
                                            arg: Functions.GetNDecimalArg(
                                                row: row,
                                                name: col.Name,
                                                defaultValue: null)));
                                    break;

                                default:
                                    sbDataRow.Append(
                                        value: Functions.PrepStringArg(
                                            arg: Functions.GetStringArg(
                                                row: row,
                                                name: col.Name,
                                                defaultValue: null)));
                                    break;
                            }
                            if (j != Columns.Values.Where(a => a.Elemental).Count())
                            {
                                sbDataRow.Append(value: comma);
                            }
                            else
                            {
                                sbDataRow.Append(value: rightBracket);
                            }
                        }

                        string exprDataRow = sbDataRow.ToString();
                        sbDataRow.Clear();

                        if (allInOne)
                        {
                            sbDataTable.Append(value: exprDataRow);
                            if (i != RowsCount)
                            {
                                sbDataTable.Append(value: comma);
                                sbDataTable.Append(value: Environment.NewLine);
                            }
                            else
                            {
                                sbDataTable.Append(value: semicolon);
                            }
                        }
                        else
                        {
                            sbDataTable.Append(value: exprInsert);
                            sbDataTable.Append(value: exprDataRow);
                            sbDataTable.Append(value: semicolon);
                            sbDataTable.Append(value: Environment.NewLine);
                            sbDataTable.Append(value: Environment.NewLine);
                        }
                    }

                    return sbDataTable.ToString();
                }


                /// <summary>
                /// Imports data of the table from xml file
                /// (file name is equal to the name of the table)
                /// </summary>
                /// <param name="folder">Folder for reading xml file with data</param>
                public void ImportDataFromXml(
                    string folder = null)
                {
                    if (String.IsNullOrEmpty(value: folder))
                    {
                        System.Windows.Forms.FolderBrowserDialog dlg = new();

                        if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                        {
                            folder = dlg.SelectedPath;
                        }
                    }

                    DataSet ds = new(dataSetName: SchemaName);

                    Condition = null;
                    Limit = null;
                    LoadingTime = null;
                    StoredProcedure = null;

                    if (String.IsNullOrEmpty(value: folder) ||
                         (!System.IO.Directory.Exists(path: folder)))
                    {
                        // Folder does not exist
                        Data = EmptyDataTable(
                                   tableName: TableName,
                                   columns: Columns);
                        return;
                    }

                    string fileName = System.IO.Path.Combine(folder, $"{TableName}.xml");

                    if (!System.IO.File.Exists(path: fileName))
                    {
                        // File does not exist
                        data = EmptyDataTable(
                                   tableName: TableName,
                                   columns: Columns);
                        return;
                    }

                    System.Diagnostics.Stopwatch stopWatch = new();
                    stopWatch.Start();

                    ds.ReadXml(fileName: fileName);

                    if (ds.Tables.Contains(name: TableName))
                    {
                        data = ds.Tables[TableName].Copy();
                    }
                    else
                    {
                        data = ALookupTable.EmptyDataTable(
                                   tableName: TableName,
                                   columns: Columns);
                    }

                    stopWatch.Stop();
                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                }


                /// <summary>
                /// Sets DataGridView DataSource
                /// </summary>
                /// <param name="dataGridView">DataGridView</param>
                public void SetDataGridViewDataSource(
                    System.Windows.Forms.DataGridView dataGridView)
                {
                    SetDataGridViewDataSource(
                        data: Data,
                        dataGridView: dataGridView);
                    dataGridView.Tag = this;
                }

                /// <summary>
                /// Sets header, visibility, formats by data type
                /// and alignment in columns for given DataGridView
                /// </summary>
                /// <param name="dataGridView">DataGridView</param>
                public void FormatDataGridView(
                    System.Windows.Forms.DataGridView dataGridView)
                {
                    FormatDataGridView(
                       columns: Columns,
                       dataGridView: dataGridView,
                       languageVersion: LanguageVersion);
                }


                /// <summary>
                /// Returns the string that represents current object
                /// </summary>
                /// <returns>Returns the string that represents current object</returns>
                public override string ToString()
                {
                    return LanguageVersion switch
                    {
                        LanguageVersion.National => String.Concat(
                                                        $"{Caption}. ",
                                                        $"Počet záznamů = {RowsCount}. ",
                                                        $"Doba pro nahrání záznamů = {LoadingTime ?? 0} sekund."),
                        LanguageVersion.International => String.Concat(
                                                        $"{Caption}. ",
                                                        $"Count = {RowsCount}. ",
                                                        $"Time for loading = {LoadingTime ?? 0} seconds."),
                        _ => String.Concat(
                                                       $"{Caption}. ",
                                                       $"Count = {RowsCount}. ",
                                                       $"Time for loading = {LoadingTime ?? 0} seconds."),
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Abstract row in database table
            /// </summary>
            /// <typeparam name="T">Abstract database table</typeparam>
            public abstract class ADatabaseTableEntry<T>
                : IDatabaseTableEntry<T>
                where T : ADatabaseTable
            {

                #region Private Fields

                /// <summary>
                /// List of records
                /// </summary>
                private T composite;

                /// <summary>
                /// Data row with record
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="composite">List of records</param>
                /// <param name="data">Data row with record</param>
                public ADatabaseTableEntry(
                    T composite = null,
                    DataRow data = null)
                {
                    Composite = composite;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// List of records (read-only)
                /// </summary>
                public T Composite
                {
                    get
                    {
                        return composite;
                    }
                    private set
                    {
                        composite = value;
                    }
                }

                /// <summary>
                /// Data row with record (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        if (data == null)
                        {
                            string name = (string)typeof(T)
                                .GetField(name: "Name")
                                .GetValue(obj: null);

                            Dictionary<string, ColumnMetadata> cols =
                                (Dictionary<string, ColumnMetadata>)typeof(T)
                                .GetField(name: "Cols")
                                .GetValue(obj: null);

                            return
                                ADatabaseTable.EmptyDataTable(
                                    tableName: name,
                                    columns: cols).NewRow();
                        }

                        return data;
                    }
                    private set
                    {
                        if (value == null)
                        {
                            string name = (string)typeof(T)
                                .GetField(name: "Name")
                                .GetValue(obj: null);

                            Dictionary<string, ColumnMetadata> cols =
                                (Dictionary<string, ColumnMetadata>)typeof(T)
                                .GetField(name: "Cols")
                                .GetValue(obj: null);

                            data = ADatabaseTable.EmptyDataTable(
                                    tableName: name,
                                    columns: cols).NewRow();
                            return;
                        }

                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                public virtual int Id { get; set; }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Text description of the object
                /// </summary>
                /// <returns>Text description of the object</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{"ID"} = {Functions.PrepIntArg(Id)}{Environment.NewLine}");
                }

                #endregion Methods

            }

        }
    }
}