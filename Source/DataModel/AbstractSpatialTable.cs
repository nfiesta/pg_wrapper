﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Abstract data table with geometries
            /// </summary>
            public abstract class ASpatialTable
            : ADatabaseTable, ISpatialTable
            {

                #region Private Fields

                /// <summary>
                /// Download geometries from database as WKT?
                /// </summary>
                private bool withGeom;

                #endregion Private Fields


                #region Constructors

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ASpatialTable(
                    IDatabase database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                         database: database,
                         data: data,
                         condition: condition,
                         limit: limit,
                         loadingTime: loadingTime,
                         storedProcedure: storedProcedure)
                {
                    WithGeom = withGeom;
                }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT?</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ASpatialTable(
                    IDatabase database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                         database: database,
                         rows: rows,
                         condition: condition,
                         limit: limit,
                         loadingTime: loadingTime,
                         storedProcedure: storedProcedure)
                {
                    WithGeom = withGeom;
                }

                #endregion Constructors


                #region Properties

                /// <summary>
                /// Download geometries from database as WKT? (read-only)
                /// </summary>
                public bool WithGeom
                {
                    get
                    {
                        return withGeom;
                    }
                    protected set
                    {
                        withGeom = value;
                    }
                }

                /// <summary>
                /// SQL Query
                /// </summary>
                public new string SQL
                {
                    get
                    {
                        // Table was loaded from stored procedure
                        if (!String.IsNullOrEmpty(value: StoredProcedure))
                        { return StoredProcedure; }

                        // SQL for direct access to database table
                        Dictionary<string, ColumnMetadata> columns = Columns;
                        if (Columns == null) { return String.Empty; }
                        if (Columns.Values.Count <= 0) { return String.Empty; }
                        if (String.IsNullOrEmpty(value: SchemaName)) { return String.Empty; }
                        if (String.IsNullOrEmpty(value: TableName)) { return String.Empty; }
                        if (String.IsNullOrEmpty(value: TableAlias)) { return String.Empty; }

                        string indentation = "    ";

                        string exprSelect = $"SELECT{Environment.NewLine}";

                        int i = 0;
                        int cnt = columns.Values.Count;
                        System.Text.StringBuilder sb = new();
                        foreach (ColumnMetadata column in columns.Values)
                        {
                            bool setToNull = (column.DbDataType == "geometry") ?
                                !(WithGeom && column.Elemental) :
                                !column.Elemental;

                            sb.Append(
                                value: String.Concat(
                                    indentation,
                                    column.SQL(
                                        cols: columns,
                                        alias: TableAlias,
                                        isLastOne: (++i == cnt),
                                        setToNull: setToNull)));
                        }
                        string exprColumns = sb.ToString();

                        string exprFrom = String.Concat(
                            $"FROM{Environment.NewLine}",
                            indentation,
                            $"{SchemaName}.{TableName} AS {TableAlias}{Environment.NewLine}");

                        string exprWhere = String.IsNullOrEmpty(value: Condition) ?
                            String.Empty :
                            String.Concat(
                                $"WHERE{Environment.NewLine}",
                                indentation,
                                $"{Condition}{Environment.NewLine}");

                        string exprOrderBy =
                            String.Concat(
                                $"ORDER BY{Environment.NewLine}",
                                indentation,
                                $"{TableAlias}.{columns.First().Value.DbName}");

                        string exprLimit =
                            (Limit == null) ? $";" :
                            (Limit <= 0) ? $"{Environment.NewLine}LIMIT 0;" :
                            $"{Environment.NewLine}LIMIT {(int)Limit};";

                        return String.Concat(
                            exprSelect,
                            exprColumns,
                            exprFrom,
                            exprWhere,
                            exprOrderBy,
                            exprLimit);
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT? </param>
                public void Load(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            condition: condition,
                            limit: limit,
                            withGeom: withGeom);
                    }
                }

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="withGeom">Download geometries from database as WKT? </param>
                public void ReLoad(
                    string condition = null,
                    Nullable<int> limit = null,
                    bool withGeom = false)
                {
                    if (Database.Postgres.Catalog.Schemas[SchemaName] == null)
                    {
                        // Database schema does not exist
                        Data =
                            ALookupTable.EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                        Condition = String.Empty;
                        Limit = null;
                        WithGeom = false;
                        LoadingTime = null;
                        return;
                    }

                    if (
                        (Database.Postgres.Catalog.Schemas[SchemaName].Tables[TableName] == null) &&
                        (Database.Postgres.Catalog.Schemas[SchemaName].ForeignTables[TableName] == null))
                    {
                        // Database table does not exits
                        Data =
                            ALookupTable.EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                        Condition = String.Empty;
                        Limit = null;
                        WithGeom = false;
                        LoadingTime = null;
                        return;
                    }

                    Condition = condition;
                    Limit = limit;
                    WithGeom = withGeom;

                    System.Diagnostics.Stopwatch stopWatch = new();

                    stopWatch.Start();
                    Data = Database.Postgres.ExecuteQuery(sqlCommand: SQL);
                    stopWatch.Stop();

                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                /// <summary>
                /// Loads list of identifiers from database table
                /// </summary>
                public void LoadIdentifiers()
                {
                    if (Database.Postgres.Catalog.Schemas[SchemaName] == null)
                    {
                        // Database schema does not exist
                        Identifiers = [];
                        return;
                    }

                    if (
                        (Database.Postgres.Catalog.Schemas[SchemaName].Tables[TableName] == null) &&
                        (Database.Postgres.Catalog.Schemas[SchemaName].ForeignTables[TableName] == null))
                    {
                        // Database table does not exist
                        Identifiers = [];
                        return;
                    }

                    ColumnMetadata pkey = Columns.Values.Where(a => a.PKey).FirstOrDefault<ColumnMetadata>();

                    if (pkey == null)
                    {
                        // Primary key column does not exist
                        Identifiers = [];
                        return;
                    }

                    if (!new List<string> { "int2", "int4", "oid" }.Contains(item: pkey.DbDataType))
                    {
                        // Primary key must be integer type
                        Identifiers = [];
                        return;
                    }

                    DataTable dt = Database.Postgres.ExecuteQuery(
                        sqlCommand: $"SELECT {pkey.DbName}::int4 AS pkey FROM {SchemaName}.{TableName};");

                    Identifiers =
                        (dt == null)
                        ? []
                        : [..
                            dt.AsEnumerable()
                                .Where(a => a.Field<Nullable<int>>(columnName: "pkey") != null)
                                .Select(a => a.Field<int>(columnName: "pkey"))
                                .OrderBy(a => a)
                          ];
                }

                #endregion Methods

            }


            /// <summary>
            /// Abstract row in data table with geometries
            /// </summary>
            /// <typeparam name="T">Abstract data table with geometries</typeparam>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public abstract class ASpatialTableEntry<T>(T composite = null, DataRow data = null)
                : ADatabaseTableEntry<T>(composite: composite, data: data),
                ISpatialTableEntry<T>
                where T : ASpatialTable
            {
            }

        }
    }
}
