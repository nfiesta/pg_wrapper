﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ZaJi
{
    namespace PostgreSQL
    {

        /// <summary>
        /// Statická třída s rozšiřujícími funkcemi
        /// </summary>
        public static partial class Functions
        {

            #region Constants

            /// <summary>
            /// Textová reprezentace NULL hodnoty
            /// </summary>
            public const string StrNull = "null";

            /// <summary>
            /// Textová reprezentace TRUE hodnoty
            /// </summary>
            public const string StrTrue = "true";

            /// <summary>
            /// Textová reprezentace FALSE hodnoty
            /// </summary>
            public const string StrFalse = "false";

            #endregion Constants



            #region Test not null type in string

            /// <summary>
            /// Obsahuje text byte se znaménkem
            /// </summary>
            /// <param name="text">Text pro převod na byte se znaménkem</param>
            /// <returns>ano|ne</returns>
            public static bool IsSByte(string text)
            {
                try
                {
                    SByte.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text byte bez znaménka
            /// </summary>
            /// <param name="text">Text pro převod na byte bez znaménka</param>
            /// <returns>ano|ne</returns>
            public static bool IsByte(string text)
            {
                try
                {
                    Byte.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text krátké celé číslo se znaménkem
            /// </summary>
            /// <param name="text">Text pro převod na krátké celé číslo se znaménkem</param>
            /// <returns>ano|ne</returns>
            public static bool IsShort(string text)
            {
                try
                {
                    Int16.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text krátké celé číslo bez znaménka
            /// </summary>
            /// <param name="text">Text pro převod na krátké celé číslo bez znaménka</param>
            /// <returns>ano|ne</returns>
            public static bool IsUShort(string text)
            {
                try
                {
                    UInt16.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text celé číslo se znaménkem
            /// </summary>
            /// <param name="text">Text pro převod na celé číslo se znaménkem</param>
            /// <returns>ano|ne</returns>
            public static bool IsInt(string text)
            {
                try
                {
                    Int32.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text celé číslo bez znaménka
            /// </summary>
            /// <param name="text">Text pro převod na celé číslo bez znaménka</param>
            /// <returns>ano|ne</returns>
            public static bool IsUInt(string text)
            {
                try
                {
                    UInt32.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text dlouhé celé číslo se znaménkem
            /// </summary>
            /// <param name="text">Text pro převod na dlouhé celé číslo se znaménkem</param>
            /// <returns>ano|ne</returns>
            public static bool IsLong(string text)
            {
                try
                {
                    Int64.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text dlouhé celé číslo bez znaménka
            /// </summary>
            /// <param name="text">Text pro převod na dlouhé celé číslo bez znaménka</param>
            /// <returns>ano|ne</returns>
            public static bool IsULong(string text)
            {
                try
                {
                    UInt64.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text desetinné číslo (float)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (float)</param>
            /// <returns>ano|ne</returns>
            public static bool IsFloat(string text)
            {
                try
                {
                    Single.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text desetinné číslo (double)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (double)</param>
            /// <returns>ano|ne</returns>
            public static bool IsDouble(string text)
            {
                try
                {
                    Double.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text desetinné číslo (decimal)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (decimal)</param>
            /// <returns>ano|ne</returns>
            public static bool IsDecimal(string text)
            {
                try
                {
                    Decimal.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text logickou hodnotu
            /// </summary>
            /// <param name="text">Text pro převod na logickou hodnotu</param>
            /// <returns>ano|ne</returns>
            public static bool IsBool(string text)
            {
                try
                {
                    Boolean.Parse(value: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text znakovou hodnotu
            /// </summary>
            /// <param name="text">Text pro převod na znakovou hodnotu</param>
            /// <returns>ano|ne</returns>
            public static bool IsChar(string text)
            {
                try
                {
                    Char.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text datum
            /// </summary>
            /// <param name="text">Text pro převod na datum</param>
            /// <returns>ano|ne</returns>
            public static bool IsDateTime(string text)
            {
                try
                {
                    DateTime.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            /// <summary>
            /// Obsahuje text časový interval
            /// </summary>
            /// <param name="text">Text pro převod na časový interval</param>
            /// <returns>ano|ne</returns>
            public static bool IsTimeSpan(string text)
            {
                try
                {
                    TimeSpan.Parse(s: text);
                    return true;
                }
                catch
                {
                    return false;
                }
            }

            #endregion Test not null type in string



            #region Convert string to not null types

            /// <summary>
            /// Převod textu na byte se znaménkem
            /// </summary>
            /// <param name="text">Text pro převod na byte se znaménkem</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Byte se znaménkem získaný z textu</returns>
            public static sbyte ConvertToSByte(string text, sbyte defaultValue = 0)
            {
                return
                    SByte.TryParse(s: text, out sbyte val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na byte bez znaménka
            /// </summary>
            /// <param name="text">Text pro převod na byte bez znaménka</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Byte bez znaménka získaný z textu</returns>
            public static byte ConvertToByte(string text, byte defaultValue = 0)
            {
                return
                    Byte.TryParse(s: text, out byte val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na krátké celé číslo se znaménkem
            /// </summary>
            /// <param name="text">Text pro převod krátké celé číslo se znaménkem</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Krátké celé číslo se znaménkem získané z textu</returns>
            public static short ConvertToShort(string text, short defaultValue = 0)
            {
                return
                    Int16.TryParse(s: text, out short val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na krátké celé číslo bez znaménka
            /// </summary>
            /// <param name="text">Text pro převod na krátké celé číslo bez znaménka</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Krátké celé číslo bez znaménka získané z textu</returns>
            public static ushort ConvertToUShort(string text, ushort defaultValue = 0)
            {
                return
                    UInt16.TryParse(s: text, out ushort val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na celé číslo se znaménkem
            /// </summary>
            /// <param name="text">Text pro převod celé číslo se znaménkem</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Celé číslo se znaménkem získané z textu</returns>
            public static int ConvertToInt(string text, int defaultValue = 0)
            {
                return
                    Int32.TryParse(s: text, out int val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na celé číslo bez znaménka
            /// </summary>
            /// <param name="text">Text pro převod na celé číslo bez znaménka</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Celé číslo bez znaménka získané z textu</returns>
            public static uint ConvertToUInt(string text, uint defaultValue = 0U)
            {
                return
                    UInt32.TryParse(s: text, out uint val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na dlouhé celé číslo se znaménkem
            /// </summary>
            /// <param name="text">Text pro převod dlouhé celé číslo se znaménkem</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Dlouhé celé číslo se znaménkem získané z textu</returns>
            public static long ConvertToLong(string text, long defaultValue = 0L)
            {
                return
                    Int64.TryParse(s: text, out long val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na dlouhé celé číslo bez znaménka
            /// </summary>
            /// <param name="text">Text pro převod na dlouhé celé číslo bez znaménka</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Dlouhé celé číslo bez znaménka získané z textu</returns>
            public static ulong ConvertToULong(string text, ulong defaultValue = 0UL)
            {
                return
                    UInt64.TryParse(s: text, out ulong val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na desetinné číslo (float)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (float)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (float) získané z textu</returns>
            public static float ConvertToFloat(string text, float defaultValue = 0.0F)
            {
                return
                    Single.TryParse(s: text, out float val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na desetinné číslo (double)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (double)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (double) získané z textu</returns>
            public static double ConvertToDouble(string text, double defaultValue = 0.0D)
            {
                return
                    Double.TryParse(s: text, out double val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na desetinné číslo (decimal)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (decimal)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (decimal) získané z textu</returns>
            public static decimal ConvertToDecimal(string text, decimal defaultValue = 0.0M)
            {
                return
                     Decimal.TryParse(s: text, out decimal val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na logickou hodnotu
            /// </summary>
            /// <param name="text">Text pro převod na logickou hodnotu</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Logická hodnota získaná z textu</returns>
            public static bool ConvertToBool(string text, bool defaultValue = false)
            {
                return
                    Boolean.TryParse(value: text, out bool val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na znakovou hodnotu
            /// </summary>
            /// <param name="text">Text pro převod na znakovou hodnotu</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Znaková hodnota</returns>
            public static char ConvertToChar(string text, char defaultValue = (char)48)
            {
                return
                    Char.TryParse(s: text, out char val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na datum
            /// </summary>
            /// <param name="text">Text pro převod na datum</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Datum</returns>
            public static DateTime ConvertToDateTime(string text, DateTime defaultValue = new DateTime())
            {
                return
                    DateTime.TryParse(s: text, out DateTime val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na časový interval
            /// </summary>
            /// <param name="text">Text pro převod na časový interval</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Časový interval</returns>
            public static TimeSpan ConvertToTimeSpan(string text, TimeSpan defaultValue = new TimeSpan())
            {
                return
                    TimeSpan.TryParse(s: text, out TimeSpan val) ? val : defaultValue;
            }

            #endregion Convert string to not null types


            #region Convert string to nullable types

            /// <summary>
            /// Převod textu na byte se znaménkem včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na byte se znaménkem včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Byte se znaménkem včetně hodnoty null získaný z textu</returns>
            public static Nullable<sbyte> ConvertToNSByte(string text, Nullable<sbyte> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    SByte.TryParse(s: text, out sbyte val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na byte bez znaménka včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na byte bez znaménka včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Byte bez znaménka včetně hodnoty null získaný z textu</returns>
            public static Nullable<byte> ConvertToNByte(string text, Nullable<byte> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Byte.TryParse(s: text, out byte val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na krátké celé číslo se znaménkem včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod krátké celé číslo se znaménkem včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Krátké celé číslo se znaménkem včetně hodnoty null získané z textu</returns>
            public static Nullable<short> ConvertToNShort(string text, Nullable<short> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Int16.TryParse(s: text, out short val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na krátké celé číslo bez znaménka včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na krátké celé číslo bez znaménka včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Krátké celé číslo bez znaménka včetně hodnoty null získané z textu</returns>
            public static Nullable<ushort> ConvertToNUShort(string text, Nullable<ushort> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    UInt16.TryParse(s: text, out ushort val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na celé číslo se znaménkem včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod celé číslo se znaménkem včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Celé číslo se znaménkem včetně hodnoty null získané z textu</returns>
            public static Nullable<int> ConvertToNInt(string text, Nullable<int> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Int32.TryParse(s: text, out int val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na celé číslo bez znaménka včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na celé číslo bez znaménka včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Celé číslo bez znaménka včetně hodnoty null získané z textu</returns>
            public static Nullable<uint> ConvertToNUInt(string text, Nullable<uint> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    UInt32.TryParse(s: text, out uint val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na dlouhé celé číslo se znaménkem včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod dlouhé celé číslo se znaménkem včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Dlouhé celé číslo se znaménkem včetně hodnoty null získané z textu</returns>
            public static Nullable<long> ConvertToNLong(string text, Nullable<long> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Int64.TryParse(s: text, out long val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na dlouhé celé číslo bez znaménka včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na dlouhé celé číslo bez znaménka včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Dlouhé celé číslo bez znaménka včetně hodnoty null získané z textu</returns>
            public static Nullable<ulong> ConvertToNULong(string text, Nullable<ulong> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    UInt64.TryParse(s: text, out ulong val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na desetinné číslo (float) včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (float) včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (float) včetně hodnoty null získané z textu</returns>
            public static Nullable<float> ConvertToNFloat(string text, Nullable<float> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Single.TryParse(s: text, out float val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na desetinné číslo (double) včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (double) včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (double) včetně hodnoty null získané z textu</returns>
            public static Nullable<double> ConvertToNDouble(string text, Nullable<double> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Double.TryParse(s: text, out double val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na desetinné číslo (decimal) včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na desetinné číslo (decimal) včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (decimal) včetně hodnoty null získané z textu</returns>
            public static Nullable<decimal> ConvertToNDecimal(string text, Nullable<decimal> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Decimal.TryParse(s: text, out decimal val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na logickou hodnotu včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na logickou hodnotu včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Logická hodnota včetně hodnoty null získaná z textu</returns>
            public static Nullable<bool> ConvertToNBool(string text, Nullable<bool> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Boolean.TryParse(value: text, out bool val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na znakovou hodnotu včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na znakovou hodnotu včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Znaková hodnota včetně hodnoty null</returns>
            public static Nullable<char> ConvertToNChar(string text, Nullable<char> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    Char.TryParse(s: text, out char val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na datum včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na datum včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Datum včetně hodnoty null</returns>
            public static Nullable<DateTime> ConvertToNDateTime(string text, Nullable<DateTime> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    DateTime.TryParse(s: text, out DateTime val) ? val : defaultValue;
            }

            /// <summary>
            /// Převod textu na časový interval včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na časový interval včetně hodnoty null</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Časový interval včetně hodnoty null</returns>
            public static Nullable<TimeSpan> ConvertToNTimeSpan(string text, Nullable<TimeSpan> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text)) { return null; }
                if (String.IsNullOrEmpty(value: text.Trim())) { return null; }
                if (text.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                return
                    TimeSpan.TryParse(s: text, out TimeSpan val) ? val : defaultValue;
            }

            #endregion Convert string to nullable types


            #region Convert string to list of not null types

            /// <summary>
            /// Převod textu na seznam byte se znaménkem
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam byte se znaménkem</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam byte se znaménkem získaný z textu</returns>
            public static List<sbyte> StringToSByteList(string text, char separator = (char)59, Nullable<sbyte> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => SByte.TryParse(s: a, out sbyte b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => SByte.TryParse(s: a, out sbyte b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => SByte.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam byte bez znaménka
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam byte bez znaménka</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam byte bez znaménka získaný z textu</returns>
            public static List<byte> StringToByteList(string text, char separator = (char)59, Nullable<byte> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Byte.TryParse(s: a, out byte b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Byte.TryParse(s: a, out byte b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Byte.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam krátkých celých čísel se znaménkem
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam krátkých celých čísel se znaménkem</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam krátkých celých čísel se znaménkem získaný z textu</returns>
            public static List<short> StringToShortList(string text, char separator = (char)59, Nullable<short> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Int16.TryParse(s: a, out short b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Int16.TryParse(s: a, out short b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Int16.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam krátkých celých čísel bez znaménka
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam krátkých celých čísel bez znaménka</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam krátkých celých čísel bez znaménka získaný z textu</returns>
            public static List<ushort> StringToUShortList(string text, char separator = (char)59, Nullable<ushort> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => UInt16.TryParse(s: a, out ushort b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => UInt16.TryParse(s: a, out ushort b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => UInt16.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam celých čísel se znaménkem
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam celých čísel se znaménkem</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam celých čísel se znaménkem získaný z textu</returns>
            public static List<int> StringToIntList(string text, char separator = (char)59, Nullable<int> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Int32.TryParse(s: a, out int b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Int32.TryParse(s: a, out int b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Int32.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam celých čísel bez znaménka
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam celých čísel bez znaménka</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam celých čísel bez znaménka získaný z textu</returns>
            public static List<uint> StringToUIntList(string text, char separator = (char)59, Nullable<uint> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => UInt32.TryParse(s: a, out uint b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => UInt32.TryParse(s: a, out uint b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => UInt32.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam dlouhých celých čísel se znaménkem
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam dlouhých celých čísel se znaménkem</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam dlouhých celých čísel se znaménkem získaný z textu</returns>
            public static List<long> StringToLongList(string text, char separator = (char)59, Nullable<long> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Int64.TryParse(s: a, out long b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Int64.TryParse(s: a, out long b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Int64.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam dlouhých celých čísel bez znaménka
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam dlouhých celých čísel bez znaménka</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam dlouhých celých čísel bez znaménka získaný z textu</returns>
            public static List<ulong> StringToULongList(string text, char separator = (char)59, Nullable<ulong> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => UInt64.TryParse(s: a, out ulong b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => UInt64.TryParse(s: a, out ulong b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => UInt64.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam desetinných čísel (float)
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam desetinných čísel (float)</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam desetinných čísel (float) získaný z textu</returns>
            public static List<float> StringToFloatList(string text, char separator = (char)59, Nullable<float> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Single.TryParse(s: a, out float b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Single.TryParse(s: a, out float b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Single.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam desetinných čísel (double)
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam desetinných čísel (double)</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam desetinných čísel (double) získaný z textu</returns>
            public static List<double> StringToDoubleList(string text, char separator = (char)59, Nullable<double> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Double.TryParse(s: a, out double b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Double.TryParse(s: a, out double b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Double.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam desetinných čísel (decimal)
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam desetinných čísel (decimal)</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam desetinných čísel (decimal) získaný z textu</returns>
            public static List<decimal> StringToDecimalList(string text, char separator = (char)59, Nullable<decimal> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Decimal.TryParse(s: a, out decimal b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Decimal.TryParse(s: a, out decimal b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Decimal.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam logických hodnot
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam logických hodnot</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam logických hodnot získaný z textu</returns>
            public static List<bool> StringToBoolList(string text, char separator = (char)59, Nullable<bool> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Boolean.TryParse(value: a, out bool b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Boolean.TryParse(value: a, out bool b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Boolean.Parse(value: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam znakových hodnot
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam znakových hodnot</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam znakových hodnot získaný z textu</returns>
            public static List<char> StringToCharList(string text, char separator = (char)59, Nullable<char> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => Char.TryParse(s: a, out char b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => Char.TryParse(s: a, out char b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => Char.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam datumů
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam datumů</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam datumů získaný z textu</returns>
            public static List<DateTime> StringToDateTimeList(string text, char separator = (char)59, Nullable<DateTime> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => DateTime.TryParse(s: a, out DateTime b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => DateTime.TryParse(s: a, out DateTime b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => DateTime.Parse(s: a))
                    .ToList();
            }

            /// <summary>
            /// Převod textu na seznam časových intervalů
            /// (Pokud default value je null,
            /// pak hodnoty, které nelze převést jsou ze seznamu vynechány,
            /// jinak jsou nahrazeny defaultní hodnotou)
            /// </summary>
            /// <param name="text">Text pro převod na seznam časových intervalů</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam časových intervalů získaný z textu</returns>
            public static List<TimeSpan> StringToTimeSpanList(string text, char separator = (char)59, Nullable<TimeSpan> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                if (defaultValue == null)
                {
                    // Vynechání prvků, které nelze převést na požadovaný typ
                    strElements = strElements
                        .Where(a => TimeSpan.TryParse(s: a, out TimeSpan b));
                }
                else
                {
                    // Nahrazení prvků, které nelze převést na požadovaný typ, defaultní hodnotou
                    strElements = strElements
                        .Select(a => TimeSpan.TryParse(s: a, out TimeSpan b) ? a : defaultValue.ToString());
                }

                return strElements
                    .Select(a => TimeSpan.Parse(s: a))
                    .ToList();
            }

            #endregion Convert string to list of not null types


            #region Convert string to list of nullable types

            /// <summary>
            /// Převod textu na seznam byte se znaménkem včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam byte se znaménkem včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam byte se znaménkem včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<sbyte>> StringToNSByteList(string text, char separator = (char)59, Nullable<sbyte> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return SByte.TryParse(s: a, out sbyte b) ? b : defaultValue;
                    })
                    .ToList<Nullable<sbyte>>();
            }

            /// <summary>
            /// Převod textu na seznam byte bez znaménka včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam byte bez znaménka včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam byte bez znaménka včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<byte>> StringToNByteList(string text, char separator = (char)59, Nullable<byte> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Byte.TryParse(s: a, out byte b) ? b : defaultValue;
                    })
                    .ToList<Nullable<byte>>();
            }

            /// <summary>
            /// Převod textu na seznam krátkých celých čísel se znaménkem včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam krátkých celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam krátkých celých čísel se znaménkem včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<short>> StringToNShortList(string text, char separator = (char)59, Nullable<short> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Int16.TryParse(s: a, out short b) ? b : defaultValue;
                    })
                    .ToList<Nullable<short>>();
            }

            /// <summary>
            /// Převod textu na seznam krátkých celých čísel bez znaménka včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam krátkých celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam krátkých celých čísel bez znaménka včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<ushort>> StringToNUShortList(string text, char separator = (char)59, Nullable<ushort> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return UInt16.TryParse(s: a, out ushort b) ? b : defaultValue;
                    })
                    .ToList<Nullable<ushort>>();
            }

            /// <summary>
            /// Převod textu na seznam celých čísel se znaménkem včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam celých čísel se znaménkem včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<int>> StringToNIntList(string text, char separator = (char)59, Nullable<int> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Int32.TryParse(s: a, out int b) ? b : defaultValue;
                    })
                    .ToList<Nullable<int>>();
            }

            /// <summary>
            /// Převod textu na seznam celých čísel bez znaménka včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam celých čísel bez znaménka včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<uint>> StringToNUIntList(string text, char separator = (char)59, Nullable<uint> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return UInt32.TryParse(s: a, out uint b) ? b : defaultValue;
                    })
                    .ToList<Nullable<uint>>();
            }

            /// <summary>
            /// Převod textu na seznam dlouhých celých čísel se znaménkem včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam dlouhých celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam dlouhých celých čísel se znaménkem včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<long>> StringToNLongList(string text, char separator = (char)59, Nullable<long> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Int64.TryParse(s: a, out long b) ? b : defaultValue;
                    })
                    .ToList<Nullable<long>>();
            }

            /// <summary>
            /// Převod textu na seznam dlouhých celých čísel bez znaménka včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam dlouhých celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam dlouhých celých čísel bez znaménka včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<ulong>> StringToNULongList(string text, char separator = (char)59, Nullable<ulong> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return UInt64.TryParse(s: a, out ulong b) ? b : defaultValue;
                    })
                    .ToList<Nullable<ulong>>();
            }

            /// <summary>
            /// Převod textu na seznam desetinných čísel (float) včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam desetinných čísel (float) včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam desetinných čísel (float) včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<float>> StringToNFloatList(string text, char separator = (char)59, Nullable<float> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Single.TryParse(s: a, out float b) ? b : defaultValue;
                    })
                    .ToList<Nullable<float>>();
            }

            /// <summary>
            /// Převod textu na seznam desetinných čísel (double) včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam desetinných čísel (double) včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam desetinných čísel (double) včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<double>> StringToNDoubleList(string text, char separator = (char)59, Nullable<double> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Double.TryParse(s: a, out double b) ? b : defaultValue;
                    })
                    .ToList<Nullable<double>>();
            }

            /// <summary>
            /// Převod textu na seznam desetinných čísel (decimal) včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam desetinných čísel (decimal) včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam desetinných čísel (decimal) včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<decimal>> StringToNDecimalList(string text, char separator = (char)59, Nullable<decimal> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Decimal.TryParse(s: a, out decimal b) ? b : defaultValue;
                    })
                    .ToList<Nullable<decimal>>();
            }

            /// <summary>
            /// Převod textu na seznam logických hodnot včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam logických hodnot včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam logických hodnot včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<bool>> StringToNBoolList(string text, char separator = (char)59, Nullable<bool> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Boolean.TryParse(value: a, out bool b) ? b : defaultValue;
                    })
                    .ToList<Nullable<bool>>();
            }

            /// <summary>
            /// Převod textu na seznam znakových hodnot včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam znakových hodnot včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam znakových hodnot včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<char>> StringToNCharList(string text, char separator = (char)59, Nullable<char> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return Char.TryParse(s: a, out char b) ? b : defaultValue;
                    })
                    .ToList<Nullable<char>>();
            }

            /// <summary>
            /// Převod textu na seznam datumů včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam datumů včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam datumů včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<DateTime>> StringToNDateTimeList(string text, char separator = (char)59, Nullable<DateTime> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return DateTime.TryParse(s: a, out DateTime b) ? b : defaultValue;
                    })
                    .ToList<Nullable<DateTime>>();
            }

            /// <summary>
            /// Převod textu na seznam časových intervalů včetně hodnoty null
            /// (Za null hodnotu je považována proměnná text,
            /// která není inicializovaná, obsahuje prázdný řetězec, nebo text null bez ohledu na velikost znaků)
            /// </summary>
            /// <param name="text">Text pro převod na seznam časových intervalů včetně hodnoty null</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Seznam časových intervalů včetně hodnoty null získaný z textu</returns>
            public static List<Nullable<TimeSpan>> StringToNTimeSpanList(string text, char separator = (char)59, Nullable<TimeSpan> defaultValue = null)
            {
                if (String.IsNullOrEmpty(value: text))
                {
                    return [];
                }

                // Rozložení na jednotlivé prvky a odstranění prázdných znaků
                IEnumerable<string> strElements =
                    text.Split(separator: separator)
                        .Select(a => a.Trim());

                return strElements
                    .Select(a =>
                    {
                        if (String.IsNullOrEmpty(a)) { return null; }
                        if (String.IsNullOrEmpty(a.Trim())) { return null; }
                        if (a.Trim().Equals(value: StrNull.Trim(), comparisonType: StringComparison.CurrentCultureIgnoreCase)) { return null; }

                        return TimeSpan.TryParse(s: a, out TimeSpan b) ? b : defaultValue;
                    })
                    .ToList<Nullable<TimeSpan>>();
            }

            /// <summary>
            /// Převod textu na seznam textových hodnot
            /// </summary>
            /// <param name="text">Text pro převod na seznam textových hodnot</param>
            /// <param name="separator">Oddělovač hodnot v textu (defaultně středník)</param>
            /// <returns>Seznam textových hodnot získaný z textu</returns>
            public static List<string> StringToStringList(string text, char separator)
            {
                if (text == null)
                {
                    return [];
                }

                if (text == String.Empty)
                {
                    return [String.Empty];
                }

                // Rozložení na jednotlivé prvky
                IEnumerable<string> strElements =
                    text.Split(separator: separator);

                return strElements
                    .ToList();
            }

            #endregion Convert string to list of nullable types



            #region Get not null value from data row

            /// <summary>
            /// Získá byte se znaménkem z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Byte se znaménkem z datového řádku</returns>
            public static sbyte GetSByteArg(DataRow row, string name, sbyte defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                    ConvertToSByte(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá byte se znaménkem z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Byte se znaménkem z datového řádku</returns>
            public static sbyte GetSByteArg(DataGridViewRow row, string name, sbyte defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                    ConvertToSByte(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá byte bez znaménka z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Byte bez znaménka z datového řádku</returns>
            public static byte GetByteArg(DataRow row, string name, byte defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                    ConvertToByte(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá byte bez znaménka z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Byte bez znaménka z datového řádku</returns>
            public static byte GetByteArg(DataGridViewRow row, string name, byte defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToByte(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá krátké celé číslo se znaménkem z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Krátké celé číslo se znaménkem z datového řádku</returns>
            public static short GetShortArg(DataRow row, string name, short defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToShort(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá krátké celé číslo se znaménkem z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Krátké celé číslo se znaménkem z datového řádku</returns>
            public static short GetShortArg(DataGridViewRow row, string name, short defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToShort(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá krátké celé číslo bez znaménka z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Krátké celé číslo bez znaménka z datového řádku</returns>
            public static ushort GetUShortArg(DataRow row, string name, ushort defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                    ConvertToUShort(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá krátké celé číslo bez znaménka z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Krátké celé číslo bez znaménka z datového řádku</returns>
            public static ushort GetUShortArg(DataGridViewRow row, string name, ushort defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                    ConvertToUShort(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá celé číslo se znaménkem z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Celé číslo se znaménkem z datového řádku</returns>
            public static int GetIntArg(DataRow row, string name, int defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToInt(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá celé číslo se znaménkem z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Celé číslo se znaménkem z datového řádku</returns>
            public static int GetIntArg(DataGridViewRow row, string name, int defaultValue = 0)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToInt(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá celé číslo bez znaménka z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Celé číslo bez znaménka z datového řádku</returns>
            public static uint GetUIntArg(DataRow row, string name, uint defaultValue = 0U)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToUInt(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá celé číslo bez znaménka z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Celé číslo bez znaménka z datového řádku</returns>
            public static uint GetUIntArg(DataGridViewRow row, string name, uint defaultValue = 0U)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToUInt(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá dlouhé celé číslo se znaménkem z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Dlouhé celé číslo se znaménkem z datového řádku</returns>
            public static long GetLongArg(DataRow row, string name, long defaultValue = 0L)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToLong(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá dlouhé celé číslo se znaménkem z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Dlouhé celé číslo se znaménkem z datového řádku</returns>
            public static long GetLongArg(DataGridViewRow row, string name, long defaultValue = 0L)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToLong(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá dlouhé celé číslo bez znaménka z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Dlouhé celé číslo bez znaménka z datového řádku</returns>
            public static ulong GetULongArg(DataRow row, string name, ulong defaultValue = 0UL)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToULong(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá dlouhé celé číslo bez znaménka z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Dlouhé celé číslo bez znaménka z datového řádku</returns>
            public static ulong GetULongArg(DataGridViewRow row, string name, ulong defaultValue = 0UL)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                      ConvertToULong(
                          text: row.Cells[name].Value.ToString(),
                          defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá desetinné číslo (float) z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Desetinné číslo (float) z datového řádku</returns>
            public static float GetFloatArg(DataRow row, string name, float defaultValue = 0.0F)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToFloat(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá desetinné číslo (float) z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Desetinné číslo (float) z datového řádku</returns>
            public static float GetFloatArg(DataGridViewRow row, string name, float defaultValue = 0.0F)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToFloat(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá desetinné číslo (double) z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Desetinné číslo (double) z datového řádku</returns>
            public static double GetDoubleArg(DataRow row, string name, double defaultValue = 0.0D)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToDouble(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá desetinné číslo (double) z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Desetinné číslo (double) z datového řádku</returns>
            public static double GetDoubleArg(DataGridViewRow row, string name, double defaultValue = 0.0D)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToDouble(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá desetinné číslo (decimal) z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Desetinné číslo (decimal) z datového řádku</returns>
            public static decimal GetDecimalArg(DataRow row, string name, decimal defaultValue = 0.0M)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToDecimal(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá desetinné číslo (decimal) z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Desetinné číslo (decimal) z datového řádku</returns>
            public static decimal GetDecimalArg(DataGridViewRow row, string name, decimal defaultValue = 0.0M)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToDecimal(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá logickou hodnotu z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Logická hodnota z datového řádku</returns>
            public static bool GetBoolArg(DataRow row, string name, bool defaultValue = false)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToBool(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá logickou hodnotu z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Logická hodnota z datového řádku</returns>
            public static bool GetBoolArg(DataGridViewRow row, string name, bool defaultValue = false)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToBool(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá znakovou hodnotu z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Znaková hodnota z datového řádku</returns>
            public static char GetCharArg(DataRow row, string name, char defaultValue = (char)48)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }

                return
                     ConvertToChar(
                         text: row[name].ToString(),
                         defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá znakovou hodnotu z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Znaková hodnota z datového řádku</returns>
            public static char GetCharArg(DataGridViewRow row, string name, char defaultValue = (char)48)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }

                return
                     ConvertToChar(
                         text: row.Cells[name].Value.ToString(),
                         defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá datum z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Datum z datového řádku</returns>
            public static DateTime GetDateTimeArg(DataRow row, string name, DateTime defaultValue = new DateTime())
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }
                if (!(row[name] is DateTime || row[name] is Nullable<DateTime>)) { return defaultValue; }

                return (DateTime)row[name];
            }

            /// <summary>
            /// Získá datum z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Datum z datového řádku</returns>
            public static DateTime GetDateTimeArg(DataGridViewRow row, string name, DateTime defaultValue = new DateTime())
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }
                if (!(row.Cells[name].Value is DateTime || row.Cells[name].Value is Nullable<DateTime>)) { return defaultValue; }

                return (DateTime)row.Cells[name].Value;
            }


            /// <summary>
            /// Získá časový interval z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Časový interval z datového řádku</returns>
            public static TimeSpan GetTimeSpanArg(DataRow row, string name, TimeSpan defaultValue = new TimeSpan())
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }
                if (!(row[name] is TimeSpan || row[name] is Nullable<TimeSpan>)) { return defaultValue; }

                return (TimeSpan)row[name];
            }

            /// <summary>
            /// Získá časový interval z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Časový interval z datového řádku</returns>
            public static TimeSpan GetTimeSpanArg(DataGridViewRow row, string name, TimeSpan defaultValue = new TimeSpan())
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }
                if (!(row.Cells[name].Value is TimeSpan || row.Cells[name].Value is Nullable<TimeSpan>)) { return defaultValue; }

                return (TimeSpan)row.Cells[name].Value;
            }


            /// <summary>
            /// Získá globálně jedinečný identifikátor z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>Globálně jedinečný identifikátor z datového řádku</returns>
            public static Guid GetGuidArg(DataRow row, string name, Guid defaultValue)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return defaultValue; }
                if (!(row[name] is Guid || row[name] is Nullable<Guid>)) { return defaultValue; }

                return (Guid)row[name];
            }

            /// <summary>
            /// Získá globálně jedinečný identifikátor z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří
            /// nebo když řádek obsahuje null hodnotu</param>
            /// <returns>globálně jedinečný identifikátor z datového řádku</returns>
            public static Guid GetGuidArg(DataGridViewRow row, string name, Guid defaultValue)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return defaultValue; }
                if (!(row.Cells[name].Value is Guid || row.Cells[name].Value is Nullable<Guid>)) { return defaultValue; }

                return (Guid)row.Cells[name].Value;
            }

            #endregion Get not null value from data row


            #region Get nullable value from data row

            /// <summary>
            /// Získá byte se znaménkem včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Byte se znaménkem včetně hodnoty null z datového řádku</returns>
            public static Nullable<sbyte> GetNSByteArg(DataRow row, string name, Nullable<sbyte> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNSByte(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá byte se znaménkem včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Byte se znaménkem včetně hodnoty null z datového řádku</returns>
            public static Nullable<sbyte> GetNSByteArg(DataGridViewRow row, string name, Nullable<sbyte> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNSByte(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá byte bez znaménka včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Byte bez znaménka včetně hodnoty null z datového řádku</returns>
            public static Nullable<byte> GetNByteArg(DataRow row, string name, Nullable<byte> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNByte(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá byte bez znaménka včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Byte bez znaménka včetně hodnoty null z datového řádku</returns>
            public static Nullable<byte> GetNByteArg(DataGridViewRow row, string name, Nullable<byte> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNByte(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá krátké celé číslo se znaménkem včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Krátké celé číslo se znaménkem včetně hodnoty null z datového řádku</returns>
            public static Nullable<short> GetNShortArg(DataRow row, string name, Nullable<short> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNShort(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá krátké celé číslo se znaménkem včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Krátké celé číslo se znaménkem včetně hodnoty null z datového řádku</returns>
            public static Nullable<short> GetNShortArg(DataGridViewRow row, string name, Nullable<short> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNShort(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá krátké celé číslo bez znaménka včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Krátké celé číslo bez znaménka včetně hodnoty null z datového řádku</returns>
            public static Nullable<ushort> GetNUShortArg(DataRow row, string name, Nullable<ushort> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNUShort(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá krátké celé číslo bez znaménka včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Krátké celé číslo bez znaménka včetně hodnoty null z datového řádku</returns>
            public static Nullable<ushort> GetNUShortArg(DataGridViewRow row, string name, Nullable<ushort> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNUShort(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá celé číslo se znaménkem včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Celé číslo se znaménkem včetně hodnoty null z datového řádku</returns>
            public static Nullable<int> GetNIntArg(DataRow row, string name, Nullable<int> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNInt(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá celé číslo se znaménkem včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Celé číslo se znaménkem včetně hodnoty null z datového řádku</returns>
            public static Nullable<int> GetNIntArg(DataGridViewRow row, string name, Nullable<int> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNInt(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá celé číslo bez znaménka včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Celé číslo bez znaménka včetně hodnoty null z datového řádku</returns>
            public static Nullable<uint> GetNUIntArg(DataRow row, string name, Nullable<uint> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNUInt(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá celé číslo bez znaménka včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Celé číslo bez znaménka včetně hodnoty null z datového řádku</returns>
            public static Nullable<uint> GetNUIntArg(DataGridViewRow row, string name, Nullable<uint> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNUInt(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá dlouhé celé číslo se znaménkem včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Dlouhé celé číslo se znaménkem včetně hodnoty null z datového řádku</returns>
            public static Nullable<long> GetNLongArg(DataRow row, string name, Nullable<long> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNLong(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá dlouhé celé číslo se znaménkem včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Dlouhé celé číslo se znaménkem včetně hodnoty null z datového řádku</returns>
            public static Nullable<long> GetNLongArg(DataGridViewRow row, string name, Nullable<long> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNLong(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá dlouhé celé číslo bez znaménka včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Dlouhé celé číslo bez znaménka včetně hodnoty null z datového řádku</returns>
            public static Nullable<ulong> GetNULongArg(DataRow row, string name, Nullable<ulong> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNULong(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá dlouhé celé číslo bez znaménka včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Dlouhé celé číslo bez znaménka včetně hodnoty null z datového řádku</returns>
            public static Nullable<ulong> GetNULongArg(DataGridViewRow row, string name, Nullable<ulong> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNULong(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá desetinné číslo (float) včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (float) včetně hodnoty null z datového řádku</returns>
            public static Nullable<float> GetNFloatArg(DataRow row, string name, Nullable<float> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNFloat(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá desetinné číslo (float) včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (float) včetně hodnoty null z datového řádku</returns>
            public static Nullable<float> GetNFloatArg(DataGridViewRow row, string name, Nullable<float> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNFloat(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá desetinné číslo (double) včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (double) včetně hodnoty null z datového řádku</returns>
            public static Nullable<double> GetNDoubleArg(DataRow row, string name, Nullable<double> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNDouble(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá desetinné číslo (double) včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (double) včetně hodnoty null z datového řádku</returns>
            public static Nullable<double> GetNDoubleArg(DataGridViewRow row, string name, Nullable<double> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNDouble(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá desetinné číslo (decimal) včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (decimal) včetně hodnoty null z datového řádku</returns>
            public static Nullable<decimal> GetNDecimalArg(DataRow row, string name, Nullable<decimal> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNDecimal(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá desetinné číslo (decimal) včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Desetinné číslo (decimal) včetně hodnoty null z datového řádku</returns>
            public static Nullable<decimal> GetNDecimalArg(DataGridViewRow row, string name, Nullable<decimal> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNDecimal(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá logickou hodnotu včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Logická hodnota včetně hodnoty null z datového řádku</returns>
            public static Nullable<bool> GetNBoolArg(DataRow row, string name, Nullable<bool> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNBool(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá logickou hodnotu včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Logická hodnota včetně hodnoty null z datového řádku</returns>
            public static Nullable<bool> GetNBoolArg(DataGridViewRow row, string name, Nullable<bool> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNBool(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá znakovou hodnotu včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Znaková hodnota včetně hodnoty null z datového řádku</returns>
            public static Nullable<char> GetNCharArg(DataRow row, string name, Nullable<char> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }

                return
                    ConvertToNChar(
                        text: row[name].ToString(),
                        defaultValue: defaultValue);
            }

            /// <summary>
            /// Získá znakovou hodnotu včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Znaková hodnota včetně hodnoty null z datového řádku</returns>
            public static Nullable<char> GetNCharArg(DataGridViewRow row, string name, Nullable<char> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }

                return
                    ConvertToNChar(
                        text: row.Cells[name].Value.ToString(),
                        defaultValue: defaultValue);
            }


            /// <summary>
            /// Získá datum včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Datum včetně hodnoty null z datového řádku</returns>
            public static Nullable<DateTime> GetNDateTimeArg(DataRow row, string name, Nullable<DateTime> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }
                if (!(row[name] is DateTime || row[name] is Nullable<DateTime>)) { return defaultValue; }

                return (Nullable<DateTime>)row[name];
            }

            /// <summary>
            /// Získá datum včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Datum včetně hodnoty null z datového řádku</returns>
            public static Nullable<DateTime> GetNDateTimeArg(DataGridViewRow row, string name, Nullable<DateTime> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }
                if (!(row.Cells[name].Value is DateTime || row.Cells[name].Value is Nullable<DateTime>)) { return defaultValue; }

                return (Nullable<DateTime>)row.Cells[name].Value;
            }


            /// <summary>
            /// Získá časový interval včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Časový interval včetně hodnoty null z datového řádku</returns>
            public static Nullable<TimeSpan> GetNTimeSpanArg(DataRow row, string name, Nullable<TimeSpan> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }
                if (!(row[name] is TimeSpan || row[name] is Nullable<TimeSpan>)) { return defaultValue; }

                return (Nullable<TimeSpan>)row[name];
            }

            /// <summary>
            /// Získá časový inteval včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Časový interval včetně hodnoty null z datového řádku</returns>
            public static Nullable<TimeSpan> GetNTimeSpanArg(DataGridViewRow row, string name, Nullable<TimeSpan> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }
                if (!(row.Cells[name].Value is TimeSpan || row.Cells[name].Value is Nullable<TimeSpan>)) { return defaultValue; }

                return (Nullable<TimeSpan>)row.Cells[name].Value;
            }

            /// <summary>
            /// Získá globálně jednoznačný identifikátor včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Globálně jednoznačný identifikátor včetně hodnoty null z datového řádku</returns>
            public static Nullable<Guid> GetNGuidArg(DataRow row, string name, Nullable<Guid> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.IsNull(columnName: name)) { return null; }
                if (row[name] == null) { return null; }
                if (row[name] == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row[name].ToString())) { return null; }
                if (!(row[name] is Guid || row[name] is Nullable<Guid>)) { return defaultValue; }

                return (Nullable<Guid>)row[name];
            }

            /// <summary>
            /// Získá globálně jednoznačný identifikátor včetně hodnoty null z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <returns>Globálně jednoznačný identifikátor  včetně hodnoty null z datového řádku</returns>
            public static Nullable<Guid> GetNGuidArg(DataGridViewRow row, string name, Nullable<Guid> defaultValue = null)
            {
                if (row == null) { return null; }
                if (row.Cells[name] == null) { return null; }
                if (row.Cells[name].Value == null) { return null; }
                if (row.Cells[name].Value == DBNull.Value) { return null; }
                if (String.IsNullOrEmpty(value: row.Cells[name].Value.ToString())) { return null; }
                if (!(row.Cells[name].Value is Guid || row.Cells[name].Value is Nullable<Guid>)) { return defaultValue; }

                return (Nullable<Guid>)row.Cells[name].Value;
            }


            /// <summary>
            /// Získá textovou hodnotu z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="format">Formát čísla</param>
            /// <returns>Textová hodnota z datového řádku</returns>
            public static string GetStringArg(DataRow row, string name, string defaultValue = null, string format = null)
            {
                if (row == null) { return defaultValue; }
                if (row.IsNull(columnName: name)) { return defaultValue; }
                if (row[name] == null) { return defaultValue; }
                if (row[name] == DBNull.Value) { return defaultValue; }
                if (row[name].ToString() == null) { return defaultValue; }

                if (format == null)
                {
                    return row[name].ToString();
                }

                if (row.Table.Columns[name].DataType.FullName == "System.Single")
                {
                    float val = GetFloatArg(row, name);
                    return val.ToString(format: format);
                }

                else if (row.Table.Columns[name].DataType.FullName == "System.Double")
                {
                    double val = GetDoubleArg(row, name);
                    return val.ToString(format: format);
                }

                else
                {
                    return row[name].ToString();
                }
            }

            /// <summary>
            /// Získá textovou hodnotu z datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="format">Formát čísla</param>
            /// <returns>Textová hodnota z datového řádku</returns>
            public static string GetStringArg(DataGridViewRow row, string name, string defaultValue = null, string format = null)
            {
                if (row == null) { return defaultValue; }
                if (row.Cells[name] == null) { return defaultValue; }
                if (row.Cells[name].Value == null) { return defaultValue; }
                if (row.Cells[name].Value == DBNull.Value) { return defaultValue; }
                if (row.Cells[name].Value.ToString() == null) { return defaultValue; }

                if (format == null)
                {
                    return row.Cells[name].Value.ToString();
                }

                if (row.Cells[name].Value.GetType().FullName == "System.Single")
                {
                    float val = GetFloatArg(row, name);
                    return val.ToString(format: format);
                }
                else if (row.Cells[name].Value.GetType().FullName == "System.Double")
                {
                    double val = GetDoubleArg(row, name);
                    return val.ToString(format: format);
                }
                else
                {
                    return row.Cells[name].Value.ToString();
                }
            }

            #endregion Get nullable value from data row


            #region Set not null value into data row

            /// <summary>
            /// Zapíše byte se znaménkem do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Byte se znaménkem</param>
            public static void SetSByteArg(DataRow row, string name, sbyte val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše byte se znaménkem do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Byte se znaménkem</param>
            public static void SetSByteArg(DataGridViewRow row, string name, sbyte val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše byte bez znaménka do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Byte bez znaménka</param>
            public static void SetByteArg(DataRow row, string name, byte val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše byte bez znaménka do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Byte bez znaménka</param>
            public static void SetByteArg(DataGridViewRow row, string name, byte val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše krátké celé číslo se znaménkem do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Krátké celé číslo se znaménkem</param>
            public static void SetShortArg(DataRow row, string name, short val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše krátké celé číslo se znaménkem do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Krátké celé číslo se znaménkem</param>
            public static void SetShortArg(DataGridViewRow row, string name, short val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše krátké celé číslo bez znaménka do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Krátké celé číslo bez znaménka</param>
            public static void SetUShortArg(DataRow row, string name, ushort val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše krátké celé číslo bez znaménka do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Krátké celé číslo bez znaménka</param>
            public static void SetUShortArg(DataGridViewRow row, string name, ushort val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše celé číslo se znaménkem do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Celé číslo se znaménkem</param>
            public static void SetIntArg(DataRow row, string name, int val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše celé číslo se znaménkem do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Celé číslo se znaménkem</param>
            public static void SetIntArg(DataGridViewRow row, string name, int val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše celé číslo bez znaménka do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Celé číslo bez znaménka</param>
            public static void SetUIntArg(DataRow row, string name, uint val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše celé číslo bez znaménka do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Celé číslo bez znaménka</param>
            public static void SetUIntArg(DataGridViewRow row, string name, uint val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše dlouhé celé číslo se znaménkem do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Dlouhé celé číslo se znaménkem</param>
            public static void SetLongArg(DataRow row, string name, long val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše dlouhé celé číslo se znaménkem do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Dlouhé celé číslo se znaménkem</param>
            public static void SetLongArg(DataGridViewRow row, string name, long val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše dlouhé celé číslo bez znaménka do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Dlouhé celé číslo bez znaménka</param>
            public static void SetULongArg(DataRow row, string name, ulong val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše dlouhé celé číslo bez znaménka do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Dlouhé celé číslo bez znaménka</param>
            public static void SetULongArg(DataGridViewRow row, string name, ulong val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše desetinné číslo (float) do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (float)</param>
            public static void SetFloatArg(DataRow row, string name, float val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše desetinné číslo (float) do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (float)</param>
            public static void SetFloatArg(DataGridViewRow row, string name, float val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše desetinné číslo (double) do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (double)</param>
            public static void SetDoubleArg(DataRow row, string name, double val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše desetinné číslo (double) do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (double)</param>
            public static void SetDoubleArg(DataGridViewRow row, string name, double val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše desetinné číslo (decimal) do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (decimal)</param>
            public static void SetDecimalArg(DataRow row, string name, double val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše desetinné číslo (decimal) do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (decimal)</param>
            public static void SetDecimalArg(DataGridViewRow row, string name, double val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše logickou hodnotu do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Logická hodnota</param>
            public static void SetBoolArg(DataRow row, string name, bool val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše logickou hodnotu do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Logická hodnota</param>
            public static void SetBoolArg(DataGridViewRow row, string name, bool val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše znakovou hodnotu do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Znaková hodnota</param>
            public static void SetCharArg(DataRow row, string name, char val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše znakovou hodnotu do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Znaková hodnota</param>
            public static void SetCharArg(DataGridViewRow row, string name, char val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše datum do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Datum</param>
            public static void SetDateTimeArg(DataRow row, string name, DateTime val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše datum do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Datum</param>
            public static void SetDateTimeArg(DataGridViewRow row, string name, DateTime val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše časový interval do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Časový interval</param>
            public static void SetTimeSpanArg(DataRow row, string name, TimeSpan val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše časový interval do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Časový interval</param>
            public static void SetTimeSpanArg(DataGridViewRow row, string name, TimeSpan val)
            {
                row.Cells[name].Value = val;
            }


            /// <summary>
            /// Zapíše globálně jednoznačný identifikátor do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Globálně jednoznačný identifikátor</param>
            public static void SetGuidArg(DataRow row, string name, Guid val)
            {
                row[name] = val;
            }

            /// <summary>
            /// Zapíše globálně jednoznačný identifikátor do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Globálně jednoznačný identifikátor</param>
            public static void SetGuidArg(DataGridViewRow row, string name, Guid val)
            {
                row.Cells[name].Value = val;
            }

            #endregion Set not null value into data row


            #region Set nullable value into data row

            /// <summary>
            /// Zapíše byte se znaménkem včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Byte se znaménkem včetně hodnoty null</param>
            public static void SetNSByteArg(DataRow row, string name, Nullable<sbyte> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše byte se znaménkem včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Byte se znaménkem včetně hodnoty null</param>
            public static void SetNSByteArg(DataGridViewRow row, string name, Nullable<sbyte> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše byte bez znaménka včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Byte bez znaménka včetně hodnoty null</param>
            public static void SetNByteArg(DataRow row, string name, Nullable<byte> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše byte bez znaménka včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Byte bez znaménka včetně hodnoty null</param>
            public static void SetNByteArg(DataGridViewRow row, string name, Nullable<byte> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše krátké celé číslo se znaménkem včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Krátké celé číslo se znaménkem včetně hodnoty null</param>
            public static void SetNShortArg(DataRow row, string name, Nullable<short> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše krátké celé číslo se znaménkem včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Krátké celé číslo se znaménkem včetně hodnoty null</param>
            public static void SetNShortArg(DataGridViewRow row, string name, Nullable<short> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše krátké celé číslo bez znaménka včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Krátké celé číslo bez znaménka včetně hodnoty null</param>
            public static void SetNUShortArg(DataRow row, string name, Nullable<ushort> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše krátké celé číslo bez znaménka včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Krátké celé číslo bez znaménka včetně hodnoty null</param>
            public static void SetNUShortArg(DataGridViewRow row, string name, Nullable<ushort> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše celé číslo se znaménkem včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Celé číslo se znaménkem včetně hodnoty null</param>
            public static void SetNIntArg(DataRow row, string name, Nullable<int> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše celé číslo se znaménkem včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Celé číslo se znaménkem včetně hodnoty null</param>
            public static void SetNIntArg(DataGridViewRow row, string name, Nullable<int> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše celé číslo bez znaménka včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Celé číslo bez znaménka včetně hodnoty null</param>
            public static void SetNUIntArg(DataRow row, string name, Nullable<uint> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše celé číslo bez znaménka včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Celé číslo bez znaménka včetně hodnoty null</param>
            public static void SetNUIntArg(DataGridViewRow row, string name, Nullable<uint> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše dlouhé celé číslo se znaménkem včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Dlouhé celé číslo se znaménkem včetně hodnoty null</param>
            public static void SetNLongArg(DataRow row, string name, Nullable<long> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše dlouhé celé číslo se znaménkem včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Dlouhé celé číslo se znaménkem včetně hodnoty null</param>
            public static void SetNLongArg(DataGridViewRow row, string name, Nullable<long> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše dlouhé celé číslo bez znaménka včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Dlouhé celé číslo bez znaménka včetně hodnoty null</param>
            public static void SetNULongArg(DataRow row, string name, Nullable<ulong> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše dlouhé celé číslo bez znaménka včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Dlouhé celé číslo bez znaménka včetně hodnoty null</param>
            public static void SetNULongArg(DataGridViewRow row, string name, Nullable<ulong> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše desetinné číslo (float) včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (float) včetně hodnoty null</param>
            public static void SetNFloatArg(DataRow row, string name, Nullable<float> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše desetinné číslo (float) včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (float) včetně hodnoty null</param>
            public static void SetNFloatArg(DataGridViewRow row, string name, Nullable<float> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše desetinné číslo (double) včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (double) včetně hodnoty null</param>
            public static void SetNDoubleArg(DataRow row, string name, Nullable<double> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše desetinné číslo (double) včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (double) včetně hodnoty null</param>
            public static void SetNDoubleArg(DataGridViewRow row, string name, Nullable<double> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše desetinné číslo (decimal) včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (decimal) včetně hodnoty null</param>
            public static void SetNDecimalArg(DataRow row, string name, Nullable<decimal> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše desetinné číslo (decimal) včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Desetinné číslo (decimal) včetně hodnoty null</param>
            public static void SetNDecimalArg(DataGridViewRow row, string name, Nullable<decimal> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše logickou hodnotu včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Logická hodnota včetně hodnoty null</param>
            public static void SetNBoolArg(DataRow row, string name, Nullable<bool> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše logickou hodnotu včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Logická hodnota včetně hodnoty null</param>
            public static void SetNBoolArg(DataGridViewRow row, string name, Nullable<bool> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše znakovou hodnotu včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Znaková hodnota včetně hodnoty null</param>
            public static void SetNCharArg(DataRow row, string name, Nullable<char> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše znakovou hodnotu včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Znaková hodnota včetně hodnoty null</param>
            public static void SetNCharArg(DataGridViewRow row, string name, Nullable<char> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše datum včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Datum včetně hodnoty null</param>
            public static void SetNDateTimeArg(DataRow row, string name, Nullable<DateTime> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše datum včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Datum včetně hodnoty null</param>
            public static void SetNDateTimeArg(DataGridViewRow row, string name, Nullable<DateTime> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše časový interval včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Časový interval včetně hodnoty null</param>
            public static void SetNTimeSpanArg(DataRow row, string name, Nullable<TimeSpan> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše časový interval včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Časový interval včetně hodnoty null</param>
            public static void SetNTimeSpanArg(DataGridViewRow row, string name, Nullable<TimeSpan> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše globálně jednoznačný identifikátor včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Globálně jednoznačný identifikátor včetně hodnoty null</param>
            public static void SetNGuidArg(DataRow row, string name, Nullable<Guid> val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše globálně jednoznačný identifikátor včetně hodnoty null do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Globálně jednoznačný identifikátor včetně hodnoty null</param>
            public static void SetNGuidArg(DataGridViewRow row, string name, Nullable<Guid> val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }


            /// <summary>
            /// Zapíše textovou hodnotu do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Textová hodnota</param>
            public static void SetStringArg(DataRow row, string name, string val)
            {
                if (val == null)
                {
                    row[name] = DBNull.Value;
                }
                else if (val == String.Empty)
                {
                    row[name] = String.Empty;
                }
                else
                {
                    row[name] = val;
                }
            }

            /// <summary>
            /// Zapíše textovou hodnotu do datového řádku
            /// </summary>
            /// <param name="row">Datový řádek</param>
            /// <param name="name">Jméno sloupce</param>
            /// <param name="val">Textová hodnota</param>
            public static void SetStringArg(DataGridViewRow row, string name, string val)
            {
                if (val == null)
                {
                    row.Cells[name].Value = DBNull.Value;
                }
                else if (val == String.Empty)
                {
                    row.Cells[name].Value = String.Empty;
                }
                else
                {
                    row.Cells[name].Value = val;
                }
            }

            #endregion Set nullable value into data row



            #region Get not null value from json

            /// <summary>
            /// Získá celé číslo (integer)
            /// z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Celé číslo (integer)</returns>
            public static int GetJInteger(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                int defaultValue = 0,
                LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return defaultValue;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return defaultValue;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return defaultValue;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return defaultValue;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Hodnota elementu {name} je null."
                                : $"The value of the {name} element is null.";
                        return defaultValue;

                    case JTokenType.Integer:
                        if (Int32.TryParse(
                            s: obj[name].Value<string>(),
                            result: out int intResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (int)intResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ integer."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type integer.";
                            return defaultValue;
                        }

                    case JTokenType.Float:
                    case JTokenType.String:
                        if (Double.TryParse(
                                s: obj[name].Value<string>(),
                                result: out double doubleResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (int)doubleResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ integer."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type integer.";
                            return defaultValue;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu integer."
                                : $"Element {name} is not the required integer type.";
                        return defaultValue;
                }
            }

            /// <summary>
            /// Získá desetinné číslo (double)
            /// z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Desetinné číslo (double)</returns>
            public static double GetJDouble(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                double defaultValue = 0.0,
                LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return defaultValue;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return defaultValue;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return defaultValue;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return defaultValue;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Hodnota elementu {name} je null."
                                : $"The value of the {name} element is null.";
                        return defaultValue;

                    case JTokenType.Integer:
                        if (Int32.TryParse(
                            s: obj[name].Value<string>(),
                            result: out int intResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (double)intResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ double."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type double.";
                            return defaultValue;
                        }

                    case JTokenType.Float:
                    case JTokenType.String:
                        if (Double.TryParse(
                                s: obj[name].Value<string>(),
                                result: out double doubleResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (double)doubleResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ double."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type double.";
                            return defaultValue;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu double."
                                : $"Element {name} is not the required double type.";
                        return defaultValue;
                }
            }

            /// <summary>
            /// Získá logickou hodnotu
            /// z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Logická hodnota</returns>
            public static bool GetJBool(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                bool defaultValue = false,
                LanguageVersion langVersion = LanguageVersion.National)
            {
                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return defaultValue;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return defaultValue;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return defaultValue;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return defaultValue;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Hodnota elementu {name} je null."
                                : $"The value of the {name} element is null.";
                        return defaultValue;

                    case JTokenType.Boolean:
                    case JTokenType.String:
                        if (Boolean.TryParse(
                            value: obj[name].Value<string>(),
                            result: out bool boolResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (bool)boolResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ boolean."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type boolean.";
                            return defaultValue;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu boolean."
                                : $"Element {name} is not the required double type.";
                        return defaultValue;
                }
            }

            #endregion Get not null value from json


            #region Get nullable value from json

            /// <summary>
            /// Získá celé číslo (integer) nebo null
            /// z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Celé číslo (integer) nebo null</returns>
            public static Nullable<int> GetJNInteger(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                Nullable<int> defaultValue = null,
                LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return defaultValue;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return defaultValue;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return defaultValue;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return defaultValue;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Integer:
                        if (Int32.TryParse(
                            s: obj[name].Value<string>(),
                            result: out int intResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (Nullable<int>)intResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ integer."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type integer.";
                            return defaultValue;
                        }

                    case JTokenType.Float:
                    case JTokenType.String:
                        if (Double.TryParse(
                                s: obj[name].Value<string>(),
                                result: out double doubleResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (Nullable<int>)doubleResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ integer."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type integer.";
                            return defaultValue;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu integer."
                                : $"Element {name} is not the required integer type.";
                        return defaultValue;
                }
            }

            /// <summary>
            /// Získá desetinné číslo (double) nebo null
            /// z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Desetinné číslo (double) nebo null</returns>
            public static Nullable<double> GetJNDouble(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                Nullable<double> defaultValue = null,
                LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return defaultValue;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return defaultValue;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return defaultValue;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return defaultValue;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Integer:
                        if (Int32.TryParse(
                            s: obj[name].Value<string>(),
                            result: out int intResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (Nullable<double>)intResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ double."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type double.";
                            return defaultValue;
                        }

                    case JTokenType.Float:
                    case JTokenType.String:
                        if (Double.TryParse(
                                s: obj[name].Value<string>(),
                                result: out double doubleResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (Nullable<double>)doubleResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ double."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type double.";
                            return defaultValue;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu double."
                                : $"Element {name} is not the required double type.";
                        return defaultValue;
                }
            }

            /// <summary>
            /// Získá logickou hodnotu nebo null
            /// z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Logická hodnota nebo null</returns>
            public static Nullable<bool> GetJNBool(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                Nullable<bool> defaultValue = null,
                LanguageVersion langVersion = LanguageVersion.National)
            {
                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return defaultValue;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return defaultValue;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return defaultValue;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return defaultValue;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Boolean:
                    case JTokenType.String:
                        if (Boolean.TryParse(
                            value: obj[name].Value<string>(),
                            result: out bool boolResult))
                        {
                            valid = true;
                            message = String.Empty;
                            return (Nullable<bool>)boolResult;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ boolean."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type boolean.";
                            return defaultValue;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu boolean."
                                : $"Element {name} is not the required boolean type.";
                        return defaultValue;
                }
            }

            /// <summary>
            /// Získá textovou hodnotu nebo null
            /// z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Textová hodnota nebo null z JSON objektu</returns>
            public static string GetJString(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                string defaultValue = null,
                LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return defaultValue;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return defaultValue;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return defaultValue;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return defaultValue;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Boolean:
                    case JTokenType.Integer:
                    case JTokenType.Float:
                    case JTokenType.String:
                        valid = true;
                        message = String.Empty;
                        return obj[name].Value<string>();

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu string."
                                : $"Element {name} is not the required string type.";
                        return defaultValue;
                }
            }

            /// <summary>
            /// Získá vnořený JSON objekt
            /// z nadřazeného JSON objektu
            /// </summary>
            /// <param name="obj">Nadřazený JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <returns>Vnořený JSON objekt z nadřazeného JSON objektu</returns>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>JSON object</returns>
            public static JObject GetJObject(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                JObject defaultValue = null,
                LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return defaultValue;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return defaultValue;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return defaultValue;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return defaultValue;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Object:
                        JObject result = obj[name].ToObject<JObject>();
                        if (result != null)
                        {
                            valid = true;
                            message = String.Empty;
                            return result;
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ JObject."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type JObject.";
                            return defaultValue;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu JObject."
                                : $"Element {name} is not the required JObject type.";
                        return defaultValue;
                }
            }

            #endregion Get nullable value from json


            #region List of nullable types from json

            /// <summary>
            /// Získá seznam celých čísel se znaménkem
            /// včetně hodnoty null z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Seznam celých čísel se znaménkem včetně hodnoty null z JSON objektu</returns>
            public static List<Nullable<int>> GetJNIntegerList(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                Nullable<int> defaultValue = null,
                LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return null;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return null;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return null;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return null;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Array:
                        List<Nullable<int>> list =
                            [];
                        JArray array = obj[name].ToObject<JArray>();
                        valid = true;

                        if (array != null)
                        {
                            foreach (JToken item in array)
                            {
                                switch (item.Type)
                                {
                                    case JTokenType.Null:
                                        valid = valid && true;
                                        list.Add(item: null);
                                        break;

                                    case JTokenType.Integer:
                                        if (Int32.TryParse(
                                                s: item.Value<string>(),
                                                result: out int intVal))
                                        {
                                            valid = valid && true;
                                            list.Add(item: (int)intVal);
                                        }
                                        else
                                        {
                                            valid = valid && false;
                                            list.Add(item: (int)defaultValue);
                                        }
                                        break;

                                    case JTokenType.Float:
                                    case JTokenType.String:
                                        if (Double.TryParse(
                                                s: item.Value<string>(),
                                                result: out double doubleVal))
                                        {
                                            valid = valid && true;
                                            list.Add(item: (int)doubleVal);
                                        }
                                        else
                                        {
                                            valid = valid && false;
                                            list.Add(item: (int)defaultValue);
                                        }
                                        break;

                                    default:
                                        valid = valid && false;
                                        list.Add(item: (int)defaultValue);
                                        break;
                                }
                            }

                            if (valid)
                            {
                                message = String.Empty;
                                return list;
                            }
                            else
                            {
                                message =
                                    (langVersion == LanguageVersion.National)
                                        ? $"Některé hodnoty v poli JArray elementu {name} nelze převést na datový typ integer a jsou nahrazeny defaultní hodnotou."
                                        : $"Some values in the JArray array of the {name} element cannot be converted to the integer data type and are replaced by the default value.";
                                return list;
                            }
                        }
                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ JArray."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type JArray.";
                            return null;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu JArray."
                                : $"Element {name} is not the required JArray type.";
                        return null;
                }
            }

            /// <summary>
            /// Získá seznam desetinných čísel (double)
            /// včetně hodnoty null z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Seznam desetinných čísel (double) včetně hodnoty null z JSON objektu</returns>
            public static List<Nullable<double>> GetJNDoubleList(
                JObject obj,
                string name,
                out bool valid,
                out string message,
                Nullable<double> defaultValue = null,
                LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return null;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return null;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return null;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return null;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Array:
                        List<Nullable<double>> list =
                            [];
                        JArray array = obj[name].ToObject<JArray>();
                        valid = true;

                        if (array != null)
                        {
                            foreach (JToken item in array)
                            {
                                switch (item.Type)
                                {
                                    case JTokenType.Null:
                                        valid = valid && true;
                                        list.Add(item: null);
                                        break;

                                    case JTokenType.Integer:
                                        if (Int32.TryParse(
                                                s: item.Value<string>(),
                                                result: out int intVal))
                                        {
                                            valid = valid && true;
                                            list.Add(item: (double)intVal);
                                        }
                                        else
                                        {
                                            valid = valid && false;
                                            list.Add(item: (double)defaultValue);
                                        }
                                        break;

                                    case JTokenType.Float:
                                    case JTokenType.String:
                                        if (Double.TryParse(
                                                s: item.Value<string>(),
                                                result: out double doubleVal))
                                        {
                                            valid = valid && true;
                                            list.Add(item: (double)doubleVal);
                                        }
                                        else
                                        {
                                            valid = valid && false;
                                            list.Add(item: (double)defaultValue);
                                        }
                                        break;

                                    default:
                                        valid = valid && false;
                                        list.Add(item: (double)defaultValue);
                                        break;
                                }
                            }

                            if (valid)
                            {
                                message = String.Empty;
                                return list;
                            }
                            else
                            {
                                message =
                                    (langVersion == LanguageVersion.National)
                                        ? $"Některé hodnoty v poli JArray elementu {name} nelze převést na datový typ double a jsou nahrazeny defaultní hodnotou."
                                        : $"Some values in the JArray array of the {name} element cannot be converted to the double data type and are replaced by the default value.";
                                return list;
                            }
                        }

                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ JArray."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type JArray.";
                            return null;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu JArray."
                                : $"Element {name} is not the required JArray type.";
                        return null;
                }
            }

            /// <summary>
            /// Získá seznam logických hodnot
            /// včetně hodnoty null z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Seznam logických hodnot včetně hodnoty null z JSON objektu</returns>
            public static List<Nullable<bool>> GetJNBoolList(
                 JObject obj,
                 string name,
                 out bool valid,
                 out string message,
                 Nullable<bool> defaultValue = null,
                 LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return null;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return null;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return null;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return null;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Array:
                        List<Nullable<bool>> list =
                            [];
                        JArray array = obj[name].ToObject<JArray>();
                        valid = true;

                        if (array != null)
                        {
                            foreach (JToken item in array)
                            {
                                switch (item.Type)
                                {
                                    case JTokenType.Null:
                                        valid = valid && true;
                                        list.Add(item: null);
                                        break;

                                    case JTokenType.Boolean:
                                    case JTokenType.String:
                                        if (Boolean.TryParse(
                                                value: item.Value<string>(),
                                                result: out bool boolVal))
                                        {
                                            valid = valid && true;
                                            list.Add(item: (bool)boolVal);
                                        }
                                        else
                                        {
                                            valid = valid && false;
                                            list.Add(item: (bool)defaultValue);
                                        }
                                        break;

                                    default:
                                        valid = valid && false;
                                        list.Add(item: (bool)defaultValue);
                                        break;
                                }
                            }

                            if (valid)
                            {
                                message = String.Empty;
                                return list;
                            }
                            else
                            {
                                message =
                                    (langVersion == LanguageVersion.National)
                                        ? $"Některé hodnoty v poli JArray elementu {name} nelze převést na datový typ boolean a jsou nahrazeny defaultní hodnotou."
                                        : $"Some values in the JArray array of the {name} element cannot be converted to the boolean data type and are replaced by the default value.";
                                return list;
                            }
                        }

                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ JArray."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type JArray.";
                            return null;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu JArray."
                                : $"Element {name} is not the required JArray type.";
                        return null;
                }
            }

            /// <summary>
            /// Získá seznam textových hodnot
            /// z JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Seznam textových hodnot z JSON objektu</returns>
            public static List<string> GetJStringArray(
                 JObject obj,
                 string name,
                 out bool valid,
                 out string message,
                 string defaultValue = null,
                 LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return null;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return null;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return null;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return null;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Array:
                        List<string> list =
                            [];
                        JArray array = obj[name].ToObject<JArray>();
                        valid = true;

                        if (array != null)
                        {
                            foreach (JToken item in array)
                            {
                                switch (item.Type)
                                {
                                    case JTokenType.Null:
                                        valid = valid && true;
                                        list.Add(item: null);
                                        break;

                                    case JTokenType.Boolean:
                                    case JTokenType.Integer:
                                    case JTokenType.Float:
                                    case JTokenType.String:
                                        valid = valid && true;
                                        list.Add(item: obj[name].Value<string>());
                                        break;

                                    default:
                                        valid = valid && false;
                                        list.Add(item: defaultValue);
                                        break;
                                }
                            }

                            if (valid)
                            {
                                message = String.Empty;
                                return list;
                            }
                            else
                            {
                                message =
                                    (langVersion == LanguageVersion.National)
                                        ? $"Některé hodnoty v poli JArray elementu {name} nelze převést na datový typ string a jsou nahrazeny defaultní hodnotou."
                                        : $"Some values in the JArray array of the {name} element cannot be converted to the string data type and are replaced by the default value.";
                                return list;
                            }
                        }

                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ JArray."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type JArray.";
                            return null;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu JArray."
                                : $"Element {name} is not the required JArray type.";
                        return null;
                }
            }

            /// <summary>
            /// Získá seznam vnořených JSON objektů
            /// z pole nadřazeného JSON objektu
            /// </summary>
            /// <param name="obj">JSON objekt</param>
            /// <param name="name">Jméno elementu</param>
            /// <param name="valid">Indikátor, true převod se zdařil, false převod skončil chybou</param>
            /// <param name="message">Popis chyby</param>
            /// <param name="defaultValue">Defaultní hodnota vrácená v případě, že se převod nezdaří</param>
            /// <param name="langVersion">Jazyková verze, chybové zprávy</param>
            /// <returns>Seznam vnořených JSON objektů z pole nadřazeného JSON objektu</returns>
            public static List<JObject> GetJObjectList(
                 JObject obj,
                 string name,
                 out bool valid,
                 out string message,
                 JObject defaultValue = null,
                 LanguageVersion langVersion = LanguageVersion.National)
            {

                if (obj == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Vstupní JSON objekt je null."
                            : "The input JSON object is null.";
                    return null;
                }

                if (String.IsNullOrEmpty(value: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? "Jméno požadovaného elementu není zadané nebo je prázdné."
                            : "The name of the requested element is not specified or is empty.";
                    return null;
                }

                if (!obj.ContainsKey(propertyName: name))
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} v JSON objektu neexistuje."
                            : $"The specified name element {name} does not exist in the JSON object.";
                    return null;
                }

                if (obj[name] == null)
                {
                    valid = false;
                    message =
                        (langVersion == LanguageVersion.National)
                            ? $"Element zadaného jména {name} je null."
                            : $"The specified name element {name} is null.";
                    return null;
                }

                switch (obj[name].Type)
                {
                    case JTokenType.Null:
                        valid = true;
                        message = String.Empty;
                        return null;

                    case JTokenType.Array:
                        List<JObject> list =
                            [];
                        JArray array = obj[name].ToObject<JArray>();
                        valid = true;

                        if (array != null)
                        {
                            foreach (JToken item in array)
                            {
                                switch (item.Type)
                                {
                                    case JTokenType.Null:
                                        valid = valid && true;
                                        list.Add(item: null);
                                        break;

                                    case JTokenType.Object:
                                        valid = valid && true;
                                        list.Add(item: obj[name].Value<JObject>());
                                        break;

                                    default:
                                        valid = valid && false;
                                        list.Add(item: defaultValue);
                                        break;
                                }
                            }

                            if (valid)
                            {
                                message = String.Empty;
                                return list;
                            }
                            else
                            {
                                message =
                                    (langVersion == LanguageVersion.National)
                                        ? $"Některé hodnoty v poli JArray elementu {name} nelze převést na datový typ JObject a jsou nahrazeny defaultní hodnotou."
                                        : $"Some values in the JArray array of the {name} element cannot be converted to the JObject data type and are replaced by the default value.";
                                return list;
                            }
                        }

                        else
                        {
                            valid = false;
                            message =
                                (langVersion == LanguageVersion.National)
                                    ? $"Hodnotu {obj[name].Value<string>()} elementu {name} nelze převést na datový typ JArray."
                                    : $"The value {obj[name].Value<string>()} of the {name} element cannot be converted to a data type JArray.";
                            return null;
                        }

                    default:
                        valid = false;
                        message =
                            (langVersion == LanguageVersion.National)
                                ? $"Element {name} není požadovaného typu JArray."
                                : $"Element {name} is not the required JArray type.";
                        return null;
                }
            }

            #endregion List of nullable types from json


            #region Prepare not null argument for insert or update

            /// <summary>
            /// Připravuje argument typu byte se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Číslo byte se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepSByteArg(sbyte arg, string dbType = null)
            {
                return String.IsNullOrEmpty(value: dbType) ?
                    arg.ToString() :
                    $"{arg}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu byte bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Číslo byte bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepByteArg(byte arg, string dbType = null)
            {
                return String.IsNullOrEmpty(value: dbType) ?
                    arg.ToString() :
                    $"{arg}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu krátké celé číslo se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Krátké celé číslo se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepShortArg(short arg, string dbType = null)
            {
                return String.IsNullOrEmpty(value: dbType) ?
                    arg.ToString() :
                    $"{arg}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu krátké celé číslo bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Krátké celé číslo bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepUShortArg(ushort arg, string dbType = null)
            {
                return String.IsNullOrEmpty(value: dbType) ?
                   arg.ToString() :
                   $"{arg}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu celé číslo se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Celé číslo se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepIntArg(int arg, string dbType = null)
            {
                return String.IsNullOrEmpty(value: dbType) ?
                     arg.ToString() :
                     $"{arg}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu celé číslo bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Celé číslo bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepUIntArg(uint arg, string dbType = null)
            {
                return String.IsNullOrEmpty(value: dbType) ?
                    arg.ToString() :
                    $"{arg}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu dlouhé celé číslo se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Dlouhé celé číslo se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepLongArg(long arg, string dbType = null)
            {
                return String.IsNullOrEmpty(value: dbType) ?
                    arg.ToString() :
                    $"{arg}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu dlouhé celé číslo bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Dlouhé celé číslo bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepULongArg(ulong arg, string dbType = null)
            {
                return String.IsNullOrEmpty(value: dbType) ?
                    arg.ToString() :
                    $"{arg}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu desetinné číslo (float) pro příkaz INSERT nebo UPDATE
            /// (desetinná čárka je nahrazena tečkou)
            /// </summary>
            /// <param name="arg">Desetinné číslo (float)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepFloatArg(float arg, string dbType = null)
            {
                string str = arg.ToString()
                    .Replace(
                        oldChar: (char)44,
                        newChar: (char)46);
                return String.IsNullOrEmpty(value: dbType) ?
                    str :
                    $"{str}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu desetinné číslo (double) pro příkaz INSERT nebo UPDATE
            /// (desetinná čárka je nahrazena tečkou)
            /// </summary>
            /// <param name="arg">Desetinné číslo (double)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDoubleArg(double arg, string dbType = null)
            {
                string str = arg.ToString()
                    .Replace(
                        oldChar: (char)44,
                        newChar: (char)46);
                return String.IsNullOrEmpty(value: dbType) ?
                    str :
                    $"{str}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu desetinné číslo (decimal) pro příkaz INSERT nebo UPDATE
            /// (desetinná čárka je nahrazena tečkou)
            /// </summary>
            /// <param name="arg">Desetinné číslo (decimal)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDecimalArg(decimal arg, string dbType = null)
            {
                string str = arg.ToString()
                    .Replace(
                        oldChar: (char)44,
                        newChar: (char)46);
                return String.IsNullOrEmpty(value: dbType) ?
                    str :
                    $"{str}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu logická hodnota pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Logická hodnota</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepBoolArg(bool arg, string dbType = null)
            {
                string str = (arg) ? StrTrue : StrFalse;
                return String.IsNullOrEmpty(value: dbType) ?
                   str :
                   $"{str}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu znaková hodnota pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Znaková hodnota</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepCharArg(char arg, string dbType = null)
            {
                string str;
                if (arg != (char)39)
                {
                    // Znak se vkládá mezi apostrofy
                    str = $"{(char)39}{arg}{(char)39}";
                }
                else
                {
                    // Znak apostrofu se musí zdvojit
                    str = $"{(char)39}{arg}{arg}{(char)39}";
                }
                return String.IsNullOrEmpty(value: dbType) ?
                   str :
                   $"{str}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu datum pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Datum</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDateTimeArg(DateTime arg, string dbType = null)
            {
                string str = arg.ToString();

                // Datum se vkládá mezi apostrofy
                str = $"{(char)39}{str}{(char)39}";

                return String.IsNullOrEmpty(value: dbType) ?
                   str :
                   $"{str}::{dbType}";
            }

            /// <summary>
            /// Připravuje argument typu časový interval pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Časový interval</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepTimeSpanArg(TimeSpan arg, string dbType = null)
            {
                string str = arg.ToString();

                // Časový interval se vkládá mezi apostrofy
                str = $"{(char)39}{str}{(char)39}";

                return String.IsNullOrEmpty(value: dbType) ?
                   str :
                   $"{str}::{dbType}";
            }

            #endregion Prepare not null argument for insert or update


            #region Prepare nullable argument for insert or update

            /// <summary>
            /// Připravuje argument typu byte se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Číslo byte se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNSByteArg(Nullable<sbyte> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepSByteArg(arg: (sbyte)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu byte bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Číslo byte bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNByteArg(Nullable<byte> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepByteArg(arg: (byte)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu krátké celé číslo se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Krátké celé číslo se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNShortArg(Nullable<short> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepShortArg(arg: (short)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu krátké celé číslo bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Krátké celé číslo bez znaménkavčetně hodnoty null </param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNUShortArg(Nullable<ushort> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepUShortArg(arg: (ushort)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu celé číslo se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Celé číslo se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNIntArg(Nullable<int> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepIntArg(arg: (int)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu celé číslo bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Celé číslo bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNUIntArg(Nullable<uint> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepUIntArg(arg: (uint)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu dlouhé celé číslo se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Dlouhé celé číslo se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNLongArg(Nullable<long> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepLongArg(arg: (long)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu dlouhé celé číslo bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Dlouhé celé číslo bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNULongArg(Nullable<ulong> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepULongArg(arg: (ulong)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu desetinné číslo (float) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// (desetinná čárka je nahrazena tečkou)
            /// </summary>
            /// <param name="arg">Desetinné číslo (float) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNFloatArg(Nullable<float> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepFloatArg(arg: (float)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu desetinné číslo (double) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// (desetinná čárka je nahrazena tečkou)
            /// </summary>
            /// <param name="arg">Desetinné číslo (double) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDoubleArg(Nullable<double> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepDoubleArg(arg: (double)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu desetinné číslo (decimal) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// (desetinná čárka je nahrazena tečkou)
            /// </summary>
            /// <param name="arg">Desetinné číslo (decimal) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDecimalArg(Nullable<decimal> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepDecimalArg(arg: (decimal)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu logická hodnota včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Logická hodnota včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNBoolArg(Nullable<bool> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepBoolArg(arg: (bool)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu znaková hodnota včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Znaková hodnota včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNCharArg(Nullable<char> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepCharArg(arg: (char)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu datum včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Datum včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDateTimeArg(Nullable<DateTime> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepDateTimeArg(arg: (DateTime)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu časový interval včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Časový interval včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNTimeSpanArg(Nullable<TimeSpan> arg, string dbType = null)
            {
                string strNull = (dbType == null) ? StrNull : $"{StrNull}::{dbType}";
                return (arg == null) ? strNull : PrepTimeSpanArg(arg: (TimeSpan)arg, dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu textová hodnota včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="arg">Textová hodnota včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <param name="replaceEmptyStringByNull">Je požadována náhrada prázdného řetezce, null hodnotou?</param>
            /// <param name="replaceNullByEmptyString">Je požadována náhrada null hodnoty, prázdným řetězecem?</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepStringArg(
                string arg,
                string dbType = null,
                bool replaceEmptyStringByNull = false,
                bool replaceNullByEmptyString = false)
            {
                // ARGUMENT JE NULL
                if (arg == null)
                {
                    if (!replaceNullByEmptyString)
                    {
                        // Není požadována náhrada null hodnoty, prázdným řetězecem
                        // Vrátí řetězec NULL
                        return
                            (dbType == null) ?
                                StrNull :
                                $"{StrNull}::{dbType}";
                    }
                    else
                    {
                        // Je požadována náhrada null hodnoty, prázdným řetězecem
                        // Vrátí prázdný řetezec
                        return (dbType == null) ?
                            $"{(char)39}{(char)39}" :
                            $"{(char)39}{(char)39}::{dbType}";
                    }
                }

                // ARGUMENT JE PRÁZDNÝ ŘETĚZEC
                if (String.IsNullOrEmpty(value: arg))
                {
                    if (!replaceEmptyStringByNull)
                    {
                        // Není požadována náhrada prázdného řetezce, null hodnotou
                        // Vrátí prázdný řetezec
                        return (dbType == null) ?
                            $"{(char)39}{(char)39}" :
                            $"{(char)39}{(char)39}::{dbType}";
                    }
                    else
                    {
                        // Je požadována náhrada prázdného řetezce, null hodnotou
                        // Vrátí řetězec NULL
                        return
                        (dbType == null) ?
                            StrNull :
                            $"{StrNull}::{dbType}";
                    }
                }

                // Řetězce se uzavírají do apostrofů
                // Vnitřní apostrofy se musí zdvojit
                arg = arg.Replace(
                    oldValue: ((char)39).ToString(),
                    newValue: $"{(char)39}{(char)39}");

                return
                    (dbType == null) ?
                        $"{(char)39}{arg}{(char)39}" :
                        $"{(char)39}{arg}{(char)39}::{dbType}";
            }

            #endregion Prepare nullable argument for insert or update


            #region Prepare array of not null arguments for insert or update

            /// <summary>
            /// Připravuje argument typu pole byte se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce čísel byte se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepSByteArrayArg(ICollection<sbyte> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepSByteArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole byte bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce čísel byte bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepByteArrayArg(ICollection<byte> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepByteArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole krátkých celých čísel se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce krátkých celých čísel se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepShortArrayArg(ICollection<short> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepShortArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole krátkých celých čísel bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce krátkých celých čísel bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepUShortArrayArg(ICollection<ushort> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepUShortArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole celých čísel se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce celých čísel se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepIntArrayArg(ICollection<int> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepIntArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole celých čísel bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce celých čísel bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepUIntArrayArg(ICollection<uint> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepUIntArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole dlouhých celých čísel se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce dlouhých celých čísel se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepLongArrayArg(ICollection<long> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepLongArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole dlouhých celých čísel bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce dlouhých celých čísel bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepULongArrayArg(ICollection<ulong> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepULongArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole desetinných čísel (float) pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce desetinných čísel (float)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepFloatArrayArg(ICollection<float> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepFloatArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole desetinných čísel (double) pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce desetinných čísel (double)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDoubleArrayArg(ICollection<double> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepDoubleArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole desetinných čísel (decimal) pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce desetinných čísel (decimal)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDecimalArrayArg(ICollection<decimal> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepDecimalArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole logických hodnot pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce logických hodnot</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepBoolArrayArg(ICollection<bool> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepBoolArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole znakových hodnot pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce znakových hodnot</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepCharArrayArg(ICollection<char> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepCharArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole datumů pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce datumů</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDateTimeArrayArg(ICollection<DateTime> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepDateTimeArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole časových intervalů pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce časových intervalů</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepTimeSpanArrayArg(ICollection<TimeSpan> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepTimeSpanArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            #endregion Prepare array of not null arguments for insert or update


            #region Prepare array of nullable arguments for insert or update

            /// <summary>
            /// Připravuje argument typu pole byte se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce čísel byte se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNSByteArrayArg(ICollection<Nullable<sbyte>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNSByteArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole byte bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce čísel byte bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNByteArrayArg(ICollection<Nullable<byte>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNByteArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole krátkých celých čísel se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce krátkých celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNShortArrayArg(ICollection<Nullable<short>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNShortArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole krátkých celých čísel bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce krátkých celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNUShortArrayArg(ICollection<Nullable<ushort>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNUShortArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole celých čísel se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNIntArrayArg(ICollection<Nullable<int>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNIntArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole celých čísel bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNUIntArrayArg(ICollection<Nullable<uint>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNUIntArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole dlouhých celých čísel se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce dlouhých celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNLongArrayArg(ICollection<Nullable<long>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNLongArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole dlouhých celých čísel bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce dlouhých celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNULongArrayArg(ICollection<Nullable<ulong>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNULongArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole desetinných čísel (float) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce desetinných čísel (float) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNFloatArrayArg(ICollection<Nullable<float>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNFloatArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole desetinných čísel (double) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce desetinných čísel (double) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDoubleArrayArg(ICollection<Nullable<double>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNDoubleArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole desetinných čísel (decimal) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce desetinných čísel (decimal) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDecimalArrayArg(ICollection<Nullable<decimal>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNDecimalArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole logických hodnot včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce logických hodnot včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNBoolArrayArg(ICollection<Nullable<bool>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNBoolArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole znakových hodnot včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce znakových hodnot včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNCharArrayArg(ICollection<Nullable<char>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNCharArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole datumů včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce datumů včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDateTimeArrayArg(ICollection<Nullable<DateTime>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNDateTimeArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole časových intervalů včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce časových intervalů včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNTimeSpanArrayArg(ICollection<Nullable<TimeSpan>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepNTimeSpanArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            /// <summary>
            /// Připravuje argument typu pole textových hodnot včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce textových hodnot včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepStringArrayArg(ICollection<string> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[]";
                }

                if (args.Count == 0)
                {
                    return $"array[]::{dbType}[]";
                }

                string strArgs = args
                    .Select(a => PrepStringArg(arg: a, dbType: null))
                    .Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{strArgs}]::{dbType}[]";
            }

            #endregion Prepare array of nullable arguments for insert or update


            #region Prepare array of arrays of not null argument for insert or update

            /// <summary>
            /// Připravuje argument typu pole polí byte se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí byte se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepSByteArrayOfArrayArg(List<List<sbyte>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNSByteArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<sbyte>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí byte bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí byte bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepByteArrayOfArrayArg(List<List<byte>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNByteArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<byte>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí krátkých celých čísel se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí krátkých celých čísel se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepShortArrayOfArrayArg(List<List<short>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNShortArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<short>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí krátkých celých čísel bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí krátkých celých čísel bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepUShortArrayOfArrayArg(List<List<ushort>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNUShortArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<ushort>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí celých čísel se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí celých čísel se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepIntArrayOfArrayArg(List<List<int>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNIntArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<int>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí celých čísel bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí celých čísel bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepUIntArrayOfArrayArg(List<List<uint>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNUIntArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<uint>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí dlouhých celých čísel se znaménkem pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí dlouhých celých čísel se znaménkem</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepLongArrayOfArrayArg(List<List<long>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNLongArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<long>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí dlouhých celých čísel bez znaménka pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí dlouhých celých čísel bez znaménka</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepULongArrayOfArrayArg(List<List<ulong>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNULongArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<ulong>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí pole desetinných čísel (float) pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí pole desetinných čísel (float)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepFloatArrayOfArrayArg(List<List<float>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNFloatArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<float>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí pole desetinných čísel (double) pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí pole desetinných čísel (double)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDoubleArrayOfArrayArg(List<List<double>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNDoubleArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<double>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí pole desetinných čísel (decimal) pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí pole desetinných čísel (decimal)</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDecimalArrayOfArrayArg(List<List<decimal>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNDecimalArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<decimal>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí logických hodnot pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí logických hodnot</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepBoolArrayOfArrayArg(List<List<bool>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNBoolArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<bool>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí znakových hodnot pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí znakových hodnot</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepCharArrayOfArrayArg(List<List<char>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNCharArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<char>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí datumů pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí datumů</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepDateTimeArrayOfArrayArg(List<List<DateTime>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNDateTimeArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<DateTime>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            /// <summary>
            /// Připravuje argument typu pole polí časových intervalů pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí časových intervalů</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepTimeSpanArrayOfArrayArg(List<List<TimeSpan>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                return PrepNTimeSpanArrayOfArrayArg(
                    args:
                        args.Select(a => (a == null) ?
                            [] :
                            a.Cast<Nullable<TimeSpan>>().ToList())
                        .ToList(),
                    dbType: dbType);
            }

            #endregion Prepare array of arrays of not null argument for insert or update


            #region Prepare array of arrays of nullable argument for insert or update

            /// <summary>
            /// Připravuje argument typu pole polí byte se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí byte se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNSByteArrayOfArrayArg(List<List<Nullable<sbyte>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();

                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<sbyte>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                        item: PrepNSByteArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí byte bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí byte bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNByteArrayOfArrayArg(List<List<Nullable<byte>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();

                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<byte>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                        item: PrepNByteArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí krátkých celých čísel se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí krátkých celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNShortArrayOfArrayArg(List<List<Nullable<short>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();


                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<short>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                        item: PrepNShortArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí krátkých celých čísel bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí krátkých celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNUShortArrayOfArrayArg(List<List<Nullable<ushort>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();


                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<ushort>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                        item: PrepNUShortArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí celých čísel se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNIntArrayOfArrayArg(List<List<Nullable<int>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();


                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<int>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNIntArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí celých čísel bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNUIntArrayOfArrayArg(List<List<Nullable<uint>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();


                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<uint>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNUIntArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí dlouhých celých čísel se znaménkem včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí dlouhých celých čísel se znaménkem včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNLongArrayOfArrayArg(List<List<Nullable<long>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();


                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<long>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                          item: PrepNLongArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí dlouhých celých čísel bez znaménka včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí dlouhých celých čísel bez znaménka včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNULongArrayOfArrayArg(List<List<Nullable<ulong>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();

                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<ulong>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                          item: PrepNULongArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí pole desetinných čísel (float) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí pole desetinných čísel (float) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNFloatArrayOfArrayArg(List<List<Nullable<float>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();


                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<float>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNFloatArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí pole desetinných čísel (double) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí pole desetinných čísel (double) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDoubleArrayOfArrayArg(List<List<Nullable<double>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();


                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<double>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNDoubleArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí pole desetinných čísel (decimal) včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí pole desetinných čísel (decimal) včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDecimalArrayOfArrayArg(List<List<Nullable<decimal>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();

                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<decimal>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNDecimalArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí logických hodnot včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí logických hodnot včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNBoolArrayOfArrayArg(List<List<Nullable<bool>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();


                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<bool>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNBoolArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí znakových hodnot včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí znakových hodnot včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNCharArrayOfArrayArg(List<List<Nullable<char>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();

                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<char>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNCharArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí datumů včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí datumů včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNDateTimeArrayOfArrayArg(List<List<Nullable<DateTime>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();

                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<DateTime>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNDateTimeArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí časových intervalů včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí časových intervalů včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepNTimeSpanArrayOfArrayArg(List<List<Nullable<TimeSpan>>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();

                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<Nullable<TimeSpan>> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepNTimeSpanArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            /// <summary>
            /// Připravuje argument typu pole polí textových hodnot včetně hodnoty null pro příkaz INSERT nebo UPDATE
            /// </summary>
            /// <param name="args">Kolekce kolekcí znakových hodnot včetně hodnoty null</param>
            /// <param name="dbType">Datový typ</param>
            /// <returns>Text pro vložení do příkazu INSERT nebo UDATE</returns>
            public static string PrepStringArrayOfArrayArg(List<List<string>> args, string dbType)
            {
                if (args == null)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                if (args.Count == 0)
                {
                    return $"{StrNull}::{dbType}[][]";
                }

                // Maximální délka vnitřního pole
                int maxLen = args.Select(a => (a == null) ? 0 : a.Count).Max();

                // PostgreSQL nepodporuje vnitřní pole s různou délkou
                // Všechna vnitřní pole se doplní na maximální délku hodnotami null
                for (int i = 0; i < args.Count; i++)
                {
                    List<string> a = args[i] ?? [];
                    for (int j = a.Count; j < maxLen; j++)
                    {
                        a.Add(null);
                    }
                    args[i] = a;
                }

                List<string> inner = [];
                for (int i = 0; i < args.Count; i++)
                {
                    inner.Add(
                         item: PrepStringArrayArg(args: args[i], dbType: dbType));
                }
                string d = inner.Aggregate((a, b) => $"{a}{(char)44}{b}");

                return $"array[{d}]::{dbType}[][]";
            }

            #endregion Prepare array of arrays of nullable argument for insert or update



            #region Zvyrazneni syntaxe SQL v RichTextBoxu

            /// <summary>
            /// Zvyrazneni syntaxe SQL v RichTextBoxu
            /// </summary>
            /// <param name="box"></param>
            public static void HighlightSQLSyntax(RichTextBox box)
            {
                box.SelectAll();
                box.SelectionColor = Color.Black;
                box.SelectionFont = new Font(box.Font.FontFamily, box.Font.Size, FontStyle.Regular);
                box.Select(0, 0);

                #region SQL prikazy
                string[] keywords = PostgreSQL.PostgreSQLWrapper.SQLKeyWords;

                for (int i = 0; i < keywords.Length; i++)
                    keywords[i] = String.Concat(@"[\W]", keywords[i], @"[\W]");

                foreach (string keyword in keywords)
                {
                    Regex regKeyword = new(keyword);
                    foreach (Match m in regKeyword.Matches(box.Text.ToLower()).Cast<Match>())
                    {
                        box.Select(m.Index + 1, m.Length - 2);
                        box.SelectionColor = Color.Navy;
                    }
                }
                #endregion SQL prikazy

                #region Textove retezce
                Regex regTextStrings = MyRegex();
                foreach (Match m in regTextStrings.Matches(box.Text).Cast<Match>())
                {
                    box.Select(m.Index, m.Length + 1);
                    box.SelectionColor = Color.DarkMagenta;
                }
                #endregion Textove retezce

                #region Komentare
                Regex regComments = MyRegex1();
                foreach (Match m in regComments.Matches(box.Text).Cast<Match>())
                {
                    box.Select(m.Index, m.Length);
                    box.SelectionColor = Color.Green;
                }
                #endregion Komentare

                #region Viceradkove komentare
                Regex regMultiLineComments = MyRegex2();
                foreach (Match m in regMultiLineComments.Matches(box.Text).Cast<Match>())
                {
                    box.Select(m.Index, m.Length);
                    box.SelectionColor = Color.Green;
                }
                #endregion Viceradkove komentare

                box.Select(0, 0);
            }

            #endregion


            #region Skryti CheckBoxu u vybraneho TreeNode v TreeView

            // Reseni nalezeno na internetu:
            // V TreeView ve Windows.Forms neni mozne skryt CheckBox u vybraneho TreeNode
            // Skryti CheckBox u vybraneho TreeNode musi byt provedeno ve vrstve Widnows.Api

            private const int TVIF_STATE = 0x8;
            private const int TVIS_STATEIMAGEMASK = 0xF000;
            private const int TV_FIRST = 0x1100;
            private const int TVM_SETITEM = TV_FIRST + 63;

            [StructLayout(LayoutKind.Sequential, Pack = 8, CharSet = CharSet.Auto)]
            private struct TVITEM
            {
                public int mask;
                public IntPtr hItem;
                public int state;
                public int stateMask;
                [MarshalAs(UnmanagedType.LPTStr)]
                public string lpszText;
                public int cchTextMax;
                public int iImage;
                public int iSelectedImage;
                public int cChildren;
                public IntPtr lParam;
            }

            [DllImport("user32.dll", CharSet = CharSet.Auto)]
            private static extern IntPtr SendMessage(
                IntPtr hWnd, int Msg, IntPtr wParam, ref TVITEM lParam);

            /// <summary>
            /// Skryje CheckBox u vybraneho TreeNode v ovladacim prvku TreeView
            /// </summary>
            /// <param name="treeView">Ovladaci prvek TreeView</param>
            /// <param name="node">Vybrany TreeNode</param>
            public static void HideCheckBox(TreeView treeView, TreeNode node)
            {
                TVITEM tvi = new()
                {
                    hItem = node.Handle,
                    mask = TVIF_STATE,
                    stateMask = TVIS_STATEIMAGEMASK,
                    state = 0
                };
                SendMessage(treeView.Handle, TVM_SETITEM, IntPtr.Zero, ref tvi);
            }

            [GeneratedRegex(@"'[^']*'", RegexOptions.Multiline)]
            private static partial Regex MyRegex();

            [GeneratedRegex(@"--.*")]
            private static partial Regex MyRegex1();

            [GeneratedRegex(@"(/\*)[^/]*(\*/)", RegexOptions.Multiline)]
            private static partial Regex MyRegex2();

            #endregion

        }


        /// <summary>
        /// Test
        /// </summary>
        internal static class FunctionsTest
        {

            /// <summary>
            /// Test
            /// </summary>
            public static void Test()
            {
                try
                {
                    TestInternal();
                    MessageBox.Show(
                       text: "OK",
                       caption: String.Empty,
                       buttons: MessageBoxButtons.OK,
                       icon: MessageBoxIcon.Information);
                }
                catch (Exception e)
                {
                    MessageBox.Show(
                       text: e.Message,
                       caption: String.Empty,
                       buttons: MessageBoxButtons.OK,
                       icon: MessageBoxIcon.Information);
                }
            }

            private static void TestInternal()
            {

                #region Prepare not null argument for insert or update

                if (Functions.PrepSByteArg(arg: 42) != "42") { throw new Exception(message: "PrepSByteArg"); }
                if (Functions.PrepByteArg(arg: 42) != "42") { throw new Exception(message: "PrepByteArg"); }
                if (Functions.PrepShortArg(arg: 42) != "42") { throw new Exception(message: "PrepShortArg"); }
                if (Functions.PrepUShortArg(arg: 42) != "42") { throw new Exception(message: "PrepUShortArg"); }
                if (Functions.PrepIntArg(arg: 42) != "42") { throw new Exception(message: "PrepIntArg"); }
                if (Functions.PrepUIntArg(arg: 42U) != "42") { throw new Exception(message: "PrepUIntArg"); }
                if (Functions.PrepLongArg(arg: 42L) != "42") { throw new Exception(message: "PrepLongArg"); }
                if (Functions.PrepULongArg(arg: 42UL) != "42") { throw new Exception(message: "PrepULongArg"); }
                if (Functions.PrepFloatArg(arg: 42.3F) != "42.3") { throw new Exception(message: "PrepFloatArg"); }
                if (Functions.PrepDoubleArg(arg: 42.3D) != "42.3") { throw new Exception(message: "PrepDoubleArg"); }
                if (Functions.PrepDecimalArg(arg: 42.0M) != "42.0") { throw new Exception(message: "PrepDecimalArg"); }
                if (Functions.PrepBoolArg(arg: false) != Functions.StrFalse) { throw new Exception(message: "PrepBoolArg"); }
                if (Functions.PrepCharArg(arg: (char)42) != "'*'") { throw new Exception(message: "PrepCharArg"); }

                if (Functions.PrepSByteArg(arg: 42, dbType: "int2") != "42::int2") { throw new Exception(message: "PrepSByteArg"); }
                if (Functions.PrepByteArg(arg: 42, dbType: "int2") != "42::int2") { throw new Exception(message: "PrepByteArg"); }
                if (Functions.PrepShortArg(arg: 42, dbType: "int2") != "42::int2") { throw new Exception(message: "PrepShortArg"); }
                if (Functions.PrepUShortArg(arg: 42, dbType: "int2") != "42::int2") { throw new Exception(message: "PrepUShortArg"); }
                if (Functions.PrepIntArg(arg: 42, dbType: "int4") != "42::int4") { throw new Exception(message: "PrepIntArg"); }
                if (Functions.PrepUIntArg(arg: 42U, dbType: "int4") != "42::int4") { throw new Exception(message: "PrepUIntArg"); }
                if (Functions.PrepLongArg(arg: 42L, dbType: "int8") != "42::int8") { throw new Exception(message: "PrepLongArg"); }
                if (Functions.PrepULongArg(arg: 42UL, dbType: "int8") != "42::int8") { throw new Exception(message: "PrepULongArg"); }
                if (Functions.PrepFloatArg(arg: 42.3F, dbType: "float4") != "42.3::float4") { throw new Exception(message: "PrepFloatArg"); }
                if (Functions.PrepDoubleArg(arg: 42.3D, dbType: "float8") != "42.3::float8") { throw new Exception(message: "PrepDoubleArg"); }
                if (Functions.PrepDecimalArg(arg: 42.0M, dbType: "float8") != "42.0::float8") { throw new Exception(message: "PrepDecimalArg"); }
                if (Functions.PrepBoolArg(arg: false, dbType: "bool") != $"{Functions.StrFalse}::bool") { throw new Exception(message: "PrepBoolArg"); }
                if (Functions.PrepCharArg(arg: (char)42, dbType: "text") != "'*'::text") { throw new Exception(message: "PrepCharArg"); }

                #endregion Prepare not null argument for insert or update


                #region Prepare nullable argument for insert or update

                if (Functions.PrepNSByteArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNSByteArg"); }
                if (Functions.PrepNByteArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNByteArg"); }
                if (Functions.PrepNShortArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNShortArg"); }
                if (Functions.PrepNUShortArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNUShortArg"); }
                if (Functions.PrepNIntArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNIntArg"); }
                if (Functions.PrepNUIntArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNUIntArg"); }
                if (Functions.PrepNLongArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNLongArg"); }
                if (Functions.PrepNULongArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNULongArg"); }
                if (Functions.PrepNFloatArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNFloatArg"); }
                if (Functions.PrepNDoubleArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNDoubleArg"); }
                if (Functions.PrepNDecimalArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNDecimalArg"); }
                if (Functions.PrepNBoolArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNBoolArg"); }
                if (Functions.PrepNCharArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepNCharArg"); }
                if (Functions.PrepStringArg(arg: null) != Functions.StrNull) { throw new Exception(message: "PrepStringArg"); }

                if (Functions.PrepNSByteArg(arg: null, dbType: "int2") != $"{Functions.StrNull}::int2") { throw new Exception(message: "PrepNSByteArg"); }
                if (Functions.PrepNByteArg(arg: null, dbType: "int2") != $"{Functions.StrNull}::int2") { throw new Exception(message: "PrepNByteArg"); }
                if (Functions.PrepNShortArg(arg: null, dbType: "int2") != $"{Functions.StrNull}::int2") { throw new Exception(message: "PrepNShortArg"); }
                if (Functions.PrepNUShortArg(arg: null, dbType: "int2") != $"{Functions.StrNull}::int2") { throw new Exception(message: "PrepNUShortArg"); }
                if (Functions.PrepNIntArg(arg: null, dbType: "int4") != $"{Functions.StrNull}::int4") { throw new Exception(message: "PrepNIntArg"); }
                if (Functions.PrepNUIntArg(arg: null, dbType: "int4") != $"{Functions.StrNull}::int4") { throw new Exception(message: "PrepNUIntArg"); }
                if (Functions.PrepNLongArg(arg: null, dbType: "int8") != $"{Functions.StrNull}::int8") { throw new Exception(message: "PrepNLongArg"); }
                if (Functions.PrepNULongArg(arg: null, dbType: "int8") != $"{Functions.StrNull}::int8") { throw new Exception(message: "PrepNULongArg"); }
                if (Functions.PrepNFloatArg(arg: null, dbType: "float4") != $"{Functions.StrNull}::float4") { throw new Exception(message: "PrepNFloatArg"); }
                if (Functions.PrepNDoubleArg(arg: null, dbType: "float8") != $"{Functions.StrNull}::float8") { throw new Exception(message: "PrepNDoubleArg"); }
                if (Functions.PrepNDecimalArg(arg: null, dbType: "float8") != $"{Functions.StrNull}::float8") { throw new Exception(message: "PrepNDecimalArg"); }
                if (Functions.PrepNBoolArg(arg: null, dbType: "bool") != $"{Functions.StrNull}::bool") { throw new Exception(message: "PrepNBoolArg"); }
                if (Functions.PrepNCharArg(arg: null, dbType: "text") != $"{Functions.StrNull}::text") { throw new Exception(message: "PrepNCharArg"); }
                if (Functions.PrepStringArg(arg: null, dbType: "text") != $"{Functions.StrNull}::text") { throw new Exception(message: "PrepStringArg"); }

                if (Functions.PrepNSByteArg(arg: 42) != "42") { throw new Exception(message: "PrepNSByteArg"); }
                if (Functions.PrepNByteArg(arg: 42) != "42") { throw new Exception(message: "PrepNByteArg"); }
                if (Functions.PrepNShortArg(arg: 42) != "42") { throw new Exception(message: "PrepNShortArg"); }
                if (Functions.PrepNUShortArg(arg: 42) != "42") { throw new Exception(message: "PrepNUShortArg"); }
                if (Functions.PrepNIntArg(arg: 42) != "42") { throw new Exception(message: "PrepNIntArg"); }
                if (Functions.PrepNUIntArg(arg: 42U) != "42") { throw new Exception(message: "PrepNUIntArg"); }
                if (Functions.PrepNLongArg(arg: 42L) != "42") { throw new Exception(message: "PrepNLongArg"); }
                if (Functions.PrepNULongArg(arg: 42UL) != "42") { throw new Exception(message: "PrepNULongArg"); }
                if (Functions.PrepNFloatArg(arg: 42.3F) != "42.3") { throw new Exception(message: "PrepNFloatArg"); }
                if (Functions.PrepNDoubleArg(arg: 42.3D) != "42.3") { throw new Exception(message: "PrepNDoubleArg"); }
                if (Functions.PrepNDecimalArg(arg: 42.0M) != "42.0") { throw new Exception(message: "PrepNDecimalArg"); }
                if (Functions.PrepNBoolArg(arg: false) != Functions.StrFalse) { throw new Exception(message: "PrepNBoolArg"); }
                if (Functions.PrepNCharArg(arg: (char)42) != "'*'") { throw new Exception(message: "PrepNCharArg"); }
                if (Functions.PrepStringArg(arg: String.Empty) != "''") { throw new Exception(message: "PrepStringArg"); }
                if (Functions.PrepStringArg(arg: "5° 42' 51.8") != "'5° 42'' 51.8'") { throw new Exception(message: "PrepStringArg"); }

                if (Functions.PrepNSByteArg(arg: 42, dbType: "int2") != "42::int2") { throw new Exception(message: "PrepNSByteArg"); }
                if (Functions.PrepNByteArg(arg: 42, dbType: "int2") != "42::int2") { throw new Exception(message: "PrepNByteArg"); }
                if (Functions.PrepNShortArg(arg: 42, dbType: "int2") != "42::int2") { throw new Exception(message: "PrepNShortArg"); }
                if (Functions.PrepNUShortArg(arg: 42, dbType: "int2") != "42::int2") { throw new Exception(message: "PrepNUShortArg"); }
                if (Functions.PrepNIntArg(arg: 42, dbType: "int4") != "42::int4") { throw new Exception(message: "PrepNIntArg"); }
                if (Functions.PrepNUIntArg(arg: 42U, dbType: "int4") != "42::int4") { throw new Exception(message: "PrepNUIntArg"); }
                if (Functions.PrepNLongArg(arg: 42L, dbType: "int8") != "42::int8") { throw new Exception(message: "PrepNLongArg"); }
                if (Functions.PrepNULongArg(arg: 42UL, dbType: "int8") != "42::int8") { throw new Exception(message: "PrepNULongArg"); }
                if (Functions.PrepNFloatArg(arg: 42.3F, dbType: "float4") != "42.3::float4") { throw new Exception(message: "PrepNFloatArg"); }
                if (Functions.PrepNDoubleArg(arg: 42.3D, dbType: "float8") != "42.3::float8") { throw new Exception(message: "PrepNDoubleArg"); }
                if (Functions.PrepNDecimalArg(arg: 42.0M, dbType: "float8") != "42.0::float8") { throw new Exception(message: "PrepNDecimalArg"); }
                if (Functions.PrepNBoolArg(arg: false, dbType: "bool") != $"{Functions.StrFalse}::bool") { throw new Exception(message: "PrepNBoolArg"); }
                if (Functions.PrepNCharArg(arg: (char)42, dbType: "text") != "'*'::text") { throw new Exception(message: "PrepNCharArg"); }
                if (Functions.PrepStringArg(arg: String.Empty, dbType: "text") != "''::text") { throw new Exception(message: "PrepStringArg"); }
                if (Functions.PrepStringArg(arg: "5° 42' 51.8", dbType: "text") != "'5° 42'' 51.8'::text") { throw new Exception(message: "PrepStringArg"); }

                #endregion Prepare nullable argument for insert or update


                #region Prepare array of not null arguments for insert or update

                if (Functions.PrepSByteArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[]") { throw new Exception(message: "PrepSByteArrayArg"); }
                if (Functions.PrepByteArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[]") { throw new Exception(message: "PrepByteArrayArg"); }
                if (Functions.PrepShortArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[]") { throw new Exception(message: "PrepShortArrayArg"); }
                if (Functions.PrepUShortArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[]") { throw new Exception(message: "PrepUShortArrayArg"); }
                if (Functions.PrepIntArrayArg(args: null, dbType: "int4") != $"{Functions.StrNull}::int4[]") { throw new Exception(message: "PrepIntArrayArg"); }
                if (Functions.PrepUIntArrayArg(args: null, dbType: "int4") != $"{Functions.StrNull}::int4[]") { throw new Exception(message: "PrepUIntArrayArg"); }
                if (Functions.PrepLongArrayArg(args: null, dbType: "int8") != $"{Functions.StrNull}::int8[]") { throw new Exception(message: "PrepLongArrayArg"); }
                if (Functions.PrepULongArrayArg(args: null, dbType: "int8") != $"{Functions.StrNull}::int8[]") { throw new Exception(message: "PrepULongArrayArg"); }
                if (Functions.PrepFloatArrayArg(args: null, dbType: "float4") != $"{Functions.StrNull}::float4[]") { throw new Exception(message: "PrepFloatArrayArg"); }
                if (Functions.PrepDoubleArrayArg(args: null, dbType: "float8") != $"{Functions.StrNull}::float8[]") { throw new Exception(message: "PrepDoubleArrayArg"); }
                if (Functions.PrepDecimalArrayArg(args: null, dbType: "float8") != $"{Functions.StrNull}::float8[]") { throw new Exception(message: "PrepDecimalArrayArg"); }
                if (Functions.PrepBoolArrayArg(args: null, dbType: "bool") != $"{Functions.StrNull}::bool[]") { throw new Exception(message: "PrepBoolArrayArg"); }
                if (Functions.PrepCharArrayArg(args: null, dbType: "text") != $"{Functions.StrNull}::text[]") { throw new Exception(message: "PrepCharArrayArg"); }

                if (Functions.PrepSByteArrayArg(args: [], dbType: "int2") != "array[]::int2[]") { throw new Exception(message: "PrepSByteArrayArg"); }
                if (Functions.PrepByteArrayArg(args: [], dbType: "int2") != "array[]::int2[]") { throw new Exception(message: "PrepByteArrayArg"); }
                if (Functions.PrepShortArrayArg(args: [], dbType: "int2") != "array[]::int2[]") { throw new Exception(message: "PrepShortArrayArg"); }
                if (Functions.PrepUShortArrayArg(args: [], dbType: "int2") != "array[]::int2[]") { throw new Exception(message: "PrepUShortArrayArg"); }
                if (Functions.PrepIntArrayArg(args: [], dbType: "int4") != "array[]::int4[]") { throw new Exception(message: "PrepIntArrayArg"); }
                if (Functions.PrepUIntArrayArg(args: [], dbType: "int4") != "array[]::int4[]") { throw new Exception(message: "PrepUIntArrayArg"); }
                if (Functions.PrepLongArrayArg(args: [], dbType: "int8") != "array[]::int8[]") { throw new Exception(message: "PrepLongArrayArg"); }
                if (Functions.PrepULongArrayArg(args: [], dbType: "int8") != "array[]::int8[]") { throw new Exception(message: "PrepULongArrayArg"); }
                if (Functions.PrepFloatArrayArg(args: [], dbType: "float4") != "array[]::float4[]") { throw new Exception(message: "PrepFloatArrayArg"); }
                if (Functions.PrepDoubleArrayArg(args: [], dbType: "float8") != "array[]::float8[]") { throw new Exception(message: "PrepDoubleArrayArg"); }
                if (Functions.PrepDecimalArrayArg(args: [], dbType: "float8") != "array[]::float8[]") { throw new Exception(message: "PrepDecimalArrayArg"); }
                if (Functions.PrepBoolArrayArg(args: [], dbType: "bool") != "array[]::bool[]") { throw new Exception(message: "PrepBoolArrayArg"); }
                if (Functions.PrepCharArrayArg(args: [], dbType: "text") != "array[]::text[]") { throw new Exception(message: "PrepCharArrayArg"); }

                if (Functions.PrepSByteArrayArg(args: [40, 41, 42], dbType: "int2") != "array[40,41,42]::int2[]") { throw new Exception(message: "PrepSByteArrayArg"); }
                if (Functions.PrepByteArrayArg(args: [40, 41, 42], dbType: "int2") != "array[40,41,42]::int2[]") { throw new Exception(message: "PrepByteArrayArg"); }
                if (Functions.PrepShortArrayArg(args: [40, 41, 42], dbType: "int2") != "array[40,41,42]::int2[]") { throw new Exception(message: "PrepShortArrayArg"); }
                if (Functions.PrepUShortArrayArg(args: [40, 41, 42], dbType: "int2") != "array[40,41,42]::int2[]") { throw new Exception(message: "PrepUShortArrayArg"); }
                if (Functions.PrepIntArrayArg(args: [40, 41, 42], dbType: "int4") != "array[40,41,42]::int4[]") { throw new Exception(message: "PrepIntArrayArg"); }
                if (Functions.PrepUIntArrayArg(args: [40U, 41U, 42U], dbType: "int4") != "array[40,41,42]::int4[]") { throw new Exception(message: "PrepUIntArrayArg"); }
                if (Functions.PrepLongArrayArg(args: [40L, 41L, 42L], dbType: "int8") != "array[40,41,42]::int8[]") { throw new Exception(message: "PrepLongArrayArg"); }
                if (Functions.PrepULongArrayArg(args: [40UL, 41UL, 42UL], dbType: "int8") != "array[40,41,42]::int8[]") { throw new Exception(message: "PrepULongArrayArg"); }
                if (Functions.PrepFloatArrayArg(args: [40.1F, 41.2F, 42.3F], dbType: "float4") != "array[40.1,41.2,42.3]::float4[]") { throw new Exception(message: "PrepFloatArrayArg"); }
                if (Functions.PrepDoubleArrayArg(args: [40.1D, 41.2D, 42.3D], dbType: "float8") != "array[40.1,41.2,42.3]::float8[]") { throw new Exception(message: "PrepDoubleArrayArg"); }
                if (Functions.PrepDecimalArrayArg(args: [40.0M, 41.1M, 42.2M], dbType: "float8") != "array[40.0,41.1,42.2]::float8[]") { throw new Exception(message: "PrepDecimalArrayArg"); }
                if (Functions.PrepBoolArrayArg(args: [true, false], dbType: "bool") != $"array[{Functions.StrTrue},{Functions.StrFalse}]::bool[]") { throw new Exception(message: "PrepBoolArrayArg"); }
                if (Functions.PrepCharArrayArg(args: [(char)40, (char)41, (char)42], dbType: "text") != "array['(',')','*']::text[]") { throw new Exception(message: "PrepCharArrayArg"); }

                #endregion Prepare array of not null arguments for insert or update


                #region Prepare array of nullable arguments for insert or update

                if (Functions.PrepNSByteArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[]") { throw new Exception(message: "PrepNSByteArrayArg"); }
                if (Functions.PrepNByteArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[]") { throw new Exception(message: "PrepNByteArrayArg"); }
                if (Functions.PrepNShortArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[]") { throw new Exception(message: "PrepNShortArrayArg"); }
                if (Functions.PrepNUShortArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[]") { throw new Exception(message: "PrepNUShortArrayArg"); }
                if (Functions.PrepNIntArrayArg(args: null, dbType: "int4") != $"{Functions.StrNull}::int4[]") { throw new Exception(message: "PrepNIntArrayArg"); }
                if (Functions.PrepNUIntArrayArg(args: null, dbType: "int4") != $"{Functions.StrNull}::int4[]") { throw new Exception(message: "PrepNUIntArrayArg"); }
                if (Functions.PrepNLongArrayArg(args: null, dbType: "int8") != $"{Functions.StrNull}::int8[]") { throw new Exception(message: "PrepNLongArrayArg"); }
                if (Functions.PrepNULongArrayArg(args: null, dbType: "int8") != $"{Functions.StrNull}::int8[]") { throw new Exception(message: "PrepNULongArrayArg"); }
                if (Functions.PrepNFloatArrayArg(args: null, dbType: "float4") != $"{Functions.StrNull}::float4[]") { throw new Exception(message: "PrepNFloatArrayArg"); }
                if (Functions.PrepNDoubleArrayArg(args: null, dbType: "float8") != $"{Functions.StrNull}::float8[]") { throw new Exception(message: "PrepNDoubleArrayArg"); }
                if (Functions.PrepNDecimalArrayArg(args: null, dbType: "float8") != $"{Functions.StrNull}::float8[]") { throw new Exception(message: "PrepNDecimalArrayArg"); }
                if (Functions.PrepNBoolArrayArg(args: null, dbType: "bool") != $"{Functions.StrNull}::bool[]") { throw new Exception(message: "PrepNBoolArrayArg"); }
                if (Functions.PrepNCharArrayArg(args: null, dbType: "text") != $"{Functions.StrNull}::text[]") { throw new Exception(message: "PrepNCharArrayArg"); }
                if (Functions.PrepStringArrayArg(args: null, dbType: "text") != $"{Functions.StrNull}::text[]") { throw new Exception(message: "PrepStringArrayArg"); }

                if (Functions.PrepNSByteArrayArg(args: [], dbType: "int2") != "array[]::int2[]") { throw new Exception(message: "PrepNSByteArrayArg"); }
                if (Functions.PrepNByteArrayArg(args: [], dbType: "int2") != "array[]::int2[]") { throw new Exception(message: "PrepNByteArrayArg"); }
                if (Functions.PrepNShortArrayArg(args: [], dbType: "int2") != "array[]::int2[]") { throw new Exception(message: "PrepNShortArrayArg"); }
                if (Functions.PrepNUShortArrayArg(args: [], dbType: "int2") != "array[]::int2[]") { throw new Exception(message: "PrepNUShortArrayArg"); }
                if (Functions.PrepNIntArrayArg(args: [], dbType: "int4") != "array[]::int4[]") { throw new Exception(message: "PrepNIntArrayArg"); }
                if (Functions.PrepNUIntArrayArg(args: [], dbType: "int4") != "array[]::int4[]") { throw new Exception(message: "PrepNUIntArrayArg"); }
                if (Functions.PrepNLongArrayArg(args: [], dbType: "int8") != "array[]::int8[]") { throw new Exception(message: "PrepNLongArrayArg"); }
                if (Functions.PrepNULongArrayArg(args: [], dbType: "int8") != "array[]::int8[]") { throw new Exception(message: "PrepNULongArrayArg"); }
                if (Functions.PrepNFloatArrayArg(args: [], dbType: "float4") != "array[]::float4[]") { throw new Exception(message: "PrepNFloatArrayArg"); }
                if (Functions.PrepNDoubleArrayArg(args: [], dbType: "float8") != "array[]::float8[]") { throw new Exception(message: "PrepNDoubleArrayArg"); }
                if (Functions.PrepNDecimalArrayArg(args: [], dbType: "float8") != "array[]::float8[]") { throw new Exception(message: "PrepNDecimalArrayArg"); }
                if (Functions.PrepNBoolArrayArg(args: [], dbType: "bool") != "array[]::bool[]") { throw new Exception(message: "PrepNBoolArrayArg"); }
                if (Functions.PrepNCharArrayArg(args: [], dbType: "text") != "array[]::text[]") { throw new Exception(message: "PrepNCharArrayArg"); }
                if (Functions.PrepStringArrayArg(args: [], dbType: "text") != "array[]::text[]") { throw new Exception(message: "PrepStringArrayArg"); }

                if (Functions.PrepNSByteArrayArg(args: [40, null, 42], dbType: "int2") != $"array[40,{Functions.StrNull},42]::int2[]") { throw new Exception(message: "PrepNSByteArrayArg"); }
                if (Functions.PrepNByteArrayArg(args: [40, null, 42], dbType: "int2") != $"array[40,{Functions.StrNull},42]::int2[]") { throw new Exception(message: "PrepNByteArrayArg"); }
                if (Functions.PrepNShortArrayArg(args: [40, null, 42], dbType: "int2") != $"array[40,{Functions.StrNull},42]::int2[]") { throw new Exception(message: "PrepNShortArrayArg"); }
                if (Functions.PrepNUShortArrayArg(args: [40, null, 42], dbType: "int2") != $"array[40,{Functions.StrNull},42]::int2[]") { throw new Exception(message: "PrepNUShortArrayArg"); }
                if (Functions.PrepNIntArrayArg(args: [40, null, 42], dbType: "int4") != $"array[40,{Functions.StrNull},42]::int4[]") { throw new Exception(message: "PrepNIntArrayArg"); }
                if (Functions.PrepNUIntArrayArg(args: [40U, null, 42U], dbType: "int4") != $"array[40,{Functions.StrNull},42]::int4[]") { throw new Exception(message: "PrepNUIntArrayArg"); }
                if (Functions.PrepNLongArrayArg(args: [40L, null, 42L], dbType: "int8") != $"array[40,{Functions.StrNull},42]::int8[]") { throw new Exception(message: "PrepNLongArrayArg"); }
                if (Functions.PrepNULongArrayArg(args: [40UL, null, 42UL], dbType: "int8") != $"array[40,{Functions.StrNull},42]::int8[]") { throw new Exception(message: "PrepNULongArrayArg"); }
                if (Functions.PrepNFloatArrayArg(args: [40.1F, null, 42.3F], dbType: "float4") != $"array[40.1,{Functions.StrNull},42.3]::float4[]") { throw new Exception(message: "PrepNFloatArrayArg"); }
                if (Functions.PrepNDoubleArrayArg(args: [40.1D, null, 42.3D], dbType: "float8") != $"array[40.1,{Functions.StrNull},42.3]::float8[]") { throw new Exception(message: "PrepNDoubleArrayArg"); }
                if (Functions.PrepNDecimalArrayArg(args: [40.0M, null, 42.2M], dbType: "float8") != $"array[40.0,{Functions.StrNull},42.2]::float8[]") { throw new Exception(message: "PrepNDecimalArrayArg"); }
                if (Functions.PrepNBoolArrayArg(args: [true, null, false], dbType: "bool") != $"array[{Functions.StrTrue},{Functions.StrNull},{Functions.StrFalse}]::bool[]") { throw new Exception(message: "PrepNBoolArrayArg"); }
                if (Functions.PrepNCharArrayArg(args: [(char)40, null, (char)42], dbType: "text") != $"array['(',{Functions.StrNull},'*']::text[]") { throw new Exception(message: "PrepNCharArrayArg"); }
                if (Functions.PrepStringArrayArg(args: [String.Empty, null, "5° 42' 51.8"], dbType: "text") != $"array['',{Functions.StrNull},'5° 42'' 51.8']::text[]") { throw new Exception(message: "PrepStringArrayArg"); }

                #endregion Prepare array of nullable arguments for insert or update


                #region Prepare array of arrays of not null argument for insert or update

                if (Functions.PrepSByteArrayOfArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepSByteArrayOfArrayArg"); }
                if (Functions.PrepByteArrayOfArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepByteArrayOfArrayArg"); }
                if (Functions.PrepShortArrayOfArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepShortArrayOfArrayArg"); }
                if (Functions.PrepUShortArrayOfArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepUShortArrayOfArrayArg"); }
                if (Functions.PrepIntArrayOfArrayArg(args: null, dbType: "int4") != $"{Functions.StrNull}::int4[][]")
                { throw new Exception(message: "PrepIntArrayOfArrayArg"); }
                if (Functions.PrepUIntArrayOfArrayArg(args: null, dbType: "int4") != $"{Functions.StrNull}::int4[][]")
                { throw new Exception(message: "PrepUIntArrayOfArrayArg"); }
                if (Functions.PrepLongArrayOfArrayArg(args: null, dbType: "int8") != $"{Functions.StrNull}::int8[][]")
                { throw new Exception(message: "PrepLongArrayOfArrayArg"); }
                if (Functions.PrepULongArrayOfArrayArg(args: null, dbType: "int8") != $"{Functions.StrNull}::int8[][]")
                { throw new Exception(message: "PrepULongArrayOfArrayArg"); }
                if (Functions.PrepFloatArrayOfArrayArg(args: null, dbType: "float4") != $"{Functions.StrNull}::float4[][]")
                { throw new Exception(message: "PrepFloatArrayOfArrayArg"); }
                if (Functions.PrepDoubleArrayOfArrayArg(args: null, dbType: "float8") != $"{Functions.StrNull}::float8[][]")
                { throw new Exception(message: "PrepDoubleArrayOfArrayArg"); }
                if (Functions.PrepDecimalArrayOfArrayArg(args: null, dbType: "float8") != $"{Functions.StrNull}::float8[][]")
                { throw new Exception(message: "PrepDecimalArrayOfArrayArg"); }
                if (Functions.PrepBoolArrayOfArrayArg(args: null, dbType: "bool") != $"{Functions.StrNull}::bool[][]")
                { throw new Exception(message: "PrepBoolArrayOfArrayArg"); }
                if (Functions.PrepCharArrayOfArrayArg(args: null, dbType: "text") != $"{Functions.StrNull}::text[][]")
                { throw new Exception(message: "PrepCharArrayOfArrayArg"); }

                if (Functions.PrepSByteArrayOfArrayArg(args: [], dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepSByteArrayOfArrayArg"); }
                if (Functions.PrepByteArrayOfArrayArg(args: [], dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepByteArrayOfArrayArg"); }
                if (Functions.PrepShortArrayOfArrayArg(args: [], dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepShortArrayOfArrayArg"); }
                if (Functions.PrepUShortArrayOfArrayArg(args: [], dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepUShortArrayOfArrayArg"); }
                if (Functions.PrepIntArrayOfArrayArg(args: [], dbType: "int4") != $"{Functions.StrNull}::int4[][]")
                { throw new Exception(message: "PrepIntArrayOfArrayArg"); }
                if (Functions.PrepUIntArrayOfArrayArg(args: [], dbType: "int4") != $"{Functions.StrNull}::int4[][]")
                { throw new Exception(message: "PrepUIntArrayOfArrayArg"); }
                if (Functions.PrepLongArrayOfArrayArg(args: [], dbType: "int8") != $"{Functions.StrNull}::int8[][]")
                { throw new Exception(message: "PrepLongArrayOfArrayArg"); }
                if (Functions.PrepULongArrayOfArrayArg(args: [], dbType: "int8") != $"{Functions.StrNull}::int8[][]")
                { throw new Exception(message: "PrepULongArrayOfArrayArg"); }
                if (Functions.PrepFloatArrayOfArrayArg(args: [], dbType: "float4") != $"{Functions.StrNull}::float4[][]")
                { throw new Exception(message: "PrepFloatArrayOfArrayArg"); }
                if (Functions.PrepDoubleArrayOfArrayArg(args: [], dbType: "float8") != $"{Functions.StrNull}::float8[][]")
                { throw new Exception(message: "PrepDoubleArrayOfArrayArg"); }
                if (Functions.PrepDecimalArrayOfArrayArg(args: [], dbType: "float8") != $"{Functions.StrNull}::float8[][]")
                { throw new Exception(message: "PrepDecimalArrayOfArrayArg"); }
                if (Functions.PrepBoolArrayOfArrayArg(args: [], dbType: "bool") != $"{Functions.StrNull}::bool[][]")
                { throw new Exception(message: "PrepBoolArrayOfArrayArg"); }
                if (Functions.PrepCharArrayOfArrayArg(args: [], dbType: "text") != $"{Functions.StrNull}::text[][]")
                { throw new Exception(message: "PrepCharArrayOfArrayArg"); }

                if (Functions.PrepSByteArrayOfArrayArg(args: [null, null], dbType: "int2") != "array[array[]::int2[],array[]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepSByteArrayOfArrayArg"); }
                if (Functions.PrepByteArrayOfArrayArg(args: [null, null], dbType: "int2") != "array[array[]::int2[],array[]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepByteArrayOfArrayArg"); }
                if (Functions.PrepShortArrayOfArrayArg(args: [null, null], dbType: "int2") != "array[array[]::int2[],array[]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepShortArrayOfArrayArg"); }
                if (Functions.PrepUShortArrayOfArrayArg(args: [null, null], dbType: "int2") != "array[array[]::int2[],array[]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepUShortArrayOfArrayArg"); }
                if (Functions.PrepIntArrayOfArrayArg(args: [null, null], dbType: "int4") != "array[array[]::int4[],array[]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepIntArrayOfArrayArg"); }
                if (Functions.PrepUIntArrayOfArrayArg(args: [null, null], dbType: "int4") != "array[array[]::int4[],array[]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepUIntArrayOfArrayArg"); }
                if (Functions.PrepLongArrayOfArrayArg(args: [null, null], dbType: "int8") != "array[array[]::int8[],array[]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepLongArrayOfArrayArg"); }
                if (Functions.PrepULongArrayOfArrayArg(args: [null, null], dbType: "int8") != "array[array[]::int8[],array[]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepULongArrayOfArrayArg"); }
                if (Functions.PrepFloatArrayOfArrayArg(args: [null, null], dbType: "float4") != "array[array[]::float4[],array[]::float4[]]::float4[][]")
                { throw new Exception(message: "PrepFloatArrayOfArrayArg"); }
                if (Functions.PrepDoubleArrayOfArrayArg(args: [null, null], dbType: "float8") != "array[array[]::float8[],array[]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepDoubleArrayOfArrayArg"); }
                if (Functions.PrepDecimalArrayOfArrayArg(args: [null, null], dbType: "float8") != "array[array[]::float8[],array[]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepDecimalArrayOfArrayArg"); }
                if (Functions.PrepBoolArrayOfArrayArg(args: [null, null], dbType: "bool") != "array[array[]::bool[],array[]::bool[]]::bool[][]")
                { throw new Exception(message: "PrepBoolArrayOfArrayArg"); }
                if (Functions.PrepCharArrayOfArrayArg(args: [null, null], dbType: "text") != "array[array[]::text[],array[]::text[]]::text[][]")
                { throw new Exception(message: "PrepCharArrayOfArrayArg"); }

                if (Functions.PrepSByteArrayOfArrayArg(
                    args: [null, [43, 44, 45]],
                    dbType: "int2")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int2[],array[43,44,45]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepSByteArrayOfArrayArg"); }
                if (Functions.PrepByteArrayOfArrayArg(
                    args: [null, [43, 44, 45]],
                    dbType: "int2")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int2[],array[43,44,45]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepByteArrayOfArrayArg"); }
                if (Functions.PrepShortArrayOfArrayArg(
                    args: [null, [43, 44, 45]],
                    dbType: "int2")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int2[],array[43,44,45]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepShortArrayOfArrayArg"); }
                if (Functions.PrepUShortArrayOfArrayArg(
                    args: [null, [43, 44, 45]],
                    dbType: "int2")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int2[],array[43,44,45]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepUShortArrayOfArrayArg"); }
                if (Functions.PrepIntArrayOfArrayArg(
                    args: [null, [43, 44, 45]],
                    dbType: "int4")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int4[],array[43,44,45]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepIntArrayOfArrayArg"); }
                if (Functions.PrepUIntArrayOfArrayArg(
                    args: [null, [43U, 44U, 45U]],
                    dbType: "int4")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int4[],array[43,44,45]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepUIntArrayOfArrayArg"); }
                if (Functions.PrepLongArrayOfArrayArg(
                    args: [null, [43L, 44L, 45L]],
                    dbType: "int8")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int8[],array[43,44,45]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepLongArrayOfArrayArg"); }
                if (Functions.PrepULongArrayOfArrayArg(
                    args: [null, [43UL, 44UL, 45UL]],
                    dbType: "int8")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int8[],array[43,44,45]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepULongArrayOfArrayArg"); }
                if (Functions.PrepFloatArrayOfArrayArg(
                    args: [null, [43.3F, 44.4F, 45.5F]],
                    dbType: "float4")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::float4[],array[43.3,44.4,45.5]::float4[]]::float4[][]")
                { throw new Exception(message: "PrepFloatArrayOfArrayArg"); }
                if (Functions.PrepDoubleArrayOfArrayArg(
                    args: [null, [43.3D, 44.4D, 45.5D]],
                    dbType: "float8")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::float8[],array[43.3,44.4,45.5]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepNDoubleArrayOfArrayArg"); }
                if (Functions.PrepDecimalArrayOfArrayArg(
                    args: [null, [43.0M, 44.4M, 45.5M]],
                    dbType: "float8")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::float8[],array[43.0,44.4,45.5]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepDecimalArrayOfArrayArg"); }
                if (Functions.PrepBoolArrayOfArrayArg(
                    args: [null, [true, false, false]],
                    dbType: "bool")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::bool[],array[{Functions.StrTrue},{Functions.StrFalse},{Functions.StrFalse}]::bool[]]::bool[][]")
                { throw new Exception(message: "PrepBoolArrayOfArrayArg"); }
                if (Functions.PrepCharArrayOfArrayArg(
                    args: [null, [(char)100, (char)101, (char)102]],
                    dbType: "text")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::text[],array['d','e','f']::text[]]::text[][]")
                { throw new Exception(message: "PrepCharArrayOfArrayArg"); }


                if (Functions.PrepSByteArrayOfArrayArg(
                    args: [[40, 41, 42], [43, 44]],
                    dbType: "int2")
                    != $"array[array[40,41,42]::int2[],array[43,44,{Functions.StrNull}]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepSByteArrayOfArrayArg"); }
                if (Functions.PrepByteArrayOfArrayArg(
                    args: [[40, 41, 42], [43, 44]],
                    dbType: "int2")
                    != $"array[array[40,41,42]::int2[],array[43,44,{Functions.StrNull}]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepByteArrayOfArrayArg"); }
                if (Functions.PrepShortArrayOfArrayArg(
                    args: [[40, 41, 42], [43, 44]],
                    dbType: "int2")
                    != $"array[array[40,41,42]::int2[],array[43,44,{Functions.StrNull}]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepShortArrayOfArrayArg"); }
                if (Functions.PrepUShortArrayOfArrayArg(
                    args: [[40, 41, 42], [43, 44]],
                    dbType: "int2")
                    != $"array[array[40,41,42]::int2[],array[43,44,{Functions.StrNull}]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepUShortArrayOfArrayArg"); }
                if (Functions.PrepIntArrayOfArrayArg(
                    args: [[40, 41, 42], [43, 44]],
                    dbType: "int4")
                    != $"array[array[40,41,42]::int4[],array[43,44,{Functions.StrNull}]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepIntArrayOfArrayArg"); }
                if (Functions.PrepUIntArrayOfArrayArg(
                    args: [[40U, 41U, 42U], [43U, 44U]],
                    dbType: "int4")
                    != $"array[array[40,41,42]::int4[],array[43,44,{Functions.StrNull}]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepUIntArrayOfArrayArg"); }
                if (Functions.PrepLongArrayOfArrayArg(
                    args: [[40L, 41L, 42L], [43L, 44L]],
                    dbType: "int8")
                    != $"array[array[40,41,42]::int8[],array[43,44,{Functions.StrNull}]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepLongArrayOfArrayArg"); }
                if (Functions.PrepULongArrayOfArrayArg(
                    args: [[40UL, 41UL, 42UL], [43UL, 44UL]],
                    dbType: "int8")
                    != $"array[array[40,41,42]::int8[],array[43,44,{Functions.StrNull}]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepULongArrayOfArrayArg"); }
                if (Functions.PrepFloatArrayOfArrayArg(
                    args: [[40.0F, 41.1F, 42.2F], [43.3F, 44.4F]],
                    dbType: "float4")
                    != $"array[array[40,41.1,42.2]::float4[],array[43.3,44.4,{Functions.StrNull}]::float4[]]::float4[][]")
                { throw new Exception(message: "PrepFloatArrayOfArrayArg"); }
                if (Functions.PrepDoubleArrayOfArrayArg(
                    args: [[40.0D, 41.1D, 42.2D], [43.3D, 44.4D]],
                    dbType: "float8")
                    != $"array[array[40,41.1,42.2]::float8[],array[43.3,44.4,{Functions.StrNull}]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepDoubleArrayOfArrayArg"); }
                if (Functions.PrepDecimalArrayOfArrayArg(
                    args: [[40.0M, 41.1M, 42.2M], [43.3M, 44.4M]],
                    dbType: "float8")
                    != $"array[array[40.0,41.1,42.2]::float8[],array[43.3,44.4,{Functions.StrNull}]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepDecimalArrayOfArrayArg"); }
                if (Functions.PrepBoolArrayOfArrayArg(
                    args: [[true, false, false], [true, false]],
                    dbType: "bool")
                    != $"array[array[{Functions.StrTrue},{Functions.StrFalse},{Functions.StrFalse}]::bool[],array[{Functions.StrTrue},{Functions.StrFalse},{Functions.StrNull}]::bool[]]::bool[][]")
                { throw new Exception(message: "PrepBoolArrayOfArrayArg"); }
                if (Functions.PrepCharArrayOfArrayArg(
                    args: [[(char)97, (char)98, (char)99], [(char)100, (char)101]],
                    dbType: "text")
                    != $"array[array['a','b','c']::text[],array['d','e',{Functions.StrNull}]::text[]]::text[][]")
                { throw new Exception(message: "PrepCharArrayOfArrayArg"); }

                #endregion Prepare array of arrays of not null argument for insert or update


                #region Prepare array of arrays of nullable argument for insert or update

                if (Functions.PrepNSByteArrayOfArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepNSByteArrayOfArrayArg"); }
                if (Functions.PrepNByteArrayOfArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepNByteArrayOfArrayArg"); }
                if (Functions.PrepNShortArrayOfArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepNShortArrayOfArrayArg"); }
                if (Functions.PrepNUShortArrayOfArrayArg(args: null, dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepNUShortArrayOfArrayArg"); }
                if (Functions.PrepNIntArrayOfArrayArg(args: null, dbType: "int4") != $"{Functions.StrNull}::int4[][]")
                { throw new Exception(message: "PrepNIntArrayOfArrayArg"); }
                if (Functions.PrepNUIntArrayOfArrayArg(args: null, dbType: "int4") != $"{Functions.StrNull}::int4[][]")
                { throw new Exception(message: "PrepNUIntArrayOfArrayArg"); }
                if (Functions.PrepNLongArrayOfArrayArg(args: null, dbType: "int8") != $"{Functions.StrNull}::int8[][]")
                { throw new Exception(message: "PrepNLongArrayOfArrayArg"); }
                if (Functions.PrepNULongArrayOfArrayArg(args: null, dbType: "int8") != $"{Functions.StrNull}::int8[][]")
                { throw new Exception(message: "PrepNULongArrayOfArrayArg"); }
                if (Functions.PrepNFloatArrayOfArrayArg(args: null, dbType: "float4") != $"{Functions.StrNull}::float4[][]")
                { throw new Exception(message: "PrepNFloatArrayOfArrayArg"); }
                if (Functions.PrepNDoubleArrayOfArrayArg(args: null, dbType: "float8") != $"{Functions.StrNull}::float8[][]")
                { throw new Exception(message: "PrepNDoubleArrayOfArrayArg"); }
                if (Functions.PrepNDecimalArrayOfArrayArg(args: null, dbType: "float8") != $"{Functions.StrNull}::float8[][]")
                { throw new Exception(message: "PrepNDecimalArrayOfArrayArg"); }
                if (Functions.PrepNBoolArrayOfArrayArg(args: null, dbType: "bool") != $"{Functions.StrNull}::bool[][]")
                { throw new Exception(message: "PrepNBoolArrayOfArrayArg"); }
                if (Functions.PrepNCharArrayOfArrayArg(args: null, dbType: "text") != $"{Functions.StrNull}::text[][]")
                { throw new Exception(message: "PrepNCharArrayOfArrayArg"); }
                if (Functions.PrepStringArrayOfArrayArg(args: null, dbType: "text") != $"{Functions.StrNull}::text[][]")
                { throw new Exception(message: "PrepStringArrayOfArrayArg"); }

                if (Functions.PrepNSByteArrayOfArrayArg(args: [], dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepNSByteArrayOfArrayArg"); }
                if (Functions.PrepNByteArrayOfArrayArg(args: [], dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepNByteArrayOfArrayArg"); }
                if (Functions.PrepNShortArrayOfArrayArg(args: [], dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepNShortArrayOfArrayArg"); }
                if (Functions.PrepNUShortArrayOfArrayArg(args: [], dbType: "int2") != $"{Functions.StrNull}::int2[][]")
                { throw new Exception(message: "PrepNUShortArrayOfArrayArg"); }
                if (Functions.PrepNIntArrayOfArrayArg(args: [], dbType: "int4") != $"{Functions.StrNull}::int4[][]")
                { throw new Exception(message: "PrepNIntArrayOfArrayArg"); }
                if (Functions.PrepNUIntArrayOfArrayArg(args: [], dbType: "int4") != $"{Functions.StrNull}::int4[][]")
                { throw new Exception(message: "PrepNUIntArrayOfArrayArg"); }
                if (Functions.PrepNLongArrayOfArrayArg(args: [], dbType: "int8") != $"{Functions.StrNull}::int8[][]")
                { throw new Exception(message: "PrepNLongArrayOfArrayArg"); }
                if (Functions.PrepNULongArrayOfArrayArg(args: [], dbType: "int8") != $"{Functions.StrNull}::int8[][]")
                { throw new Exception(message: "PrepNULongArrayOfArrayArg"); }
                if (Functions.PrepNFloatArrayOfArrayArg(args: [], dbType: "float4") != $"{Functions.StrNull}::float4[][]")
                { throw new Exception(message: "PrepNFloatArrayOfArrayArg"); }
                if (Functions.PrepNDoubleArrayOfArrayArg(args: [], dbType: "float8") != $"{Functions.StrNull}::float8[][]")
                { throw new Exception(message: "PrepNDoubleArrayOfArrayArg"); }
                if (Functions.PrepNDecimalArrayOfArrayArg(args: [], dbType: "float8") != $"{Functions.StrNull}::float8[][]")
                { throw new Exception(message: "PrepNDecimalArrayOfArrayArg"); }
                if (Functions.PrepNBoolArrayOfArrayArg(args: [], dbType: "bool") != $"{Functions.StrNull}::bool[][]")
                { throw new Exception(message: "PrepNBoolArrayOfArrayArg"); }
                if (Functions.PrepNCharArrayOfArrayArg(args: [], dbType: "text") != $"{Functions.StrNull}::text[][]")
                { throw new Exception(message: "PrepNCharArrayOfArrayArg"); }
                if (Functions.PrepNCharArrayOfArrayArg(args: [], dbType: "text") != $"{Functions.StrNull}::text[][]")
                { throw new Exception(message: "PrepNCharArrayOfArrayArg"); }
                if (Functions.PrepStringArrayOfArrayArg(args: [], dbType: "text") != $"{Functions.StrNull}::text[][]")
                { throw new Exception(message: "PrepStringArrayOfArrayArg"); }

                if (Functions.PrepNSByteArrayOfArrayArg(args: [null, null], dbType: "int2") != "array[array[]::int2[],array[]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNSByteArrayOfArrayArg"); }
                if (Functions.PrepNByteArrayOfArrayArg(args: [null, null], dbType: "int2") != "array[array[]::int2[],array[]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNByteArrayOfArrayArg"); }
                if (Functions.PrepNShortArrayOfArrayArg(args: [null, null], dbType: "int2") != "array[array[]::int2[],array[]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNShortArrayOfArrayArg"); }
                if (Functions.PrepNUShortArrayOfArrayArg(args: [null, null], dbType: "int2") != "array[array[]::int2[],array[]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNUShortArrayOfArrayArg"); }
                if (Functions.PrepNIntArrayOfArrayArg(args: [null, null], dbType: "int4") != "array[array[]::int4[],array[]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepNIntArrayOfArrayArg"); }
                if (Functions.PrepNUIntArrayOfArrayArg(args: [null, null], dbType: "int4") != "array[array[]::int4[],array[]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepNUIntArrayOfArrayArg"); }
                if (Functions.PrepNLongArrayOfArrayArg(args: [null, null], dbType: "int8") != "array[array[]::int8[],array[]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepNLongArrayOfArrayArg"); }
                if (Functions.PrepNULongArrayOfArrayArg(args: [null, null], dbType: "int8") != "array[array[]::int8[],array[]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepNULongArrayOfArrayArg"); }
                if (Functions.PrepNFloatArrayOfArrayArg(args: [null, null], dbType: "float4") != "array[array[]::float4[],array[]::float4[]]::float4[][]")
                { throw new Exception(message: "PrepNFloatArrayOfArrayArg"); }
                if (Functions.PrepNDoubleArrayOfArrayArg(args: [null, null], dbType: "float8") != "array[array[]::float8[],array[]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepNDoubleArrayOfArrayArg"); }
                if (Functions.PrepNDecimalArrayOfArrayArg(args: [null, null], dbType: "float8") != "array[array[]::float8[],array[]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepNDecimalArrayOfArrayArg"); }
                if (Functions.PrepNBoolArrayOfArrayArg(args: [null, null], dbType: "bool") != "array[array[]::bool[],array[]::bool[]]::bool[][]")
                { throw new Exception(message: "PrepNBoolArrayOfArrayArg"); }
                if (Functions.PrepNCharArrayOfArrayArg(args: [null, null], dbType: "text") != "array[array[]::text[],array[]::text[]]::text[][]")
                { throw new Exception(message: "PrepNCharArrayOfArrayArg"); }
                if (Functions.PrepStringArrayOfArrayArg(args: [null, null], dbType: "text") != "array[array[]::text[],array[]::text[]]::text[][]")
                { throw new Exception(message: "PrepStringArrayOfArrayArg"); }

                if (Functions.PrepNSByteArrayOfArrayArg(
                    args: [null, [43, null, 45]],
                    dbType: "int2")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int2[],array[43,{Functions.StrNull},45]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNSByteArrayOfArrayArg"); }
                if (Functions.PrepNByteArrayOfArrayArg(
                    args: [null, [43, null, 45]],
                    dbType: "int2")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int2[],array[43,{Functions.StrNull},45]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNByteArrayOfArrayArg"); }
                if (Functions.PrepNShortArrayOfArrayArg(
                    args: [null, [43, null, 45]],
                    dbType: "int2")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int2[],array[43,{Functions.StrNull},45]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNShortArrayOfArrayArg"); }
                if (Functions.PrepNUShortArrayOfArrayArg(
                    args: [null, [43, null, 45]],
                    dbType: "int2")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int2[],array[43,{Functions.StrNull},45]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNUShortArrayOfArrayArg"); }
                if (Functions.PrepNIntArrayOfArrayArg(
                    args: [null, [43, null, 45]],
                    dbType: "int4")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int4[],array[43,{Functions.StrNull},45]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepNIntArrayOfArrayArg"); }
                if (Functions.PrepNUIntArrayOfArrayArg(
                    args: [null, [43U, null, 45U]],
                    dbType: "int4")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int4[],array[43,{Functions.StrNull},45]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepNUIntArrayOfArrayArg"); }
                if (Functions.PrepNLongArrayOfArrayArg(
                    args: [null, [43L, null, 45L]],
                    dbType: "int8")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int8[],array[43,{Functions.StrNull},45]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepNLongArrayOfArrayArg"); }
                if (Functions.PrepNULongArrayOfArrayArg(
                    args: [null, [43UL, null, 45UL]],
                    dbType: "int8")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::int8[],array[43,{Functions.StrNull},45]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepNULongArrayOfArrayArg"); }
                if (Functions.PrepNFloatArrayOfArrayArg(
                    args: [null, [43.3F, null, 45.5F]],
                    dbType: "float4")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::float4[],array[43.3,{Functions.StrNull},45.5]::float4[]]::float4[][]")
                { throw new Exception(message: "PrepNFloatArrayOfArrayArg"); }
                if (Functions.PrepNDoubleArrayOfArrayArg(
                    args: [null, [43.3D, null, 45.5D]],
                    dbType: "float8")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::float8[],array[43.3,{Functions.StrNull},45.5]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepNDoubleArrayOfArrayArg"); }
                if (Functions.PrepNDecimalArrayOfArrayArg(
                    args: [null, [43.0M, null, 45.5M]],
                    dbType: "float8")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::float8[],array[43.0,{Functions.StrNull},45.5]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepNDecimalArrayOfArrayArg"); }
                if (Functions.PrepNBoolArrayOfArrayArg(
                    args: [null, [true, null, false]],
                    dbType: "bool")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::bool[],array[{Functions.StrTrue},{Functions.StrNull},{Functions.StrFalse}]::bool[]]::bool[][]")
                { throw new Exception(message: "PrepNBoolArrayOfArrayArg"); }
                if (Functions.PrepNCharArrayOfArrayArg(
                    args: [null, [(char)100, null, (char)102]],
                    dbType: "text")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::text[],array['d',{Functions.StrNull},'f']::text[]]::text[][]")
                { throw new Exception(message: "PrepNCharArrayOfArrayArg"); }
                if (Functions.PrepStringArrayOfArrayArg(
                    args: [null, ["ddd", null, "fff"]],
                    dbType: "text")
                    != $"array[array[{Functions.StrNull},{Functions.StrNull},{Functions.StrNull}]::text[],array['ddd',{Functions.StrNull},'fff']::text[]]::text[][]")
                { throw new Exception(message: "PrepStringArrayOfArrayArg"); }


                if (Functions.PrepNSByteArrayOfArrayArg(
                    args: [[40, null, 42], [43, 44]],
                    dbType: "int2")
                    != $"array[array[40,{Functions.StrNull},42]::int2[],array[43,44,{Functions.StrNull}]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNSByteArrayOfArrayArg"); }
                if (Functions.PrepNByteArrayOfArrayArg(
                    args: [[40, null, 42], [43, 44]],
                    dbType: "int2")
                    != $"array[array[40,{Functions.StrNull},42]::int2[],array[43,44,{Functions.StrNull}]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNByteArrayOfArrayArg"); }
                if (Functions.PrepNShortArrayOfArrayArg(
                    args: [[40, null, 42], [43, 44]],
                    dbType: "int2")
                    != $"array[array[40,{Functions.StrNull},42]::int2[],array[43,44,{Functions.StrNull}]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNShortArrayOfArrayArg"); }
                if (Functions.PrepNUShortArrayOfArrayArg(
                    args: [[40, null, 42], [43, 44]],
                    dbType: "int2")
                    != $"array[array[40,{Functions.StrNull},42]::int2[],array[43,44,{Functions.StrNull}]::int2[]]::int2[][]")
                { throw new Exception(message: "PrepNUShortArrayOfArrayArg"); }
                if (Functions.PrepNIntArrayOfArrayArg(
                    args: [[40, null, 42], [43, 44]],
                    dbType: "int4")
                    != $"array[array[40,{Functions.StrNull},42]::int4[],array[43,44,{Functions.StrNull}]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepNIntArrayOfArrayArg"); }
                if (Functions.PrepNUIntArrayOfArrayArg(
                    args: [[40U, null, 42U], [43U, 44U]],
                    dbType: "int4")
                    != $"array[array[40,{Functions.StrNull},42]::int4[],array[43,44,{Functions.StrNull}]::int4[]]::int4[][]")
                { throw new Exception(message: "PrepNUIntArrayOfArrayArg"); }
                if (Functions.PrepNLongArrayOfArrayArg(
                    args: [[40L, null, 42L], [43L, 44L]],
                    dbType: "int8")
                    != $"array[array[40,{Functions.StrNull},42]::int8[],array[43,44,{Functions.StrNull}]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepNLongArrayOfArrayArg"); }
                if (Functions.PrepNULongArrayOfArrayArg(
                    args: [[40UL, null, 42UL], [43UL, 44UL]],
                    dbType: "int8")
                    != $"array[array[40,{Functions.StrNull},42]::int8[],array[43,44,{Functions.StrNull}]::int8[]]::int8[][]")
                { throw new Exception(message: "PrepNULongArrayOfArrayArg"); }
                if (Functions.PrepNFloatArrayOfArrayArg(
                    args: [[40.0F, null, 42.2F], [43.3F, 44.4F]],
                    dbType: "float4")
                    != $"array[array[40,{Functions.StrNull},42.2]::float4[],array[43.3,44.4,{Functions.StrNull}]::float4[]]::float4[][]")
                { throw new Exception(message: "PrepNFloatArrayOfArrayArg"); }
                if (Functions.PrepNDoubleArrayOfArrayArg(
                    args: [[40.0D, null, 42.2D], [43.3D, 44.4D]],
                    dbType: "float8")
                    != $"array[array[40,{Functions.StrNull},42.2]::float8[],array[43.3,44.4,{Functions.StrNull}]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepNDoubleArrayOfArrayArg"); }
                if (Functions.PrepNDecimalArrayOfArrayArg(
                    args: [[40.0M, null, 42.2M], [43.3M, 44.4M]],
                    dbType: "float8")
                    != $"array[array[40.0,{Functions.StrNull},42.2]::float8[],array[43.3,44.4,{Functions.StrNull}]::float8[]]::float8[][]")
                { throw new Exception(message: "PrepNDecimalArrayOfArrayArg"); }
                if (Functions.PrepNBoolArrayOfArrayArg(
                    args: [[true, null, false], [true, false]],
                    dbType: "bool")
                    != $"array[array[{Functions.StrTrue},{Functions.StrNull},{Functions.StrFalse}]::bool[],array[{Functions.StrTrue},{Functions.StrFalse},{Functions.StrNull}]::bool[]]::bool[][]")
                { throw new Exception(message: "PrepNBoolArrayOfArrayArg"); }
                if (Functions.PrepNCharArrayOfArrayArg(
                    args: [[(char)97, null, (char)99], [(char)100, (char)101]],
                    dbType: "text")
                    != $"array[array['a',{Functions.StrNull},'c']::text[],array['d','e',{Functions.StrNull}]::text[]]::text[][]")
                { throw new Exception(message: "PrepNCharArrayOfArrayArg"); }
                if (Functions.PrepStringArrayOfArrayArg(
                    args: [["aaa", null, "ccc"], ["ddd", "eee"]],
                    dbType: "text")
                    != $"array[array['aaa',{Functions.StrNull},'ccc']::text[],array['ddd','eee',{Functions.StrNull}]::text[]]::text[][]")
                { throw new Exception(message: "PrepStringArrayOfArrayArg"); }

                #endregion Prepare array of arrays of nullable argument for insert or update

            }

        }

    }
}
