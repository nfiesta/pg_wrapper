﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Interface for lookup table
            /// </summary>
            public interface ILookupTable
            : IDatabaseTable
            {

                #region Properties

                /// <summary>
                /// Altogether category is included - true|false (read-only)
                /// </summary>
                bool Extended { get; }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                void Load(string condition, System.Nullable<int> limit, bool extended);

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="extended">Altogether category is included - true|false</param>
                void ReLoad(string condition, System.Nullable<int> limit, bool extended);


                /// <summary>
                /// Loads list of identifiers from database table
                /// </summary>
                void LoadIdentifiers();

                #endregion Methods

            }


            /// <summary>
            /// Interface for row in database lookup table
            /// </summary>
            public interface ILookupTableEntry
                : IDatabaseTableEntry
            {

                #region Derived Properties

                /// <summary>
                /// Identifier
                /// </summary>
                int Id { get; set; }

                /// <summary>
                /// Label in national language
                /// </summary>
                string LabelCs { get; set; }

                /// <summary>
                /// Description in national language
                /// </summary>
                string DescriptionCs { get; set; }

                /// <summary>
                /// Extended label in national language (read-only)
                /// </summary>
                string ExtendedLabelCs { get; }

                /// <summary>
                /// Label in English
                /// </summary>
                string LabelEn { get; set; }

                /// <summary>
                /// Description in English
                /// </summary>
                string DescriptionEn { get; set; }

                /// <summary>
                /// Extended label in English (read-only)
                /// </summary>
                string ExtendedLabelEn { get; }

                #endregion Derived Properties

            }


            /// <summary>
            /// Generic interface for row in database lookup table
            /// </summary>
            public interface ILookupTableEntry<T>
                : ILookupTableEntry, IDatabaseTableEntry<T>
                 where T : ILookupTable
            {
            }

        }
    }
}