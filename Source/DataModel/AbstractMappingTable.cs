﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Abstract mapping table
            /// </summary>
            public abstract class AMappingTable
            : ADatabaseTable, IMappingTable
            {

                #region Constructors

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AMappingTable(
                    IDatabase database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                         database: database,
                         data: data,
                         condition: condition,
                         limit: limit,
                         loadingTime: loadingTime,
                         storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public AMappingTable(
                    IDatabase database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                         database: database,
                         rows: rows,
                         condition: condition,
                         limit: limit,
                         loadingTime: loadingTime,
                         storedProcedure: storedProcedure)
                { }

                #endregion Constructors


                #region Methods

                /// <summary>
                /// Loads data from database table
                /// (if it was not done before)
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void Load(
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (!Loaded)
                    {
                        ReLoad(
                            condition: condition,
                            limit: limit);
                    }
                }

                /// <summary>
                /// Loads data from database table
                /// </summary>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                public void ReLoad(
                    string condition = null,
                    Nullable<int> limit = null)
                {
                    if (Database.Postgres.Catalog.Schemas[SchemaName] == null)
                    {
                        // Database schema does not exist
                        Data =
                            AMappingTable.EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                        Condition = String.Empty;
                        Limit = null;
                        LoadingTime = null;
                        return;
                    }

                    if (
                        (Database.Postgres.Catalog.Schemas[SchemaName].Tables[TableName] == null) &&
                        (Database.Postgres.Catalog.Schemas[SchemaName].ForeignTables[TableName] == null))
                    {
                        // Database table does not exits
                        Data =
                            AMappingTable.EmptyDataTable(
                                tableName: TableName,
                                columns: Columns);
                        Condition = String.Empty;
                        Limit = null;
                        LoadingTime = null;
                        return;
                    }

                    Condition = condition;
                    Limit = limit;

                    System.Diagnostics.Stopwatch stopWatch = new();

                    stopWatch.Start();
                    Data = Database.Postgres.ExecuteQuery(sqlCommand: SQL);
                    stopWatch.Stop();

                    LoadingTime = 0.001 * stopWatch.ElapsedMilliseconds;
                    Data.TableName = TableName;
                }

                /// <summary>
                /// Loads list of identifiers from database table
                /// </summary>
                public void LoadIdentifiers()
                {
                    if (Database.Postgres.Catalog.Schemas[SchemaName] == null)
                    {
                        // Database schema does not exist
                        Identifiers = [];
                        return;
                    }

                    if (
                        (Database.Postgres.Catalog.Schemas[SchemaName].Tables[TableName] == null) &&
                        (Database.Postgres.Catalog.Schemas[SchemaName].ForeignTables[TableName] == null))
                    {
                        // Database table does not exist
                        Identifiers = [];
                        return;
                    }

                    ColumnMetadata pkey = Columns.Values.Where(a => a.PKey).FirstOrDefault<ColumnMetadata>();

                    if (pkey == null)
                    {
                        // Primary key column does not exist
                        Identifiers = [];
                        return;
                    }

                    if (!new List<string> { "int2", "int4", "oid" }.Contains(item: pkey.DbDataType))
                    {
                        // Primary key must be integer type
                        Identifiers = [];
                        return;
                    }

                    DataTable dt = Database.Postgres.ExecuteQuery(
                        sqlCommand: $"SELECT {pkey.DbName}::int4 AS pkey FROM {SchemaName}.{TableName};");

                    Identifiers =
                        (dt == null)
                        ? []
                        : [..
                            dt.AsEnumerable()
                                .Where(a => a.Field<Nullable<int>>(columnName: "pkey") != null)
                                .Select(a => a.Field<int>(columnName: "pkey"))
                                .OrderBy(a => a)
                          ];
                }

                #endregion Methods

            }


            /// <summary>
            /// Abstract row in mapping table
            /// </summary>
            /// <typeparam name="T">Abstract mapping table</typeparam>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public abstract class AMappingTableEntry<T>(T composite = null, DataRow data = null)
                : ADatabaseTableEntry<T>(composite: composite, data: data),
                IMappingTableEntry<T>
                where T : AMappingTable
            {
            }

        }
    }
}
