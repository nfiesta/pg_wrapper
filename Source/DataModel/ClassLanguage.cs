﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {

        /// <summary>
        /// Language item
        /// </summary>
        /// <remarks>
        /// Constructor of the language item
        /// </remarks>
        /// <param name="id">
        /// Identifier
        /// </param>
        /// <param name="name">
        /// Languge name
        /// </param>
        /// <param name="iso_639_1">
        /// Two-letter codes, one per language for ISO 639 macrolanguage
        /// </param>
        /// <param name="iso_639_2T">
        /// Three-letter codes, for the same languages as 639-1
        /// </param>
        /// <param name="iso_639_2B">
        /// Three-letter codes, mostly the same as 639-2/T,
        /// but with some codes derived from English names rather than native names of languages
        /// </param>
        /// <param name="iso_639_3">
        /// Three-letter codes, the same as 639-2/T for languages,
        /// but with distinct codes for each variety of an ISO 639 macrolanguage
        /// </param>
        public class LanguageItem(
            int id,
            string name,
            string iso_639_1,
            string iso_639_2T,
            string iso_639_2B,
            string iso_639_3)
        {

            #region Private Fields

            /// <summary>
            /// Identifier
            /// </summary>
            private readonly int id = id;

            /// <summary>
            /// Language name
            /// </summary>
            private readonly string name = name;

            /// <summary>
            /// Two-letter codes, one per language for ISO 639 macrolanguage
            /// </summary>
            private readonly string iso_639_1 = iso_639_1;

            /// <summary>
            /// Three-letter codes, for the same languages as 639-1
            /// </summary>
            private readonly string iso_639_2T = iso_639_2T;

            /// <summary>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </summary>
            private readonly string iso_639_2B = iso_639_2B;

            /// <summary>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </summary>
            private readonly string iso_639_3 = iso_639_3;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// Identifier (read-only)
            /// </summary>
            public int Id
            {
                get
                {
                    return id;
                }
            }

            /// <summary>
            /// Language name (read-only)
            /// </summary>
            public string Name
            {
                get
                {
                    return name;
                }
            }

            /// <summary>
            /// Two-letter codes, one per language for ISO 639 macrolanguage (read-only)
            /// </summary>
            public string ISO_639_1
            {
                get
                {
                    return iso_639_1;
                }
            }

            /// <summary>
            /// Three-letter codes, for the same languages as 639-1 (read-only)
            /// </summary>
            public string ISO_639_2T
            {
                get
                {
                    return iso_639_2T;
                }
            }

            /// <summary>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages (read-only)
            /// </summary>
            public string ISO_639_2B
            {
                get
                {
                    return iso_639_2B;
                }
            }

            /// <summary>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage (read-only)
            /// </summary>
            public string ISO_639_3
            {
                get
                {
                    return iso_639_3;
                }
            }

            /// <summary>
            /// Language Name (read-only)
            /// </summary>
            public LanguageName LanguageName
            {
                get
                {
                    return (LanguageName)Id;
                }
            }

            /// <summary>
            /// Language Code (ISO_639_1) (two letters) (read-only)
            /// </summary>
            public Language Language
            {
                get
                {
                    return (Language)Id;
                }
            }

            #endregion Properties


            #region Methods

            /// <summary>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací text popisující objekt</para>
            /// <para lang="en">Returns a string that represents the current object</para>
            /// </returns>
            public override string ToString()
            {
                return $"{Id} - {Name}";
            }

            /// <summary>
            /// <para lang="cs">Určuje zda zadaný objekt je stejný jako aktuální objekt</para>
            /// <para lang="en">Determines whether specified object is equal to the current object</para>
            /// </summary>
            /// <param name="obj">
            /// <para lang="cs">Zadaný objekt</para>
            /// <para lang="en">Speicified object</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">ano/ne</para>
            /// <para lang="en">true/false</para>
            /// </returns>
            public override bool Equals(object obj)
            {
                // If the passed object is null, return False
                if (obj == null)
                {
                    return false;
                }

                // If the passed object is not LanguageItem Type, return False
                if (obj is not LanguageItem)
                {
                    return false;
                }

                return
                    Id == ((LanguageItem)obj).Id;
            }

            /// <summary>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </summary>
            /// <returns>
            /// <para lang="cs">Vrací hash code</para>
            /// <para lang="en">Returns the hash code</para>
            /// </returns>
            public override int GetHashCode()
            {
                return Id.GetHashCode();
            }

            #endregion Methods

        }


        /// <summary>
        /// List of ISO 639 language codes
        /// Source: https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
        /// </summary>
        public static class LanguageList
        {

            /// <summary>
            /// List of ISO 639 languge codes (read-only)
            /// </summary>
            public static List<LanguageItem> Items
            {
                get
                {
                    return
                [
                    new (id: (int)LanguageName.Abkhazian,           name: "Abkhazian",         iso_639_1: "ab", iso_639_2T: "abk", iso_639_2B: "abk", iso_639_3: "abk"),
                    new (id: (int)LanguageName.Afar,                name: "Afar",              iso_639_1: "aa", iso_639_2T: "aar", iso_639_2B: "aar", iso_639_3: "aar"),
                    new (id: (int)LanguageName.Afrikaans,           name: "Afrikaans",         iso_639_1: "af", iso_639_2T: "afr", iso_639_2B: "afr", iso_639_3: "afr"),
                    new (id: (int)LanguageName.Akan,                name: "Akan",              iso_639_1: "ak", iso_639_2T: "aka", iso_639_2B: "aka", iso_639_3: "aka"),
                    new (id: (int)LanguageName.Albanian,            name: "Albanian",          iso_639_1: "sq", iso_639_2T: "sqi", iso_639_2B: "alb", iso_639_3: "sqi"),
                    new (id: (int)LanguageName.Amharic,             name: "Amharic",           iso_639_1: "am", iso_639_2T: "amh", iso_639_2B: "amh", iso_639_3: "amh"),
                    new (id: (int)LanguageName.Arabic,              name: "Arabic",            iso_639_1: "ar", iso_639_2T: "ara", iso_639_2B: "ara", iso_639_3: "ara"),
                    new (id: (int)LanguageName.Aragonese,           name: "Aragonese",         iso_639_1: "an", iso_639_2T: "arg", iso_639_2B: "arg", iso_639_3: "arg"),
                    new (id: (int)LanguageName.Armenian,            name: "Armenian",          iso_639_1: "hy", iso_639_2T: "hye", iso_639_2B: "arm", iso_639_3: "hye"),
                    new (id: (int)LanguageName.Assamese,            name: "Assamese",          iso_639_1: "as", iso_639_2T: "asm", iso_639_2B: "asm", iso_639_3: "asm"),
                    new (id: (int)LanguageName.Avaric,              name: "Avaric",            iso_639_1: "av", iso_639_2T: "ava", iso_639_2B: "ava", iso_639_3: "ava"),
                    new (id: (int)LanguageName.Avestan,             name: "Avestan",           iso_639_1: "ae", iso_639_2T: "ave", iso_639_2B: "ave", iso_639_3: "ave"),
                    new (id: (int)LanguageName.Aymara,              name: "Aymara",            iso_639_1: "ay", iso_639_2T: "aym", iso_639_2B: "aym", iso_639_3: "aym"),
                    new (id: (int)LanguageName.Azerbaijani,         name: "Azerbaijani",       iso_639_1: "az", iso_639_2T: "aze", iso_639_2B: "aze", iso_639_3: "aze"),
                    new (id: (int)LanguageName.Bambara,             name: "Bambara",           iso_639_1: "bm", iso_639_2T: "bam", iso_639_2B: "bam", iso_639_3: "bam"),
                    new (id: (int)LanguageName.Bashkir,             name: "Bashkir",           iso_639_1: "ba", iso_639_2T: "bak", iso_639_2B: "bak", iso_639_3: "bak"),
                    new (id: (int)LanguageName.Basque,              name: "Basque",            iso_639_1: "eu", iso_639_2T: "eus", iso_639_2B: "baq", iso_639_3: "eus"),
                    new (id: (int)LanguageName.Belarusian,          name: "Belarusian",        iso_639_1: "be", iso_639_2T: "bel", iso_639_2B: "bel", iso_639_3: "bel"),
                    new (id: (int)LanguageName.Bengali,             name: "Bengali",           iso_639_1: "bn", iso_639_2T: "ben", iso_639_2B: "ben", iso_639_3: "ben"),
                    new (id: (int)LanguageName.Bislama,             name: "Bislama",           iso_639_1: "bi", iso_639_2T: "bis", iso_639_2B: "bis", iso_639_3: "bis"),
                    new (id: (int)LanguageName.Bosnian,             name: "Bosnian",           iso_639_1: "bs", iso_639_2T: "bos", iso_639_2B: "bos", iso_639_3: "bos"),
                    new (id: (int)LanguageName.Breton,              name: "Breton",            iso_639_1: "br", iso_639_2T: "bre", iso_639_2B: "bre", iso_639_3: "bre"),
                    new (id: (int)LanguageName.Bulgarian,           name: "Bulgarian",         iso_639_1: "bg", iso_639_2T: "bul", iso_639_2B: "bul", iso_639_3: "bul"),
                    new (id: (int)LanguageName.Burmese,             name: "Burmese",           iso_639_1: "my", iso_639_2T: "mya", iso_639_2B: "bur", iso_639_3: "mya"),
                    new (id: (int)LanguageName.Catalan,             name: "Catalan",           iso_639_1: "ca", iso_639_2T: "cat", iso_639_2B: "cat", iso_639_3: "cat"),
                    new (id: (int)LanguageName.Chamorro,            name: "Chamorro",          iso_639_1: "ch", iso_639_2T: "cha", iso_639_2B: "cha", iso_639_3: "cha"),
                    new (id: (int)LanguageName.Chechen,             name: "Chechen",           iso_639_1: "ce", iso_639_2T: "che", iso_639_2B: "che", iso_639_3: "che"),
                    new (id: (int)LanguageName.Chichewa,            name: "Chichewa",          iso_639_1: "ny", iso_639_2T: "nya", iso_639_2B: "nya", iso_639_3: "nya"),
                    new (id: (int)LanguageName.Chinese,             name: "Chinese",           iso_639_1: "zh", iso_639_2T: "zho", iso_639_2B: "chi", iso_639_3: "zho"),
                    new (id: (int)LanguageName.Slavonic,            name: "Church Slavonic",   iso_639_1: "cu", iso_639_2T: "chu", iso_639_2B: "chu", iso_639_3: "chu"),
                    new (id: (int)LanguageName.Chuvash,             name: "Chuvash",           iso_639_1: "cv", iso_639_2T: "chv", iso_639_2B: "chv", iso_639_3: "chv"),
                    new (id: (int)LanguageName.Cornish,             name: "Cornish",           iso_639_1: "kw", iso_639_2T: "cor", iso_639_2B: "cor", iso_639_3: "cor"),
                    new (id: (int)LanguageName.Corsican,            name: "Corsican",          iso_639_1: "co", iso_639_2T: "cos", iso_639_2B: "cos", iso_639_3: "cos"),
                    new (id: (int)LanguageName.Cree,                name: "Cree",              iso_639_1: "cr", iso_639_2T: "cre", iso_639_2B: "cre", iso_639_3: "cre"),
                    new (id: (int)LanguageName.Croatian,            name: "Croatian",          iso_639_1: "hr", iso_639_2T: "hrv", iso_639_2B: "hrv", iso_639_3: "hrv"),
                    new (id: (int)LanguageName.Czech,               name: "Czech",             iso_639_1: "cs", iso_639_2T: "ces", iso_639_2B: "cze", iso_639_3: "ces"),
                    new (id: (int)LanguageName.Danish,              name: "Danish",            iso_639_1: "da", iso_639_2T: "dan", iso_639_2B: "dan", iso_639_3: "dan"),
                    new (id: (int)LanguageName.Divehi,              name: "Divehi",            iso_639_1: "dv", iso_639_2T: "div", iso_639_2B: "div", iso_639_3: "div"),
                    new (id: (int)LanguageName.Dutch,               name: "Dutch",             iso_639_1: "nl", iso_639_2T: "nld", iso_639_2B: "dut", iso_639_3: "nld"),
                    new (id: (int)LanguageName.Dzongkha,            name: "Dzongkha",          iso_639_1: "dz", iso_639_2T: "dzo", iso_639_2B: "dzo", iso_639_3: "dzo"),
                    new (id: (int)LanguageName.English,             name: "English",           iso_639_1: "en", iso_639_2T: "eng", iso_639_2B: "eng", iso_639_3: "eng"),
                    new (id: (int)LanguageName.Esperanto,           name: "Esperanto",         iso_639_1: "eo", iso_639_2T: "epo", iso_639_2B: "epo", iso_639_3: "epo"),
                    new (id: (int)LanguageName.Estonian,            name: "Estonian",          iso_639_1: "et", iso_639_2T: "est", iso_639_2B: "est", iso_639_3: "est"),
                    new (id: (int)LanguageName.Ewe,                 name: "Ewe",               iso_639_1: "ee", iso_639_2T: "ewe", iso_639_2B: "ewe", iso_639_3: "ewe"),
                    new (id: (int)LanguageName.Faroese,             name: "Faroese",           iso_639_1: "fo", iso_639_2T: "fao", iso_639_2B: "fao", iso_639_3: "fao"),
                    new (id: (int)LanguageName.Fijian,              name: "Fijian",            iso_639_1: "fj", iso_639_2T: "fij", iso_639_2B: "fij", iso_639_3: "fij"),
                    new (id: (int)LanguageName.Finnish,             name: "Finnish",           iso_639_1: "fi", iso_639_2T: "fin", iso_639_2B: "fin", iso_639_3: "fin"),
                    new (id: (int)LanguageName.French,              name: "French",            iso_639_1: "fr", iso_639_2T: "fra", iso_639_2B: "fre", iso_639_3: "fra"),
                    new (id: (int)LanguageName.Frisian,             name: "Western Frisian",   iso_639_1: "fy", iso_639_2T: "fry", iso_639_2B: "fry", iso_639_3: "fry"),
                    new (id: (int)LanguageName.Fulah,               name: "Fulah",             iso_639_1: "ff", iso_639_2T: "ful", iso_639_2B: "ful", iso_639_3: "ful"),
                    new (id: (int)LanguageName.Gaelic,              name: "Gaelic",            iso_639_1: "gd", iso_639_2T: "gla", iso_639_2B: "gla", iso_639_3: "gla"),
                    new (id: (int)LanguageName.Galician,            name: "Galician",          iso_639_1: "gl", iso_639_2T: "glg", iso_639_2B: "glg", iso_639_3: "glg"),
                    new (id: (int)LanguageName.Ganda,               name: "Ganda",             iso_639_1: "lg", iso_639_2T: "lug", iso_639_2B: "lug", iso_639_3: "lug"),
                    new (id: (int)LanguageName.Georgian,            name: "Georgian",          iso_639_1: "ka", iso_639_2T: "kat", iso_639_2B: "geo", iso_639_3: "kat"),
                    new (id: (int)LanguageName.German,              name: "German",            iso_639_1: "de", iso_639_2T: "deu", iso_639_2B: "ger", iso_639_3: "deu"),
                    new (id: (int)LanguageName.Greek,               name: "Greek",             iso_639_1: "el", iso_639_2T: "ell", iso_639_2B: "gre", iso_639_3: "ell"),
                    new (id: (int)LanguageName.Kalaallisut,         name: "Kalaallisut",       iso_639_1: "kl", iso_639_2T: "kal", iso_639_2B: "kal", iso_639_3: "kal"),
                    new (id: (int)LanguageName.Guarani,             name: "Guarani",           iso_639_1: "gn", iso_639_2T: "grn", iso_639_2B: "grn", iso_639_3: "grn"),
                    new (id: (int)LanguageName.Gujarati,            name: "Gujarati",          iso_639_1: "gu", iso_639_2T: "guj", iso_639_2B: "guj", iso_639_3: "guj"),
                    new (id: (int)LanguageName.Haitian,             name: "Haitian",           iso_639_1: "ht", iso_639_2T: "hat", iso_639_2B: "hat", iso_639_3: "hat"),
                    new (id: (int)LanguageName.Hausa,               name: "Hausa",             iso_639_1: "ha", iso_639_2T: "hau", iso_639_2B: "hau", iso_639_3: "hau"),
                    new (id: (int)LanguageName.Hebrew,              name: "Hebrew",            iso_639_1: "he", iso_639_2T: "heb", iso_639_2B: "heb", iso_639_3: "heb"),
                    new (id: (int)LanguageName.Herero,              name: "Herero",            iso_639_1: "hz", iso_639_2T: "her", iso_639_2B: "her", iso_639_3: "her"),
                    new (id: (int)LanguageName.Hindi,               name: "Hindi",             iso_639_1: "hi", iso_639_2T: "hin", iso_639_2B: "hin", iso_639_3: "hin"),
                    new (id: (int)LanguageName.HiriMotu,            name: "Hiri Motu",         iso_639_1: "ho", iso_639_2T: "hmo", iso_639_2B: "hmo", iso_639_3: "hmo"),
                    new (id: (int)LanguageName.Hungarian,           name: "Hungarian",         iso_639_1: "hu", iso_639_2T: "hun", iso_639_2B: "hun", iso_639_3: "hun"),
                    new (id: (int)LanguageName.Icelandic,           name: "Icelandic",         iso_639_1: "is", iso_639_2T: "isl", iso_639_2B: "ice", iso_639_3: "isl"),
                    new (id: (int)LanguageName.Ido,                 name: "Ido",               iso_639_1: "io", iso_639_2T: "ido", iso_639_2B: "ido", iso_639_3: "ido"),
                    new (id: (int)LanguageName.Igbo,                name: "Igbo",              iso_639_1: "ig", iso_639_2T: "ibo", iso_639_2B: "ibo", iso_639_3: "ibo"),
                    new (id: (int)LanguageName.Indonesian,          name: "Indonesian",        iso_639_1: "id", iso_639_2T: "ind", iso_639_2B: "ind", iso_639_3: "ind"),
                    new (id: (int)LanguageName.Interlingua,         name: "Interlingua",       iso_639_1: "ia", iso_639_2T: "ina", iso_639_2B: "ina", iso_639_3: "ina"),
                    new (id: (int)LanguageName.Interlingue,         name: "Interlingue",       iso_639_1: "ie", iso_639_2T: "ile", iso_639_2B: "ile", iso_639_3: "ile"),
                    new (id: (int)LanguageName.Inuktitut,           name: "Inuktitut",         iso_639_1: "iu", iso_639_2T: "iku", iso_639_2B: "iku", iso_639_3: "iku"),
                    new (id: (int)LanguageName.Inupiaq,             name: "Inupiaq",           iso_639_1: "ik", iso_639_2T: "ipk", iso_639_2B: "ipk", iso_639_3: "ipk"),
                    new (id: (int)LanguageName.Irish,               name: "Irish",             iso_639_1: "ga", iso_639_2T: "gle", iso_639_2B: "gle", iso_639_3: "gle"),
                    new (id: (int)LanguageName.Italian,             name: "Italian",           iso_639_1: "it", iso_639_2T: "ita", iso_639_2B: "ita", iso_639_3: "ita"),
                    new (id: (int)LanguageName.Japanese,            name: "Japanese",          iso_639_1: "ja", iso_639_2T: "jpn", iso_639_2B: "jpn", iso_639_3: "jpn"),
                    new (id: (int)LanguageName.Javanese,            name: "Javanese",          iso_639_1: "jv", iso_639_2T: "jav", iso_639_2B: "jav", iso_639_3: "jav"),
                    new (id: (int)LanguageName.Kannada,             name: "Kannada",           iso_639_1: "kn", iso_639_2T: "kan", iso_639_2B: "kan", iso_639_3: "kan"),
                    new (id: (int)LanguageName.Kanuri,              name: "Kanuri",            iso_639_1: "kr", iso_639_2T: "kau", iso_639_2B: "kau", iso_639_3: "kau"),
                    new (id: (int)LanguageName.Kashmiri,            name: "Kashmiri",          iso_639_1: "ks", iso_639_2T: "kas", iso_639_2B: "kas", iso_639_3: "kas"),
                    new (id: (int)LanguageName.Kazakh,              name: "Kazakh",            iso_639_1: "kk", iso_639_2T: "kaz", iso_639_2B: "kaz", iso_639_3: "kaz"),
                    new (id: (int)LanguageName.Khmer,               name: "Central Khmer",     iso_639_1: "km", iso_639_2T: "khm", iso_639_2B: "khm", iso_639_3: "khm"),
                    new (id: (int)LanguageName.Kikuyu,              name: "Kikuyu",            iso_639_1: "ki", iso_639_2T: "kik", iso_639_2B: "kik", iso_639_3: "kik"),
                    new (id: (int)LanguageName.Kinyarwanda,         name: "Kinyarwanda",       iso_639_1: "rw", iso_639_2T: "kin", iso_639_2B: "kin", iso_639_3: "kin"),
                    new (id: (int)LanguageName.Kirghiz,             name: "Kirghiz",           iso_639_1: "ky", iso_639_2T: "kir", iso_639_2B: "kir", iso_639_3: "kir"),
                    new (id: (int)LanguageName.Komi,                name: "Komi",              iso_639_1: "kv", iso_639_2T: "kom", iso_639_2B: "kom", iso_639_3: "kom"),
                    new (id: (int)LanguageName.Kongo,               name: "Kongo",             iso_639_1: "kg", iso_639_2T: "kon", iso_639_2B: "kon", iso_639_3: "kon"),
                    new (id: (int)LanguageName.Korean,              name: "Korean",            iso_639_1: "ko", iso_639_2T: "kor", iso_639_2B: "kor", iso_639_3: "kor"),
                    new (id: (int)LanguageName.Kuanyama,            name: "Kuanyama",          iso_639_1: "kj", iso_639_2T: "kua", iso_639_2B: "kua", iso_639_3: "kua"),
                    new (id: (int)LanguageName.Kurdish,             name: "Kurdish",           iso_639_1: "ku", iso_639_2T: "kur", iso_639_2B: "kur", iso_639_3: "kur"),
                    new (id: (int)LanguageName.Lao,                 name: "Lao",               iso_639_1: "lo", iso_639_2T: "lao", iso_639_2B: "lao", iso_639_3: "lao"),
                    new (id: (int)LanguageName.Latin,               name: "Latin",             iso_639_1: "la", iso_639_2T: "lat", iso_639_2B: "lat", iso_639_3: "lat"),
                    new (id: (int)LanguageName.Latvian,             name: "Latvian",           iso_639_1: "lv", iso_639_2T: "lav", iso_639_2B: "lav", iso_639_3: "lav"),
                    new (id: (int)LanguageName.Limburgan,           name: "Limburgan",         iso_639_1: "li", iso_639_2T: "lim", iso_639_2B: "lim", iso_639_3: "lim"),
                    new (id: (int)LanguageName.Lingala,             name: "Lingala",           iso_639_1: "ln", iso_639_2T: "lin", iso_639_2B: "lin", iso_639_3: "lin"),
                    new (id: (int)LanguageName.Lithuanian,          name: "Lithuanian",        iso_639_1: "lt", iso_639_2T: "lit", iso_639_2B: "lit", iso_639_3: "lit"),
                    new (id: (int)LanguageName.LubaKatanga,         name: "Luba-Katanga",      iso_639_1: "lu", iso_639_2T: "lub", iso_639_2B: "lub", iso_639_3: "lub"),
                    new (id: (int)LanguageName.Luxembourgish,       name: "Luxembourgish",     iso_639_1: "lb", iso_639_2T: "ltz", iso_639_2B: "ltz", iso_639_3: "ltz"),
                    new (id: (int)LanguageName.Macedonian,          name: "Macedonian",        iso_639_1: "mk", iso_639_2T: "mkd", iso_639_2B: "mac", iso_639_3: "mkd"),
                    new (id: (int)LanguageName.Malagasy,            name: "Malagasy",          iso_639_1: "mg", iso_639_2T: "mlg", iso_639_2B: "mlg", iso_639_3: "mlg"),
                    new (id: (int)LanguageName.Malay,               name: "Malay",             iso_639_1: "ms", iso_639_2T: "msa", iso_639_2B: "may", iso_639_3: "msa"),
                    new (id: (int)LanguageName.Malayalam,           name: "Malayalam",         iso_639_1: "ml", iso_639_2T: "mal", iso_639_2B: "mal", iso_639_3: "mal"),
                    new (id: (int)LanguageName.Maltese,             name: "Maltese",           iso_639_1: "mt", iso_639_2T: "mlt", iso_639_2B: "mlt", iso_639_3: "mlt"),
                    new (id: (int)LanguageName.Manx,                name: "Manx",              iso_639_1: "gv", iso_639_2T: "glv", iso_639_2B: "glv", iso_639_3: "glv"),
                    new (id: (int)LanguageName.Maori,               name: "Maori",             iso_639_1: "mi", iso_639_2T: "mri", iso_639_2B: "mao", iso_639_3: "mri"),
                    new (id: (int)LanguageName.Marathi,             name: "Marathi",           iso_639_1: "mr", iso_639_2T: "mar", iso_639_2B: "mar", iso_639_3: "mar"),
                    new (id: (int)LanguageName.Marshallese,         name: "Marshallese",       iso_639_1: "mh", iso_639_2T: "mah", iso_639_2B: "mah", iso_639_3: "mah"),
                    new (id: (int)LanguageName.Mongolian,           name: "Mongolian",         iso_639_1: "mn", iso_639_2T: "mon", iso_639_2B: "mon", iso_639_3: "mon"),
                    new (id: (int)LanguageName.Nauru,               name: "Nauru",             iso_639_1: "na", iso_639_2T: "nau", iso_639_2B: "nau", iso_639_3: "nau"),
                    new (id: (int)LanguageName.Navajo,              name: "Navajo",            iso_639_1: "nv", iso_639_2T: "nav", iso_639_2B: "nav", iso_639_3: "nav"),
                    new (id: (int)LanguageName.NorthNdebele,        name: "North Ndebele",     iso_639_1: "nd", iso_639_2T: "nde", iso_639_2B: "nde", iso_639_3: "nde"),
                    new (id: (int)LanguageName.SouthNdebele,        name: "South Ndebele",     iso_639_1: "nr", iso_639_2T: "nbl", iso_639_2B: "nbl", iso_639_3: "nbl"),
                    new (id: (int)LanguageName.Ndonga,              name: "Ndonga",            iso_639_1: "ng", iso_639_2T: "ndo", iso_639_2B: "ndo", iso_639_3: "ndo"),
                    new (id: (int)LanguageName.Nepali,              name: "Nepali",            iso_639_1: "ne", iso_639_2T: "nep", iso_639_2B: "nep", iso_639_3: "nep"),
                    new (id: (int)LanguageName.Norwegian,           name: "Norwegian",         iso_639_1: "no", iso_639_2T: "nor", iso_639_2B: "nor", iso_639_3: "nor"),
                    new (id: (int)LanguageName.NorwegianBokmal,     name: "Norwegian Bokmål",  iso_639_1: "nb", iso_639_2T: "nob", iso_639_2B: "nob", iso_639_3: "nob"),
                    new (id: (int)LanguageName.NorwegianNynorsk,    name: "Norwegian Nynorsk", iso_639_1: "nn", iso_639_2T: "nno", iso_639_2B: "nno", iso_639_3: "nno"),
                    new (id: (int)LanguageName.SichuanYi,           name: "Sichuan Yi",        iso_639_1: "ii", iso_639_2T: "iii", iso_639_2B: "iii", iso_639_3: "iii"),
                    new (id: (int)LanguageName.Occitan,             name: "Occitan",           iso_639_1: "oc", iso_639_2T: "oci", iso_639_2B: "oci", iso_639_3: "oci"),
                    new (id: (int)LanguageName.Ojibwa,              name: "Ojibwa",            iso_639_1: "oj", iso_639_2T: "oji", iso_639_2B: "oji", iso_639_3: "oji"),
                    new (id: (int)LanguageName.Oriya,               name: "Oriya",             iso_639_1: "or", iso_639_2T: "ori", iso_639_2B: "ori", iso_639_3: "ori"),
                    new (id: (int)LanguageName.Oromo,               name: "Oromo",             iso_639_1: "om", iso_639_2T: "orm", iso_639_2B: "orm", iso_639_3: "orm"),
                    new (id: (int)LanguageName.Ossetian,            name: "Ossetian",          iso_639_1: "os", iso_639_2T: "oss", iso_639_2B: "oss", iso_639_3: "oss"),
                    new (id: (int)LanguageName.Pali,                name: "Pali",              iso_639_1: "pi", iso_639_2T: "pli", iso_639_2B: "pli", iso_639_3: "pli"),
                    new (id: (int)LanguageName.Pashto,              name: "Pashto",            iso_639_1: "ps", iso_639_2T: "pus", iso_639_2B: "pus", iso_639_3: "pus"),
                    new (id: (int)LanguageName.Persian,             name: "Persian",           iso_639_1: "fa", iso_639_2T: "fas", iso_639_2B: "per", iso_639_3: "fas"),
                    new (id: (int)LanguageName.Polish,              name: "Polish",            iso_639_1: "pl", iso_639_2T: "pol", iso_639_2B: "pol", iso_639_3: "pol"),
                    new (id: (int)LanguageName.Portuguese,          name: "Portuguese",        iso_639_1: "pt", iso_639_2T: "por", iso_639_2B: "por", iso_639_3: "por"),
                    new (id: (int)LanguageName.Punjabi,             name: "Punjabi",           iso_639_1: "pa", iso_639_2T: "pan", iso_639_2B: "pan", iso_639_3: "pan"),
                    new (id: (int)LanguageName.Quechua,             name: "Quechua",           iso_639_1: "qu", iso_639_2T: "que", iso_639_2B: "que", iso_639_3: "que"),
                    new (id: (int)LanguageName.Romanian,            name: "Romanian",          iso_639_1: "ro", iso_639_2T: "ron", iso_639_2B: "rum", iso_639_3: "ron"),
                    new (id: (int)LanguageName.Romansh,             name: "Romansh",           iso_639_1: "rm", iso_639_2T: "roh", iso_639_2B: "roh", iso_639_3: "roh"),
                    new (id: (int)LanguageName.Rundi,               name: "Rundi",             iso_639_1: "rn", iso_639_2T: "run", iso_639_2B: "run", iso_639_3: "run"),
                    new (id: (int)LanguageName.Russian,             name: "Russian",           iso_639_1: "ru", iso_639_2T: "rus", iso_639_2B: "rus", iso_639_3: "rus"),
                    new (id: (int)LanguageName.Sami,                name: "Northern Sami",     iso_639_1: "se", iso_639_2T: "sme", iso_639_2B: "sme", iso_639_3: "sme"),
                    new (id: (int)LanguageName.Samoan,              name: "Samoan",            iso_639_1: "sm", iso_639_2T: "smo", iso_639_2B: "smo", iso_639_3: "smo"),
                    new (id: (int)LanguageName.Sango,               name: "Sango",             iso_639_1: "sg", iso_639_2T: "sag", iso_639_2B: "sag", iso_639_3: "sag"),
                    new (id: (int)LanguageName.Sanskrit,            name: "Sanskrit",          iso_639_1: "sa", iso_639_2T: "san", iso_639_2B: "san", iso_639_3: "san"),
                    new (id: (int)LanguageName.Sardinian,           name: "Sardinian",         iso_639_1: "sc", iso_639_2T: "srd", iso_639_2B: "srd", iso_639_3: "srd"),
                    new (id: (int)LanguageName.Serbian,             name: "Serbian",           iso_639_1: "sr", iso_639_2T: "srp", iso_639_2B: "srp", iso_639_3: "srp"),
                    new (id: (int)LanguageName.Shona,               name: "Shona",             iso_639_1: "sn", iso_639_2T: "sna", iso_639_2B: "sna", iso_639_3: "sna"),
                    new (id: (int)LanguageName.Sindhi,              name: "Sindhi",            iso_639_1: "sd", iso_639_2T: "snd", iso_639_2B: "snd", iso_639_3: "snd"),
                    new (id: (int)LanguageName.Sinhala,             name: "Sinhala",           iso_639_1: "si", iso_639_2T: "sin", iso_639_2B: "sin", iso_639_3: "sin"),
                    new (id: (int)LanguageName.Slovak,              name: "Slovak",            iso_639_1: "sk", iso_639_2T: "slk", iso_639_2B: "slo", iso_639_3: "slk"),
                    new (id: (int)LanguageName.Slovenian,           name: "Slovenian",         iso_639_1: "sl", iso_639_2T: "slv", iso_639_2B: "slv", iso_639_3: "slv"),
                    new (id: (int)LanguageName.Somali,              name: "Somali",            iso_639_1: "so", iso_639_2T: "som", iso_639_2B: "som", iso_639_3: "som"),
                    new (id: (int)LanguageName.Sotho,               name: "Southern Sotho",    iso_639_1: "st", iso_639_2T: "sot", iso_639_2B: "sot", iso_639_3: "sot"),
                    new (id: (int)LanguageName.Spanish,             name: "Spanish",           iso_639_1: "es", iso_639_2T: "spa", iso_639_2B: "spa", iso_639_3: "spa"),
                    new (id: (int)LanguageName.Sundanese,           name: "Sundanese",         iso_639_1: "su", iso_639_2T: "sun", iso_639_2B: "sun", iso_639_3: "sun"),
                    new (id: (int)LanguageName.Swahili,             name: "Swahili",           iso_639_1: "sw", iso_639_2T: "swa", iso_639_2B: "swa", iso_639_3: "swa"),
                    new (id: (int)LanguageName.Swati,               name: "Swati",             iso_639_1: "ss", iso_639_2T: "ssw", iso_639_2B: "ssw", iso_639_3: "ssw"),
                    new (id: (int)LanguageName.Swedish,             name: "Swedish",           iso_639_1: "sv", iso_639_2T: "swe", iso_639_2B: "swe", iso_639_3: "swe"),
                    new (id: (int)LanguageName.Tagalog,             name: "Tagalog",           iso_639_1: "tl", iso_639_2T: "tgl", iso_639_2B: "tgl", iso_639_3: "tgl"),
                    new (id: (int)LanguageName.Tahitian,            name: "Tahitian",          iso_639_1: "ty", iso_639_2T: "tah", iso_639_2B: "tah", iso_639_3: "tah"),
                    new (id: (int)LanguageName.Tajik,               name: "Tajik",             iso_639_1: "tg", iso_639_2T: "tgk", iso_639_2B: "tgk", iso_639_3: "tgk"),
                    new (id: (int)LanguageName.Tamil,               name: "Tamil",             iso_639_1: "ta", iso_639_2T: "tam", iso_639_2B: "tam", iso_639_3: "tam"),
                    new (id: (int)LanguageName.Tatar,               name: "Tatar",             iso_639_1: "tt", iso_639_2T: "tat", iso_639_2B: "tat", iso_639_3: "tat"),
                    new (id: (int)LanguageName.Telugu,              name: "Telugu",            iso_639_1: "te", iso_639_2T: "tel", iso_639_2B: "tel", iso_639_3: "tel"),
                    new (id: (int)LanguageName.Thai,                name: "Thai",              iso_639_1: "th", iso_639_2T: "tha", iso_639_2B: "tha", iso_639_3: "tha"),
                    new (id: (int)LanguageName.Tibetan,             name: "Tibetan",           iso_639_1: "bo", iso_639_2T: "bod", iso_639_2B: "tib", iso_639_3: "bod"),
                    new (id: (int)LanguageName.Tigrinya,            name: "Tigrinya",          iso_639_1: "ti", iso_639_2T: "tir", iso_639_2B: "tir", iso_639_3: "tir"),
                    new (id: (int)LanguageName.Tonga,               name: "Tonga",             iso_639_1: "to", iso_639_2T: "ton", iso_639_2B: "ton", iso_639_3: "ton"),
                    new (id: (int)LanguageName.Tsonga,              name: "Tsonga",            iso_639_1: "ts", iso_639_2T: "tso", iso_639_2B: "tso", iso_639_3: "tso"),
                    new (id: (int)LanguageName.Tswana,              name: "Tswana",            iso_639_1: "tn", iso_639_2T: "tsn", iso_639_2B: "tsn", iso_639_3: "tsn"),
                    new (id: (int)LanguageName.Turkish,             name: "Turkish",           iso_639_1: "tr", iso_639_2T: "tur", iso_639_2B: "tur", iso_639_3: "tur"),
                    new (id: (int)LanguageName.Turkmen,             name: "Turkmen",           iso_639_1: "tk", iso_639_2T: "tuk", iso_639_2B: "tuk", iso_639_3: "tuk"),
                    new (id: (int)LanguageName.Twi,                 name: "Twi",               iso_639_1: "tw", iso_639_2T: "twi", iso_639_2B: "twi", iso_639_3: "twi"),
                    new (id: (int)LanguageName.Uighur,              name: "Uighur",            iso_639_1: "ug", iso_639_2T: "uig", iso_639_2B: "uig", iso_639_3: "uig"),
                    new (id: (int)LanguageName.Ukrainian,           name: "Ukrainian",         iso_639_1: "uk", iso_639_2T: "ukr", iso_639_2B: "ukr", iso_639_3: "ukr"),
                    new (id: (int)LanguageName.Urdu,                name: "Urdu",              iso_639_1: "ur", iso_639_2T: "urd", iso_639_2B: "urd", iso_639_3: "urd"),
                    new (id: (int)LanguageName.Uzbek,               name: "Uzbek",             iso_639_1: "uz", iso_639_2T: "uzb", iso_639_2B: "uzb", iso_639_3: "uzb"),
                    new (id: (int)LanguageName.Venda,               name: "Venda",             iso_639_1: "ve", iso_639_2T: "ven", iso_639_2B: "ven", iso_639_3: "ven"),
                    new (id: (int)LanguageName.Vietnamese,          name: "Vietnamese",        iso_639_1: "vi", iso_639_2T: "vie", iso_639_2B: "vie", iso_639_3: "vie"),
                    new (id: (int)LanguageName.Volapuk,             name: "Volapük",           iso_639_1: "vo", iso_639_2T: "vol", iso_639_2B: "vol", iso_639_3: "vol"),
                    new (id: (int)LanguageName.Walloon,             name: "Walloon",           iso_639_1: "wa", iso_639_2T: "wln", iso_639_2B: "wln", iso_639_3: "wln"),
                    new (id: (int)LanguageName.Welsh,               name: "Welsh",             iso_639_1: "cy", iso_639_2T: "cym", iso_639_2B: "wel", iso_639_3: "cym"),
                    new (id: (int)LanguageName.Wolof,               name: "Wolof",             iso_639_1: "wo", iso_639_2T: "wol", iso_639_2B: "wol", iso_639_3: "wol"),
                    new (id: (int)LanguageName.Xhosa,               name: "Xhosa",             iso_639_1: "xh", iso_639_2T: "xho", iso_639_2B: "xho", iso_639_3: "xho"),
                    new (id: (int)LanguageName.Yiddish,             name: "Yiddish",           iso_639_1: "yi", iso_639_2T: "yid", iso_639_2B: "yid", iso_639_3: "yid"),
                    new (id: (int)LanguageName.Yoruba,              name: "Yoruba",            iso_639_1: "yo", iso_639_2T: "yor", iso_639_2B: "yor", iso_639_3: "yor"),
                    new (id: (int)LanguageName.Zhuang,              name: "Zhuang",            iso_639_1: "za", iso_639_2T: "zha", iso_639_2B: "zha", iso_639_3: "zha"),
                    new (id: (int)LanguageName.Zulu,                name: "Zulu",              iso_639_1: "zu", iso_639_2T: "zul", iso_639_2B: "zul", iso_639_3: "zul")
                ];
                }
            }

            /// <summary>
            /// Data table of ISO 639 language codes (read-only)
            /// </summary>
            public static DataTable Data
            {
                get
                {
                    DataTable data = new();

                    data.Columns.Add(column: new DataColumn(columnName: "id", dataType: Type.GetType(typeName: "System.Int32")));
                    data.Columns.Add(column: new DataColumn(columnName: "name", dataType: Type.GetType(typeName: "System.String")));
                    data.Columns.Add(column: new DataColumn(columnName: "iso_639_1", dataType: Type.GetType(typeName: "System.String")));
                    data.Columns.Add(column: new DataColumn(columnName: "iso_639_2T", dataType: Type.GetType(typeName: "System.String")));
                    data.Columns.Add(column: new DataColumn(columnName: "iso_639_2B", dataType: Type.GetType(typeName: "System.String")));
                    data.Columns.Add(column: new DataColumn(columnName: "iso_639_3", dataType: Type.GetType(typeName: "System.String")));

                    foreach (LanguageItem item in Items)
                    {
                        DataRow row = data.NewRow();
                        Functions.SetIntArg(row: row, name: "id", val: item.Id);
                        Functions.SetStringArg(row: row, name: "name", val: item.Name);
                        Functions.SetStringArg(row: row, name: "iso_639_1", val: item.ISO_639_1);
                        Functions.SetStringArg(row: row, name: "iso_639_2T", val: item.ISO_639_2T);
                        Functions.SetStringArg(row: row, name: "iso_639_2B", val: item.ISO_639_2B);
                        Functions.SetStringArg(row: row, name: "iso_639_3", val: item.ISO_639_3);
                        data.Rows.Add(row: row);
                    }

                    return data;
                }
            }


            /// <summary>
            /// Language name
            /// </summary>
            /// <param name="id">Language identifier</param>
            /// <returns>Language name</returns>
            private static string Name(int id)
            {
                LanguageItem item =
                    Items
                    .Where(a => a.Id == id)
                    .FirstOrDefault();

                return (item == null) ? String.Empty : item.Name;
            }

            /// <summary>
            /// Language name
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>Language name</returns>
            public static string Name(Language language)
            {
                return Name(id: (int)language);
            }

            /// <summary>
            /// Language name
            /// </summary>
            /// <param name="language">Language name</param>
            /// <returns>Language name</returns>
            public static string Name(LanguageName language)
            {
                return Name(id: (int)language);
            }


            /// <summary>
            /// Two-letter codes, one per language for ISO 639 macrolanguage
            /// </summary>
            /// <param name="id">Language identifier</param>
            /// <returns>Two-letter codes, one per language for ISO 639 macrolanguage</returns>
            private static string ISO_639_1(int id)
            {
                LanguageItem item =
                    Items
                    .Where(a => a.Id == id)
                    .FirstOrDefault();

                return (item == null) ? String.Empty : item.ISO_639_1;
            }

            /// <summary>
            /// Two-letter codes, one per language for ISO 639 macrolanguage
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>Two-letter codes, one per language for ISO 639 macrolanguage</returns>
            public static string ISO_639_1(Language language)
            {
                return ISO_639_1(id: (int)language);
            }

            /// <summary>
            /// Two-letter codes, one per language for ISO 639 macrolanguage
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>Two-letter codes, one per language for ISO 639 macrolanguage</returns>
            public static string ISO_639_1(LanguageName language)
            {
                return ISO_639_1(id: (int)language);
            }

            /// <summary>
            /// Two-letter codes, one per language for ISO 639 macrolanguage
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>Two-letter codes, one per language for ISO 639 macrolanguage</returns>
            public static string ISO_639_1(LanguageCode language)
            {
                return ISO_639_1(id: (int)language);
            }

            /// <summary>
            /// Converts ISO 639-1 language code to enum Language
            /// </summary>
            /// <param name="code">ISO 639-1 language code</param>
            /// <returns>enum Language</returns>
            public static Language LanguageFromISO_639_1(string code)
            {
                LanguageItem item = Items
                    .Where(a => code == a.ISO_639_1)
                    .FirstOrDefault();

                return (item == null) ? Language.Null : item.Language;
            }

            /// <summary>
            /// Converts ISO 639-1 language code to enum LanguageName
            /// </summary>
            /// <param name="code">ISO 639-1 language code</param>
            /// <returns>enum LanguageName</returns>
            public static LanguageName LanguageNameFromISO_639_1(string code)
            {
                LanguageItem item = Items
                    .Where(a => code == a.ISO_639_1)
                    .FirstOrDefault();

                return (item == null) ? LanguageName.Null : item.LanguageName;
            }


            /// <summary>
            /// Three-letter codes, for the same languages as 639-1
            /// </summary>
            /// <param name="id">Language identifier</param>
            /// <returns>Three-letter codes, for the same languages as 639-1</returns>
            private static string ISO_639_2T(int id)
            {
                LanguageItem item =
                    Items
                    .Where(a => a.Id == id)
                    .FirstOrDefault();

                return (item == null) ? String.Empty : item.ISO_639_2T;
            }

            /// <summary>
            /// Three-letter codes, for the same languages as 639-1
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>Three-letter codes, for the same languages as 639-1</returns>
            public static string ISO_639_2T(Language language)
            {
                return ISO_639_2T(id: (int)language);
            }

            /// <summary>
            /// Three-letter codes, for the same languages as 639-1
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>Three-letter codes, for the same languages as 639-1</returns>
            public static string ISO_639_2T(LanguageName language)
            {
                return ISO_639_2T(id: (int)language);
            }

            /// <summary>
            /// Three-letter codes, for the same languages as 639-1
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>Three-letter codes, for the same languages as 639-1</returns>
            public static string ISO_639_2T(LanguageCode language)
            {
                return ISO_639_2T(id: (int)language);
            }

            /// <summary>
            /// Converts ISO 639-2T language code to enum Language
            /// </summary>
            /// <param name="code">ISO 639-2T language code</param>
            /// <returns>enum Language</returns>
            public static Language LanguageFromISO_639_2T(string code)
            {
                LanguageItem item = Items
                    .Where(a => code == a.ISO_639_2T)
                    .FirstOrDefault();

                return (item == null) ? Language.Null : item.Language;
            }

            /// <summary>
            /// Converts ISO 639-2T language code to enum LanguageName
            /// </summary>
            /// <param name="code">ISO 639-2T language code</param>
            /// <returns>enum LanguageName</returns>
            public static LanguageName LanguageNameFromISO_639_2T(string code)
            {
                LanguageItem item = Items
                    .Where(a => code == a.ISO_639_2T)
                    .FirstOrDefault();

                return (item == null) ? LanguageName.Null : item.LanguageName;
            }


            /// <summary>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </summary>
            /// <param name="id">Language identifier</param>
            /// <returns>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </returns>
            private static string ISO_639_2B(int id)
            {
                LanguageItem item =
                    Items
                    .Where(a => a.Id == id)
                    .FirstOrDefault();

                return (item == null) ? String.Empty : item.ISO_639_2B;
            }

            /// <summary>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </returns>
            public static string ISO_639_2B(Language language)
            {
                return ISO_639_2B(id: (int)language);
            }

            /// <summary>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </returns>
            public static string ISO_639_2B(LanguageName language)
            {
                return ISO_639_2B(id: (int)language);
            }

            /// <summary>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>
            /// Three-letter codes, mostly the same as 639-2/T,
            /// but with some codes derived from English names rather than native names of languages
            /// </returns>
            public static string ISO_639_2B(LanguageCode language)
            {
                return ISO_639_2B(id: (int)language);
            }

            /// <summary>
            /// Converts ISO 639-2B language code to enum Language
            /// </summary>
            /// <param name="code">ISO 639-2B language code</param>
            /// <returns>enum Language</returns>
            public static Language LanguageFromISO_639_2B(string code)
            {
                LanguageItem item = Items
                    .Where(a => code == a.ISO_639_2B)
                    .FirstOrDefault();

                return (item == null) ? Language.Null : item.Language;
            }

            /// <summary>
            /// Converts ISO 639-2B language code to enum LanguageName
            /// </summary>
            /// <param name="code">ISO 639-2B language code</param>
            /// <returns>enum LanguageName</returns>
            public static LanguageName LanguageNameFromISO_639_2B(string code)
            {
                LanguageItem item = Items
                    .Where(a => code == a.ISO_639_2B)
                    .FirstOrDefault();

                return (item == null) ? LanguageName.Null : item.LanguageName;
            }


            /// <summary>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </summary>
            /// <param name="id">Language identifier</param>
            /// <returns>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </returns>
            private static string ISO_639_3(int id)
            {
                LanguageItem item =
                    Items
                    .Where(a => a.Id == id)
                    .FirstOrDefault();

                return (item == null) ? String.Empty : item.ISO_639_3;
            }

            /// <summary>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </returns>
            public static string ISO_639_3(Language language)
            {
                return ISO_639_3(id: (int)language);
            }

            /// <summary>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </returns>
            public static string ISO_639_3(LanguageName language)
            {
                return ISO_639_3(id: (int)language);
            }

            /// <summary>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </summary>
            /// <param name="language">Language</param>
            /// <returns>
            /// Three-letter codes, the same as 639-2/T for languages,
            /// but with distinct codes for each variety of an ISO 639 macrolanguage
            /// </returns>
            public static string ISO_639_3(LanguageCode language)
            {
                return ISO_639_3(id: (int)language);
            }

            /// <summary>
            /// Converts ISO 639-3 language code to enum Language
            /// </summary>
            /// <param name="code">ISO 639-3 language code</param>
            /// <returns>enum Language</returns>
            public static Language LanguageFromISO_639_3(string code)
            {
                LanguageItem item = Items
                    .Where(a => code == a.ISO_639_3)
                    .FirstOrDefault();

                return (item == null) ? Language.Null : item.Language;
            }

            /// <summary>
            /// Converts ISO 639-3 language code to enum LanguageName
            /// </summary>
            /// <param name="code">ISO 639-3 language code</param>
            /// <returns>enum LanguageName</returns>
            public static LanguageName LanguageNameFromISO_639_3(string code)
            {
                LanguageItem item = Items
                    .Where(a => code == a.ISO_639_3)
                    .FirstOrDefault();

                return (item == null) ? LanguageName.Null : item.LanguageName;
            }

        }


        /// <summary>
        /// Language Name
        /// </summary>
        public enum LanguageName
        {
            /// <summary>
            /// Language is not defined
            /// </summary>
            Null = 0,

            /// <summary>
            /// Abkhazian
            /// </summary>
            Abkhazian = 1,

            /// <summary>
            /// Afar
            /// </summary>
            Afar = 2,

            /// <summary>
            /// Afrikaans
            /// </summary>
            Afrikaans = 3,

            /// <summary>
            /// Akan
            /// </summary>
            Akan = 4,

            /// <summary>
            /// Albanian
            /// </summary>
            Albanian = 5,

            /// <summary>
            /// Amharic
            /// </summary>
            Amharic = 6,

            /// <summary>
            /// Arabic
            /// </summary>
            Arabic = 7,

            /// <summary>
            /// Aragonese
            /// </summary>
            Aragonese = 8,

            /// <summary>
            /// Armenian
            /// </summary>
            Armenian = 9,

            /// <summary>
            /// Assamese
            /// </summary>
            Assamese = 10,

            /// <summary>
            /// Avaric
            /// </summary>
            Avaric = 11,

            /// <summary>
            /// Avestan
            /// </summary>
            Avestan = 12,

            /// <summary>
            /// Aymara
            /// </summary>
            Aymara = 13,

            /// <summary>
            /// Azerbaijani
            /// </summary>
            Azerbaijani = 14,

            /// <summary>
            /// Bambara
            /// </summary>
            Bambara = 15,

            /// <summary>
            /// Bashkir
            /// </summary>
            Bashkir = 16,

            /// <summary>
            /// Basque
            /// </summary>
            Basque = 17,

            /// <summary>
            /// Belarusian
            /// </summary>
            Belarusian = 18,

            /// <summary>
            /// Bengali
            /// </summary>
            Bengali = 19,

            /// <summary>
            /// Bislama
            /// </summary>
            Bislama = 20,

            /// <summary>
            /// Bosnian
            /// </summary>
            Bosnian = 21,

            /// <summary>
            /// Breton
            /// </summary>
            Breton = 22,

            /// <summary>
            /// Bulgarian
            /// </summary>
            Bulgarian = 23,

            /// <summary>
            /// Burmese
            /// </summary>
            Burmese = 24,

            /// <summary>
            /// Catalan
            /// </summary>
            Catalan = 25,

            /// <summary>
            /// Chamorro
            /// </summary>
            Chamorro = 26,

            /// <summary>
            /// Chechen
            /// </summary>
            Chechen = 27,

            /// <summary>
            /// Chichewa
            /// </summary>
            Chichewa = 28,

            /// <summary>
            /// Chinese
            /// </summary>
            Chinese = 29,

            /// <summary>
            /// Church Slavonic
            /// </summary>
            Slavonic = 30,

            /// <summary>
            /// Chuvash
            /// </summary>
            Chuvash = 31,

            /// <summary>
            /// Cornish
            /// </summary>
            Cornish = 32,

            /// <summary>
            /// Corsican
            /// </summary>
            Corsican = 33,

            /// <summary>
            /// Cree
            /// </summary>
            Cree = 34,

            /// <summary>
            /// Croatian
            /// </summary>
            Croatian = 35,

            /// <summary>
            /// Czech
            /// </summary>
            Czech = 36,

            /// <summary>
            /// Danish
            /// </summary>
            Danish = 37,

            /// <summary>
            /// Divehi
            /// </summary>
            Divehi = 38,

            /// <summary>
            /// Dutch
            /// </summary>
            Dutch = 39,

            /// <summary>
            /// Dzongkha
            /// </summary>
            Dzongkha = 40,

            /// <summary>
            /// English
            /// </summary>
            English = 41,

            /// <summary>
            /// Esperanto
            /// </summary>
            Esperanto = 42,

            /// <summary>
            /// Estonian
            /// </summary>
            Estonian = 43,

            /// <summary>
            /// Ewe
            /// </summary>
            Ewe = 44,

            /// <summary>
            /// Faroese
            /// </summary>
            Faroese = 45,

            /// <summary>
            /// Fijian
            /// </summary>
            Fijian = 46,

            /// <summary>
            /// Finnish
            /// </summary>
            Finnish = 47,

            /// <summary>
            /// French
            /// </summary>
            French = 48,

            /// <summary>
            /// Western Frisian
            /// </summary>
            Frisian = 49,

            /// <summary>
            /// Fulah
            /// </summary>
            Fulah = 50,

            /// <summary>
            /// Gaelic
            /// </summary>
            Gaelic = 51,

            /// <summary>
            /// Galician
            /// </summary>
            Galician = 52,

            /// <summary>
            /// Ganda
            /// </summary>
            Ganda = 53,

            /// <summary>
            /// Georgian
            /// </summary>
            Georgian = 54,

            /// <summary>
            /// German
            /// </summary>
            German = 55,

            /// <summary>
            /// Greek
            /// </summary>
            Greek = 56,

            /// <summary>
            /// Kalaallisut
            /// </summary>
            Kalaallisut = 57,

            /// <summary>
            /// Guarani
            /// </summary>
            Guarani = 58,

            /// <summary>
            /// Gujarati
            /// </summary>
            Gujarati = 59,

            /// <summary>
            /// Haitian
            /// </summary>
            Haitian = 60,

            /// <summary>
            /// Hausa
            /// </summary>
            Hausa = 61,

            /// <summary>
            /// Hebrew
            /// </summary>
            Hebrew = 62,

            /// <summary>
            /// Herero
            /// </summary>
            Herero = 63,

            /// <summary>
            /// Hindi
            /// </summary>
            Hindi = 64,

            /// <summary>
            /// Hiri Motu
            /// </summary>
            HiriMotu = 65,

            /// <summary>
            /// Hungarian
            /// </summary>
            Hungarian = 66,

            /// <summary>
            /// Icelandic
            /// </summary>
            Icelandic = 67,

            /// <summary>
            /// Ido
            /// </summary>
            Ido = 68,

            /// <summary>
            /// Igbo
            /// </summary>
            Igbo = 69,

            /// <summary>
            /// Indonesian
            /// </summary>
            Indonesian = 70,

            /// <summary>
            /// Interlingua
            /// </summary>
            Interlingua = 71,

            /// <summary>
            /// Interlingue
            /// </summary>
            Interlingue = 72,

            /// <summary>
            /// Inuktitut
            /// </summary>
            Inuktitut = 73,

            /// <summary>
            /// Inupiaq
            /// </summary>
            Inupiaq = 74,

            /// <summary>
            /// Irish
            /// </summary>
            Irish = 75,

            /// <summary>
            /// Italian
            /// </summary>
            Italian = 76,

            /// <summary>
            /// Japanese
            /// </summary>
            Japanese = 77,

            /// <summary>
            /// Javanese
            /// </summary>
            Javanese = 78,

            /// <summary>
            /// Kannada
            /// </summary>
            Kannada = 79,

            /// <summary>
            /// Kanuri
            /// </summary>
            Kanuri = 80,

            /// <summary>
            /// Kashmiri
            /// </summary>
            Kashmiri = 81,

            /// <summary>
            /// Kazakh
            /// </summary>
            Kazakh = 82,

            /// <summary>
            /// Central Khmer
            /// </summary>
            Khmer = 83,

            /// <summary>
            /// Kikuyu
            /// </summary>
            Kikuyu = 84,

            /// <summary>
            /// Kinyarwanda
            /// </summary>
            Kinyarwanda = 85,

            /// <summary>
            /// Kirghiz
            /// </summary>
            Kirghiz = 86,

            /// <summary>
            /// Komi
            /// </summary>
            Komi = 87,

            /// <summary>
            /// Kongo
            /// </summary>
            Kongo = 88,

            /// <summary>
            /// Korean
            /// </summary>
            Korean = 89,

            /// <summary>
            /// Kuanyama
            /// </summary>
            Kuanyama = 90,

            /// <summary>
            /// Kurdish
            /// </summary>
            Kurdish = 91,

            /// <summary>
            /// Lao
            /// </summary>
            Lao = 92,

            /// <summary>
            /// Latin
            /// </summary>
            Latin = 93,

            /// <summary>
            /// Latvian
            /// </summary>
            Latvian = 94,

            /// <summary>
            /// Limburgan
            /// </summary>
            Limburgan = 95,

            /// <summary>
            /// Lingala
            /// </summary>
            Lingala = 96,

            /// <summary>
            /// Lithuanian
            /// </summary>
            Lithuanian = 97,

            /// <summary>
            /// Luba-Katanga
            /// </summary>
            LubaKatanga = 98,

            /// <summary>
            /// Luxembourgish
            /// </summary>
            Luxembourgish = 99,

            /// <summary>
            /// Macedonian
            /// </summary>
            Macedonian = 100,

            /// <summary>
            /// Malagasy
            /// </summary>
            Malagasy = 101,

            /// <summary>
            /// Malay
            /// </summary>
            Malay = 102,

            /// <summary>
            /// Malayalam
            /// </summary>
            Malayalam = 103,

            /// <summary>
            /// Maltese
            /// </summary>
            Maltese = 104,

            /// <summary>
            /// Manx
            /// </summary>
            Manx = 105,

            /// <summary>
            /// Maori
            /// </summary>
            Maori = 106,

            /// <summary>
            /// Marathi
            /// </summary>
            Marathi = 107,

            /// <summary>
            /// Marshallese
            /// </summary>
            Marshallese = 108,

            /// <summary>
            /// Mongolian
            /// </summary>
            Mongolian = 109,

            /// <summary>
            /// Nauru
            /// </summary>
            Nauru = 110,

            /// <summary>
            /// Navajo
            /// </summary>
            Navajo = 111,

            /// <summary>
            /// North Ndebele
            /// </summary>
            NorthNdebele = 112,

            /// <summary>
            /// South Ndebele
            /// </summary>
            SouthNdebele = 113,

            /// <summary>
            /// Ndonga
            /// </summary>
            Ndonga = 114,

            /// <summary>
            /// Nepali
            /// </summary>
            Nepali = 115,

            /// <summary>
            /// Norwegian
            /// </summary>
            Norwegian = 116,

            /// <summary>
            /// Norwegian Bokmål
            /// </summary>
            NorwegianBokmal = 117,

            /// <summary>
            /// Norwegian Nynorsk
            /// </summary>
            NorwegianNynorsk = 118,

            /// <summary>
            /// Sichuan Yi
            /// </summary>
            SichuanYi = 119,

            /// <summary>
            /// Occitan
            /// </summary>
            Occitan = 120,

            /// <summary>
            /// Ojibwa
            /// </summary>
            Ojibwa = 121,

            /// <summary>
            /// Oriya
            /// </summary>
            Oriya = 122,

            /// <summary>
            /// Oromo
            /// </summary>
            Oromo = 123,

            /// <summary>
            /// Ossetian
            /// </summary>
            Ossetian = 124,

            /// <summary>
            /// Pali
            /// </summary>
            Pali = 125,

            /// <summary>
            /// Pashto
            /// </summary>
            Pashto = 126,

            /// <summary>
            /// Persian
            /// </summary>
            Persian = 127,

            /// <summary>
            /// Polish
            /// </summary>
            Polish = 128,

            /// <summary>
            /// Portuguese
            /// </summary>
            Portuguese = 129,

            /// <summary>
            /// Punjabi
            /// </summary>
            Punjabi = 130,

            /// <summary>
            /// Quechua
            /// </summary>
            Quechua = 131,

            /// <summary>
            /// Romanian
            /// </summary>
            Romanian = 132,

            /// <summary>
            /// Romansh
            /// </summary>
            Romansh = 133,

            /// <summary>
            /// Rundi
            /// </summary>
            Rundi = 134,

            /// <summary>
            /// Russian
            /// </summary>
            Russian = 135,

            /// <summary>
            /// Northern Sami
            /// </summary>
            Sami = 136,

            /// <summary>
            /// Samoan
            /// </summary>
            Samoan = 137,

            /// <summary>
            /// Sango
            /// </summary>
            Sango = 138,

            /// <summary>
            /// Sanskrit
            /// </summary>
            Sanskrit = 139,

            /// <summary>
            /// Sardinian
            /// </summary>
            Sardinian = 140,

            /// <summary>
            /// Serbian
            /// </summary>
            Serbian = 141,

            /// <summary>
            /// Shona
            /// </summary>
            Shona = 142,

            /// <summary>
            /// Sindhi
            /// </summary>
            Sindhi = 143,

            /// <summary>
            /// Sinhala
            /// </summary>
            Sinhala = 144,

            /// <summary>
            /// Slovak
            /// </summary>
            Slovak = 145,

            /// <summary>
            /// Slovenian
            /// </summary>
            Slovenian = 146,

            /// <summary>
            /// Somali
            /// </summary>
            Somali = 147,

            /// <summary>
            /// Southern Sotho
            /// </summary>
            Sotho = 148,

            /// <summary>
            /// Spanish
            /// </summary>
            Spanish = 149,

            /// <summary>
            /// Sundanese
            /// </summary>
            Sundanese = 150,

            /// <summary>
            /// Swahili
            /// </summary>
            Swahili = 151,

            /// <summary>
            /// Swati
            /// </summary>
            Swati = 152,

            /// <summary>
            /// Swedish
            /// </summary>
            Swedish = 153,

            /// <summary>
            /// Tagalog
            /// </summary>
            Tagalog = 154,

            /// <summary>
            /// Tahitian
            /// </summary>
            Tahitian = 155,

            /// <summary>
            /// Tajik
            /// </summary>
            Tajik = 156,

            /// <summary>
            /// Tamil
            /// </summary>
            Tamil = 157,

            /// <summary>
            /// Tatar
            /// </summary>
            Tatar = 158,

            /// <summary>
            /// Telugu
            /// </summary>
            Telugu = 159,

            /// <summary>
            /// Thai
            /// </summary>
            Thai = 160,

            /// <summary>
            /// Tibetan
            /// </summary>
            Tibetan = 161,

            /// <summary>
            /// Tigrinya
            /// </summary>
            Tigrinya = 162,

            /// <summary>
            /// Tonga
            /// </summary>
            Tonga = 163,

            /// <summary>
            /// Tsonga
            /// </summary>
            Tsonga = 164,

            /// <summary>
            /// Tswana
            /// </summary>
            Tswana = 165,

            /// <summary>
            /// Turkish
            /// </summary>
            Turkish = 166,

            /// <summary>
            /// Turkmen
            /// </summary>
            Turkmen = 167,

            /// <summary>
            /// Twi
            /// </summary>
            Twi = 168,

            /// <summary>
            /// Uighur
            /// </summary>
            Uighur = 169,

            /// <summary>
            /// Ukrainian
            /// </summary>
            Ukrainian = 170,

            /// <summary>
            /// Urdu
            /// </summary>
            Urdu = 171,

            /// <summary>
            /// Uzbek
            /// </summary>
            Uzbek = 172,

            /// <summary>
            /// Venda
            /// </summary>
            Venda = 173,

            /// <summary>
            /// Vietnamese
            /// </summary>
            Vietnamese = 174,

            /// <summary>
            /// Volapük
            /// </summary>
            Volapuk = 175,

            /// <summary>
            /// Walloon
            /// </summary>
            Walloon = 176,

            /// <summary>
            /// Welsh
            /// </summary>
            Welsh = 177,

            /// <summary>
            /// Wolof
            /// </summary>
            Wolof = 178,

            /// <summary>
            /// Xhosa
            /// </summary>
            Xhosa = 179,

            /// <summary>
            /// Yiddish
            /// </summary>
            Yiddish = 180,

            /// <summary>
            /// Yoruba
            /// </summary>
            Yoruba = 181,

            /// <summary>
            /// Zhuang
            /// </summary>
            Zhuang = 182,

            /// <summary>
            /// Zulu
            /// </summary>
            Zulu = 183
        }


        /// <summary>
        /// Language Code (ISO 639_1) (two letters)
        /// </summary>
        public enum Language
        {
            /// <summary>
            /// Language is not defined
            /// </summary>
            Null = (int)LanguageName.Null,

            /// <summary>
            /// Abkhazian
            /// </summary>
            AB = (int)LanguageName.Abkhazian,

            /// <summary>
            /// Afar
            /// </summary>
            AA = (int)LanguageName.Afar,

            /// <summary>
            /// Afrikaans
            /// </summary>
            AF = (int)LanguageName.Afrikaans,

            /// <summary>
            /// Akan
            /// </summary>
            AK = (int)LanguageName.Akan,

            /// <summary>
            /// Albanian
            /// </summary>
            SQ = (int)LanguageName.Albanian,

            /// <summary>
            /// Amharic
            /// </summary>
            AM = (int)LanguageName.Amharic,

            /// <summary>
            /// Arabic
            /// </summary>
            AR = (int)LanguageName.Arabic,

            /// <summary>
            /// Aragonese
            /// </summary>
            AN = (int)LanguageName.Aragonese,

            /// <summary>
            /// Armenian
            /// </summary>
            HY = (int)LanguageName.Armenian,

            /// <summary>
            /// Assamese
            /// </summary>
            AS = (int)LanguageName.Assamese,

            /// <summary>
            /// Avaric
            /// </summary>
            AV = (int)LanguageName.Avaric,

            /// <summary>
            /// Avestan
            /// </summary>
            AE = (int)LanguageName.Avestan,

            /// <summary>
            /// Aymara
            /// </summary>
            AY = (int)LanguageName.Aymara,

            /// <summary>
            /// Azerbaijani
            /// </summary>
            AZ = (int)LanguageName.Azerbaijani,

            /// <summary>
            /// Bambara
            /// </summary>
            BM = (int)LanguageName.Bambara,

            /// <summary>
            /// Bashkir
            /// </summary>
            BA = (int)LanguageName.Bashkir,

            /// <summary>
            /// Basque
            /// </summary>
            EU = (int)LanguageName.Basque,

            /// <summary>
            /// Belarusian
            /// </summary>
            BE = (int)LanguageName.Belarusian,

            /// <summary>
            /// Bengali
            /// </summary>
            BN = (int)LanguageName.Bengali,

            /// <summary>
            /// Bislama
            /// </summary>
            BI = (int)LanguageName.Bislama,

            /// <summary>
            /// Bosnian
            /// </summary>
            BS = (int)LanguageName.Bosnian,

            /// <summary>
            /// Breton
            /// </summary>
            BR = (int)LanguageName.Breton,

            /// <summary>
            /// Bulgarian
            /// </summary>
            BG = (int)LanguageName.Bulgarian,

            /// <summary>
            /// Burmese
            /// </summary>
            MY = (int)LanguageName.Burmese,

            /// <summary>
            /// Catalan
            /// </summary>
            CA = (int)LanguageName.Catalan,

            /// <summary>
            /// Chamorro
            /// </summary>
            CH = (int)LanguageName.Chamorro,

            /// <summary>
            /// Chechen
            /// </summary>
            CE = (int)LanguageName.Chechen,

            /// <summary>
            /// Chichewa
            /// </summary>
            NY = (int)LanguageName.Chichewa,

            /// <summary>
            /// Chinese
            /// </summary>
            ZH = (int)LanguageName.Chinese,

            /// <summary>
            /// Church Slavonic
            /// </summary>
            CU = (int)LanguageName.Slavonic,

            /// <summary>
            /// Chuvash
            /// </summary>
            CV = (int)LanguageName.Chuvash,

            /// <summary>
            /// Cornish
            /// </summary>
            KW = (int)LanguageName.Cornish,

            /// <summary>
            /// Corsican
            /// </summary>
            CO = (int)LanguageName.Corsican,

            /// <summary>
            /// Cree
            /// </summary>
            CR = (int)LanguageName.Cree,

            /// <summary>
            /// Croatian
            /// </summary>
            HR = (int)LanguageName.Croatian,

            /// <summary>
            /// Czech
            /// </summary>
            CS = (int)LanguageName.Czech,

            /// <summary>
            /// Danish
            /// </summary>
            DA = (int)LanguageName.Danish,

            /// <summary>
            /// Divehi
            /// </summary>
            DV = (int)LanguageName.Divehi,

            /// <summary>
            /// Dutch
            /// </summary>
            NL = (int)LanguageName.Dutch,

            /// <summary>
            /// Dzongkha
            /// </summary>
            DZ = (int)LanguageName.Dzongkha,

            /// <summary>
            /// English
            /// </summary>
            EN = (int)LanguageName.English,

            /// <summary>
            /// Esperanto
            /// </summary>
            EO = (int)LanguageName.Esperanto,

            /// <summary>
            /// Estonian
            /// </summary>
            ET = (int)LanguageName.Estonian,

            /// <summary>
            /// Ewe
            /// </summary>
            EE = (int)LanguageName.Ewe,

            /// <summary>
            /// Faroese
            /// </summary>
            FO = (int)LanguageName.Faroese,

            /// <summary>
            /// Fijian
            /// </summary>
            FJ = (int)LanguageName.Fijian,

            /// <summary>
            /// Finnish
            /// </summary>
            FI = (int)LanguageName.Finnish,

            /// <summary>
            /// French
            /// </summary>
            FR = (int)LanguageName.French,

            /// <summary>
            /// Western Frisian
            /// </summary>
            FY = (int)LanguageName.Frisian,

            /// <summary>
            /// Fulah
            /// </summary>
            FF = (int)LanguageName.Fulah,

            /// <summary>
            /// Gaelic
            /// </summary>
            GD = (int)LanguageName.Gaelic,

            /// <summary>
            /// Galician
            /// </summary>
            GL = (int)LanguageName.Galician,

            /// <summary>
            /// Ganda
            /// </summary>
            LG = (int)LanguageName.Ganda,

            /// <summary>
            /// Georgian
            /// </summary>
            KA = (int)LanguageName.Georgian,

            /// <summary>
            /// German
            /// </summary>
            DE = (int)LanguageName.German,

            /// <summary>
            /// Greek
            /// </summary>
            EL = (int)LanguageName.Greek,

            /// <summary>
            /// Kalaallisut
            /// </summary>
            KL = (int)LanguageName.Kalaallisut,

            /// <summary>
            /// Guarani
            /// </summary>
            GN = (int)LanguageName.Guarani,

            /// <summary>
            /// Gujarati
            /// </summary>
            GU = (int)LanguageName.Gujarati,

            /// <summary>
            /// Haitian
            /// </summary>
            HT = (int)LanguageName.Haitian,

            /// <summary>
            /// Hausa
            /// </summary>
            HA = (int)LanguageName.Hausa,

            /// <summary>
            /// Hebrew
            /// </summary>
            HE = (int)LanguageName.Hebrew,

            /// <summary>
            /// Herero
            /// </summary>
            HZ = (int)LanguageName.Herero,

            /// <summary>
            /// Hindi
            /// </summary>
            HI = (int)LanguageName.Hindi,

            /// <summary>
            /// Hiri Motu
            /// </summary>
            HO = (int)LanguageName.HiriMotu,

            /// <summary>
            /// Hungarian
            /// </summary>
            HU = (int)LanguageName.Hungarian,

            /// <summary>
            /// Icelandic
            /// </summary>
            IS = (int)LanguageName.Icelandic,

            /// <summary>
            /// Ido
            /// </summary>
            IO = (int)LanguageName.Ido,

            /// <summary>
            /// Igbo
            /// </summary>
            IG = (int)LanguageName.Igbo,

            /// <summary>
            /// Indonesian
            /// </summary>
            ID = (int)LanguageName.Indonesian,

            /// <summary>
            /// Interlingua
            /// </summary>
            IA = (int)LanguageName.Interlingua,

            /// <summary>
            /// Interlingue
            /// </summary>
            IE = (int)LanguageName.Interlingue,

            /// <summary>
            /// Inuktitut
            /// </summary>
            IU = (int)LanguageName.Inuktitut,

            /// <summary>
            /// Inupiaq
            /// </summary>
            IK = (int)LanguageName.Inupiaq,

            /// <summary>
            /// Irish
            /// </summary>
            GA = (int)LanguageName.Irish,

            /// <summary>
            /// Italian
            /// </summary>
            IT = (int)LanguageName.Italian,

            /// <summary>
            /// Japanese
            /// </summary>
            JA = (int)LanguageName.Japanese,

            /// <summary>
            /// Javanese
            /// </summary>
            JV = (int)LanguageName.Javanese,

            /// <summary>
            /// Kannada
            /// </summary>
            KN = (int)LanguageName.Kannada,

            /// <summary>
            /// Kanuri
            /// </summary>
            KR = (int)LanguageName.Kanuri,

            /// <summary>
            /// Kashmiri
            /// </summary>
            KS = (int)LanguageName.Kashmiri,

            /// <summary>
            /// Kazakh
            /// </summary>
            KK = (int)LanguageName.Kazakh,

            /// <summary>
            /// Central Khmer
            /// </summary>
            KM = (int)LanguageName.Khmer,

            /// <summary>
            /// Kikuyu
            /// </summary>
            KI = (int)LanguageName.Kikuyu,

            /// <summary>
            /// Kinyarwanda
            /// </summary>
            RW = (int)LanguageName.Kinyarwanda,

            /// <summary>
            /// Kirghiz
            /// </summary>
            KY = (int)LanguageName.Kirghiz,

            /// <summary>
            /// Komi
            /// </summary>
            KV = (int)LanguageName.Komi,

            /// <summary>
            /// Kongo
            /// </summary>
            KG = (int)LanguageName.Kongo,

            /// <summary>
            /// Korean
            /// </summary>
            KO = (int)LanguageName.Korean,

            /// <summary>
            /// Kuanyama
            /// </summary>
            KJ = (int)LanguageName.Kuanyama,

            /// <summary>
            /// Kurdish
            /// </summary>
            KU = (int)LanguageName.Kurdish,

            /// <summary>
            /// Lao
            /// </summary>
            LO = (int)LanguageName.Lao,

            /// <summary>
            /// Latin
            /// </summary>
            LA = (int)LanguageName.Latin,

            /// <summary>
            /// Latvian
            /// </summary>
            LV = (int)LanguageName.Latvian,

            /// <summary>
            /// Limburgan
            /// </summary>
            LI = (int)LanguageName.Limburgan,

            /// <summary>
            /// Lingala
            /// </summary>
            LN = (int)LanguageName.Lingala,

            /// <summary>
            /// Lithuanian
            /// </summary>
            LT = (int)LanguageName.Lithuanian,

            /// <summary>
            /// Luba-Katanga
            /// </summary>
            LU = (int)LanguageName.LubaKatanga,

            /// <summary>
            /// Luxembourgish
            /// </summary>
            LB = (int)LanguageName.Luxembourgish,

            /// <summary>
            /// Macedonian
            /// </summary>
            MK = (int)LanguageName.Macedonian,

            /// <summary>
            /// Malagasy
            /// </summary>
            MG = (int)LanguageName.Malagasy,

            /// <summary>
            /// Malay
            /// </summary>
            MS = (int)LanguageName.Malay,

            /// <summary>
            /// Malayalam
            /// </summary>
            ML = (int)LanguageName.Malayalam,

            /// <summary>
            /// Maltese
            /// </summary>
            MT = (int)LanguageName.Maltese,

            /// <summary>
            /// Manx
            /// </summary>
            GV = (int)LanguageName.Manx,

            /// <summary>
            /// Maori
            /// </summary>
            MI = (int)LanguageName.Maori,

            /// <summary>
            /// Marathi
            /// </summary>
            MR = (int)LanguageName.Marathi,

            /// <summary>
            /// Marshallese
            /// </summary>
            MH = (int)LanguageName.Marshallese,

            /// <summary>
            /// Mongolian
            /// </summary>
            MN = (int)LanguageName.Mongolian,

            /// <summary>
            /// Nauru
            /// </summary>
            NA = (int)LanguageName.Nauru,

            /// <summary>
            /// Navajo
            /// </summary>
            NV = (int)LanguageName.Navajo,

            /// <summary>
            /// North Ndebele
            /// </summary>
            ND = (int)LanguageName.NorthNdebele,

            /// <summary>
            /// South Ndebele
            /// </summary>
            NR = (int)LanguageName.SouthNdebele,

            /// <summary>
            /// Ndonga
            /// </summary>
            NG = (int)LanguageName.Ndonga,

            /// <summary>
            /// Nepali
            /// </summary>
            NE = (int)LanguageName.Nepali,

            /// <summary>
            /// Norwegian
            /// </summary>
            NO = (int)LanguageName.Norwegian,

            /// <summary>
            /// Norwegian Bokmål
            /// </summary>
            NB = (int)LanguageName.NorwegianBokmal,

            /// <summary>
            /// Norwegian Nynorsk
            /// </summary>
            NN = (int)LanguageName.NorwegianNynorsk,

            /// <summary>
            /// Sichuan Yi
            /// </summary>
            II = (int)LanguageName.SichuanYi,

            /// <summary>
            /// Occitan
            /// </summary>
            OC = (int)LanguageName.Occitan,

            /// <summary>
            /// Ojibwa
            /// </summary>
            OJ = (int)LanguageName.Ojibwa,

            /// <summary>
            /// Oriya
            /// </summary>
            OR = (int)LanguageName.Oriya,

            /// <summary>
            /// Oromo
            /// </summary>
            OM = (int)LanguageName.Oromo,

            /// <summary>
            /// Ossetian
            /// </summary>
            OS = (int)LanguageName.Ossetian,

            /// <summary>
            /// Pali
            /// </summary>
            PI = (int)LanguageName.Pali,

            /// <summary>
            /// Pashto
            /// </summary>
            PS = (int)LanguageName.Pashto,

            /// <summary>
            /// Persian
            /// </summary>
            FA = (int)LanguageName.Persian,

            /// <summary>
            /// Polish
            /// </summary>
            PL = (int)LanguageName.Polish,

            /// <summary>
            /// Portuguese
            /// </summary>
            PT = (int)LanguageName.Portuguese,

            /// <summary>
            /// Punjabi
            /// </summary>
            PA = (int)LanguageName.Punjabi,

            /// <summary>
            /// Quechua
            /// </summary>
            QU = (int)LanguageName.Quechua,

            /// <summary>
            /// Romanian
            /// </summary>
            RO = (int)LanguageName.Romanian,

            /// <summary>
            /// Romansh
            /// </summary>
            RM = (int)LanguageName.Romansh,

            /// <summary>
            /// Rundi
            /// </summary>
            RN = (int)LanguageName.Rundi,

            /// <summary>
            /// Russian
            /// </summary>
            RU = (int)LanguageName.Russian,

            /// <summary>
            /// Northern Sami
            /// </summary>
            SE = (int)LanguageName.Sami,

            /// <summary>
            /// Samoan
            /// </summary>
            SM = (int)LanguageName.Samoan,

            /// <summary>
            /// Sango
            /// </summary>
            SG = (int)LanguageName.Sango,

            /// <summary>
            /// Sanskrit
            /// </summary>
            SA = (int)LanguageName.Sanskrit,

            /// <summary>
            /// Sardinian
            /// </summary>
            SC = (int)LanguageName.Sardinian,

            /// <summary>
            /// Serbian
            /// </summary>
            SR = (int)LanguageName.Serbian,

            /// <summary>
            /// Shona
            /// </summary>
            SN = (int)LanguageName.Shona,

            /// <summary>
            /// Sindhi
            /// </summary>
            SD = (int)LanguageName.Sindhi,

            /// <summary>
            /// Sinhala
            /// </summary>
            SI = (int)LanguageName.Sinhala,

            /// <summary>
            /// Slovak
            /// </summary>
            SK = (int)LanguageName.Slovak,

            /// <summary>
            /// Slovenian
            /// </summary>
            SL = (int)LanguageName.Slovenian,

            /// <summary>
            /// Somali
            /// </summary>
            SO = (int)LanguageName.Somali,

            /// <summary>
            /// Southern Sotho
            /// </summary>
            ST = (int)LanguageName.Sotho,

            /// <summary>
            /// Spanish
            /// </summary>
            ES = (int)LanguageName.Spanish,

            /// <summary>
            /// Sundanese
            /// </summary>
            SU = (int)LanguageName.Sundanese,

            /// <summary>
            /// Swahili
            /// </summary>
            SW = (int)LanguageName.Swahili,

            /// <summary>
            /// Swati
            /// </summary>
            SS = (int)LanguageName.Swati,

            /// <summary>
            /// Swedish
            /// </summary>
            SV = (int)LanguageName.Swedish,

            /// <summary>
            /// Tagalog
            /// </summary>
            TL = (int)LanguageName.Tagalog,

            /// <summary>
            /// Tahitian
            /// </summary>
            TY = (int)LanguageName.Tahitian,

            /// <summary>
            /// Tajik
            /// </summary>
            TG = (int)LanguageName.Tajik,

            /// <summary>
            /// Tamil
            /// </summary>
            TA = (int)LanguageName.Tamil,

            /// <summary>
            /// Tatar
            /// </summary>
            TT = (int)LanguageName.Tatar,

            /// <summary>
            /// Telugu
            /// </summary>
            TE = (int)LanguageName.Telugu,

            /// <summary>
            /// Thai
            /// </summary>
            TH = (int)LanguageName.Thai,

            /// <summary>
            /// Tibetan
            /// </summary>
            BO = (int)LanguageName.Tibetan,

            /// <summary>
            /// Tigrinya
            /// </summary>
            TI = (int)LanguageName.Tigrinya,

            /// <summary>
            /// Tonga
            /// </summary>
            TO = (int)LanguageName.Tonga,

            /// <summary>
            /// Tsonga
            /// </summary>
            TS = (int)LanguageName.Tsonga,

            /// <summary>
            /// Tswana
            /// </summary>
            TN = (int)LanguageName.Tswana,

            /// <summary>
            /// Turkish
            /// </summary>
            TR = (int)LanguageName.Turkish,

            /// <summary>
            /// Turkmen
            /// </summary>
            TK = (int)LanguageName.Turkmen,

            /// <summary>
            /// Twi
            /// </summary>
            TW = (int)LanguageName.Twi,

            /// <summary>
            /// Uighur
            /// </summary>
            UG = (int)LanguageName.Uighur,

            /// <summary>
            /// Ukrainian
            /// </summary>
            UK = (int)LanguageName.Ukrainian,

            /// <summary>
            /// Urdu
            /// </summary>
            UR = (int)LanguageName.Urdu,

            /// <summary>
            /// Uzbek
            /// </summary>
            UZ = (int)LanguageName.Uzbek,

            /// <summary>
            /// Venda
            /// </summary>
            VE = (int)LanguageName.Venda,

            /// <summary>
            /// Vietnamese
            /// </summary>
            VI = (int)LanguageName.Vietnamese,

            /// <summary>
            /// Volapük
            /// </summary>
            VO = (int)LanguageName.Volapuk,

            /// <summary>
            /// Walloon
            /// </summary>
            WA = (int)LanguageName.Walloon,

            /// <summary>
            /// Welsh
            /// </summary>
            CY = (int)LanguageName.Welsh,

            /// <summary>
            /// Wolof
            /// </summary>
            WO = (int)LanguageName.Wolof,

            /// <summary>
            /// Xhosa
            /// </summary>
            XH = (int)LanguageName.Xhosa,

            /// <summary>
            /// Yiddish
            /// </summary>
            YI = (int)LanguageName.Yiddish,

            /// <summary>
            /// Yoruba
            /// </summary>
            YO = (int)LanguageName.Yoruba,

            /// <summary>
            /// Zhuang
            /// </summary>
            ZA = (int)LanguageName.Zhuang,

            /// <summary>
            /// Zulu
            /// </summary>
            ZU = (int)LanguageName.Zulu,
        }


        /// <summary>
        /// Language Code (ISO 639_3) (three letters)
        /// </summary>
        public enum LanguageCode
        {
            /// <summary>
            /// Language is not defined
            /// </summary>
            Null = (int)LanguageName.Null,

            /// <summary>
            /// Abkhazian
            /// </summary>
            ABK = (int)LanguageName.Abkhazian,

            /// <summary>
            /// Afar
            /// </summary>
            AAR = (int)LanguageName.Afar,

            /// <summary>
            /// Afrikaans
            /// </summary>
            AFR = (int)LanguageName.Afrikaans,

            /// <summary>
            /// Akan
            /// </summary>
            AKA = (int)LanguageName.Akan,

            /// <summary>
            /// Albanian
            /// </summary>
            SQI = (int)LanguageName.Albanian,

            /// <summary>
            /// Amharic
            /// </summary>
            AMH = (int)LanguageName.Amharic,

            /// <summary>
            /// Arabic
            /// </summary>
            ARA = (int)LanguageName.Arabic,

            /// <summary>
            /// Aragonese
            /// </summary>
            ARG = (int)LanguageName.Aragonese,

            /// <summary>
            /// Armenian
            /// </summary>
            HYE = (int)LanguageName.Armenian,

            /// <summary>
            /// Assamese
            /// </summary>
            ASM = (int)LanguageName.Assamese,

            /// <summary>
            /// Avaric
            /// </summary>
            AVA = (int)LanguageName.Avaric,

            /// <summary>
            /// Avestan
            /// </summary>
            AVE = (int)LanguageName.Avestan,

            /// <summary>
            /// Aymara
            /// </summary>
            AYM = (int)LanguageName.Aymara,

            /// <summary>
            /// Azerbaijani
            /// </summary>
            AZE = (int)LanguageName.Azerbaijani,

            /// <summary>
            /// Bambara
            /// </summary>
            BAM = (int)LanguageName.Bambara,

            /// <summary>
            /// Bashkir
            /// </summary>
            BAK = (int)LanguageName.Bashkir,

            /// <summary>
            /// Basque
            /// </summary>
            EUS = (int)LanguageName.Basque,

            /// <summary>
            /// Belarusian
            /// </summary>
            BEL = (int)LanguageName.Belarusian,

            /// <summary>
            /// Bengali
            /// </summary>
            BEN = (int)LanguageName.Bengali,

            /// <summary>
            /// Bislama
            /// </summary>
            BIS = (int)LanguageName.Bislama,

            /// <summary>
            /// Bosnian
            /// </summary>
            BOS = (int)LanguageName.Bosnian,

            /// <summary>
            /// Breton
            /// </summary>
            BRE = (int)LanguageName.Breton,

            /// <summary>
            /// Bulgarian
            /// </summary>
            BUL = (int)LanguageName.Bulgarian,

            /// <summary>
            /// Burmese
            /// </summary>
            MYA = (int)LanguageName.Burmese,

            /// <summary>
            /// Catalan
            /// </summary>
            CAT = (int)LanguageName.Catalan,

            /// <summary>
            /// Chamorro
            /// </summary>
            CHA = (int)LanguageName.Chamorro,

            /// <summary>
            /// Chechen
            /// </summary>
            CHE = (int)LanguageName.Chechen,

            /// <summary>
            /// Chichewa
            /// </summary>
            NYA = (int)LanguageName.Chichewa,

            /// <summary>
            /// Chinese
            /// </summary>
            ZHO = (int)LanguageName.Chinese,

            /// <summary>
            /// Church Slavonic
            /// </summary>
            CHU = (int)LanguageName.Slavonic,

            /// <summary>
            /// Chuvash
            /// </summary>
            CHV = (int)LanguageName.Chuvash,

            /// <summary>
            /// Cornish
            /// </summary>
            COR = (int)LanguageName.Cornish,

            /// <summary>
            /// Corsican
            /// </summary>
            COS = (int)LanguageName.Corsican,

            /// <summary>
            /// Cree
            /// </summary>
            CRE = (int)LanguageName.Cree,

            /// <summary>
            /// Croatian
            /// </summary>
            HRV = (int)LanguageName.Croatian,

            /// <summary>
            /// Czech
            /// </summary>
            CES = (int)LanguageName.Czech,

            /// <summary>
            /// Danish
            /// </summary>
            DAN = (int)LanguageName.Danish,

            /// <summary>
            /// Divehi
            /// </summary>
            DIV = (int)LanguageName.Divehi,

            /// <summary>
            /// Dutch
            /// </summary>
            NLD = (int)LanguageName.Dutch,

            /// <summary>
            /// Dzongkha
            /// </summary>
            DZO = (int)LanguageName.Dzongkha,

            /// <summary>
            /// English
            /// </summary>
            ENG = (int)LanguageName.English,

            /// <summary>
            /// Esperanto
            /// </summary>
            EPO = (int)LanguageName.Esperanto,

            /// <summary>
            /// Estonian
            /// </summary>
            EST = (int)LanguageName.Estonian,

            /// <summary>
            /// Ewe
            /// </summary>
            EWE = (int)LanguageName.Ewe,

            /// <summary>
            /// Faroese
            /// </summary>
            FAO = (int)LanguageName.Faroese,

            /// <summary>
            /// Fijian
            /// </summary>
            FIJ = (int)LanguageName.Fijian,

            /// <summary>
            /// Finnish
            /// </summary>
            FIN = (int)LanguageName.Finnish,

            /// <summary>
            /// French
            /// </summary>
            FRA = (int)LanguageName.French,

            /// <summary>
            /// Western Frisian
            /// </summary>
            FRY = (int)LanguageName.Frisian,

            /// <summary>
            /// Fulah
            /// </summary>
            FUL = (int)LanguageName.Fulah,

            /// <summary>
            /// Gaelic
            /// </summary>
            GLA = (int)LanguageName.Gaelic,

            /// <summary>
            /// Galician
            /// </summary>
            GLG = (int)LanguageName.Galician,

            /// <summary>
            /// Ganda
            /// </summary>
            LUG = (int)LanguageName.Ganda,

            /// <summary>
            /// Georgian
            /// </summary>
            KAT = (int)LanguageName.Georgian,

            /// <summary>
            /// German
            /// </summary>
            DEU = (int)LanguageName.German,

            /// <summary>
            /// Greek
            /// </summary>
            ELL = (int)LanguageName.Greek,

            /// <summary>
            /// Kalaallisut
            /// </summary>
            KAL = (int)LanguageName.Kalaallisut,

            /// <summary>
            /// Guarani
            /// </summary>
            GRN = (int)LanguageName.Guarani,

            /// <summary>
            /// Gujarati
            /// </summary>
            GUJ = (int)LanguageName.Gujarati,

            /// <summary>
            /// Haitian
            /// </summary>
            HAT = (int)LanguageName.Haitian,

            /// <summary>
            /// Hausa
            /// </summary>
            HAU = (int)LanguageName.Hausa,

            /// <summary>
            /// Hebrew
            /// </summary>
            HEB = (int)LanguageName.Hebrew,

            /// <summary>
            /// Herero
            /// </summary>
            HER = (int)LanguageName.Herero,

            /// <summary>
            /// Hindi
            /// </summary>
            HIN = (int)LanguageName.Hindi,

            /// <summary>
            /// Hiri Motu
            /// </summary>
            HMO = (int)LanguageName.HiriMotu,

            /// <summary>
            /// Hungarian
            /// </summary>
            HUN = (int)LanguageName.Hungarian,

            /// <summary>
            /// Icelandic
            /// </summary>
            ISL = (int)LanguageName.Icelandic,

            /// <summary>
            /// Ido
            /// </summary>
            IDO = (int)LanguageName.Ido,

            /// <summary>
            /// Igbo
            /// </summary>
            IBO = (int)LanguageName.Igbo,

            /// <summary>
            /// Indonesian
            /// </summary>
            IND = (int)LanguageName.Indonesian,

            /// <summary>
            /// Interlingua
            /// </summary>
            INA = (int)LanguageName.Interlingua,

            /// <summary>
            /// Interlingue
            /// </summary>
            ILE = (int)LanguageName.Interlingue,

            /// <summary>
            /// Inuktitut
            /// </summary>
            IKU = (int)LanguageName.Inuktitut,

            /// <summary>
            /// Inupiaq
            /// </summary>
            IPK = (int)LanguageName.Inupiaq,

            /// <summary>
            /// Irish
            /// </summary>
            GLE = (int)LanguageName.Irish,

            /// <summary>
            /// Italian
            /// </summary>
            ITA = (int)LanguageName.Italian,

            /// <summary>
            /// Japanese
            /// </summary>
            JPN = (int)LanguageName.Japanese,

            /// <summary>
            /// Javanese
            /// </summary>
            JAV = (int)LanguageName.Javanese,

            /// <summary>
            /// Kannada
            /// </summary>
            KAN = (int)LanguageName.Kannada,

            /// <summary>
            /// Kanuri
            /// </summary>
            KAU = (int)LanguageName.Kanuri,

            /// <summary>
            /// Kashmiri
            /// </summary>
            KAS = (int)LanguageName.Kashmiri,

            /// <summary>
            /// Kazakh
            /// </summary>
            KAZ = (int)LanguageName.Kazakh,

            /// <summary>
            /// Central Khmer
            /// </summary>
            KHM = (int)LanguageName.Khmer,

            /// <summary>
            /// Kikuyu
            /// </summary>
            KIK = (int)LanguageName.Kikuyu,

            /// <summary>
            /// Kinyarwanda
            /// </summary>
            KIN = (int)LanguageName.Kinyarwanda,

            /// <summary>
            /// Kirghiz
            /// </summary>
            KIR = (int)LanguageName.Kirghiz,

            /// <summary>
            /// Komi
            /// </summary>
            KOM = (int)LanguageName.Komi,

            /// <summary>
            /// Kongo
            /// </summary>
            KON = (int)LanguageName.Kongo,

            /// <summary>
            /// Korean
            /// </summary>
            KOR = (int)LanguageName.Korean,

            /// <summary>
            /// Kuanyama
            /// </summary>
            KUA = (int)LanguageName.Kuanyama,

            /// <summary>
            /// Kurdish
            /// </summary>
            KUR = (int)LanguageName.Kurdish,

            /// <summary>
            /// Lao
            /// </summary>
            LAO = (int)LanguageName.Lao,

            /// <summary>
            /// Latin
            /// </summary>
            LAT = (int)LanguageName.Latin,

            /// <summary>
            /// Latvian
            /// </summary>
            LAV = (int)LanguageName.Latvian,

            /// <summary>
            /// Limburgan
            /// </summary>
            LIM = (int)LanguageName.Limburgan,

            /// <summary>
            /// Lingala
            /// </summary>
            LIN = (int)LanguageName.Lingala,

            /// <summary>
            /// Lithuanian
            /// </summary>
            LIT = (int)LanguageName.Lithuanian,

            /// <summary>
            /// Luba-Katanga
            /// </summary>
            LUB = (int)LanguageName.LubaKatanga,

            /// <summary>
            /// Luxembourgish
            /// </summary>
            LTZ = (int)LanguageName.Luxembourgish,

            /// <summary>
            /// Macedonian
            /// </summary>
            MKD = (int)LanguageName.Macedonian,

            /// <summary>
            /// Malagasy
            /// </summary>
            MLG = (int)LanguageName.Malagasy,

            /// <summary>
            /// Malay
            /// </summary>
            MSA = (int)LanguageName.Malay,

            /// <summary>
            /// Malayalam
            /// </summary>
            MAL = (int)LanguageName.Malayalam,

            /// <summary>
            /// Maltese
            /// </summary>
            MLT = (int)LanguageName.Maltese,

            /// <summary>
            /// Manx
            /// </summary>
            GLV = (int)LanguageName.Manx,

            /// <summary>
            /// Maori
            /// </summary>
            MRI = (int)LanguageName.Maori,

            /// <summary>
            /// Marathi
            /// </summary>
            MAR = (int)LanguageName.Marathi,

            /// <summary>
            /// Marshallese
            /// </summary>
            MAH = (int)LanguageName.Marshallese,

            /// <summary>
            /// Mongolian
            /// </summary>
            MON = (int)LanguageName.Mongolian,

            /// <summary>
            /// Nauru
            /// </summary>
            NAU = (int)LanguageName.Nauru,

            /// <summary>
            /// Navajo
            /// </summary>
            NAV = (int)LanguageName.Navajo,

            /// <summary>
            /// North Ndebele
            /// </summary>
            NDE = (int)LanguageName.NorthNdebele,

            /// <summary>
            /// South Ndebele
            /// </summary>
            NBL = (int)LanguageName.SouthNdebele,

            /// <summary>
            /// Ndonga
            /// </summary>
            NDO = (int)LanguageName.Ndonga,

            /// <summary>
            /// Nepali
            /// </summary>
            NEP = (int)LanguageName.Nepali,

            /// <summary>
            /// Norwegian
            /// </summary>
            NOR = (int)LanguageName.Norwegian,

            /// <summary>
            /// Norwegian Bokmål
            /// </summary>
            NOB = (int)LanguageName.NorwegianBokmal,

            /// <summary>
            /// Norwegian Nynorsk
            /// </summary>
            NNO = (int)LanguageName.NorwegianNynorsk,

            /// <summary>
            /// Sichuan Yi
            /// </summary>
            III = (int)LanguageName.SichuanYi,

            /// <summary>
            /// Occitan
            /// </summary>
            OCI = (int)LanguageName.Occitan,

            /// <summary>
            /// Ojibwa
            /// </summary>
            OJI = (int)LanguageName.Ojibwa,

            /// <summary>
            /// Oriya
            /// </summary>
            ORI = (int)LanguageName.Oriya,

            /// <summary>
            /// Oromo
            /// </summary>
            ORM = (int)LanguageName.Oromo,

            /// <summary>
            /// Ossetian
            /// </summary>
            OSS = (int)LanguageName.Ossetian,

            /// <summary>
            /// Pali
            /// </summary>
            PLI = (int)LanguageName.Pali,

            /// <summary>
            /// Pashto
            /// </summary>
            PUS = (int)LanguageName.Pashto,

            /// <summary>
            /// Persian
            /// </summary>
            FAS = (int)LanguageName.Persian,

            /// <summary>
            /// Polish
            /// </summary>
            POL = (int)LanguageName.Polish,

            /// <summary>
            /// Portuguese
            /// </summary>
            POR = (int)LanguageName.Portuguese,

            /// <summary>
            /// Punjabi
            /// </summary>
            PAN = (int)LanguageName.Punjabi,

            /// <summary>
            /// Quechua
            /// </summary>
            QUE = (int)LanguageName.Quechua,

            /// <summary>
            /// Romanian
            /// </summary>
            RON = (int)LanguageName.Romanian,

            /// <summary>
            /// Romansh
            /// </summary>
            ROH = (int)LanguageName.Romansh,

            /// <summary>
            /// Rundi
            /// </summary>
            RUN = (int)LanguageName.Rundi,

            /// <summary>
            /// Russian
            /// </summary>
            RUS = (int)LanguageName.Russian,

            /// <summary>
            /// Northern Sami
            /// </summary>
            SME = (int)LanguageName.Sami,

            /// <summary>
            /// Samoan
            /// </summary>
            SMO = (int)LanguageName.Samoan,

            /// <summary>
            /// Sango
            /// </summary>
            SAG = (int)LanguageName.Sango,

            /// <summary>
            /// Sanskrit
            /// </summary>
            SAN = (int)LanguageName.Sanskrit,

            /// <summary>
            /// Sardinian
            /// </summary>
            SRD = (int)LanguageName.Sardinian,

            /// <summary>
            /// Serbian
            /// </summary>
            SRP = (int)LanguageName.Serbian,

            /// <summary>
            /// Shona
            /// </summary>
            SNA = (int)LanguageName.Shona,

            /// <summary>
            /// Sindhi
            /// </summary>
            SND = (int)LanguageName.Sindhi,

            /// <summary>
            /// Sinhala
            /// </summary>
            SIN = (int)LanguageName.Sinhala,

            /// <summary>
            /// Slovak
            /// </summary>
            SLK = (int)LanguageName.Slovak,

            /// <summary>
            /// Slovenian
            /// </summary>
            SLV = (int)LanguageName.Slovenian,

            /// <summary>
            /// Somali
            /// </summary>
            SOM = (int)LanguageName.Somali,

            /// <summary>
            /// Southern Sotho
            /// </summary>
            SOT = (int)LanguageName.Sotho,

            /// <summary>
            /// Spanish
            /// </summary>
            SPA = (int)LanguageName.Spanish,

            /// <summary>
            /// Sundanese
            /// </summary>
            SUN = (int)LanguageName.Sundanese,

            /// <summary>
            /// Swahili
            /// </summary>
            SWA = (int)LanguageName.Swahili,

            /// <summary>
            /// Swati
            /// </summary>
            SSW = (int)LanguageName.Swati,

            /// <summary>
            /// Swedish
            /// </summary>
            SWE = (int)LanguageName.Swedish,

            /// <summary>
            /// Tagalog
            /// </summary>
            TGL = (int)LanguageName.Tagalog,

            /// <summary>
            /// Tahitian
            /// </summary>
            TAH = (int)LanguageName.Tahitian,

            /// <summary>
            /// Tajik
            /// </summary>
            TGK = (int)LanguageName.Tajik,

            /// <summary>
            /// Tamil
            /// </summary>
            TAM = (int)LanguageName.Tamil,

            /// <summary>
            /// Tatar
            /// </summary>
            TAT = (int)LanguageName.Tatar,

            /// <summary>
            /// Telugu
            /// </summary>
            TEL = (int)LanguageName.Telugu,

            /// <summary>
            /// Thai
            /// </summary>
            THA = (int)LanguageName.Thai,

            /// <summary>
            /// Tibetan
            /// </summary>
            BOD = (int)LanguageName.Tibetan,

            /// <summary>
            /// Tigrinya
            /// </summary>
            TIR = (int)LanguageName.Tigrinya,

            /// <summary>
            /// Tonga
            /// </summary>
            TON = (int)LanguageName.Tonga,

            /// <summary>
            /// Tsonga
            /// </summary>
            TSO = (int)LanguageName.Tsonga,

            /// <summary>
            /// Tswana
            /// </summary>
            TSN = (int)LanguageName.Tswana,

            /// <summary>
            /// Turkish
            /// </summary>
            TUR = (int)LanguageName.Turkish,

            /// <summary>
            /// Turkmen
            /// </summary>
            TUK = (int)LanguageName.Turkmen,

            /// <summary>
            /// Twi
            /// </summary>
            TWI = (int)LanguageName.Twi,

            /// <summary>
            /// Uighur
            /// </summary>
            UIG = (int)LanguageName.Uighur,

            /// <summary>
            /// Ukrainian
            /// </summary>
            UKR = (int)LanguageName.Ukrainian,

            /// <summary>
            /// Urdu
            /// </summary>
            URD = (int)LanguageName.Urdu,

            /// <summary>
            /// Uzbek
            /// </summary>
            UZB = (int)LanguageName.Uzbek,

            /// <summary>
            /// Venda
            /// </summary>
            VEN = (int)LanguageName.Venda,

            /// <summary>
            /// Vietnamese
            /// </summary>
            VIE = (int)LanguageName.Vietnamese,

            /// <summary>
            /// Volapük
            /// </summary>
            VOL = (int)LanguageName.Volapuk,

            /// <summary>
            /// Walloon
            /// </summary>
            WLN = (int)LanguageName.Walloon,

            /// <summary>
            /// Welsh
            /// </summary>
            CYM = (int)LanguageName.Welsh,

            /// <summary>
            /// Wolof
            /// </summary>
            WOL = (int)LanguageName.Wolof,

            /// <summary>
            /// Xhosa
            /// </summary>
            XHO = (int)LanguageName.Xhosa,

            /// <summary>
            /// Yiddish
            /// </summary>
            YID = (int)LanguageName.Yiddish,

            /// <summary>
            /// Yoruba
            /// </summary>
            YOR = (int)LanguageName.Yoruba,

            /// <summary>
            /// Zhuang
            /// </summary>
            ZHA = (int)LanguageName.Zhuang,

            /// <summary>
            /// Zulu
            /// </summary>
            ZUL = (int)LanguageName.Zulu,
        }


        /// <summary>
        /// Language version
        /// </summary>
        public enum LanguageVersion
        {
            /// <summary>
            /// International
            /// </summary>
            International = 1,

            /// <summary>
            /// National
            /// </summary>
            National = 2
        }

    }
}
