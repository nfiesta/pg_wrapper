﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Abstract data view
            /// </summary>
            public abstract class ALinqView
            : ADatabaseTable, ILinqView
            {

                #region Constructors

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="data">Data</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ALinqView(
                    IDatabase database,
                    DataTable data = null,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                         database: database,
                         data: data,
                         condition: condition,
                         limit: limit,
                         loadingTime: loadingTime,
                         storedProcedure: storedProcedure)
                { }

                /// <summary>
                /// Constructor
                /// </summary>
                /// <param name="database">Database connection</param>
                /// <param name="rows">List of data rows</param>
                /// <param name="condition">Condition</param>
                /// <param name="limit">Rows count limit</param>
                /// <param name="loadingTime">Time used for loading data from database</param>
                /// <param name="storedProcedure">Database stored procedure that has provided data for current object</param>
                public ALinqView(
                    IDatabase database,
                    IEnumerable<DataRow> rows,
                    string condition = null,
                    Nullable<int> limit = null,
                    Nullable<double> loadingTime = null,
                    string storedProcedure = null)
                    : base(
                         database: database,
                         rows: rows,
                         condition: condition,
                         limit: limit,
                         loadingTime: loadingTime,
                         storedProcedure: storedProcedure)
                { }

                #endregion Constructors

            }


            /// <summary>
            /// Abstract row in data view
            /// </summary>
            /// <typeparam name="T">Abstract data view</typeparam>
            /// <remarks>
            /// Constructor
            /// </remarks>
            /// <param name="composite">List of records</param>
            /// <param name="data">Data row with record</param>
            public abstract class ALinqViewEntry<T>(T composite = null, DataRow data = null)
                : ADatabaseTableEntry<T>(composite: composite, data: data),
                ILinqViewEntry<T>
                where T : ALinqView
            {
            }

        }
    }
}
