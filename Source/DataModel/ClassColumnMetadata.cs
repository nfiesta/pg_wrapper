﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Description of table column
            /// </summary>
            public class ColumnMetadata
            {

                #region Private Fields

                /// <summary>
                /// Column name
                /// </summary>
                private string name;

                /// <summary>
                /// Column name in database
                /// </summary>
                private string dbName;

                /// <summary>
                /// Column data type
                /// </summary>
                private string dataType;

                /// <summary>
                /// Column data type in database
                /// </summary>
                private string dbDataType;

                /// <summary>
                /// Column new data type
                /// </summary>
                private string newDataType;

                /// <summary>
                /// Column is part of primary key
                /// </summary>
                private bool pKey;

                /// <summary>
                /// Not null;
                /// </summary>
                private bool notNull;

                /// <summary>
                /// Column default value
                /// </summary>
                private string defaultValue;

                /// <summary>
                /// Function call
                /// </summary>
                private string funcCall;

                /// <summary>
                /// Is column loaded from database?
                /// </summary>
                private bool projection;

                /// <summary>
                /// Is column visible in DataGridView?
                /// </summary>
                private bool visible;

                /// <summary>
                /// Is it possible to edit column value in DataGridView?
                /// </summary>
                private bool readOnly;

                /// <summary>
                /// Format of the number in DataGridView Cell
                /// </summary>
                private string numericFormat;

                /// <summary>
                /// Alignment of the content in DataGridView Cell
                /// </summary>
                private DataGridViewContentAlignment alignment;

                /// <summary>
                /// Column header text in national language
                /// </summary>
                private string headerTextCs;

                /// <summary>
                /// Column header text in English
                /// </summary>
                private string headerTextEn;

                /// <summary>
                /// Column tool tip text in national language
                /// </summary>
                private string toolTipTextCs;

                /// <summary>
                /// Column tool tip text in English
                /// </summary>
                private string toolTipTextEn;

                /// <summary>
                /// Comment for the column
                /// </summary>
                private string comment;

                /// <summary>
                /// Unit of measure
                /// </summary>
                private string unit;

                /// <summary>
                /// Column width in DataGridView
                /// </summary>
                private int width;

                /// <summary>
                /// Column origin is in database table
                /// </summary>
                private bool elemental;

                /// <summary>
                /// Order of the column in DataGridView
                /// </summary>
                private int displayIndex;

                /// <summary>
                /// <para lang="cs">Volby pro JSON serializer</para>
                /// <para lang="en">JSON serializer options</para>
                /// </summary>
                private JsonSerializerOptions serializerOptions;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Constructor for description of table column
                /// </summary>
                public ColumnMetadata()
                {
                    SerializerOptions = null;

                    name = null;
                    dbName = null;
                    dataType = null;
                    dbDataType = null;
                    newDataType = null;
                    pKey = false;
                    notNull = false;
                    defaultValue = null;
                    funcCall = null;
                    projection = true;
                    visible = false;
                    readOnly = false;
                    numericFormat = null;
                    alignment = DataGridViewContentAlignment.MiddleLeft;
                    headerTextCs = null;
                    headerTextEn = null;
                    toolTipTextCs = null;
                    toolTipTextEn = null;
                    comment = null;
                    unit = null;
                    width = 0;
                    elemental = true;
                    displayIndex = 0;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Column name
                /// </summary>
                public string Name
                {
                    get
                    {
                        return name;
                    }
                    set
                    {
                        name = value;
                    }
                }

                /// <summary>
                /// Column name in database nfiesta
                /// </summary>
                public string DbName
                {
                    get
                    {
                        return dbName;
                    }
                    set
                    {
                        dbName = value;
                    }
                }

                /// <summary>
                /// Column data type
                /// </summary>
                public string DataType
                {
                    get
                    {
                        return dataType;
                    }
                    set
                    {
                        dataType = value;
                    }
                }

                /// <summary>
                /// Column data type in database
                /// </summary>
                public string DbDataType
                {
                    get
                    {
                        return dbDataType;
                    }
                    set
                    {
                        dbDataType = value;
                    }
                }

                /// <summary>
                /// Column new data type
                /// </summary>
                public string NewDataType
                {
                    get
                    {
                        return newDataType;
                    }
                    set
                    {
                        newDataType = value;
                    }
                }

                /// <summary>
                /// Column is part of primary key
                /// </summary>
                public bool PKey
                {
                    get
                    {
                        return pKey;
                    }
                    set
                    {
                        pKey = value;
                    }
                }

                /// <summary>
                /// Not null;
                /// </summary>
                public bool NotNull
                {
                    get
                    {
                        return notNull;
                    }
                    set
                    {
                        notNull = value;
                    }
                }

                /// <summary>
                /// Column default value
                /// </summary>
                public string DefaultValue
                {
                    get
                    {
                        return defaultValue;
                    }
                    set
                    {
                        defaultValue = value;
                    }
                }

                /// <summary>
                /// Function call
                /// </summary>
                public string FuncCall
                {
                    get
                    {
                        return funcCall;
                    }
                    set
                    {
                        funcCall = value;
                    }
                }

                /// <summary>
                /// Is column loaded from database?
                /// </summary>
                public bool Projection
                {
                    get
                    {
                        return projection;
                    }
                    set
                    {
                        projection = value;
                    }
                }

                /// <summary>
                /// Is column visible in DataGridView?
                /// </summary>
                public bool Visible
                {
                    get
                    {
                        return visible;
                    }
                    set
                    {
                        visible = value;
                    }
                }

                /// <summary>
                /// Is it possible to edit column value in DataGridView?
                /// </summary>
                public bool ReadOnly
                {
                    get
                    {
                        return readOnly;
                    }
                    set
                    {
                        readOnly = value;
                    }
                }

                /// <summary>
                /// Format of the number in DataGridView Cell
                /// </summary>
                public string NumericFormat
                {
                    get
                    {
                        return numericFormat;
                    }
                    set
                    {
                        numericFormat = value;
                    }
                }

                /// <summary>
                /// Alignment of the content in DataGridView Cell
                /// </summary>
                public DataGridViewContentAlignment Alignment
                {
                    get
                    {
                        return alignment;
                    }
                    set
                    {
                        alignment = value;
                    }
                }

                /// <summary>
                /// Column header text in national language
                /// </summary>
                public string HeaderTextCs
                {
                    get
                    {
                        return headerTextCs;
                    }
                    set
                    {
                        headerTextCs = value;
                    }
                }

                /// <summary>
                /// Column header text in English
                /// </summary>
                public string HeaderTextEn
                {
                    get
                    {
                        return headerTextEn;
                    }
                    set
                    {
                        headerTextEn = value;
                    }
                }

                /// <summary>
                /// Column tool tip text in national language
                /// </summary>
                public string ToolTipTextCs
                {
                    get
                    {
                        return toolTipTextCs;
                    }
                    set
                    {
                        toolTipTextCs = value;
                    }
                }

                /// <summary>
                /// Column tool tip text in English
                /// </summary>
                public string ToolTipTextEn
                {
                    get
                    {
                        return toolTipTextEn;
                    }
                    set
                    {
                        toolTipTextEn = value;
                    }
                }

                /// <summary>
                /// Comment for the column
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return comment;
                    }
                    set
                    {
                        comment = value;
                    }
                }

                /// <summary>
                /// Unit of measure
                /// </summary>
                public string Unit
                {
                    get
                    {
                        return unit;
                    }
                    set
                    {
                        unit = value;
                    }
                }

                /// <summary>
                /// Column width in DataGridView
                /// </summary>
                public int Width
                {
                    get
                    {
                        return width;
                    }
                    set
                    {
                        width = value;
                    }
                }

                /// <summary>
                /// Column origin is in database table
                /// </summary>
                public bool Elemental
                {
                    get
                    {
                        return elemental;
                    }
                    set
                    {
                        elemental = value;
                    }
                }

                /// <summary>
                /// Order of the column in DataGridView
                /// </summary>
                public int DisplayIndex
                {
                    get
                    {
                        return displayIndex;
                    }
                    set
                    {
                        displayIndex = value;
                    }
                }

                /// <summary>
                /// <para lang="cs">Volby pro JSON serializer</para>
                /// <para lang="en">JSON serializer options</para>
                /// </summary>
                [JsonIgnore]
                private JsonSerializerOptions SerializerOptions
                {
                    get
                    {
                        return serializerOptions ??
                            new()
                            {
                                Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                                WriteIndented = true
                            };
                    }
                    set
                    {
                        serializerOptions = value ??
                            new()
                            {
                                Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                                WriteIndented = true
                            };
                    }
                }

                /// <summary>
                /// Object serialization to JSON format
                /// </summary>
                [JsonIgnore]
                public string JSON
                {
                    get
                    {
                        return JsonSerializer.Serialize(
                            value: this,
                            options: SerializerOptions);
                    }
                }

                #endregion Properties


                #region Static Methods

                /// <summary>
                /// Restoring an object from JSON format
                /// </summary>
                /// <param name="json">
                /// JSON text
                /// </param>
                /// <returns>
                /// Description of table column
                /// </returns>
                public static ColumnMetadata Deserialize(string json)
                {
                    return
                        JsonSerializer.Deserialize<ColumnMetadata>(json: json);
                }

                #endregion Static Methods


                #region Methods

                /// <summary>
                /// SQL statement for select column from database
                /// </summary>
                /// <param name="alias">Table alias</param>
                /// <param name="cols">Columns metadata</param>
                /// <param name="isLastOne">Column is last one in SELECT statement</param>
                /// <param name="setToNull">Set column value to null</param>
                /// <returns>SQL statement for select column from database</returns>
                public string SQL(
                    Dictionary<string, ColumnMetadata> cols,
                    string alias = null,
                    bool isLastOne = false,
                    bool setToNull = false)
                {
                    string colName, colType, sep;
                    int maxLenColName, maxLenColType;

                    if (!setToNull)
                    {
                        colName = String.IsNullOrEmpty(FuncCall) ?
                                String.IsNullOrEmpty(alias) ? DbName : $"{alias}.{DbName}" :
                                FuncCall.Replace("{0}", String.IsNullOrEmpty(alias) ? DbName : $"{alias}.{DbName}");
                    }
                    else
                    {
                        colName = "null";
                    }

                    maxLenColName = cols.Values
                        .Select(a => String.IsNullOrEmpty(a.FuncCall) ?
                            String.IsNullOrEmpty(alias) ? DbName : $"{alias}.{a.DbName}" :
                            a.FuncCall.Replace("{0}", String.IsNullOrEmpty(alias) ? a.DbName : $"{alias}.{a.DbName}"))
                        .Select(a => a.Length).Max();

                    colName = colName.PadRight(maxLenColName);

                    colType = String.IsNullOrEmpty(NewDataType) ? String.Empty : $"::{NewDataType}";

                    maxLenColType = cols.Values
                        .Select(a => String.IsNullOrEmpty(a.NewDataType) ? String.Empty : $"::{a.NewDataType}")
                        .Select(a => a.Length).Max();

                    colType = colType.PadRight(maxLenColType);

                    sep = isLastOne ? String.Empty : ",";

                    colName = $"{colName}{colType} AS {Name}{sep}{Environment.NewLine}";

                    return colName;
                }

                /// <summary>
                /// Returns copy of the current object
                /// </summary>
                /// <returns>Returns copy of the current object</returns>
                public ColumnMetadata Copy()
                {
                    return new ColumnMetadata()
                    {
                        Name = Name,
                        DbName = DbName,
                        DataType = DataType,
                        DbDataType = DbDataType,
                        NewDataType = NewDataType,
                        PKey = PKey,
                        NotNull = NotNull,
                        DefaultValue = DefaultValue,
                        FuncCall = FuncCall,
                        Projection = Projection,
                        Visible = Visible,
                        ReadOnly = ReadOnly,
                        NumericFormat = NumericFormat,
                        Alignment = Alignment,
                        HeaderTextCs = HeaderTextCs,
                        HeaderTextEn = HeaderTextEn,
                        ToolTipTextCs = ToolTipTextCs,
                        ToolTipTextEn = ToolTipTextEn,
                        Comment = Comment,
                        Unit = Unit,
                        Width = Width,
                        Elemental = Elemental,
                        DisplayIndex = DisplayIndex
                    };
                }

                /// <summary>
                /// Converts the value of this instance to its equivalent string representation
                /// </summary>
                /// <returns>Converts the value of this instance to its equivalent string representation</returns>
                public override string ToString()
                {
                    return JSON;
                }

                /// <summary>
                /// Determines whether specified object is equal to the current object
                /// </summary>
                /// <param name="obj">Speciefied object</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not ColumnMetadata Type, return False
                    if (obj is not ColumnMetadata)
                    {
                        return false;
                    }

                    return
                        JSON == ((ColumnMetadata)obj).JSON;
                }

                /// <summary>
                /// Returns the hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        JSON.GetHashCode();
                }

                #endregion Methods

            }

            /// <summary>
            /// DataGridViewContentAlignment
            /// </summary>
            public enum DataGridViewContentAlignment
            {
                /// <summary>
                /// NotSet
                /// </summary>
                NotSet = 0x000,

                /// <summary>
                ///  Content is vertically aligned at the top, and horizontally aligned
                ///  on the left.
                /// </summary>
                TopLeft = 0x001,

                /// <summary>
                ///  Content is vertically aligned at the top, and horizontally aligned
                ///  at the center.
                /// </summary>
                TopCenter = 0x002,

                /// <summary>
                ///  Content is vertically aligned at the top, and horizontally aligned
                ///  on the right.
                /// </summary>
                TopRight = 0x004,

                /// <summary>
                ///  Content is vertically aligned in the middle, and horizontally aligned
                ///  on the left.
                /// </summary>
                MiddleLeft = 0x010,

                /// <summary>
                ///  Content is vertically aligned in the middle, and horizontally aligned
                ///  at the center.
                /// </summary>
                MiddleCenter = 0x020,

                /// <summary>
                ///  Content is vertically aligned in the middle, and horizontally aligned
                ///  on the right.
                /// </summary>
                MiddleRight = 0x040,

                /// <summary>
                ///  Content is vertically aligned at the bottom, and horizontally aligned
                ///  on the left.
                /// </summary>
                BottomLeft = 0x100,

                /// <summary>
                ///  Content is vertically aligned at the bottom, and horizontally aligned
                ///  at the center.
                /// </summary>
                BottomCenter = 0x200,

                /// <summary>
                ///  Content is vertically aligned at the bottom, and horizontally aligned
                ///  on the right.
                /// </summary>
                BottomRight = 0x400,
            }

        }
    }
}
