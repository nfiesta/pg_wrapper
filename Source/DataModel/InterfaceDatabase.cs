﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace DataModel
        {

            /// <summary>
            /// Interface for database
            /// </summary>
            public interface IDatabase
            {

                #region Properties

                /// <summary>
                /// PostgreSQL database connection (not-null) (read-only)
                /// </summary>
                ZaJi.PostgreSQL.PostgreSQLWrapper Postgres { get; }

                /// <summary>
                /// List of database schemas (not-null) (read-only)
                /// </summary>
                System.Collections.Generic.List<IDatabaseSchema> Schemas { get; }

                /// <summary>
                /// Database managers group role (not-null) (read-only)
                /// </summary>
                string ManagerGroupRoleName { get; }

                /// <summary>
                /// Is selected login role member of group role for database managers? (not-null) (read-only)
                /// </summary>
                bool UserIsManager { get; }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Object initialization
                /// </summary>
                void Initialize();

                /// <summary>
                /// Connect to database and load catalog data
                /// </summary>
                /// <param name="applicationName">Application name</param>
                void Connect(string applicationName);

                #endregion Methods

            }

            /// <summary>
            /// Format of the file with exported data
            /// </summary>
            public enum ExportFileFormat
            {
                /// <summary>
                /// Xml file
                /// </summary>
                Xml = 100,

                /// <summary>
                /// Sql file with insert commands
                /// </summary>
                SqlInsert = 200,

                /// <summary>
                /// Sql file with one insert command
                /// </summary>
                SqlInsertAll = 300
            }

        }
    }
}
