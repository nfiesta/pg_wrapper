﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Unicode;

namespace ZaJi
{
    namespace PostgreSQL
    {

        /// <summary>
        /// <para lang="cs">Nastavení</para>
        /// <para lang="en">Setting</para>
        /// </summary>
        public class Setting
        {

            #region Constants

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Jméno aplikace</para>
            /// <para lang="en">Default value - Application name</para>
            /// </summary>
            private const string DefaultApplicationName = "PostgreSQLWrapper";

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Jazyk zobrazovaných popisků</para>
            /// <para lang="en">Default value - Language of displayed labels</para>
            /// </summary>
            private const LanguageVersion DefaultLanguageVersion = LanguageVersion.International;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - IP adresa nebo jméno databázového serveru PostgreSQL</para>
            /// <para lang="en">Default value - IP address or name of the PostgreSQL database server</para>
            /// </summary>
            private const string DefaultHost = "localhost";

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Seznam IP adres nebo jmen databázových serverů PostgreSQL</para>
            /// <para lang="en">Default value - IP address list or name list of the PostgreSQL database servers</para>
            /// </summary>
            private static readonly List<string> DefaultHosts = ["localhost"];

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - TCP port databázového serveru PostgreSQL</para>
            /// <para lang="en">Default value - TCP port of the PostgreSQL database server</para>
            /// </summary>
            private const int DefaultPort = 5432;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Seznam TCP portů databázových serverů PostgreSQL</para>
            /// <para lang="en">Default value - TCP port list of the PostgreSQL database servers</para>
            /// </summary>
            private static readonly List<int> DefaultPorts = [5432];

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Jméno databáze</para>
            /// <para lang="en">Default value - Database name</para>
            /// </summary>
            private const string DefaultDatabase = "nfi_data";

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Seznam databázových jmen</para>
            /// <para lang="en">Default value - List of database names</para>
            /// </summary>
            private static readonly List<string> DefaultDatabases = ["nfi_data"];

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Jméno uživatele</para>
            /// <para lang="en">Default value - User name</para>
            /// </summary>
            private const string DefaultUserName = "postgres";

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Seznam jmen uživatelů</para>
            /// <para lang="en">Default value - List of user names</para>
            /// </summary>
            private static readonly List<string> DefaultUserNames = ["postgres"];

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Doba čekání na vykonání příkazu v sekundách</para>
            /// <para lang="en">Default value - Waiting time for command execution in seconds</para>
            /// </summary>
            private static readonly int DefaultCommandTimeout = 30;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Doba v sekundách, po které je obnoveno připojení</para>
            /// <para lang="en">Default value - Time in seconds after which the connection is restored</para>
            /// </summary>
            private static readonly int DefaultKeepAlive = 0;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Posílat TCP keep alive podle defaultního nastavení systému</para>
            /// <para lang="en">Default value - Whether to use TCP keepalive with system defaults if overrides isn't specified</para>
            /// </summary>
            private static readonly bool DefaultTcpKeepAlive = false;

            /// <summary>
            /// <para lang="cs">Výchozí hodnota - Počet sekund neaktivního připojení než je poslán TCP keepalive dotaz</para>
            /// <para lang="en">Default value - The number of seconds of connection inactivity before a TCP keepalive query is sent.</para>
            /// </summary>
            private static readonly int DefaultTcpKeepAliveTime = 0;

            /// <summary>
            /// Default value - The interval, in seconds, between when successive keep-alive packets are sent if no acknowledgement is received.
            /// </summary>
            private static readonly int DefaultTcpKeepAliveInterval = DefaultTcpKeepAliveTime;

            #endregion Constants


            #region Private Fields

            /// <summary>
            /// <para lang="cs">Jméno aplikace</para>
            /// <para lang="en">Application name</para>
            /// </summary>
            private string applicationName;

            /// <summary>
            /// <para lang="cs">Jazyk zobrazovaných popisků</para>
            /// <para lang="en">Language of displayed labels</para>
            /// </summary>
            private LanguageVersion languageVersion;

            /// <summary>
            /// <para lang="cs">IP adresa nebo jméno databázového serveru PostgreSQL</para>
            /// <para lang="en">IP address or name of the PostgreSQL database server</para>
            /// </summary>
            private string host;

            /// <summary>
            /// <para lang="cs">Seznam IP adres nebo jmen databázových serverů PostgreSQL</para>
            /// <para lang="en">IP address list or name list of the PostgreSQL database servers</para>
            /// </summary>
            private List<string> hosts;

            /// <summary>
            /// <para lang="cs">TCP port databázového serveru PostgreSQL</para>
            /// <para lang="en">TCP port of the PostgreSQL database server</para>
            /// </summary>
            private Nullable<int> port;

            /// <summary>
            /// <para lang="cs">Seznam TCP portů databázových serverů PostgreSQL</para>
            /// <para lang="en">TCP port list of the PostgreSQL database servers</para>
            /// </summary>
            private List<int> ports;

            /// <summary>
            /// <para lang="cs">Jméno databáze</para>
            /// <para lang="en">Database name</para>
            /// </summary>
            private string database;

            /// <summary>
            /// <para lang="cs">Seznam databázových jmen</para>
            /// <para lang="en">List of database names</para>
            /// </summary>
            private List<string> databases;

            /// <summary>
            /// <para lang="cs">Jméno uživatele</para>
            /// <para lang="en">User name</para>
            /// </summary>
            private string userName;

            /// <summary>
            /// <para lang="cs">Seznam jmen uživatelů</para>
            /// <para lang="en">List of user names</para>
            /// </summary>
            private List<string> userNames;

            /// <summary>
            /// <para lang="cs">Doba čekání na vykonání příkazu v sekundách</para>
            /// <para lang="en">Waiting time for command execution in seconds</para>
            /// </summary>
            private Nullable<int> commandTimeout;

            /// <summary>
            /// <para lang="cs">Doba v sekundách, po které je obnoveno připojení</para>
            /// <para lang="en">Time in seconds after which the connection is restored</para>
            /// </summary>
            private Nullable<int> keepAlive;

            /// <summary>
            /// <para lang="cs">Posílat TCP keep alive podle defaultního nastavení systému</para>
            /// <para lang="en">Whether to use TCP keepalive with system defaults if overrides isn't specified</para>
            /// </summary>
            private Nullable<bool> tcpKeepAlive;

            /// <summary>
            /// <para lang="cs">Počet sekund neaktivního připojení než je poslán TCP keepalive dotaz</para>
            /// <para lang="en">The number of seconds of connection inactivity before a TCP keepalive query is sent.</para>
            /// </summary>
            private Nullable<int> tcpKeepAliveTime;

            /// <summary>
            /// The interval, in seconds, between when successive keep-alive packets are sent if no acknowledgement is received.
            /// </summary>
            private Nullable<int> tcpKeepAliveInterval;

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            private JsonSerializerOptions serializerOptions;

            #endregion Private Fields


            #region Constructor

            /// <summary>
            /// <para lang="cs">Konstruktor objektu</para>
            /// <para lang="en">Object constructor</para>
            /// </summary>
            public Setting()
            {
                SerializerOptions = null;

                ApplicationName = DefaultApplicationName;
                LanguageVersion = DefaultLanguageVersion;
                Host = DefaultHost;
                Hosts = DefaultHosts;
                Port = DefaultPort;
                Ports = DefaultPorts;
                Database = DefaultDatabase;
                Databases = DefaultDatabases;
                UserName = DefaultUserName;
                UserNames = DefaultUserNames;
                CommandTimeout = DefaultCommandTimeout;
                KeepAlive = DefaultKeepAlive;
                TcpKeepAlive = DefaultTcpKeepAlive;
                TcpKeepAliveTime = DefaultTcpKeepAliveTime;
                TcpKeepAliveInterval = DefaultTcpKeepAliveInterval;
            }

            #endregion Constructor


            #region Properties

            /// <summary>
            /// <para lang="cs">Jméno aplikace</para>
            /// <para lang="en">Application name</para>
            /// </summary>
            public string ApplicationName
            {
                get
                {
                    return String.IsNullOrEmpty(value: applicationName) ?
                        DefaultApplicationName :
                        applicationName;
                }
                set
                {
                    applicationName = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Jazyk zobrazovaných popisků</para>
            /// <para lang="en">Language of displayed labels</para>
            /// </summary>
            public LanguageVersion LanguageVersion
            {
                get
                {
                    return languageVersion;
                }
                set
                {
                    languageVersion = value;
                }
            }

            /// <summary>
            /// <para lang="cs">IP adresa nebo jméno databázového serveru PostgreSQL</para>
            /// <para lang="en">IP address or name of the PostgreSQL database server</para>
            /// </summary>
            public string Host
            {
                get
                {
                    return String.IsNullOrEmpty(value: host) ?
                        DefaultHost :
                        host;
                }
                set
                {
                    host = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam IP adres nebo jmen databázových serverů PostgreSQL</para>
            /// <para lang="en">IP address list or name list of the PostgreSQL database servers</para>
            /// </summary>
            public List<string> Hosts
            {
                get
                {
                    return
                        [.. (hosts ?? DefaultHosts).OrderBy(a => a)];
                }
                set
                {
                    hosts = value;
                }
            }

            /// <summary>
            /// <para lang="cs">TCP port databázového serveru PostgreSQL</para>
            /// <para lang="en">TCP port of the PostgreSQL database server</para>
            /// </summary>
            public int Port
            {
                get
                {
                    return port ?? DefaultPort;
                }
                set
                {
                    port = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam TCP portů databázových serverů PostgreSQL</para>
            /// <para lang="en">TCP port list of the PostgreSQL database servers</para>
            /// </summary>
            public List<int> Ports
            {
                get
                {
                    return
                        [.. (ports ?? DefaultPorts).OrderBy(a => a)];
                }
                set
                {
                    ports = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Jméno databáze</para>
            /// <para lang="en">Database name</para>
            /// </summary>
            public string Database
            {
                get
                {
                    return String.IsNullOrEmpty(value: database) ?
                        DefaultDatabase :
                        database;
                }
                set
                {
                    database = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam databázových jmen</para>
            /// <para lang="en">List of database names</para>
            /// </summary>
            public List<string> Databases
            {
                get
                {
                    return
                        [.. (databases ?? DefaultDatabases).OrderBy(a => a)];
                }
                set
                {
                    databases = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Jméno uživatele</para>
            /// <para lang="en">User name</para>
            /// </summary>
            public string UserName
            {
                get
                {
                    return String.IsNullOrEmpty(value: userName) ?
                        DefaultUserName :
                        userName;
                }
                set
                {
                    userName = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Seznam jmen uživatelů</para>
            /// <para lang="en">List of user names</para>
            /// </summary>
            public List<string> UserNames
            {
                get
                {
                    return
                        [.. (userNames ?? DefaultUserNames).OrderBy(a => a)];
                }
                set
                {
                    userNames = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Doba čekání na vykonání příkazu v sekundách</para>
            /// <para lang="en">Waiting time for command execution in seconds</para>
            /// </summary>
            public int CommandTimeout
            {
                get
                {
                    return commandTimeout ?? DefaultCommandTimeout;
                }
                set
                {
                    commandTimeout = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Doba v sekundách, po které je obnoveno připojení</para>
            /// <para lang="en">Time in seconds after which the connection is restored</para>
            /// </summary>
            public int KeepAlive
            {
                get
                {
                    return keepAlive ?? DefaultKeepAlive;
                }
                set
                {
                    keepAlive = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Posílat TCP keep alive podle defaultního nastavení systému</para>
            /// <para lang="en">Whether to use TCP keepalive with system defaults if overrides isn't specified</para>
            /// </summary>
            public bool TcpKeepAlive
            {
                get
                {
                    return tcpKeepAlive ?? DefaultTcpKeepAlive;
                }
                set
                {
                    tcpKeepAlive = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Počet sekund neaktivního připojení než je poslán TCP keepalive dotaz</para>
            /// <para lang="en">The number of seconds of connection inactivity before a TCP keepalive query is sent.</para>
            /// </summary>
            public int TcpKeepAliveTime
            {
                get
                {
                    return tcpKeepAliveTime ?? DefaultTcpKeepAliveTime;
                }
                set
                {
                    tcpKeepAliveTime = value;
                }
            }

            /// <summary>
            /// The interval, in seconds, between when successive keep-alive packets are sent if no acknowledgement is received.
            /// </summary>
            public int TcpKeepAliveInterval
            {
                get
                {
                    return tcpKeepAliveInterval ?? DefaultTcpKeepAliveInterval;
                }
                set
                {
                    tcpKeepAliveInterval = value;
                }
            }

            /// <summary>
            /// <para lang="cs">Volby pro JSON serializer</para>
            /// <para lang="en">JSON serializer options</para>
            /// </summary>
            [JsonIgnore]
            private JsonSerializerOptions SerializerOptions
            {
                get
                {
                    return serializerOptions ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
                set
                {
                    serializerOptions = value ??
                        new()
                        {
                            Encoder = JavaScriptEncoder.Create(allowedRanges: UnicodeRanges.All),
                            WriteIndented = true
                        };
                }
            }

            /// <summary>
            /// <para lang="cs">Serializace objektu do formátu JSON</para>
            /// <para lang="en">Object serialization to JSON format</para>
            /// </summary>
            [JsonIgnore]
            public string JSON
            {
                get
                {
                    return JsonSerializer.Serialize(
                        value: this,
                        options: SerializerOptions);
                }
            }

            #endregion Properties


            #region Static Methods

            /// <summary>
            /// <para lang="cs">Obnovení objektu z formátu JSON</para>
            /// <para lang="en">Restoring an object from JSON format</para>
            /// </summary>
            /// <param name="json">
            /// <para lang="cs">JSON text</para>
            /// <para lang="en">JSON text</para>
            /// </param>
            /// <returns>
            /// <para lang="cs">Nastavení</para>
            /// <para lang="en">Setting</para>
            /// </returns>
            public static Setting Deserialize(string json)
            {
                return
                    JsonSerializer.Deserialize<Setting>(json: json);
            }

            #endregion Static Methods

        }

    }
}