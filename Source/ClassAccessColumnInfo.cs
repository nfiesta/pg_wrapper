﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Data;
using System.Data.OleDb;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace Access
    {

        /// <summary>
        /// Připojení k databázi Access
        /// </summary>
        public partial class AccessWrapper
        {

            /// <summary>
            /// Informace o atributu databázové tabulky Access
            /// </summary>
            public class ColumnInfo
            {

                #region Private Fields

                /// <summary>
                /// Datový řádek se záznamem o atributu databázové tabulky
                /// </summary>
                private DataRow data;

                /// <summary>
                /// Databázová tabulka
                /// </summary>
                private TableInfo table;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor
                /// </summary>
                /// <param name="data">Datový řádek se záznamem o atributu databázové tabulky</param>
                public ColumnInfo(DataRow data)
                {
                    Data = data;
                    Table = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Datový řádek se záznamem o atributu databázové tabulky (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                /// <summary>
                /// Databázová tabulka
                /// </summary>
                public TableInfo Table
                {
                    get
                    {
                        return table;
                    }
                    set
                    {
                        table = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// TABLE_CATALOG
                /// </summary>
                public string TableCatalog
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "TABLE_CATALOG",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "TABLE_CATALOG",
                            val: value);
                    }
                }

                /// <summary>
                /// TABLE_SCHEMA
                /// </summary>
                public string TableSchema
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "TABLE_SCHEMA",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "TABLE_SCHEMA",
                            val: value);
                    }
                }

                /// <summary>
                /// TABLE_NAME
                /// </summary>
                public string TableName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "TABLE_NAME",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "TABLE_NAME",
                            val: value);
                    }
                }

                /// <summary>
                /// COLUMN_NAME
                /// </summary>
                public string ColumnName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "COLUMN_NAME",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "COLUMN_NAME",
                            val: value);
                    }
                }

                /// <summary>
                /// COLUMN_GUID
                /// </summary>
                public Nullable<Guid> ColumnGuid
                {
                    get
                    {
                        return Functions.GetNGuidArg(
                            row: Data,
                            name: "COLUMN_GUID",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNGuidArg(
                            row: Data,
                            name: "COLUMN_GUID",
                            val: value);
                    }
                }

                /// <summary>
                /// COLUMN_PROPID
                /// </summary>
                public Nullable<long> ColumnPropid
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: "COLUMN_PROPID",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: "COLUMN_PROPID",
                            val: value);
                    }
                }

                /// <summary>
                /// ORDINAL_POSITION
                /// </summary>
                public Nullable<long> OrdinalPosition
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: "ORDINAL_POSITION",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: "ORDINAL_POSITION",
                            val: value);
                    }
                }

                /// <summary>
                /// COLUMN_HASDEFAULT
                /// </summary>
                public Nullable<bool> ColumnHasDefault
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: "COLUMN_HASDEFAULT",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: "COLUMN_HASDEFAULT",
                            val: value);
                    }
                }

                /// <summary>
                /// COLUMN_DEFAULT
                /// </summary>
                public string ColumnDefault
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "COLUMN_DEFAULT",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "COLUMN_DEFAULT",
                            val: value);
                    }
                }

                /// <summary>
                /// COLUMN_FLAGS
                /// </summary>
                public Nullable<long> ColumnFlags
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: "COLUMN_FLAGS",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: "COLUMN_FLAGS",
                            val: value);
                    }
                }

                /// <summary>
                /// IS_NULLABLE
                /// </summary>
                public Nullable<bool> IsNullable
                {
                    get
                    {
                        return Functions.GetNBoolArg(
                            row: Data,
                            name: "IS_NULLABLE",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNBoolArg(
                            row: Data,
                            name: "IS_NULLABLE",
                            val: value);
                    }
                }

                /// <summary>
                /// DATA_TYPE
                /// </summary>
                public Nullable<int> DataTypeCode
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: "DATA_TYPE",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: "DATA_TYPE",
                            val: value);
                    }
                }

                /// <summary>
                /// DATA_TYPE
                /// </summary>
                public Nullable<OleDbType> DataType
                {
                    get
                    {
                        if (DataTypeCode == null) { return null; }

                        return (OleDbType)DataTypeCode;
                    }
                }

                /// <summary>
                /// TYPE_GUID
                /// </summary>
                public Nullable<Guid> TypeGuid
                {
                    get
                    {
                        return Functions.GetNGuidArg(
                            row: Data,
                            name: "TYPE_GUID",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNGuidArg(
                            row: Data,
                            name: "TYPE_GUID",
                            val: value);
                    }
                }

                /// <summary>
                /// CHARACTER_MAXIMUM_LENGTH
                /// </summary>
                public Nullable<long> CharacterMaximumLength
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: "CHARACTER_MAXIMUM_LENGTH",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: "CHARACTER_MAXIMUM_LENGTH",
                            val: value);
                    }
                }

                /// <summary>
                /// CHARACTER_OCTET_LENGTH
                /// </summary>
                public Nullable<long> CharacterOctetLength
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: "CHARACTER_OCTET_LENGTH",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: "CHARACTER_OCTET_LENGTH",
                            val: value);
                    }
                }

                /// <summary>
                /// NUMERIC_PRECISION
                /// </summary>
                public Nullable<int> NumericPrecision
                {
                    get
                    {
                        return Functions.GetNIntArg(
                            row: Data,
                            name: "NUMERIC_PRECISION",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNIntArg(
                            row: Data,
                            name: "NUMERIC_PRECISION",
                            val: value);
                    }
                }

                /// <summary>
                /// NUMERIC_SCALE
                /// </summary>
                public Nullable<short> NumericScale
                {
                    get
                    {
                        return Functions.GetNShortArg(
                            row: Data,
                            name: "NUMERIC_SCALE",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNShortArg(
                            row: Data,
                            name: "NUMERIC_SCALE",
                            val: value);
                    }
                }

                /// <summary>
                /// DATETIME_PRECISION
                /// </summary>
                public Nullable<long> DateTimePrecision
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: "DATETIME_PRECISION",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: "DATETIME_PRECISION",
                            val: value);
                    }
                }

                /// <summary>
                /// CHARACTER_SET_CATALOG
                /// </summary>
                public string CharacterSetCatalog
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "CHARACTER_SET_CATALOG",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "CHARACTER_SET_CATALOG",
                            val: value);
                    }
                }

                /// <summary>
                /// CHARACTER_SET_SCHEMA
                /// </summary>
                public string CharacterSetSchema
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "CHARACTER_SET_SCHEMA",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "CHARACTER_SET_SCHEMA",
                            val: value);
                    }
                }

                /// <summary>
                /// CHARACTER_SET_NAME
                /// </summary>
                public string CharacterSetName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "CHARACTER_SET_NAME",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "CHARACTER_SET_NAME",
                            val: value);
                    }
                }

                /// <summary>
                /// COLLATION_CATALOG
                /// </summary>
                public string CollationCatalog
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "COLLATION_CATALOG",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "COLLATION_CATALOG",
                            val: value);
                    }
                }

                /// <summary>
                /// COLLATION_SCHEMA
                /// </summary>
                public string CollationSchema
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "COLLATION_SCHEMA",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "COLLATION_SCHEMA",
                            val: value);
                    }
                }

                /// <summary>
                /// COLLATION_NAME
                /// </summary>
                public string CollationName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "COLLATION_NAME",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "COLLATION_NAME",
                            val: value);
                    }
                }

                /// <summary>
                /// DOMAIN_CATALOG
                /// </summary>
                public string DomainCatalog
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "DOMAIN_CATALOG",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "DOMAIN_CATALOG",
                            val: value);
                    }
                }

                /// <summary>
                /// DOMAIN_SCHEMA
                /// </summary>
                public string DomainSchema
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "DOMAIN_SCHEMA",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "DOMAIN_SCHEMA",
                            val: value);
                    }
                }

                /// <summary>
                /// DOMAIN_NAME
                /// </summary>
                public string DomainName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "DOMAIN_NAME",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "DOMAIN_NAME",
                            val: value);
                    }
                }

                /// <summary>
                /// DESCRIPTION
                /// </summary>
                public string Description
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "DESCRIPTION",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "DESCRIPTION",
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určí, zda se zadaný objekt rovná aktuálnímu objektu
                /// </summary>
                /// <param name="obj">Zadný objekt</param>
                /// <returns>ano|ne</returns>
                public override bool Equals(object obj)
                {
                    // Pokud je předaný objekt null, vrátí se hodnota False
                    if (obj == null)
                    {
                        return false;
                    }

                    // Pokud předaný objekt není typ ColumnInfo, vrátí se hodnota False
                    if (obj is not ColumnInfo)
                    {
                        return false;
                    }

                    return
                        (((ColumnInfo)obj).TableName ?? string.Empty) == (TableName ?? String.Empty) &&
                        (((ColumnInfo)obj).ColumnName ?? string.Empty) == (ColumnName ?? String.Empty);
                }

                /// <summary>
                /// Vrací hash kód pro aktuální objekt
                /// </summary>
                /// <returns>Hash kód pro aktuální objekt</returns>
                public override int GetHashCode()
                {
                    return
                        (TableName ?? String.Empty).GetHashCode() ^
                        (ColumnName ?? String.Empty).GetHashCode();
                }

                /// <summary>
                /// Vrací řetězec, který představuje aktuální objekt
                /// </summary>
                /// <returns>Řetězec, který představuje aktuální objekt</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(ColumnInfo)}: ",
                        $"{{",
                        $"{nameof(TableName)}: {Functions.PrepStringArg(arg: TableName)}; ",
                        $"{nameof(ColumnName)}: {Functions.PrepStringArg(arg: ColumnName)}",
                        $"}}");
                }

                #endregion Methods

            }

        }
    }
}