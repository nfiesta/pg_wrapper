﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ZaJi.PostgreSQL.Catalog;

namespace ZaJi
{
    namespace PostgreSQL
    {

        /// <summary>
        /// Připojení k databázi PostgreSQL
        /// </summary>
        public partial class PostgreSQLWrapper
        {

            #region Constants

            /// <summary>
            /// Velikost načítané části Large Objectu (1MB)
            /// </summary>
            public const int LargeObjectBufferSize = 1024 * 1024;

            #endregion Constants


            #region Large Object Methods
            #pragma warning disable 0618

            /// <summary>
            /// Načte data Large Objectu z databáze do objektu MemoryStream
            /// </summary>
            /// <param name="oid">Identifikační číslo Large Objectu</param>
            /// <returns>MemoryStream s daty Large Objectu</returns>
            public MemoryStream LoRead(
                uint oid)
            {
                if (!TryOpenConnection(
                    functionId: 20,
                    functionName: nameof(LoRead)))
                {
                    return null;
                }

                NpgsqlLargeObjectStream largeObject = null;

                MemoryStream ms = null;

                try
                {
                    NpgsqlLargeObjectManager manager =
                        new(
                            connection: Connection);

                    using NpgsqlTransaction transaction = Connection.BeginTransaction();
                    largeObject = manager.OpenRead(oid: oid);
                    ms = new MemoryStream();

                    byte[] buffer = new byte[LargeObjectBufferSize];
                    int bytesRead;

                    while ((bytesRead = largeObject.Read(
                        buffer: buffer,
                        offset: 0,
                        count: buffer.Length)) > 0)
                    {
                        ms.Write(
                            buffer: buffer,
                            offset: 0,
                            count: bytesRead);
                    }

                    largeObject.Close();
                }

                catch (Exception exception)
                {
                    largeObject?.Close();

                    ms = null;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 203,
                        FunctionName = nameof(LoRead),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                return ms;
            }

            /// <summary>
            /// Načte data Large Objectu z databáze do objektu Image
            /// </summary>
            /// <param name="oid">Identifikační číslo Large Objectu</param>
            /// <returns>Obrázek z Large Objectu</returns>
            public Image ImageFromLo(uint oid)
            {
                try
                {
                    MemoryStream ms = LoRead(oid: oid);
                    if ((ms != null) && (ms.CanRead))
                    {
                        Image image = Image.FromStream(stream: ms);
                        ms.Close();
                        return image;
                    }
                }
                catch { }

                return null;
            }

            /// <summary>
            /// Uloží soubor jako Large Object do databáze
            /// </summary>
            /// <param name="path">Cesta k ukládanému souboru</param>
            /// <param name="schemaName">Jméno databázového schéma s tabulkou pro registraci Large Objectu</param>
            /// <param name="tableName">Jméno tabulky pro registraci Large Objectu</param>
            /// <param name="colKeyName">Jméno sloupce s jednoznačným identifikátorem záznamu v tabulce (primární klíč tabulky)</param>
            /// <param name="colLoName">Jméno sloupce s identifikátorem Large Objectu</param>
            /// <param name="strKeyValue">Hodnota identifikátoru záznamu v tabulce (určuje do kterého řádku v tabulce bude Large Object uložen)</param>
            /// <returns>Vrací identifikátor oid uloženého Large Objectu nebo null pokud Large Object uložen nebyl</returns>
            public Nullable<uint> LoUpload(
                string path,
                string schemaName,
                string tableName,
                string colKeyName,
                string colLoName,
                string strKeyValue)
            {
                // Pokud vstupní argumenty metody nejsou zadané, metoda se ukončí
                if (String.IsNullOrEmpty(value: path)) { return null; }
                if (String.IsNullOrEmpty(value: schemaName)) { return null; }
                if (String.IsNullOrEmpty(value: tableName)) { return null; }
                if (String.IsNullOrEmpty(value: colKeyName)) { return null; }
                if (String.IsNullOrEmpty(value: colLoName)) { return null; }
                if (String.IsNullOrEmpty(value: strKeyValue)) { return null; }

                // Pokud připojení k databázi nelze otevřít, metoda se ukončí
                if (!TryOpenConnection(
                       functionId: 21,
                       functionName: "LoUpload")) { return null; }

                // Načte se katalog databáze, pokud nebyl načten dříve
                Catalog.Load();

                // Identifikační číslo řádku
                Nullable<int> keyValue =
                    ZaJi.PostgreSQL.Functions.ConvertToNInt(
                        text: strKeyValue,
                        defaultValue: null);

                // Test zda řádek s požadovaným identifikačním číslem v tabulce existuje
                if (!GetRowExists(
                   schemaName: schemaName,
                   tableName: tableName,
                   colKeyName: colKeyName,
                   keyValue: keyValue) ?? false)
                {
                    // Záznam, do kterého má být Large Object zapsán neexistuje
                    string message =
                        (Dictionary.TryGetValue(key: "msgRowNotExist", out string value) ? value : String.Empty)
                            .Replace(oldValue: "$1", colKeyName)
                            .Replace(oldValue: "$2", Functions.PrepNIntArg(arg: keyValue))
                            .Replace(oldValue: "$3", schemaName)
                            .Replace(oldValue: "$4", tableName);

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 213,
                        FunctionName = nameof(LoUpload),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = new Exception(message: message),
                        CommandObject = null
                    });

                    return null;
                }

                // Zahájení transakce
                NpgsqlTransaction transaction
                    = BeginTransaction();

                if (transaction == null)
                {
                    // Transakce není otevřená
                    return null;
                }

                Nullable<uint> oid;
                NpgsqlLargeObjectStream largeObject = null;
                FileStream fs = null;
                try
                {
                    # region Vytvoření nového large objektu

                    fs = File.OpenRead(path: path);

                    // Vytvoří prázdný Large Object a získá jeho identifikátor oid:
                    NpgsqlLargeObjectManager manager =
                        new(connection: Connection);
                    oid = manager.Create();

                    if (oid == null)
                    {
                        transaction?.Rollback();
                        fs.Close();
                        return null;
                    }

                    // Do Large Objectu zapíše data souboru
                    // (v cyklu po 1MB - určeno v konstantě largeObjectBufferSize)
                    largeObject =
                        manager.OpenReadWrite(
                            oid: oid ?? 0);
                    byte[] buffer = new byte[ZaJi.PostgreSQL.PostgreSQLWrapper.LargeObjectBufferSize];
                    int bytesRead;
                    while ((bytesRead = fs.Read(buffer,
                        offset: 0,
                        count: buffer.Length)) > 0)
                    {
                        largeObject.Write(
                            buffer: buffer,
                            offset: 0,
                            count: bytesRead);
                    }
                    fs.Close();
                    largeObject.Close();

                    #endregion Vytvoření nového large objektu

                    // Parametry příkazů:
                    NpgsqlParameter pKeyValue = new(
                        parameterName: "KeyValue",
                        parameterType: NpgsqlDbType.Integer);

                    NpgsqlParameter pOid = new(
                       parameterName: "Oid",
                       parameterType: NpgsqlDbType.Integer);

                    if (keyValue != null)
                    {
                        pKeyValue.Value = (int)keyValue;
                    }
                    else
                    {
                        pKeyValue.Value = DBNull.Value;
                    }

                    if (oid != null)
                    {
                        pOid.Value = (int)oid;
                    }
                    else
                    {
                        pOid.Value = DBNull.Value;
                    }

                    // Nastavení vlastníka large objektu
                    string commandText;
                    NpgsqlCommand command;
                    if (Catalog.Schemas.Items
                        .Where(a => a.Name == "public_functions")
                        .Any())
                    {
                        commandText = String.Concat(
                        $"SELECT{Environment.NewLine}",
                        $"    public_functions.fn_set_lo_owner(@Oid);{Environment.NewLine}");

                        command =
                             new NpgsqlCommand()
                             {
                                 Connection = Connection,
                                 Transaction = transaction,
                                 CommandText = commandText
                             };
                        command.Parameters.Add(value: pOid);

                        command.ExecuteNonQuery();
                    }
                    else
                    {
                        string sql = String.Concat(
                            $"DO {Environment.NewLine}",
                            $"$$ {Environment.NewLine}",
                            $"BEGIN {Environment.NewLine}",
                            $"    ALTER LARGE OBJECT {oid} OWNER TO \"adm_nfi_data\"; {Environment.NewLine}",
                            $"    GRANT SELECT ON LARGE OBJECT {oid} TO \"usr_nfi_data\"; {Environment.NewLine}",
                            $"END {Environment.NewLine}",
                            $"$$; {Environment.NewLine}");

                        command =
                             new NpgsqlCommand()
                             {
                                 Connection = Connection,
                                 Transaction = transaction,
                                 CommandText = sql
                             };
                        command.ExecuteNonQuery();
                    }

                    //// Získá oid dříve zaregistrovaného Large Objectu
                    //commandText = String.Concat(
                    //    $"SELECT{Environment.NewLine}",
                    //    $"    a.{colLoName} AS {colLoName}{Environment.NewLine}",
                    //    $"FROM{Environment.NewLine}",
                    //    $"    {schemaName}.{tableName} AS a{Environment.NewLine}",
                    //    $"WHERE{Environment.NewLine}",
                    //    $"    a.{colKeyName} = @KeyValue;{Environment.NewLine}");

                    //command =
                    //     new NpgsqlCommand()
                    //     {
                    //         Connection = Connection,
                    //         Transaction = transaction,
                    //         CommandText = commandText
                    //     };
                    //command.Parameters.Add(value: pKeyValue);

                    //object obj = command.ExecuteScalar();

                    //string strOldOid =
                    //    (obj == null)
                    //        ? String.Empty
                    //        : obj.ToString();

                    //Nullable<uint> oldOid =
                    //    ZaJi.PostgreSQL.Functions.ConvertToNUInt(
                    //        text: strOldOid,
                    //        defaultValue: null);

                    //// Pokud existuje dříve zaregistrovaný Large Object, pak se odstraní
                    //if (oldOid != null)
                    //{
                    //    try
                    //    {
                    //        NpgsqlLargeObjectManager deleteManager =
                    //            new NpgsqlLargeObjectManager(
                    //                connection: Connection);
                    //        deleteManager.Unlink(oid: (uint)oldOid);
                    //    }
                    //    finally { }
                    //}

                    // Zápis nového Large Objectu do tabulky
                    commandText = String.Concat(
                        $"UPDATE{Environment.NewLine}",
                        $"    {schemaName}.{tableName} AS a{Environment.NewLine}",
                        $"SET{Environment.NewLine}",
                        $"    {colLoName} = @Oid{Environment.NewLine}",
                        $"WHERE{Environment.NewLine}",
                        $"    a.{colKeyName} = @KeyValue;{Environment.NewLine}");

                    command =
                         new NpgsqlCommand()
                         {
                             Connection = Connection,
                             Transaction = transaction,
                             CommandText = commandText
                         };
                    command.Parameters.Add(value: pOid);
                    command.Parameters.Add(value: pKeyValue);

                    command.ExecuteNonQuery();

                    transaction?.Commit();
                }

                catch (Exception exception)
                {
                    largeObject?.Close();
                    fs?.Close();
                    transaction?.Rollback();
                    oid = null;

                    RaiseExceptionReceived(new ExceptionEventArgs()
                    {
                        Id = 214,
                        FunctionName = nameof(LoUpload),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                return oid;
            }

            /// <summary>
            /// Stáhne Large Object z databáze do lokálního souboru.
            /// Verze: Large Object je identifikován primárním klíčem záznamu v tabulce.
            /// </summary>
            /// <param name="path">Cesta k ukládanému souboru</param>
            /// <param name="schemaName">Jméno databázového schéma s tabulkou s registrovanými Large Objecty</param>
            /// <param name="tableName">Tabulka s registrovanými Large Objecty</param>
            /// <param name="colKeyName">Jméno sloupce s jednoznačným identifikátorem záznamu v tabulce (primární klíč tabulky)</param>
            /// <param name="colLoName">Jméno sloupce s identifikátorem Large Objectu</param>
            /// <param name="strKeyValue">Hodnota identifikátoru záznamu v tabulce (určuje ze kterého řádku v tabulce bude Large Object stažen)</param>
            public void LoDownload(
                string path,
                string schemaName,
                string tableName,
                string colKeyName,
                string colLoName,
                string strKeyValue)
            {
                if (String.IsNullOrEmpty(value: path) ||
                    String.IsNullOrEmpty(value: schemaName) ||
                    String.IsNullOrEmpty(value: tableName) ||
                    String.IsNullOrEmpty(value: colKeyName) ||
                    String.IsNullOrEmpty(value: colLoName))
                {
                    // Vstupy do metody nejsou zadané
                    return;
                }

                Nullable<int> keyValue =
                   Functions.ConvertToNInt(
                       text: strKeyValue,
                       defaultValue: null);

                NpgsqlParameter pKeyValue = new(
                    parameterName: "KeyValue",
                    parameterType: NpgsqlDbType.Integer);

                if (keyValue != null)
                {
                    pKeyValue.Value = (int)keyValue;
                }
                else
                {
                    pKeyValue.Value = DBNull.Value;
                }

                string commandText = String.Concat(
                    $"SELECT{Environment.NewLine}",
                    $"    a.{colLoName} AS {colLoName}{Environment.NewLine}",
                    $"FROM{Environment.NewLine}",
                    $"    {schemaName}.{tableName} AS a{Environment.NewLine}",
                    $"WHERE{Environment.NewLine}",
                    $"    a.{colKeyName} = @KeyValue;{Environment.NewLine}");

                string strOid = ExecuteScalar(commandText, pKeyValue);

                Nullable<uint> oid = Functions.ConvertToNUInt(
                    text: strOid, defaultValue: null);

                if (oid == null)
                {
                    return;
                }

                MemoryStream ms = null;
                FileStream fs = null;
                try
                {
                    ms = LoRead(oid: (uint)oid);
                    if (ms.Length > 0)
                    {
                        fs = File.Create(path: path);
                        fs.Write(ms.GetBuffer(),
                            offset: 0,
                            count: Convert.ToInt32(value: ms.Length));
                    }
                    else
                    {
                        string message =
                            (Dictionary.TryGetValue(key: "msgLoNullLength", out string value) ? value : String.Empty)
                                .Replace(oldValue: "$1", newValue: colLoName)
                                .Replace(oldValue: "$2", newValue: Functions.PrepNULongArg(arg: oid))
                                .Replace(oldValue: "$3", newValue: schemaName)
                                .Replace(oldValue: "$4", newValue: tableName)
                                .Replace(oldValue: "$5", newValue: colKeyName)
                                .Replace(oldValue: "$6", newValue: Functions.PrepNIntArg(arg: keyValue));

                        throw new Exception(
                            message: message);
                    }
                }

                catch (Exception exception)
                {
                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 221,
                        FunctionName = nameof(LoDownload),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }
                finally
                {
                    ms?.Close();
                    fs?.Close();
                }
            }

            /// <summary>
            /// Stáhne Large Object z databáze do lokálního souboru.
            /// Verze: Large Object je určen identifikačním číslem oid.
            /// </summary>
            /// <param name="path">Cesta k ukládanému souboru</param>
            /// <param name="oid">Identifikační číslo Large Objectu</param>
            public void LoDownload(
                string path,
                uint oid)
            {
                if (String.IsNullOrEmpty(value: path))
                {
                    // Vstupy do metody nejsou zadané
                    return;
                }

                MemoryStream ms = null;
                FileStream fs = null;
                try
                {
                    ms = LoRead(oid: oid);
                    if (ms.Length > 0)
                    {
                        fs = File.Create(path: path);
                        fs.Write(ms.GetBuffer(),
                            offset: 0,
                            count: Convert.ToInt32(value: ms.Length));
                    }
                    else
                    {
                        string message =
                            (Dictionary.TryGetValue(key: "msgLoOidNullLength", out string value) ? value : String.Empty)
                                .Replace(oldValue: "$1", newValue: Functions.PrepUIntArg(arg: oid));

                        throw new Exception(
                            message: message);
                    }
                }

                catch (Exception exception)
                {
                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 231,
                        FunctionName = nameof(LoDownload),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                finally
                {
                    ms?.Close();
                    fs?.Close();
                }
            }

            /// <summary>
            /// Zobrazí dialog pro výběr souboru pro uložení
            /// a stáhne Large Object z databáze do lokálního souboru.
            /// Verze: Large Object je identifikován primárním klíčem záznamu v tabulce.
            /// </summary>
            /// <param name="schemaName">Jméno databázového schéma s tabulkou s registrovanými Large Objecty</param>
            /// <param name="tableName">Tabulka s registrovanými Large Objecty</param>
            /// <param name="colKeyName">Jméno sloupce s jednoznačným identifikátorem záznamu v tabulce (primární klíč tabulky)</param>
            /// <param name="colLoName">Jméno sloupce s identifikátorem Large Objectu</param>
            /// <param name="strKeyValue">Hodnota identifikátoru záznamu v tabulce (určuje ze kterého řádku v tabulce bude Large Object stažen)</param>
            /// <param name="fileName">Jméno lokálního souboru (pokud není zadáno, pak se použije hodnota identifikátoru záznamu v tabulce)</param>
            /// <param name="fileExt">Přípona lokálního souboru (pokud není zadána, zůstane prázdná)</param>
            /// <param name="fileFilter">Filtr typu souboru (pokud není zadán, pak všechny soubory)</param>
            public void LoDownloadDialog(
                string schemaName,
                string tableName,
                string colKeyName,
                string colLoName,
                string strKeyValue,
                string fileName = null,
                string fileExt = null,
                string fileFilter = null)
            {
                if (String.IsNullOrEmpty(value: schemaName) ||
                    String.IsNullOrEmpty(value: tableName) ||
                    String.IsNullOrEmpty(value: colKeyName) ||
                    String.IsNullOrEmpty(value: colLoName))
                {
                    // Vstupy do metody nejsou zadané
                    return;
                }

                fileName = String.IsNullOrEmpty(value: fileName) ? strKeyValue : fileName;
                fileExt = String.IsNullOrEmpty(value: fileExt) ? String.Empty : fileExt;
                fileFilter = String.IsNullOrEmpty(value: fileFilter) ? "All files (*.*)|*.*" : fileFilter;

                System.Windows.Forms.SaveFileDialog form = new()
                {
                    FileName = fileName,
                    DefaultExt = fileExt,
                    Filter = fileFilter
                };

                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoDownload(
                        path: form.FileName,
                        schemaName: schemaName,
                        tableName: tableName,
                        colKeyName: colKeyName,
                        colLoName: colLoName,
                        strKeyValue: strKeyValue);
                }
            }

            /// <summary>
            /// Zobrazí dialog pro výběr souboru pro uložení
            /// a stáhne Large Object z databáze do lokálního souboru.
            /// Verze: Large Object je určen identifikačním číslem oid.
            /// </summary>
            /// <param name="oid">Identifikační číslo Large Objectu</param>
            /// <param name="fileName">Jméno lokálního souboru (pokud není zadáno, pak se použije hodnota identifikátoru Large Objectu)</param>
            /// <param name="fileExt">Přípona lokálního souboru (pokud není zadána, zůstane prázdná)</param>
            /// <param name="fileFilter">Filtr typu souboru (pokud není zadán, pak všechny soubory)</param>
            public void LoDownloadDialog(
                uint oid,
                string fileName = null,
                string fileExt = null,
                string fileFilter = null)
            {
                fileName = String.IsNullOrEmpty(value: fileName) ? Functions.PrepUIntArg(arg: oid) : fileName;
                fileExt = String.IsNullOrEmpty(value: fileExt) ? String.Empty : fileExt;
                fileFilter = String.IsNullOrEmpty(value: fileFilter) ? "All files (*.*)|*.*" : fileFilter;

                System.Windows.Forms.SaveFileDialog form =
                    new()
                    {
                        FileName = fileName,
                        DefaultExt = fileExt,
                        Filter = fileFilter
                    };

                if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    LoDownload(
                        path: form.FileName,
                        oid: oid);
                }
            }

            /// <summary>
            /// Smaže Large Object z databáze
            /// a provede jeho odregistrování v datové tabulce
            /// </summary>
            /// <param name="schemaName">Jméno databázového schéma s tabulkou s registrovanými Large Objecty</param>
            /// <param name="tableName">Tabulka s registrovanými Large Objecty</param>
            /// <param name="colKeyName">Jméno sloupce s jednoznačným identifikátorem záznamu v tabulce (primární klíč tabulky)</param>
            /// <param name="colLoName">Jméno sloupce s identifikátorem Large Objectu</param>
            /// <param name="strKeyValue">Hodnota identifikátoru záznamu v tabulce (určuje ze kterého řádku v tabulce bude Large Object odstraněn)</param>
            public void LoDelete(
                string schemaName,
                string tableName,
                string colKeyName,
                string colLoName,
                string strKeyValue)
            {
                if (String.IsNullOrEmpty(value: schemaName) ||
                    String.IsNullOrEmpty(value: tableName) ||
                    String.IsNullOrEmpty(value: colKeyName) ||
                    String.IsNullOrEmpty(value: colLoName))
                {
                    // Vstupy do metody nejsou zadané
                    return;
                }

                // Parametry příkazů:

                Nullable<int> keyValue =
                   Functions.ConvertToNInt(
                       text: strKeyValue,
                       defaultValue: null);

                NpgsqlParameter pKeyValue = new(
                    parameterName: "KeyValue",
                    parameterType: NpgsqlDbType.Integer);

                if (keyValue != null)
                {
                    pKeyValue.Value = (int)keyValue;
                }
                else
                {
                    pKeyValue.Value = DBNull.Value;
                }

                NpgsqlParameter pOid = new(
                    parameterName: "Oid",
                    parameterType: NpgsqlDbType.Oid)
                {
                    Value = DBNull.Value
                };

                // Ziskání oid Large Objectu pro odstranění
                string commandText = String.Concat(
                    $"SELECT{Environment.NewLine}",
                    $"    a.{colLoName} AS {colLoName}{Environment.NewLine}",
                    $"FROM{Environment.NewLine}",
                    $"    {schemaName}.{tableName} AS a{Environment.NewLine}",
                    $"WHERE{Environment.NewLine}",
                    $"    a.{colKeyName} = @KeyValue;{Environment.NewLine}");

                string strOid = ExecuteScalar(commandText, pKeyValue);

                Nullable<uint> oid = Functions.ConvertToNUInt(
                    text: strOid, defaultValue: null);

                if (oid == null)
                {
                    return;
                }

                NpgsqlTransaction transaction
                   = BeginTransaction();

                if (transaction == null)
                {
                    // Transakce není otevřená
                    return;
                }

                try
                {
                    // Smaže Large Object
                    NpgsqlLargeObjectManager manager =
                        new(connection: Connection);
                    manager.Unlink(oid: (uint)oid);

                    // Odstraní registraci Large Objektu z tabulky
                    commandText = String.Concat(
                        $"UPDATE{Environment.NewLine}",
                        $"    {schemaName}.{tableName}{Environment.NewLine}",
                        $"SET{Environment.NewLine}",
                        $"    {colLoName} = @Oid{Environment.NewLine}",
                        $"WHERE{Environment.NewLine}",
                        $"    a.{colKeyName} = @KeyValue;{Environment.NewLine}");

                    NpgsqlCommand command =
                         new()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = commandText
                         };

                    command.Parameters.Add(value: pOid);
                    command.Parameters.Add(value: pKeyValue);

                    command.ExecuteNonQuery();

                    transaction?.Commit();
                }
                catch (Exception exception)
                {
                    transaction?.Rollback();

                    RaiseExceptionReceived(new ExceptionEventArgs()
                    {
                        Id = 241,
                        FunctionName = nameof(LoDelete),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }
            }

            /// <summary>
            /// Dump všech large objektů tabulky do vybraného adresáře
            /// (tabulka musí mít jednoduchý primární klíč)
            /// </summary>
            /// <param name="table">Tabulka</param>
            /// <param name="column">Sloupeček s large objekty</param>
            /// <param name="folder">Vybraný adresář</param>
            public void LoDump(
                DBTable table,
                DBColumn column,
                string folder)
            {
                if (table == null) { return; }
                if (column == null) { return; }
                if (String.IsNullOrEmpty(value: folder)) { return; }
                try
                {
                    if (!Directory.Exists(path: folder))
                    {
                        Directory.CreateDirectory(path: folder);
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show(
                        text: e.Message,
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return;
                }

                if (table.PrimaryKeys.Items.Count != 1)
                {
                    MessageBox.Show(
                        text: "Tabulka nemá primární klíč",
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return;
                }
                DBPrimaryKey pkey = table.PrimaryKeys.Items[0];

                if (pkey.ConstraintColumns.Count != 1)
                {
                    MessageBox.Show(
                        text: "Tabulka nemá jednoduchý primární klíč",
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return;
                }
                DBColumn pkeyColumn = pkey.ConstraintColumns[0];

                string sql = String.Concat(
                        $"SELECT{Environment.NewLine}",
                        $"    {pkeyColumn.Name} AS pkey,{Environment.NewLine}",
                        $"    {column.Name} AS loid{Environment.NewLine}",
                        $"FROM{Environment.NewLine}",
                        $"    {table.FullName}{Environment.NewLine}",
                        $"ORDER BY{Environment.NewLine}",
                        $"    {pkeyColumn.Name};{Environment.NewLine}");

                DataTable data = ExecuteQuery(sqlCommand: sql);
                if (data == null) { return; }

                foreach (DataRow row in data.Rows)
                {
                    // identifikační číslo řádku
                    string strPKey =
                       Functions.GetStringArg(
                           row: row,
                           name: "pkey",
                           defaultValue: null);
                    if (String.IsNullOrEmpty(value: strPKey)) { continue; }

                    // identifikační číslo large objektu
                    Nullable<uint> loid =
                        Functions.GetNUIntArg(
                            row: row,
                            name: "loid",
                            defaultValue: null);
                    if (loid == null) { continue; }

                    // název souboru je složený ze jména složky a identifikačního čísla řádku
                    string filePath = Path.Combine(folder, strPKey);

                    // smaže staré soubory ve složce
                    try
                    {
                        if (File.Exists(path: filePath))
                        {
                            File.Delete(path: filePath);
                        }
                    }
                    catch (Exception e)
                    {
                        MessageBox.Show(
                            text: e.Message,
                            caption: String.Empty,
                            buttons: MessageBoxButtons.OK,
                            icon: MessageBoxIcon.Information);
                        continue;
                    }

                    // stáhne large objekt do složky
                    LoDownload(
                        path: filePath,
                        oid: (uint)loid);
                }
            }

            /// <summary>
            /// Restore všech large objektů tabulky z vybraného adresáře
            /// (tabulka musí mít jednoduchý primární klíč)
            /// </summary>
            /// <param name="folder">Vybraný adresář</param>
            /// <param name="table">Tabulka</param>
            /// <param name="column">Sloupeček s large objekty</param>
            public void LoRestore(
                string folder,
                DBTable table,
                DBColumn column)
            {
                if (String.IsNullOrEmpty(value: folder)) { return; }
                if (!Directory.Exists(path: folder)) { return; }
                if (table == null) { return; }
                if (column == null) { return; }

                if (table.PrimaryKeys.Items.Count != 1)
                {
                    MessageBox.Show(
                        text: "Tabulka nemá primární klíč",
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return;
                }
                DBPrimaryKey pkey = table.PrimaryKeys.Items[0];

                if (pkey.ConstraintColumns.Count != 1)
                {
                    MessageBox.Show(
                        text: "Tabulka nemá jednoduchý primární klíč",
                        caption: String.Empty,
                        buttons: MessageBoxButtons.OK,
                        icon: MessageBoxIcon.Information);
                    return;
                }
                DBColumn pkeyColumn = pkey.ConstraintColumns[0];

                string sql = String.Concat(
                        $"SELECT{Environment.NewLine}",
                        $"    {pkeyColumn.Name} AS pkey,{Environment.NewLine}",
                        $"    {column.Name} AS loid{Environment.NewLine}",
                        $"FROM{Environment.NewLine}",
                        $"    {table.FullName}{Environment.NewLine}",
                        $"ORDER BY{Environment.NewLine}",
                        $"    {pkeyColumn.Name};{Environment.NewLine}");

                DataTable data = ExecuteQuery(sqlCommand: sql);
                if (data == null) { return; }

                foreach (DataRow row in data.Rows)
                {
                    // identifikační číslo řádku
                    string strPKey =
                        ZaJi.PostgreSQL.Functions.GetStringArg(
                            row: row,
                            name: "pkey",
                            defaultValue: null);
                    if (String.IsNullOrEmpty(value: strPKey)) { continue; }

                    // název souboru je složený ze jména složky a identifikačního čísla řádku
                    string filePath = Path.Combine(folder, strPKey);

                    // pokud soubor ve složce neexistuje, není co importovat
                    if (!File.Exists(path: filePath)) { continue; }

                    // importuje soubor do large objektu
                    LoUpload(
                        path: filePath,
                        schemaName: table.Schema.Name,
                        tableName: table.Name,
                        colKeyName: pkeyColumn.Name,
                        colLoName: column.Name,
                        strKeyValue: strPKey);
                }
            }

            #pragma warning restore 0618
            #endregion Large Object Methods

        }

    }
}