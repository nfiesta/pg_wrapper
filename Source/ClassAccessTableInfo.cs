﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using ZaJi.PostgreSQL;

namespace ZaJi
{
    namespace Access
    {

        /// <summary>
        /// Připojení k databázi Access
        /// </summary>
        public partial class AccessWrapper
        {

            /// <summary>
            /// Informace o databázové tabulce Access
            /// </summary>
            public class TableInfo
            {

                #region Private Fields

                /// <summary>
                /// Datový řádek se záznamem o tabulce
                /// </summary>
                private DataRow data;

                /// <summary>
                /// Seznam atributů databázové tabulky Access
                /// </summary>
                private List<ColumnInfo> columns;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor
                /// </summary>
                /// <param name="data">Datový řádek se záznamem o tabulce</param>
                public TableInfo(DataRow data)
                {
                    Data = data;
                    Columns = [];
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Datový řádek se záznamem o tabulce (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                /// <summary>
                /// Seznam atributů databázové tabulky Access
                /// </summary>
                public List<ColumnInfo> Columns
                {
                    get
                    {
                        return columns ?? [];
                    }
                    set
                    {
                        columns = value ?? [];
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// TABLE_CATALOG
                /// </summary>
                public string TableCatalog
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "TABLE_CATALOG",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "TABLE_CATALOG",
                            val: value);
                    }
                }

                /// <summary>
                /// TABLE_SCHEMA
                /// </summary>
                public string TableSchema
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "TABLE_SCHEMA",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "TABLE_SCHEMA",
                            val: value);
                    }
                }

                /// <summary>
                /// TABLE_NAME
                /// </summary>
                public string TableName
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "TABLE_NAME",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "TABLE_NAME",
                            val: value);
                    }
                }

                /// <summary>
                /// TABLE_TYPE
                /// </summary>
                public string TableType
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "TABLE_TYPE",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "TABLE_TYPE",
                            val: value);
                    }
                }

                /// <summary>
                /// TABLE_GUID
                /// </summary>
                public Nullable<Guid> TableGuid
                {
                    get
                    {
                        return Functions.GetNGuidArg(
                            row: Data,
                            name: "TABLE_GUID",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNGuidArg(
                            row: Data,
                            name: "TABLE_GUID",
                            val: value);
                    }
                }

                /// <summary>
                /// DESCRIPTION
                /// </summary>
                public string Description
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "DESCRIPTION",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "DESCRIPTION",
                            val: value);
                    }
                }

                /// <summary>
                /// TABLE_PROPID
                /// </summary>
                public Nullable<long> TablePropid
                {
                    get
                    {
                        return Functions.GetNLongArg(
                            row: Data,
                            name: "TABLE_PROPID",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNLongArg(
                            row: Data,
                            name: "TABLE_PROPID",
                            val: value);
                    }
                }

                /// <summary>
                /// DATE_CREATED
                /// </summary>
                public Nullable<DateTime> DateCreated
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: "DATE_CREATED",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: "DATE_CREATED",
                            val: value);
                    }
                }

                /// <summary>
                /// DATE_MODIFIED
                /// </summary>
                public Nullable<DateTime> DateModified
                {
                    get
                    {
                        return Functions.GetNDateTimeArg(
                            row: Data,
                            name: "DATE_MODIFIED",
                            defaultValue: null);
                    }
                    set
                    {
                        Functions.SetNDateTimeArg(
                            row: Data,
                            name: "DATE_MODIFIED",
                            val: value);
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určí, zda se zadaný objekt rovná aktuálnímu objektu
                /// </summary>
                /// <param name="obj">Zadný objekt</param>
                /// <returns>ano|ne</returns>
                public override bool Equals(object obj)
                {
                    // Pokud je předaný objekt null, vrátí se hodnota False
                    if (obj == null)
                    {
                        return false;
                    }

                    // Pokud předaný objekt není typ TableInfo, vrátí se hodnota False
                    if (obj is not TableInfo)
                    {
                        return false;
                    }

                    return
                        (((TableInfo)obj).TableName ?? string.Empty) == (TableName ?? String.Empty);
                }

                /// <summary>
                /// Vrací hash kód pro aktuální objekt
                /// </summary>
                /// <returns>Hash kód pro aktuální objekt</returns>
                public override int GetHashCode()
                {
                    return
                        (TableName ?? String.Empty).GetHashCode();
                }

                /// <summary>
                /// Vrací řetězec, který představuje aktuální objekt
                /// </summary>
                /// <returns>Řetězec, který představuje aktuální objekt</returns>
                public override string ToString()
                {
                    return String.Concat(
                        $"{nameof(TableInfo)}: ",
                        $"{{",
                        $"{nameof(TableName)}: {Functions.PrepStringArg(arg: TableName)}",
                        $"}}");
                }

                #endregion Methods

            }

        }
    }
}