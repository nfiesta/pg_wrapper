﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.IO;
using Microsoft.Win32;

namespace ZaJi
{
    namespace Access
    {

        /// <summary>
        /// Připojení k databázi Access
        /// </summary>
        public partial class AccessWrapper
        {

            #region Constants

            /// <summary>
            /// Přípona mdb Access souboru
            /// </summary>
            private const string MdbExtension = "mdb";

            /// <summary>
            /// Přípona accdb Access souboru
            /// </summary>
            private const string AceExtension = "accdb";

            /// <summary>
            /// OLE DB Provider pro mdb soubory
            /// </summary>
            private const string MdbOleDbProvider = "Microsoft.Jet.OLEDB.4.0";

            /// <summary>
            /// OLE DB Provider pro accdb soubory verze 12.0
            /// </summary>
            private const string AceOleDbProvider12 = "Microsoft.ACE.OLEDB.12.0";

            /// <summary>
            /// OLE DB Provider pro accdb soubory verze 16.0
            /// </summary>
            private const string AceOleDbProvider16 = "Microsoft.ACE.OLEDB.16.0";

            /// <summary>
            /// Filter pro Access soubory
            /// </summary>
            private const string AceFileFilter = "Databáze aplikace Microsoft Access|*mdb; *accdb";

            #endregion Constants


            #region Private Fields

            /// <summary>
            /// Objekt sestavení připojovacího řetězce
            /// </summary>
            private OleDbConnectionStringBuilder connectionStringBuilder;

            /// <summary>
            /// Objekt připojení k databázi
            /// </summary>
            private OleDbConnection connection;

            /// <summary>
            /// Udržovat připojení k databázi
            /// </summary>
            private bool stayConnected;

            /// <summary>
            /// Příznak při vzniku chyby při provádění SQL příkazu
            /// </summary>
            private bool exceptionFlag;

            /// <summary>
            /// Zpráva o chybě při provádění SQL příkazu
            /// </summary>
            private string exceptionMessage;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// Objekt sestavení připojovacího řetězce (read-only)
            /// </summary>
            private OleDbConnectionStringBuilder ConnectionStringBuilder
            {
                get
                {
                    return connectionStringBuilder;
                }
                set
                {
                    connectionStringBuilder = value;
                }
            }

            /// <summary>
            /// Objekt připojení k databázi (read-only)
            /// </summary>
            public OleDbConnection Connection
            {
                get
                {
                    return connection;
                }
                private set
                {
                    connection = value;
                }
            }

            /// <summary>
            /// Udržovat připojení k databázi
            /// </summary>
            public bool StayConnected
            {
                get
                {
                    return stayConnected;
                }
                set
                {
                    stayConnected = value;
                }
            }

            /// <summary>
            /// Příznak při vzniku chyby při provádění SQL příkazu
            /// </summary>
            public bool ExceptionFlag
            {
                get
                {
                    return exceptionFlag;
                }
                set
                {
                    exceptionFlag = value;
                }
            }

            /// <summary>
            /// Zpráva o chybě při provádění SQL příkazu
            /// </summary>
            public string ExceptionMessage
            {
                get
                {
                    return exceptionMessage;
                }
                set
                {
                    exceptionMessage = value;
                }
            }

            #endregion Properties


            #region Derived Properties

            /// <summary>
            /// Stav inicializace připojení k databázi
            /// - připojení nastaveno (true) | nenastaveno (false) (read-only)
            /// </summary>
            public bool Initialized
            {
                get
                {
                    return
                        ConnectionStringBuilder != null;
                }
            }

            /// <summary>
            /// Stav připojení k dababázi - připojeno (true) | nepřipojeno (false) (read-only)
            /// </summary>
            public bool Connected
            {
                get
                {
                    if (Connection == null)
                    {
                        return false;
                    }

                    return
                        Connection.State == ConnectionState.Open;
                }
            }

            /// <summary>
            /// Jméno databáze (Cesta k Access souboru) (read-only)
            /// </summary>
            public string Database
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            String.Empty :
                            ConnectionStringBuilder.DataSource;
                }
            }

            /// <summary>
            /// Připojovací řetězec (read-only)
            /// </summary>
            public string ConnectionString
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            String.Empty :
                            ConnectionStringBuilder.ConnectionString;
                }
            }

            /// <summary>
            /// Slovník s texty hlášení
            /// </summary>
            public static Dictionary<string, string> Dictionary
            {
                get
                {
                    return new Dictionary<string, string>()
                    {
                        { "msgSetConnectionFailed",
                            $"Nastavení připojení k Access souboru $1 se nezdařilo."
                        },
                        { "msgConnectionNotInitialized",
                            "Access soubor není vybrán."
                        },
                        { "msgConnectionInfoConnected",
                            $"Aplikace je připojena k databázi $1."
                        },
                        { "msgConnectionInfoNotConnected",
                             "Aplikace není připojena k žádné databázi."
                        },
                        { "msgConnectionClosed",
                            "SQL příkaz nelze provést, připojení k databázi je uzavřeno."
                        },
                        { "msgNoOLEDBProvider",
                            "Nebyl nalezen poskytovatel připojení Microsoft OLE DB."
                        }
                    };
                }
            }

            #endregion Derived Properties


            #region Events

            #region ConnectionError (Výjimka při připojení k databázi)

            /// <summary>
            /// Delegát funkce obsluhující událost vzniku výjimky při připojení k databázi
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            public delegate void ConnectionErrorHandler(object sender, ExceptionEventArgs e);

            /// <summary>
            /// Událost vzniku výjimky při připojení k databázi
            /// </summary>
            public event ConnectionErrorHandler ConnectionError;

            /// <summary>
            /// Vyvolání události vzniku výjimky při připojení k databázi
            /// </summary>
            /// <param name="e">Parametry události</param>
            private void RaiseConnectionError(ExceptionEventArgs e)
            {
                ConnectionError?.Invoke(
                    sender: this,
                    e: e);
                System.Windows.Forms.Application.DoEvents();
            }

            #endregion ConnectionError (Výjimka při připojení k databázi)


            #region ExceptionReceived (Výjimka při zpracování SQL příkazu)

            /// <summary>
            /// Delegát funkce obsluhující událost vzniku výjimky při zpracování SQL příkazu
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            public delegate void ExceptionReceivedHandler(object sender, ExceptionEventArgs e);

            /// <summary>
            /// Událost vzniku výjimky při zpracování SQL příkazu
            /// </summary>
            public event ExceptionReceivedHandler ExceptionReceived;

            /// <summary>
            /// Vyvolání události vzniku výjimky při zpracování SQL příkazu
            /// </summary>
            /// <param name="e">Parametry události</param>
            private void RaiseExceptionReceived(ExceptionEventArgs e)
            {
                ExceptionReceived?.Invoke(
                    sender: this,
                    e: e);
                System.Windows.Forms.Application.DoEvents();
            }

            #endregion ExceptionReceived (Výjimka při zpracování SQL příkazu)


            #region ConnectionStateChanged (Změna stavu připojení)

            /// <summary>
            /// Delegát funkce obsluhující událost změny stavu připojení
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            public delegate void ConnectionStateChangedHandler(object sender, StateChangeEventArgs e);

            /// <summary>
            /// Událost změny stavu připojení
            /// </summary>
            public event ConnectionStateChangedHandler ConnectionStateChanged;

            /// <summary>
            /// Vyvolání události změny stavu připojení
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            private void RaiseConnectionStateChanged(object sender, StateChangeEventArgs e)
            {
                ConnectionStateChanged?.Invoke(
                    sender: sender,
                    e: e);
                System.Windows.Forms.Application.DoEvents();
            }

            #endregion ConnectionStateChanged (Změna stavu připojení)

            #endregion Events


            #region Constructor

            /// <summary>
            /// Konstruktor připojení k databázi Access
            /// </summary>
            public AccessWrapper()
            {
                ConnectionStringBuilder = null;

                Connection = new OleDbConnection();
                Connection.StateChange += RaiseConnectionStateChanged;

                StayConnected = false;

                ExceptionFlag = false;

                ExceptionMessage = String.Empty;
            }

            /// <summary>
            /// Inicializace objektu připojení k databázi
            /// </summary>
            /// <param name="database">Jméno databáze (Cesta k Access souboru)</param>
            /// <param name="provider">OLE DB poskytovatele připojení k databázi Access</param>
            private void InitializeConnection(string database, string provider)
            {
                ConnectionStringBuilder = new()
                {
                    DataSource = database,
                    Provider = provider
                };

                Connection = new(connectionString: ConnectionString);
                Connection.StateChange += RaiseConnectionStateChanged;
            }

            #endregion Constructor


            #region Connection Methods

            /// <summary>
            /// Vrací OLE DB poskytovatele připojení k databázi Access
            /// </summary>
            /// <param name="database">Jméno databáze (Cesta k Access souboru)</param>
            public static string GetInstalledOleDbProvider(string database)
            {
                switch (Path.GetExtension(path: database).Trim(trimChar:(char)46))
                {
                    case MdbExtension:

                        // Zkusí najít nainstalovaný "Microsoft.Jet.OLEDB.4.0" v 64-bit registru
                        using (RegistryKey key = Registry.LocalMachine.OpenSubKey(
                            name: $@"SOFTWARE\Classes\{MdbOleDbProvider}",
                            writable: false))
                        {
                            if (key != null) { return MdbOleDbProvider; };
                        }

                        // Zkusí najít nainstalovaný "Microsoft.Jet.OLEDB.4.0" v 32-bit registru
                        using (RegistryKey key = Registry.LocalMachine.OpenSubKey(
                            name: $@"SOFTWARE\WOW6432Node\Classes\{MdbOleDbProvider}",
                            writable: false))
                        {
                            if (key != null) { return MdbOleDbProvider; };
                        }

                        // "Microsoft.Jet.OLEDB.4.0" nebyl nalezen
                        return String.Empty;

                    case AceExtension:

                        // Zkusí najít nainstalovaný "Microsoft.ACE.OLEDB.16.0" v 64-bit registru
                        using (RegistryKey key = Registry.LocalMachine.OpenSubKey(
                            name: $@"SOFTWARE\Classes\{AceOleDbProvider16}",
                            writable: false))
                        {
                            if (key != null) { return AceOleDbProvider16; };
                        }

                        // Zkusí najít nainstalovaný "Microsoft.ACE.OLEDB.16.0" v 32-bit registru
                        using (RegistryKey key = Registry.LocalMachine.OpenSubKey(
                            name: $@"SOFTWARE\WOW6432Node\Classes\{AceOleDbProvider16}",
                            writable: false))
                        {
                            if (key != null) { return AceOleDbProvider16; };
                        }

                        // Zkusí najít nainstalovaný "Microsoft.ACE.OLEDB.12.0" v 64-bit registru
                        using (RegistryKey key = Registry.LocalMachine.OpenSubKey(
                            name: $@"SOFTWARE\Classes\{AceOleDbProvider12}",
                            writable: false))
                        {
                            if (key != null) { return AceOleDbProvider12; };
                        }

                        // Zkusí najít nainstalovaný "Microsoft.ACE.OLEDB.12.0" v 32-bit registru
                        using (RegistryKey key = Registry.LocalMachine.OpenSubKey(
                            name: $@"SOFTWARE\WOW6432Node\Classes\{AceOleDbProvider12}",
                            writable: false))
                        {
                            if (key != null) { return AceOleDbProvider12; };
                        }

                        // "Microsoft.ACE.OLEDB" nebyl nalezen
                        return String.Empty;

                    default:
                        // Přípona souboru neodpovídá Access databázi
                        return String.Empty;
                }
            }

            /// <summary>
            /// Zobrazí formulář pro připojení k databázi
            /// </summary>
            public void SetConnectionDialog()
            {
                System.Windows.Forms.OpenFileDialog dlg = new()
                {
                    DefaultExt = AceExtension,
                    Filter = AceFileFilter
                };

                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    SetConnection(database: dlg.FileName);
                }
            }

            /// <summary>
            /// Provede nastavení připojení podle zadaných parametrů
            /// </summary>
            /// <param name="database">Jméno databáze (Cesta k Access souboru)</param>
            public void SetConnection(string database)
            {
                string provider = GetInstalledOleDbProvider(database: database);

                if (String.IsNullOrEmpty(value: provider))
                {
                    string message =
                            Dictionary.TryGetValue(
                                key: "msgNoOLEDBProvider",
                                out string msgNoOLEDBProvider)
                                    ? msgNoOLEDBProvider
                                    : String.Empty;

                    RaiseConnectionError(e: new ExceptionEventArgs()
                    {
                        Id = 11,
                        FunctionName = nameof(SetConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = new Exception(message: message),
                        CommandObject = null,
                    });

                    ConnectionStringBuilder = null;
                    return;
                }

                OleDbConnectionStringBuilder testConnectionStringBuilder = new()
                {
                    DataSource = database,
                    Provider = provider
                };

                OleDbConnection testConnection = new(
                     connectionString: testConnectionStringBuilder.ConnectionString);

                try
                {
                    testConnection.Open();

                    if (testConnection.State == ConnectionState.Open)
                    {
                        InitializeConnection(database: database, provider: provider);
                    }
                    else
                    {
                        string message =
                            (Dictionary.TryGetValue(
                                key: "msgSetConnectionFailed",
                                out string msgSetConnectionFailed)
                                    ? msgSetConnectionFailed
                                    : String.Empty)
                            .Replace(oldValue: "$1", newValue: Database);

                        RaiseConnectionError(e: new ExceptionEventArgs()
                        {
                            Id = 12,
                            FunctionName = nameof(SetConnection),
                            MessageBoxCaption = String.Empty,
                            MessageBoxText = message,
                            ExceptionObject = new Exception(message: message),
                            CommandObject = null,
                        });

                        ConnectionStringBuilder = null;
                    }
                }

                catch (Exception exception)
                {
                    string message =
                            (Dictionary.TryGetValue(
                                key: "msgSetConnectionFailed",
                                out string msgSetConnectionFailed)
                                    ? msgSetConnectionFailed
                                    : String.Empty)
                            .Replace(oldValue: "$1", newValue: Database);

                    RaiseConnectionError(new ExceptionEventArgs()
                    {
                        Id = 13,
                        FunctionName = nameof(SetConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });

                    ConnectionStringBuilder = null;
                }

                finally
                {
                    testConnection.Close();
                }
            }

            /// <summary>
            /// Otevře připojení k databázi Access
            /// </summary>
            public void OpenConnection()
            {
                if (!Initialized)
                {
                    string message =
                            Dictionary.TryGetValue(
                                key: "msgConnectionNotInitialized",
                                out string msgConnectionNotInitialized)
                            ? msgConnectionNotInitialized
                            : String.Empty;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 21,
                        FunctionName = nameof(OpenConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = new Exception(message: message),
                        CommandObject = null
                    });

                    return;
                }

                if (Connected)
                {
                    return;
                }

                try
                {
                    Connection.Open();
                }

                catch (Exception exception)
                {
                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 22,
                        FunctionName = nameof(OpenConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }
            }

            /// <summary>
            /// Zavře připojení k databázi Access
            /// </summary>
            public void CloseConnection()
            {
                if (!Initialized)
                {
                    return;
                }

                if (Connection.State == ConnectionState.Closed)
                {
                    return;
                }

                try
                {
                    Connection.Close();
                }
                catch (Exception exception)
                {
                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 31,
                        FunctionName = nameof(CloseConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }
            }

            /// <summary>
            /// Vrátí informační text o nastaveném připojení k databázi Access
            /// </summary>
            /// <returns>Informační text o nastaveném připojení k databázi Access</returns>
            public string ConnectionInfo()
            {
                return
                    Initialized
                        ? Dictionary.TryGetValue(
                            key: "msgConnectionInfoConnected",
                            out string msgConnectionInfoConnected)
                            ? msgConnectionInfoConnected
                                .Replace(oldValue: "$1", newValue: Database)
                            : String.Empty
                        : Dictionary.TryGetValue(
                            key: "msgConnectionInfoNotConnected",
                            out string msgConnectionInfoNotConnected)
                            ? msgConnectionInfoNotConnected
                            : String.Empty;
            }

            /// <summary>
            /// Vyzkouší, zda je připojení inicializované,
            /// pokud ano, pak se ho pokusí otevřít (pokud již otevřené není)
            /// </summary>
            /// <param name="functionId">Identifikační číslo funkce</param>
            /// <param name="functionName">Jméno funkce</param>
            /// <returns>Vrátí true pokud je připojení inicializované a otevřené, jinak false</returns>
            public bool TryOpenConnection(
                int functionId,
                string functionName)
            {
                if (!Initialized)
                {
                    string message =
                        Dictionary.TryGetValue(
                            key: "msgConnectionNotInitialized",
                            out string value)
                        ? value
                        : string.Empty;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = (10 * functionId) + 1,
                        FunctionName = functionName,
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = new Exception(message: message),
                        CommandObject = null
                    });

                    return false;
                }

                OpenConnection();

                if (!Connected)
                {
                    string message =
                        Dictionary.TryGetValue(key: "msgConnectionClosed", out string value)
                            ? value
                            : String.Empty;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = (10 * functionId) + 2,
                        FunctionName = functionName,
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = new Exception(message: message),
                        CommandObject = null
                    });
                    return false;
                }

                return true;
            }

            /// <summary>
            /// Vrací textový řetězec reprezentující objekt
            /// </summary>
            /// <returns>Vrací textový řetězec reprezentující objekt</returns>
            public override string ToString()
            {
                return
                String.Concat(
                        $"Object AccessWrapper:{Environment.NewLine}",
                        $"database = {Database};{Environment.NewLine}",
                        $"stay connected = {StayConnected};{Environment.NewLine}",
                        $"initialized = {Initialized};{Environment.NewLine}",
                        $"connected = {Connected};{Environment.NewLine}"
                    );
            }

            #endregion Connection Methods


            #region SQL Command Methods

            #region ExecuteQuery

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            private DataTable ExecuteQueryInternal(
                string sqlCommand,
                OleDbTransaction transaction,
                params OleDbParameter[] parameters)
            {
                if (!TryOpenConnection(
                    functionId: 10,
                    functionName: nameof(ExecuteQuery)))
                {
                    // Připojení není otevřené
                    return new DataTable();
                }

                OleDbCommand command =
                    (transaction == null) ?
                         new OleDbCommand()
                         {
                             Connection = connection,
                             CommandText = sqlCommand
                         } :
                         new OleDbCommand()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = sqlCommand
                         };

                foreach (OleDbParameter parameter in parameters)
                {
                    command.Parameters.Add(value: parameter);
                }

                OleDbDataAdapter adapter = new()
                {
                    SelectCommand = command
                };

                DataTable result = new();

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    adapter.Fill(dataTable: result);
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    result = new DataTable();

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 103,
                        FunctionName = nameof(ExecuteQuery),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = command
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }

                return result;
            }

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            public DataTable ExecuteQuery(
                string sqlCommand)
            {
                return ExecuteQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: []);
            }

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            public DataTable ExecuteQuery(
                string sqlCommand,
                params OleDbParameter[] parameters)
            {
                return ExecuteQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: parameters);
            }

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            public DataTable ExecuteQuery(
                string sqlCommand,
                OleDbTransaction transaction)
            {
                return ExecuteQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: []);
            }

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            public DataTable ExecuteQuery(
                string sqlCommand,
                OleDbTransaction transaction,
                params OleDbParameter[] parameters)
            {
                return ExecuteQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: parameters);
            }

            #endregion ExecuteQuery


            #region ExecuteScalar

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            private string ExecuteScalarInternal(
                string sqlCommand,
                OleDbTransaction transaction,
                params OleDbParameter[] parameters)
            {
                if (!TryOpenConnection(
                    functionId: 11,
                    functionName: nameof(ExecuteScalar)))
                {
                    // Připojení není otevřené
                    return null;
                }

                OleDbCommand command =
                    (transaction == null) ?
                         new OleDbCommand()
                         {
                             Connection = connection,
                             CommandText = sqlCommand
                         } :
                         new OleDbCommand()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = sqlCommand
                         };

                foreach (OleDbParameter parameter in parameters)
                {
                    command.Parameters.Add(value: parameter);
                }

                string result;

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    object obj = command.ExecuteScalar();

                    result = (obj == null) ? null :
                        Convert.ToString(value: obj);
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    result = null;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 113,
                        FunctionName = nameof(ExecuteScalar),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = command
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }

                return result;
            }

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            public string ExecuteScalar(
                string sqlCommand)
            {
                return ExecuteScalarInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: []);
            }

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            public string ExecuteScalar(
               string sqlCommand,
               params OleDbParameter[] parameters)
            {
                return ExecuteScalarInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: parameters);
            }

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            public string ExecuteScalar(
                string sqlCommand,
                OleDbTransaction transaction)
            {
                return ExecuteScalarInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: []);
            }

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            public string ExecuteScalar(
                string sqlCommand,
                OleDbTransaction transaction,
                params OleDbParameter[] parameters)
            {
                return ExecuteScalarInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: parameters);
            }

            #endregion ExecuteScalar


            #region ExecuteNonQuery

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            private void ExecuteNonQueryInternal(
                string sqlCommand,
                OleDbTransaction transaction,
                params OleDbParameter[] parameters)
            {
                if (!TryOpenConnection(
                    functionId: 12,
                    functionName: nameof(ExecuteNonQuery)))
                {
                    // Připojení není otevřené
                    return;
                }

                OleDbCommand command =
                    (transaction == null) ?
                         new OleDbCommand()
                         {
                             Connection = connection,
                             CommandText = sqlCommand
                         } :
                         new OleDbCommand()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = sqlCommand
                         };

                foreach (OleDbParameter parameter in parameters)
                {
                    command.Parameters.Add(value: parameter);
                }

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    command.ExecuteNonQuery();
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 123,
                        FunctionName = nameof(ExecuteNonQuery),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = command
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }
            }

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            public void ExecuteNonQuery(
               string sqlCommand)
            {
                ExecuteNonQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: []);
            }

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="parameters">Parametry příkazu</param>
            public void ExecuteNonQuery(
               string sqlCommand,
               params OleDbParameter[] parameters)
            {
                ExecuteNonQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: parameters);
            }

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            public void ExecuteNonQuery(
               string sqlCommand,
               OleDbTransaction transaction)
            {
                ExecuteNonQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: []);
            }

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            public void ExecuteNonQuery(
               string sqlCommand,
               OleDbTransaction transaction,
               params OleDbParameter[] parameters)
            {
                ExecuteNonQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: parameters);
            }

            #endregion ExecuteNonQuery


            #region ExecuteList

            /// <summary>
            /// Provede skupinu SQL příkazů v jedné transakci
            /// </summary>
            /// <param name="sqlCommands">SQL prikazy</param>
            public void ExecuteList(
                List<string> sqlCommands)
            {
                if (!TryOpenConnection(
                    functionId: 13,
                    functionName: nameof(ExecuteList)))
                {
                    // Připojení není otevřené
                    return;
                }

                OleDbTransaction transaction
                    = BeginTransaction();

                if (transaction == null)
                {
                    // Transakce není otevřená
                    return;
                }

                OleDbCommand command = null;
                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    foreach (string sqlCommand in sqlCommands)
                    {
                        command = new()
                        {
                            CommandText = sqlCommand,
                            Connection = connection,
                            Transaction = transaction
                        };
                        command.ExecuteNonQuery();
                    }

                    transaction?.Commit();
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 133,
                        FunctionName = nameof(ExecuteList),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = command
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }
            }

            #endregion ExecuteList


            #region BeginTransaction

            /// <summary>
            /// Zahájení transakce
            /// </summary>
            /// <returns>Transakce</returns>
            public OleDbTransaction BeginTransaction()
            {
                if (!TryOpenConnection(
                    functionId: 14,
                    functionName: nameof(BeginTransaction)))
                {
                    // Připojení není otevřené
                    return null;
                }

                OleDbTransaction transaction = null;
                try
                {
                    transaction = Connection.BeginTransaction();
                }

                catch (Exception exception)
                {
                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 143,
                        FunctionName = nameof(BeginTransaction),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                return transaction;
            }

            #endregion Begin Transaction


            #region GetQueryColumns

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            private DataTable GetQueryColumnsInternal(
                string sqlCommand,
                OleDbTransaction transaction,
                params OleDbParameter[] parameters)
            {
                if (!TryOpenConnection(
                    functionId: 15,
                    functionName: nameof(GetQueryColumns)))
                {
                    // Připojení není otevřené
                    return null;
                }

                OleDbCommand command =
                    (transaction == null) ?
                         new OleDbCommand()
                         {
                             Connection = connection,
                             CommandText = sqlCommand
                         } :
                         new OleDbCommand()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = sqlCommand
                         };

                foreach (OleDbParameter parameter in parameters)
                {
                    command.Parameters.Add(value: parameter);
                }

                OleDbDataReader reader;
                DataTable result;

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    reader = command.ExecuteReader();
                    reader.Read();
                    result = reader.GetSchemaTable();
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    result = null;

                    RaiseExceptionReceived(new ExceptionEventArgs()
                    {
                        Id = 153,
                        FunctionName = nameof(GetQueryColumns),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }

                return result;
            }

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            public DataTable GetQueryColumns(
                string sqlCommand)
            {
                return GetQueryColumnsInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: []);
            }

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            public DataTable GetQueryColumns(
                string sqlCommand,
                params OleDbParameter[] parameters)
            {
                return GetQueryColumnsInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: parameters);
            }

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            public DataTable GetQueryColumns(
                string sqlCommand,
                OleDbTransaction transaction)
            {
                return GetQueryColumnsInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: []);
            }

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            public DataTable GetQueryColumns(
                string sqlCommand,
                OleDbTransaction transaction,
                params OleDbParameter[] parameters)
            {
                return GetQueryColumnsInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: parameters);
            }

            #endregion GetQueryColumns


            #region GetListOfTables

            /// <summary>
            /// Vrací seznam datových tabulek v Access souboru
            /// </summary>
            /// <returns>Vrací seznam datových tabulek v Access souboru</returns>
            public List<TableInfo> GetListOfTables()
            {
                List<TableInfo> tables = [];
                List<ColumnInfo> columns = [];

                if (!TryOpenConnection(
                    functionId: 16,
                    functionName: nameof(GetListOfTables)))
                {
                    // Připojení není otevřené
                    return tables;
                }

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    tables = (Connection != null)
                        ? [..
                            Connection
                                .GetSchema(collectionName: "Tables")
                                .AsEnumerable()
                                .Select(row => new TableInfo(data: row))
                          ]
                        : [];

                    columns = (Connection != null)
                        ? [..
                            Connection
                                .GetSchema(collectionName: "Columns")
                                .AsEnumerable()
                                .Select(row => new ColumnInfo(data: row))
                          ]
                        : [];

                    foreach (TableInfo table in tables)
                    {
                        table.Columns = columns.Where(a => a.TableName == table.TableName).ToList<ColumnInfo>();
                    }

                    foreach (ColumnInfo column in columns)
                    {
                        column.Table = tables.Where(a => a.TableName == column.TableName).FirstOrDefault<TableInfo>();
                    }
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    tables = [];
                    columns = [];

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 161,
                        FunctionName = nameof(GetListOfTables),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }

                return tables;
            }

            #endregion GetListOfTables


            #region GetListOfColumns

            /// <summary>
            /// Vrací seznam attributů tabulek v Access souboru
            /// </summary>
            /// <param name="tableName">Jméno tabulky</param>
            /// <returns>Vrací seznam attributů tabulek v Access souboru</returns>
            public List<ColumnInfo> GetListOfColumns(string tableName = null)
            {
                List<TableInfo> tables = [];
                List<ColumnInfo> columns = [];

                if (!TryOpenConnection(
                    functionId: 17,
                    functionName: nameof(GetListOfColumns)))
                {
                    // Připojení není otevřené
                    return columns;
                }

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    tables =
                        (Connection != null)
                            ? String.IsNullOrEmpty(value: tableName)
                                ? [..
                                    Connection
                                        .GetSchema(collectionName: "Tables")
                                        .AsEnumerable()
                                        .Select(row => new TableInfo(data: row))
                                  ]
                                : [..
                                    Connection
                                        .GetSchema(collectionName: "Tables")
                                        .AsEnumerable()
                                        .Where(a => (a.Field<string>(columnName: "TABLE_NAME") ?? String.Empty) == (tableName ?? String.Empty))
                                        .Select(row => new TableInfo(data: row))
                                  ]
                            : [];

                    columns =
                        (Connection != null)
                            ? String.IsNullOrEmpty(value: tableName)
                                ? [..Connection
                                        .GetSchema(collectionName: "Columns")
                                        .AsEnumerable()
                                        .Select(row => new ColumnInfo(data: row))
                                  ]
                                : [..
                                    Connection
                                        .GetSchema(collectionName: "Columns")
                                        .AsEnumerable()
                                        .Where(a => (a.Field<string>(columnName: "TABLE_NAME") ?? String.Empty) == (tableName ?? String.Empty))
                                        .Select(row => new ColumnInfo(data: row))
                                  ]
                            : [];

                    foreach (TableInfo table in tables)
                    {
                        table.Columns = columns.Where(a => a.TableName == table.TableName).ToList<ColumnInfo>();
                    }

                    foreach (ColumnInfo column in columns)
                    {
                        column.Table = tables.Where(a => a.TableName == column.TableName).FirstOrDefault<TableInfo>();
                    }
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    tables = [];
                    columns = [];

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 171,
                        FunctionName = nameof(GetListOfColumns),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }

                return columns;
            }

            #endregion GetListOfColumns


            #region GetRowExists

            /// <summary>
            /// Existuje řádek se zvoleným identifikátorem v datové tabulce?
            /// </summary>
            /// <param name="schemaName">Jméno databázového schéma</param>
            /// <param name="tableName">Jméno tabulky</param>
            /// <param name="colKeyName">Jméno sloupce s jednoznačným identifikátorem záznamu v tabulce (primární klíč tabulky)</param>
            /// <param name="keyValue">Hodnota identifikátoru záznamu v tabulce</param>
            /// <returns>
            /// true - pokud řádek existuje,
            /// false - pokud řádek neexistuje,
            /// null - v případě chyby
            /// </returns>
            public Nullable<bool> GetRowExists(
                string schemaName,
                string tableName,
                string colKeyName,
                Nullable<int> keyValue)
            {
                if (String.IsNullOrEmpty(value: schemaName) ||
                    String.IsNullOrEmpty(value: tableName) ||
                    String.IsNullOrEmpty(value: colKeyName))
                {
                    // Vstupy do metody nejsou zadané
                    return null;
                }

                string sqlCommand = String.Concat(
                        $"SELECT EXISTS{Environment.NewLine}",
                        $"({Environment.NewLine}",
                        $"    SELECT{Environment.NewLine}",
                        $"        a.{colKeyName} AS {colKeyName}{Environment.NewLine}",
                        $"    FROM{Environment.NewLine}",
                        $"        {schemaName}.{tableName} AS a{Environment.NewLine}",
                        $"    WHERE{Environment.NewLine}",
                        $"        a.{colKeyName} = @keyValue{Environment.NewLine}",
                        $");{Environment.NewLine}");

                OleDbParameter pKeyValue = new(
                        name: "keyValue",
                        dataType: OleDbType.Integer);

                if (keyValue != null)
                {
                    pKeyValue.Value = (int)keyValue;
                }
                else
                {
                    pKeyValue.Value = DBNull.Value;
                }

                string strResult =
                    ExecuteScalar(sqlCommand, pKeyValue);

                if (String.IsNullOrEmpty(value: strResult))
                {
                    return null;
                }
                else
                {
                    if (Boolean.TryParse(value: strResult, result: out bool boolResult))
                    {
                        return boolResult;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            #endregion GetRowExists

            #endregion SQL Command Methods

        }

    }
}