﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;

namespace ZaJi
{
    namespace PostgreSQL
    {

        /// <summary>
        /// Připojení k databázi PostgreSQL
        /// </summary>
        public partial class PostgreSQLWrapper
        {

            #region Private Fields

            /// <summary>
            /// Objekt sestavení připojovacího řetězce
            /// </summary>
            private NpgsqlConnectionStringBuilder connectionStringBuilder;

            /// <summary>
            /// Objekt připojení k databázi
            /// </summary>
            private NpgsqlConnection connection;

            /// <summary>
            /// Udržovat připojení k databázi
            /// </summary>
            private bool stayConnected;

            /// <summary>
            /// Příznak při vzniku chyby při provádění SQL příkazu
            /// </summary>
            private bool exceptionFlag;

            /// <summary>
            /// Zpráva o chybě při provádění SQL příkazu
            /// </summary>
            private string exceptionMessage;

            /// <summary>
            /// Zpráva
            /// </summary>
            private PostgresNotice notice;

            /// <summary>
            /// Nastavení
            /// </summary>
            private Setting setting;

            /// <summary>
            /// Informace o databázi
            /// </summary>
            private Catalog.DBCatalog catalog;

            #endregion Private Fields


            #region Properties

            /// <summary>
            /// Objekt sestavení připojovacího řetězce (read-only)
            /// </summary>
            private NpgsqlConnectionStringBuilder ConnectionStringBuilder
            {
                get
                {
                    return connectionStringBuilder;
                }
                set
                {
                    connectionStringBuilder = value;
                }
            }

            /// <summary>
            /// Objekt připojení k databázi (read-only)
            /// </summary>
            public NpgsqlConnection Connection
            {
                get
                {
                    return connection;
                }
                private set
                {
                    connection = value;
                }
            }

            /// <summary>
            /// Udržovat připojení k databázi
            /// </summary>
            public bool StayConnected
            {
                get
                {
                    return stayConnected;
                }
                set
                {
                    stayConnected = value;
                }
            }

            /// <summary>
            /// Příznak při vzniku chyby při provádění SQL příkazu
            /// </summary>
            public bool ExceptionFlag
            {
                get
                {
                    return exceptionFlag;
                }
                set
                {
                    exceptionFlag = value;
                }
            }

            /// <summary>
            /// Zpráva o chybě při provádění SQL příkazu
            /// </summary>
            public string ExceptionMessage
            {
                get
                {
                    return exceptionMessage;
                }
                set
                {
                    exceptionMessage = value;
                }
            }

            /// <summary>
            /// Zpráva
            /// </summary>
            public PostgresNotice Notice
            {
                get
                {
                    return notice;
                }
                set
                {
                    notice = value;
                }
            }

            /// <summary>
            /// Nastavení
            /// </summary>
            public Setting Setting
            {
                get
                {
                    return setting ??
                        new Setting();
                }
                set
                {
                    setting = value ??
                        new Setting();
                }
            }

            /// <summary>
            /// Informace o databázi (read-only)
            /// </summary>
            public Catalog.DBCatalog Catalog
            {
                get
                {
                    return catalog;
                }
                private set
                {
                    catalog = value;
                }
            }

            #endregion Properties


            #region Derived Properties

            /// <summary>
            /// Stav inicializace připojení k databázi
            /// - připojení nastaveno (true) | nenastaveno (false) (read-only)
            /// </summary>
            public bool Initialized
            {
                get
                {
                    return
                        ConnectionStringBuilder != null;
                }
            }

            /// <summary>
            /// Stav připojení k dababázi - připojeno (true) | nepřipojeno (false) (read-only)
            /// </summary>
            public bool Connected
            {
                get
                {
                    if (Connection == null)
                    {
                        return false;
                    }

                    return
                        Connection.State == ConnectionState.Open;
                }
            }

            /// <summary>
            /// Jméno aplikace (read-only)
            /// </summary>
            public string ApplicationName
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            String.Empty :
                            ConnectionStringBuilder.ApplicationName;
                }
            }

            /// <summary>
            /// IP adresa nebo jméno databázového serveru PostgreSQL (read-only)
            /// </summary>
            public string Host
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            String.Empty :
                            ConnectionStringBuilder.Host;
                }
            }

            /// <summary>
            /// TCP port databázového serveru PostgreSQL (read-only)
            /// </summary>
            public int Port
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            0 :
                            ConnectionStringBuilder.Port;
                }
            }

            /// <summary>
            /// Jméno databáze (read-only)
            /// </summary>
            public string Database
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            String.Empty :
                            ConnectionStringBuilder.Database;
                }
            }

            /// <summary>
            /// Jméno uživatele (read-only)
            /// </summary>
            public string UserName
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            String.Empty :
                            ConnectionStringBuilder.Username;
                }
            }

            /// <summary>
            /// Heslo uživatele (read-only)
            /// </summary>
            public string Password
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            String.Empty :
                            ConnectionStringBuilder.Password;
                }
            }

            /// <summary>
            /// Doba čekání na vykonání příkazu, default 30s (read-only)
            /// </summary>
            public int CommandTimeout
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            Setting.CommandTimeout :
                            ConnectionStringBuilder.CommandTimeout;
                }
            }

            /// <summary>
            /// Doba v sekundách, po které je obnoveno připojení,
            /// default 0s vypnuto (read-only)
            /// </summary>
            public int KeepAlive
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            Setting.KeepAlive :
                            ConnectionStringBuilder.KeepAlive;
                }
            }

            /// <summary>
            /// <para lang="cs">Posílat TCP keep alive
            /// podle defaultního nastavení systému</para>
            /// <para lang="en">Whether to use TCP keepalive
            /// with system defaults if overrides isn't specified</para>
            /// </summary>
            public bool TcpKeepAlive
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            Setting.TcpKeepAlive :
                            ConnectionStringBuilder.TcpKeepAlive;
                }
            }

            /// <summary>
            /// <para lang="cs">Počet sekund neaktivního připojení než je poslán TCP keepalive dotaz</para>
            /// <para lang="en">The number of seconds of connection inactivity before a TCP keepalive query is sent.</para>
            /// </summary>
            public int TcpKeepAliveTime
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            Setting.TcpKeepAliveTime :
                            ConnectionStringBuilder.TcpKeepAliveTime;
                }
            }

            /// <summary>
            /// The interval, in seconds, between when successive keep-alive packets are sent if no acknowledgement is received.
            /// </summary>
            public int TcpKeepAliveInterval
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            Setting.TcpKeepAliveInterval :
                            ConnectionStringBuilder.TcpKeepAliveInterval;
                }
            }

            /// <summary>
            /// Připojovací řetězec (read-only)
            /// </summary>
            public string ConnectionString
            {
                get
                {
                    return
                        (ConnectionStringBuilder == null) ?
                            String.Empty :
                            ConnectionStringBuilder.ConnectionString;
                }
            }

            /// <summary>
            /// Slovník s texty hlášení
            /// </summary>
            public Dictionary<string, string> Dictionary
            {
                get
                {
                    return Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => new Dictionary<string, string>()
                            {
                                 { "msgSetConnectionFailed",
                                        String.Concat(
                                        $"Connection to database $1 has failed.{Environment.NewLine}",
                                        $"Check your user name and password and try again.")
                                 },
                                 { "msgConnectionNotInitialized",
                                        $"Application has no database connection, connection can not be opened."
                                 },
                                 { "msgConnectionInfoConnected",
                                        $"Application is connected to database $1 on server $2. [$3]" },
                                 { "msgConnectionInfoNotConnected",
                                        "Application is not connected to any database." },
                                 { "msgConnectionClosed",
                                        "SQL command can not be executed, connection to database is closed." },
                                 { "msgRowNotExist",
                                        $"Row $1=$2 does not exist in data table $3.$4." },
                                 { "msgLoNullLength",
                                        $"Large object $1=$2 (table: $3.$4, row: $5=$6) has null file length." },
                                 { "msgLoOidNullLength",
                                        $"Large object oid=$1 has null file length." }
                            },

                        LanguageVersion.National => new Dictionary<string, string>()
                            {
                                 { "msgSetConnectionFailed",
                                        String.Concat(
                                        $"Přihlášení do databáze $1 se nezdařilo.{Environment.NewLine}",
                                        $"Zkontrolujte si své uživatelské jméno a heslo a pokuste se znovu připojit.")
                                 },
                                 { "msgConnectionNotInitialized",
                                        $"Aplikace nemá žádné připojení k databázi, připojení nelze otevřít."
                                 },
                                 { "msgConnectionInfoConnected",
                                        $"Aplikace je připojena k databázi $1 na serveru $2. [$3]" },
                                 { "msgConnectionInfoNotConnected",
                                        "Aplikace není připojena k žádné databázi." },
                                 { "msgConnectionClosed",
                                        "SQL příkaz nelze provést, připojení k databázi je uzavřeno." },
                                 { "msgRowNotExist",
                                        $"Řádek $1=$2 neexistuje v datové tabulce $3.$4." },
                                 { "msgLoNullLength",
                                        $"Large object $1=$2 (tabulka: $3.$4, řádek: $5=$6) má nulovou délku souboru." },
                                 { "msgLoOidNullLength",
                                        $"Large object oid=$1 má nulovou délku souboru." }
                            },

                        _ => new Dictionary<string, string>()
                            {
                                 { "msgSetConnectionFailed", String.Empty },
                                 { "msgConnectionNotInitialized", String.Empty },
                                 { "msgConnectionInfoConnected", String.Empty },
                                 { "msgConnectionInfoNotConnected", String.Empty },
                                 { "msgConnectionClosed", String.Empty },
                                 { "msgRowNotExist", String.Empty },
                                 { "msgLoNullLength", String.Empty },
                                 { "msgLoOidNullLength", String.Empty }
                            },
                    };
                }
            }

            #endregion Derived Properties


            #region Events

            #region ConnectionError (Výjimka při připojení k databázi)

            /// <summary>
            /// Delegát funkce obsluhující událost vzniku výjimky při připojení k databázi
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            public delegate void ConnectionErrorHandler(object sender, ExceptionEventArgs e);

            /// <summary>
            /// Událost vzniku výjimky při připojení k databázi
            /// </summary>
            public event ConnectionErrorHandler ConnectionError;

            /// <summary>
            /// Vyvolání události vzniku výjimky při připojení k databázi
            /// </summary>
            /// <param name="e">Parametry události</param>
            private void RaiseConnectionError(ExceptionEventArgs e)
            {
                ConnectionError?.Invoke(
                    sender: this,
                    e: e);
                System.Windows.Forms.Application.DoEvents();
            }

            #endregion ConnectionError (Výjimka při připojení k databázi)


            #region ExceptionReceived (Výjimka při zpracování SQL příkazu)

            /// <summary>
            /// Delegát funkce obsluhující událost vzniku výjimky při zpracování SQL příkazu
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            public delegate void ExceptionReceivedHandler(object sender, ExceptionEventArgs e);

            /// <summary>
            /// Událost vzniku výjimky při zpracování SQL příkazu
            /// </summary>
            public event ExceptionReceivedHandler ExceptionReceived;

            /// <summary>
            /// Vyvolání události vzniku výjimky při zpracování SQL příkazu
            /// </summary>
            /// <param name="e">Parametry události</param>
            private void RaiseExceptionReceived(ExceptionEventArgs e)
            {
                ExceptionReceived?.Invoke(
                    sender: this,
                    e: e);
                System.Windows.Forms.Application.DoEvents();
            }

            #endregion ExceptionReceived (Výjimka při zpracování SQL příkazu)


            #region ConnectionStateChanged (Změna stavu připojení)

            /// <summary>
            /// Delegát funkce obsluhující událost změny stavu připojení
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            public delegate void ConnectionStateChangedHandler(object sender, StateChangeEventArgs e);

            /// <summary>
            /// Událost změny stavu připojení
            /// </summary>
            public event ConnectionStateChangedHandler ConnectionStateChanged;

            /// <summary>
            /// Vyvolání události změny stavu připojení
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            private void RaiseConnectionStateChanged(object sender, StateChangeEventArgs e)
            {
                ConnectionStateChanged?.Invoke(
                    sender: sender,
                    e: e);
                System.Windows.Forms.Application.DoEvents();
            }

            #endregion ConnectionStateChanged (Změna stavu připojení)


            #region NoticeReceived (Přijetí Warning nebo Notice)

            /// <summary>
            /// Delegát funkce obsluhující událost přijetí Warning nebo Notice
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            public delegate void NoticeReceivedHandler(object sender, NpgsqlNoticeEventArgs e);

            /// <summary>
            /// Událost přijetí Warning nebo Notice
            /// </summary>
            public event NoticeReceivedHandler NoticeReceived;

            /// <summary>
            /// Vyvolání události přijetí Warning nebo Notice
            /// </summary>
            /// <param name="sender">Objekt odesílající událost</param>
            /// <param name="e">Parametry události</param>
            private void RaiseNoticeReceived(object sender, NpgsqlNoticeEventArgs e)
            {
                Notice = e.Notice;
                NoticeReceived?.Invoke(
                    sender: sender,
                    e: e);
                System.Windows.Forms.Application.DoEvents();
            }

            #endregion NoticeReceived (Přijetí Warning nebo Notice)

            #endregion Events


            #region Constructor

            /// <summary>
            /// Konstruktor připojení k databázi PostgreSQL
            /// </summary>
            public PostgreSQLWrapper()
            {
                ConnectionStringBuilder = null;

                Connection = new NpgsqlConnection();
                Connection.StateChange += RaiseConnectionStateChanged;
                Connection.Notice += RaiseNoticeReceived;

                StayConnected = false;

                ExceptionFlag = false;

                ExceptionMessage = String.Empty;

                Notice = null;

                Setting = new Setting();

                Catalog = new Catalog.DBCatalog(database: this);
            }

            /// <summary>
            /// Inicializace objektu připojení k databázi
            /// </summary>
            /// <param name="applicationName">Jméno aplikace</param>
            /// <param name="host">IP adresa nebo jméno databázového serveru PostgreSQL</param>
            /// <param name="port">TCP port databázového serveru PostgreSQL</param>
            /// <param name="database"> Jméno databáze</param>
            /// <param name="userName">Jméno uživatele</param>
            /// <param name="password">Heslo uživatele</param>
            /// <param name="commandTimeout">Doba čekání na vykonání příkazu</param>
            /// <param name="keepAlive">Doba v sekundách, po které je obnoveno připojení </param>
            /// <param name="tcpKeepAlive">Posílat TCP keep alive podle defaultního nastavení systému</param>
            /// <param name="tcpKeepAliveTime">Počet sekund neaktivního připojení než je poslán TCP keepalive dotaz</param>
            /// <param name="tcpKeepAliveInterval">The interval, in seconds, between when successive keep-alive packets are sent if no acknowledgement is received.</param>
            private void InitializeConnection(
                string applicationName,
                string host,
                int port,
                string database,
                string userName,
                string password,
                Nullable<int> commandTimeout = null,
                Nullable<int> keepAlive = null,
                Nullable<bool> tcpKeepAlive = null,
                Nullable<int> tcpKeepAliveTime = null,
                Nullable<int> tcpKeepAliveInterval = null)
            {
                ConnectionStringBuilder = new()
                {
                    Host = host,
                    Port = port,
                    Database = database,
                    Username = userName,
                    Password = password
                };

                Setting.Host = host;
                if (!Setting.Hosts.Contains(item: host))
                {
                    Setting.Hosts.Add(item: host);
                }

                Setting.Port = port;
                if (!Setting.Ports.Contains(item: port))
                {
                    Setting.Ports.Add(item: port);
                }

                Setting.Database = database;
                if (!Setting.Databases.Contains(item: database))
                {
                    Setting.Databases.Add(item: database);
                }

                Setting.UserName = userName;
                if (!Setting.UserNames.Contains(item: userName))
                {
                    Setting.UserNames.Add(item: userName);
                }

                if (!String.IsNullOrEmpty(value: applicationName))
                {
                    connectionStringBuilder.ApplicationName = applicationName;
                    Setting.ApplicationName = applicationName;
                }

                if (commandTimeout != null)
                {
                    connectionStringBuilder.CommandTimeout = (int)commandTimeout;
                    Setting.CommandTimeout = (int)commandTimeout;
                }

                if (keepAlive != null)
                {
                    connectionStringBuilder.KeepAlive = (int)keepAlive;
                    Setting.KeepAlive = (int)keepAlive;
                }

                if (tcpKeepAlive != null)
                {
                    connectionStringBuilder.TcpKeepAlive = (bool)tcpKeepAlive;
                    Setting.TcpKeepAlive = (bool)tcpKeepAlive;
                }

                if (tcpKeepAliveTime != null)
                {
                    connectionStringBuilder.TcpKeepAliveTime = (int)tcpKeepAliveTime;
                    Setting.TcpKeepAliveTime = (int)tcpKeepAliveTime;
                }

                if (tcpKeepAliveInterval != null)
                {
                    connectionStringBuilder.TcpKeepAliveInterval = (int)tcpKeepAliveInterval;
                    Setting.TcpKeepAliveInterval = (int)tcpKeepAliveInterval;
                }

                Connection = new(connectionString: ConnectionString);
                Connection.StateChange += RaiseConnectionStateChanged;
                Connection.Notice += RaiseNoticeReceived;

                Catalog = new(database: this);
            }

            #endregion Constructor


            #region Connection Methods

            /// <summary>
            /// Zobrazí formulář pro připojení k databázi
            /// </summary>
            /// <param name="applicationName">Jméno aplikace</param>
            public void SetConnectionDialog(
                string applicationName = null)
            {
                FormConnection formConnection = new(postgres: this);
                if (formConnection.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    SetConnection(
                        applicationName: applicationName,
                        host: formConnection.Host,
                        port: formConnection.Port,
                        database: formConnection.Database,
                        userName: formConnection.UserName,
                        password: formConnection.Password,
                        commandTimeout: formConnection.CommandTimeout,
                        keepAlive: formConnection.KeepAlive,
                        tcpKeepAlive: formConnection.TcpKeepAlive,
                        tcpKeepAliveTime: formConnection.TcpKeepAliveTime,
                        tcpKeepAliveInterval: formConnection.TcpKeepAliveInterval);
                }
            }

            /// <summary>
            /// Provede nastavení připojení podle zadaných parametrů
            /// </summary>
            /// <param name="applicationName">Jméno aplikace</param>
            /// <param name="host">IP adresa databázového serveru PostgreSQL</param>
            /// <param name="port">TCP port databázového serveru PostgreSQL</param>
            /// <param name="database">Jméno databáze</param>
            /// <param name="userName">Jméno uživatele</param>
            /// <param name="password">Heslo uživatele</param>
            /// <param name="commandTimeout">Doba čekání na vykonání příkazu</param>
            /// <param name="keepAlive">Doba v sekundách, po které je obnovováno připojení</param>
            /// <param name="tcpKeepAlive">Posílat TCP keep alive podle defaultního nastavení systému</param>
            /// <param name="tcpKeepAliveTime">Počet sekund neaktivního připojení než je poslán TCP keepalive dotaz</param>
            /// <param name="tcpKeepAliveInterval">The interval, in seconds, between when successive keep-alive packets are sent if no acknowledgement is received.</param>
            public void SetConnection(
                string applicationName,
                string host,
                int port,
                string database,
                string userName,
                string password,
                Nullable<int> commandTimeout = null,
                Nullable<int> keepAlive = null,
                Nullable<bool> tcpKeepAlive = null,
                Nullable<int> tcpKeepAliveTime = null,
                Nullable<int> tcpKeepAliveInterval = null)
            {
                NpgsqlConnectionStringBuilder testConnectionStringBuilder = new()
                {
                    Host = host,
                    Port = port,
                    Database = database,
                    Username = userName,
                    Password = password
                };

                if (!String.IsNullOrEmpty(value: applicationName))
                {
                    testConnectionStringBuilder.ApplicationName = applicationName;
                }

                if (commandTimeout != null)
                {
                    testConnectionStringBuilder.CommandTimeout = (int)commandTimeout;
                }

                if (keepAlive != null)
                {
                    testConnectionStringBuilder.KeepAlive = (int)keepAlive;
                }

                if (tcpKeepAlive != null)
                {
                    testConnectionStringBuilder.TcpKeepAlive = (bool)tcpKeepAlive;
                }

                if (tcpKeepAliveTime != null)
                {
                    testConnectionStringBuilder.TcpKeepAliveTime = (int)tcpKeepAliveTime;
                }

                if (tcpKeepAliveInterval != null)
                {
                    testConnectionStringBuilder.TcpKeepAliveInterval = (int)tcpKeepAliveInterval;
                }

                NpgsqlConnection testConnection = new(
                    connectionString: testConnectionStringBuilder.ConnectionString);

                try
                {
                    testConnection.Open();

                    if (testConnection.State == ConnectionState.Open)
                    {
                        InitializeConnection(
                            applicationName: applicationName,
                            host: host,
                            port: port,
                            database: database,
                            userName: userName,
                            password: password,
                            commandTimeout: commandTimeout,
                            keepAlive: keepAlive,
                            tcpKeepAlive: tcpKeepAlive,
                            tcpKeepAliveTime: tcpKeepAliveTime,
                            tcpKeepAliveInterval: tcpKeepAliveInterval);
                    }
                    else
                    {
                        string message =
                            (Dictionary.TryGetValue(
                                key: "msgSetConnectionFailed",
                                out string msgSetConnectionFailed)
                                    ? msgSetConnectionFailed
                                    : String.Empty)
                            .Replace(oldValue: "$1", newValue: Database);

                        RaiseConnectionError(e: new ExceptionEventArgs()
                        {
                            Id = 11,
                            FunctionName = nameof(SetConnection),
                            MessageBoxCaption = String.Empty,
                            MessageBoxText = message,
                            ExceptionObject = new Exception(message: message),
                            CommandObject = null,
                        });

                        ConnectionStringBuilder = null;
                    }
                }

                catch (Exception exception)
                {
                    string message =
                            (Dictionary.TryGetValue(
                                key: "msgSetConnectionFailed",
                                out string msgSetConnectionFailed)
                                    ? msgSetConnectionFailed
                                    : String.Empty)
                            .Replace(oldValue: "$1", newValue: Database);

                    RaiseConnectionError(new ExceptionEventArgs()
                    {
                        Id = 12,
                        FunctionName = nameof(SetConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });

                    ConnectionStringBuilder = null;
                }

                finally
                {
                    testConnection.Close();
                }
            }

            /// <summary>
            /// Otevře připojení k databázi PostgreSQL
            /// </summary>
            public void OpenConnection()
            {
                if (!Initialized)
                {
                    string message =
                            Dictionary.TryGetValue(
                                key: "msgConnectionNotInitialized",
                                out string msgConnectionNotInitialized)
                            ? msgConnectionNotInitialized
                            : String.Empty;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 21,
                        FunctionName = nameof(OpenConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = new Exception(message: message),
                        CommandObject = null
                    });

                    return;
                }

                if (Connected)
                {
                    return;
                }

                try
                {
                    Connection.Open();
                }

                catch (Exception exception)
                {
                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 22,
                        FunctionName = nameof(OpenConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }
            }

            /// <summary>
            /// Zavře připojení k databázi PostgreSQL
            /// </summary>
            public void CloseConnection()
            {
                if (!Initialized)
                {
                    return;
                }

                if (connection.State == ConnectionState.Closed)
                {
                    return;
                }

                try
                {
                    Connection.Close();
                }
                catch (Exception exception)
                {
                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 31,
                        FunctionName = nameof(CloseConnection),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }
            }

            /// <summary>
            /// Vrátí informační text o nastaveném připojení k databázi PostgreSQL
            /// </summary>
            /// <returns>Informační text o nastaveném připojení k databázi PostgreSQL</returns>
            public string ConnectionInfo()
            {
                return
                    Initialized
                        ? Dictionary.TryGetValue(key: "msgConnectionInfoConnected", out string msgConnectionInfoConnected)
                            ? msgConnectionInfoConnected.Replace(oldValue: "$1", newValue: Database)
                                .Replace(oldValue: "$2", newValue: Host)
                                .Replace(oldValue: "$3", newValue: UserName)
                            : String.Empty
                        : Dictionary.TryGetValue(key: "msgConnectionInfoNotConnected", out string msgConnectionInfoNotConnected)
                            ? msgConnectionInfoNotConnected
                            : String.Empty;
            }

            /// <summary>
            /// Vyzkouší, zda je připojení inicializované,
            /// pokud ano, pak se ho pokusí otevřít (pokud již otevřené není)
            /// </summary>
            /// <param name="functionId">Identifikační číslo funkce</param>
            /// <param name="functionName">Jméno funkce</param>
            /// <returns>Vrátí true pokud je připojení inicializované a otevřené, jinak false</returns>
            public bool TryOpenConnection(
                int functionId,
                string functionName)
            {
                if (!Initialized)
                {
                    string message =
                        Dictionary.TryGetValue(
                            key: "msgConnectionNotInitialized",
                            out string value)
                        ? value
                        : string.Empty;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = (10 * functionId) + 1,
                        FunctionName = functionName,
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = new Exception(message: message),
                        CommandObject = null
                    });

                    return false;
                }

                OpenConnection();

                if (!Connected)
                {
                    string message =
                        Dictionary.TryGetValue(key: "msgConnectionClosed", out string value)
                        ? value
                        : String.Empty;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = (10 * functionId) + 2,
                        FunctionName = functionName,
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = message,
                        ExceptionObject = new Exception(message: message),
                        CommandObject = null
                    });
                    return false;
                }

                return true;
            }

            /// <summary>
            /// Vrací textový řetězec reprezentující objekt
            /// </summary>
            /// <returns>Vrací textový řetězec reprezentující objekt</returns>
            public override string ToString()
            {
                return
                    String.Concat(
                        $"Object PostgreSQLWrapper:{Environment.NewLine}",
                        $"application name = {ApplicationName};{Environment.NewLine}",
                        $"host = {Host};{Environment.NewLine}",
                        $"port = {Port};{Environment.NewLine}",
                        $"database = {Database};{Environment.NewLine}",
                        $"user name = {UserName};{Environment.NewLine}",
                        $"command timeout = {CommandTimeout};{Environment.NewLine}",
                        $"keep alive = {KeepAlive}; {Environment.NewLine}",
                        $"tcp keep alive = {TcpKeepAlive}; {Environment.NewLine}",
                        $"tcp keep alive time = {TcpKeepAliveTime}; {Environment.NewLine}",
                        $"tcp keep alive interval = {TcpKeepAliveInterval}; {Environment.NewLine}",
                        $"stay connected = {StayConnected};{Environment.NewLine}",
                        $"initialized = {Initialized};{Environment.NewLine}",
                        $"connected = {Connected};{Environment.NewLine}"
                    );
            }

            #endregion Connection Methods


            #region SQL Command Methods

            #region ExecuteQuery

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            private DataTable ExecuteQueryInternal(
                string sqlCommand,
                NpgsqlTransaction transaction,
                params NpgsqlParameter[] parameters)
            {
                if (!TryOpenConnection(
                    functionId: 10,
                    functionName: nameof(ExecuteQuery)))
                {
                    // Připojení není otevřené
                    return new DataTable();
                }

                NpgsqlCommand command =
                    (transaction == null) ?
                         new NpgsqlCommand()
                         {
                             Connection = connection,
                             CommandText = sqlCommand
                         } :
                         new NpgsqlCommand()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = sqlCommand
                         };

                foreach (NpgsqlParameter parameter in parameters)
                {
                    command.Parameters.Add(value: parameter);
                }

                NpgsqlDataAdapter adapter = new()
                {
                    SelectCommand = command
                };

                DataTable result = new();

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    adapter.Fill(dataTable: result);
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    result = new DataTable();

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 103,
                        FunctionName = nameof(ExecuteQuery),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = command
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }

                return result;
            }

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            public DataTable ExecuteQuery(
                string sqlCommand)
            {
                return ExecuteQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: []);
            }

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            public DataTable ExecuteQuery(
                string sqlCommand,
                params NpgsqlParameter[] parameters)
            {
                return ExecuteQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: parameters);
            }

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            public DataTable ExecuteQuery(
                string sqlCommand,
                NpgsqlTransaction transaction)
            {
                return ExecuteQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: []);
            }

            /// <summary>
            /// Datová tabulka s výsledky SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku s výsledky SQL příkazu</returns>
            public DataTable ExecuteQuery(
                string sqlCommand,
                NpgsqlTransaction transaction,
                params NpgsqlParameter[] parameters)
            {
                return ExecuteQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: parameters);
            }

            #endregion ExecuteQuery


            #region ExecuteScalar

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            private string ExecuteScalarInternal(
                string sqlCommand,
                NpgsqlTransaction transaction,
                params NpgsqlParameter[] parameters)
            {
                if (!TryOpenConnection(
                    functionId: 11,
                    functionName: nameof(ExecuteScalar)))
                {
                    // Připojení není otevřené
                    return null;
                }

                NpgsqlCommand command =
                    (transaction == null) ?
                         new NpgsqlCommand()
                         {
                             Connection = connection,
                             CommandText = sqlCommand
                         } :
                         new NpgsqlCommand()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = sqlCommand
                         };

                foreach (NpgsqlParameter parameter in parameters)
                {
                    command.Parameters.Add(value: parameter);
                }

                string result;

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    object obj = command.ExecuteScalar();

                    result = (obj == null) ? null :
                        Convert.ToString(value: obj);
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    result = null;

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 113,
                        FunctionName = nameof(ExecuteScalar),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = command
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }

                return result;
            }

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            public string ExecuteScalar(
                string sqlCommand)
            {
                return ExecuteScalarInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: []);
            }

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            public string ExecuteScalar(
               string sqlCommand,
               params NpgsqlParameter[] parameters)
            {
                return ExecuteScalarInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: parameters);
            }

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            public string ExecuteScalar(
                string sqlCommand,
                NpgsqlTransaction transaction)
            {
                return ExecuteScalarInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: []);
            }

            /// <summary>
            /// Text s výsledkem SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací text s výsledkem SQL příkazu</returns>
            public string ExecuteScalar(
                string sqlCommand,
                NpgsqlTransaction transaction,
                params NpgsqlParameter[] parameters)
            {
                return ExecuteScalarInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: parameters);
            }

            #endregion ExecuteScalar


            #region ExecuteNonQuery

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            private void ExecuteNonQueryInternal(
                string sqlCommand,
                NpgsqlTransaction transaction,
                params NpgsqlParameter[] parameters)
            {
                if (!TryOpenConnection(
                    functionId: 12,
                    functionName: nameof(ExecuteNonQuery)))
                {
                    // Připojení není otevřené
                    return;
                }

                NpgsqlCommand command =
                    (transaction == null) ?
                         new NpgsqlCommand()
                         {
                             Connection = connection,
                             CommandText = sqlCommand
                         } :
                         new NpgsqlCommand()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = sqlCommand
                         };

                foreach (NpgsqlParameter parameter in parameters)
                {
                    command.Parameters.Add(value: parameter);
                }

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    command.ExecuteNonQuery();
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 123,
                        FunctionName = nameof(ExecuteNonQuery),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = command
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }
            }

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            public void ExecuteNonQuery(
               string sqlCommand)
            {
                ExecuteNonQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: []);
            }

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="parameters">Parametry příkazu</param>
            public void ExecuteNonQuery(
               string sqlCommand,
               params NpgsqlParameter[] parameters)
            {
                ExecuteNonQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: parameters);
            }

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            public void ExecuteNonQuery(
               string sqlCommand,
               NpgsqlTransaction transaction)
            {
                ExecuteNonQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: []);
            }

            /// <summary>
            /// Provede SQL příkaz
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            public void ExecuteNonQuery(
               string sqlCommand,
               NpgsqlTransaction transaction,
               params NpgsqlParameter[] parameters)
            {
                ExecuteNonQueryInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: parameters);
            }

            #endregion ExecuteNonQuery


            #region ExecuteList

            /// <summary>
            /// Provede skupinu SQL příkazů v jedné transakci
            /// </summary>
            /// <param name="sqlCommands">SQL prikazy</param>
            public void ExecuteList(
                List<string> sqlCommands)
            {
                if (!TryOpenConnection(
                    functionId: 13,
                    functionName: nameof(ExecuteList)))
                {
                    // Připojení není otevřené
                    return;
                }

                NpgsqlTransaction transaction
                    = BeginTransaction();

                if (transaction == null)
                {
                    // Transakce není otevřená
                    return;
                }

                NpgsqlCommand command = null;
                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    foreach (string sqlCommand in sqlCommands)
                    {
                        command = new()
                        {
                            CommandText = sqlCommand,
                            Connection = connection,
                            Transaction = transaction
                        };
                        command.ExecuteNonQuery();
                    }

                    transaction?.Commit();
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 133,
                        FunctionName = nameof(ExecuteList),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = command
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }
            }

            #endregion ExecuteList


            #region BeginTransaction

            /// <summary>
            /// Zahájení transakce
            /// </summary>
            /// <returns>Transakce</returns>
            public NpgsqlTransaction BeginTransaction()
            {
                if (!TryOpenConnection(
                    functionId: 14,
                    functionName: nameof(BeginTransaction)))
                {
                    // Připojení není otevřené
                    return null;
                }

                NpgsqlTransaction transaction = null;
                try
                {
                    transaction = Connection.BeginTransaction();
                }

                catch (Exception exception)
                {
                    RaiseExceptionReceived(e: new ExceptionEventArgs()
                    {
                        Id = 143,
                        FunctionName = nameof(BeginTransaction),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = exception.Message,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                return transaction;
            }

            #endregion Begin Transaction


            #region GetQueryColumns

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            private DataTable GetQueryColumnsInternal(
                string sqlCommand,
                NpgsqlTransaction transaction,
                params NpgsqlParameter[] parameters)
            {
                if (!TryOpenConnection(
                    functionId: 15,
                    functionName: nameof(GetQueryColumns)))
                {
                    // Připojení není otevřené
                    return null;
                }

                NpgsqlCommand command =
                    (transaction == null) ?
                         new NpgsqlCommand()
                         {
                             Connection = connection,
                             CommandText = sqlCommand
                         } :
                         new NpgsqlCommand()
                         {
                             Connection = connection,
                             Transaction = transaction,
                             CommandText = sqlCommand
                         };

                foreach (NpgsqlParameter parameter in parameters)
                {
                    command.Parameters.Add(value: parameter);
                }

                NpgsqlDataReader reader;
                DataTable result;

                try
                {
                    ExceptionFlag = false;
                    ExceptionMessage = String.Empty;

                    reader = command.ExecuteReader();
                    reader.Read();
                    result = reader.GetSchemaTable();
                }

                catch (Exception exception)
                {
                    ExceptionFlag = true;
                    ExceptionMessage = exception.Message;

                    try
                    {
                        transaction?.Rollback();
                    }
                    catch (InvalidOperationException exception2)
                    {
                        ExceptionMessage = $"{ExceptionMessage}{Environment.NewLine}{exception2.Message}";
                    }

                    result = null;

                    RaiseExceptionReceived(new ExceptionEventArgs()
                    {
                        Id = 153,
                        FunctionName = nameof(GetQueryColumns),
                        MessageBoxCaption = String.Empty,
                        MessageBoxText = ExceptionMessage,
                        ExceptionObject = exception,
                        CommandObject = null
                    });
                }

                finally
                {
                    if (!StayConnected)
                    {
                        CloseConnection();
                    }
                }

                return result;
            }

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            public DataTable GetQueryColumns(
                string sqlCommand)
            {
                return GetQueryColumnsInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: []);
            }

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            public DataTable GetQueryColumns(
                string sqlCommand,
                params NpgsqlParameter[] parameters)
            {
                return GetQueryColumnsInternal(
                    sqlCommand: sqlCommand,
                    transaction: null,
                    parameters: parameters);
            }

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            public DataTable GetQueryColumns(
                string sqlCommand,
                NpgsqlTransaction transaction)
            {
                return GetQueryColumnsInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: []);
            }

            /// <summary>
            /// Seznam sloupců ve výsledku SQL příkazu
            /// </summary>
            /// <param name="sqlCommand">SQL příkaz</param>
            /// <param name="transaction">Transakce</param>
            /// <param name="parameters">Parametry příkazu</param>
            /// <returns>Vrací datovou tabulku se seznamem sloupců v SQL příkazu</returns>
            public DataTable GetQueryColumns(
                string sqlCommand,
                NpgsqlTransaction transaction,
                params NpgsqlParameter[] parameters)
            {
                return GetQueryColumnsInternal(
                    sqlCommand: sqlCommand,
                    transaction: transaction,
                    parameters: parameters);
            }

            #endregion GetQueryColumns


            #region GetRowExists

            /// <summary>
            /// Existuje řádek se zvoleným identifikátorem v datové tabulce?
            /// </summary>
            /// <param name="schemaName">Jméno databázového schéma</param>
            /// <param name="tableName">Jméno tabulky</param>
            /// <param name="colKeyName">Jméno sloupce s jednoznačným identifikátorem záznamu v tabulce (primární klíč tabulky)</param>
            /// <param name="keyValue">Hodnota identifikátoru záznamu v tabulce</param>
            /// <returns>
            /// true - pokud řádek existuje,
            /// false - pokud řádek neexistuje,
            /// null - v případě chyby
            /// </returns>
            public Nullable<bool> GetRowExists(
                string schemaName,
                string tableName,
                string colKeyName,
                Nullable<int> keyValue)
            {
                if (String.IsNullOrEmpty(value: schemaName) ||
                    String.IsNullOrEmpty(value: tableName) ||
                    String.IsNullOrEmpty(value: colKeyName))
                {
                    // Vstupy do metody nejsou zadané
                    return null;
                }

                string sqlCommand = String.Concat(
                        $"SELECT EXISTS{Environment.NewLine}",
                        $"({Environment.NewLine}",
                        $"    SELECT{Environment.NewLine}",
                        $"        a.{colKeyName} AS {colKeyName}{Environment.NewLine}",
                        $"    FROM{Environment.NewLine}",
                        $"        {schemaName}.{tableName} AS a{Environment.NewLine}",
                        $"    WHERE{Environment.NewLine}",
                        $"        a.{colKeyName} = @keyValue{Environment.NewLine}",
                        $");{Environment.NewLine}");

                NpgsqlParameter pKeyValue = new(
                        parameterName: "keyValue",
                        parameterType: NpgsqlDbType.Integer);

                if (keyValue != null)
                {
                    pKeyValue.Value = (int)keyValue;
                }
                else
                {
                    pKeyValue.Value = DBNull.Value;
                }

                string strResult =
                    ExecuteScalar(sqlCommand, pKeyValue);

                if (String.IsNullOrEmpty(value: strResult))
                {
                    return null;
                }
                else
                {
                    if (Boolean.TryParse(value: strResult, result: out bool boolResult))
                    {
                        return boolResult;
                    }
                    else
                    {
                        return null;
                    }
                }
            }

            #endregion GetRowExists

            #endregion SQL Command Methods

        }

    }
}