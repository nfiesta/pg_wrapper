﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Databázová extenze
            /// </summary>
            public class DBExtension
                : IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam databázových extenzí
                /// </summary>
                private DBExtensionList extensions;

                /// <summary>
                /// Metadata databázových extenzí
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu databázové extenze
                /// </summary>
                /// <param name="extensions">Seznam databázových extenzí</param>
                /// <param name="data">Metadata databázových extenzí</param>
                public DBExtension(DBExtensionList extensions, DataRow data)
                {
                    Extensions = extensions;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam databázových extenzí (read-only)
                /// </summary>
                public DBExtensionList Extensions
                {
                    get
                    {
                        return extensions;
                    }
                    private set
                    {
                        extensions = value;
                    }
                }

                /// <summary>
                /// Metadata databázových extenzí (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Katalogy databáze (read-only)
                /// </summary>
                public DBCatalog Catalog
                {
                    get
                    {
                        return Extensions.Catalog;
                    }
                }

                /// <summary>
                /// Identifikátor databázové extenze (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "oid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Jméno databázové extenze (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "extname",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Vlastník databázové extenze (read-only)
                /// </summary>
                public Nullable<uint> OwnerId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "extowner",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Databázové schéma - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> SchemaId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "extnamespace",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Databázové schéma - objekt (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        if (SchemaId == null)
                        {
                            return null;
                        }

                        return Extensions.Catalog.Schemas[(uint)SchemaId];
                    }
                }

                /// <summary>
                /// Extenze může být přemístěna do jiného schéma (read-only)
                /// </summary>
                public Nullable<bool> Relocatable
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "extrelocatable",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Verze databázové extenze (read-only)
                /// </summary>
                public string Version
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "extversion",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Schema.Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }

                /// <summary>
                /// SQL definice databázové extenze (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        if (Name == null)
                        {
                            return String.Empty;
                        }

                        StringBuilder sb = new();
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--{tab}Extension: {Name}");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();

                        sb.AppendLine(value: $"{tab} -- DROP EXTENSION IF EXISTS {Name}");
                        sb.AppendLine();

                        sb.AppendLine(value: $"{tab}CREATE EXTENSION {Name}");

                        if (Schema != null)
                        {
                            sb.AppendLine(value: $"{tab}{tab}SCHEMA \"{Schema.Name ?? String.Empty}\"");
                        }

                        if (Version != null)
                        {
                            sb.AppendLine(value: $"{tab}{tab}VERSION {Version};");
                        }

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaná databázová extenze je stejná
                /// </summary>
                /// <param name="obj">Zadaná databázová extenze</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBExtension Type, return False
                    if (obj is not DBExtension)
                    {
                        return false;
                    }

                    return
                        Id == ((DBExtension)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis databázové extenze
                /// </summary>
                /// <returns>Textový popis databázové extenze</returns>
                public override string ToString()
                {
                    return Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Database extension oid = {Id} name = {Name} version = {Version}.",
                        LanguageVersion.National => $"Databázová extenze oid = {Id} name = {Name} version = {Version}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam databázových extenzí
            /// </summary>
            public class DBExtensionList
            {

                #region Private Fields

                /// <summary>
                /// Katalogy databáze
                /// </summary>
                private DBCatalog catalog;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu databázových extenzí
                /// </summary>
                /// <param name="catalog">Katalogy databáze</param>
                public DBExtensionList(DBCatalog catalog)
                {
                    Catalog = catalog;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Katalogy databáze (read-only)
                /// </summary>
                public DBCatalog Catalog
                {
                    get
                    {
                        return catalog;
                    }
                    private set
                    {
                        catalog = value;
                    }
                }

                /// <summary>
                /// Data databázových extenzí (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Catalog.PgExtension
                            .Select();

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgExtension();
                        }
                    }
                }

                /// <summary>
                /// Seznam databázových extenzí (read-only)
                /// </summary>
                public List<DBExtension> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBExtension(extensions: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Databázová extenze podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor databázové extenze</param>
                /// <returns>Databázová extenze</returns>
                public DBExtension this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBExtension>();
                    }
                }

                /// <summary>
                /// Databázová extenze podle jména
                /// </summary>
                /// <param name="name">Jméno databázové extenze</param>
                /// <returns>Databázová extenze</returns>
                public DBExtension this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBExtension>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}