﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Omezení
            /// </summary>
            public abstract class DBConstraint
            {

                #region Private Fields

                /// <summary>
                /// Tabulka
                /// </summary>
                private DBTable table;

                /// <summary>
                /// Metadata omezení
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu omezení
                /// </summary>
                /// <param name="table">Tabulka</param>
                /// <param name="data">Metadata omezení</param>
                public DBConstraint(DBTable table, DataRow data)
                {
                    Table = table;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBTable Table
                {
                    get
                    {
                        return table;
                    }
                    private set
                    {
                        table = value;
                    }
                }

                /// <summary>
                /// Metadata omezení (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Identifikátor omezení (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "oid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Jméno omezení (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "conname",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Databázové schéma - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> SchemaId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "connamespace",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Tabulka - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> TableId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "conrelid",
                                defaultValue: null);
                    }
                }


                /// <summary>
                /// Typ omezení (read-only)
                /// </summary>
                public ConType ConType
                {
                    get
                    {
                        Nullable<char> val =
                            Functions.GetNCharArg(
                                row: Data,
                                name: "contype",
                                defaultValue: null);

                        if (val == null)
                        {
                            return ConType.Unknown;
                        }

                        return val switch
                        {
                            'c' => ConType.CheckConstraint,
                            'f' => ConType.ForeignKey,
                            'p' => ConType.PrimaryKey,
                            'u' => ConType.UniqueConstraint,
                            't' => ConType.ContraintTrigger,
                            'x' => ConType.ExclusionContraint,
                            _ => ConType.Unknown,
                        };
                    }
                }


                /// <summary>
                /// Omezení je odložitelné (read-only)
                /// </summary>
                public Nullable<bool> Deferrable
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "condeferrable",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Omezení je odložitelné - text (read-only)
                /// </summary>
                protected string DeferrableText
                {
                    get
                    {
                        return
                            (Deferrable == null)
                                ? String.Empty
                                : (bool)Deferrable
                                    ? "DEFERRABLE"
                                    : "NOT DEFERRABLE";
                    }
                }


                /// <summary>
                /// Omezení je odloženo (read-only)
                /// </summary>
                public Nullable<bool> Deferred
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "condeferred",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Omezení je odloženo - text (read-only)
                /// </summary>
                protected string DeferredText
                {
                    get
                    {
                        return
                            (Deferrable == null)
                                ? String.Empty
                                : (bool)Deferred
                                    ? "INITIALLY DEFERRED"
                                    : "INITIALLY IMMEDIATE";
                    }
                }


                /// <summary>
                /// Omezení je validováno (read-only)
                /// </summary>
                public Nullable<bool> Validated
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "convalidated",
                                defaultValue: null);
                    }
                }


                /// <summary>
                /// Index - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> IndexId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "conindid",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Index - objekt (read-only)
                /// </summary>
                public DBIndex Index
                {
                    get
                    {
                        if (IndexId == null)
                        {
                            return null;
                        }

                        return
                            Table.Schema.Catalog.Schemas.Items
                                .SelectMany(a => a.Indexes.Items)
                                .Where(a => a.Id == IndexId)
                                .FirstOrDefault<DBIndex>();
                    }
                }


                /// <summary>
                /// Cizí tabulka - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> ForeignTableId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "confrelid",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Cizí tabulka - objekt (read-only)
                /// </summary>
                public DBTable ForeignTable
                {
                    get
                    {
                        if (ForeignTableId == null)
                        {
                            return null;
                        }

                        return
                            Table.Schema.Catalog.Schemas.Items
                                .SelectMany(a => a.Tables.Items)
                                .Where(a => a.Id == ForeignTableId)
                                .FirstOrDefault<DBTable>();
                    }
                }


                /// <summary>
                /// Akce cizího klíče při aktualizaci záznamu (read-only)
                /// </summary>
                public ForeignKeyOnUpdateAction OnUpdateAction
                {
                    get
                    {
                        Nullable<char> val =
                            Functions.GetNCharArg(
                                row: Data,
                                name: "confupdtype",
                                defaultValue: null);

                        if (val == null)
                        {
                            return ForeignKeyOnUpdateAction.Unknown;
                        }

                        return val switch
                        {
                            'a' => ForeignKeyOnUpdateAction.NoAction,
                            'r' => ForeignKeyOnUpdateAction.Restrict,
                            'c' => ForeignKeyOnUpdateAction.Cascade,
                            'n' => ForeignKeyOnUpdateAction.SetNull,
                            'd' => ForeignKeyOnUpdateAction.SetDefault,
                            _ => ForeignKeyOnUpdateAction.Unknown,
                        };
                    }
                }

                /// <summary>
                /// Akce cizího klíče při aktualizaci záznamu - text(read-only)
                /// </summary>
                protected string OnUpdateActionText
                {
                    get
                    {
                        return OnUpdateAction switch
                        {
                            ForeignKeyOnUpdateAction.NoAction => "ON UPDATE NO ACTION",
                            ForeignKeyOnUpdateAction.Restrict => "ON UPDATE RESTRICT",
                            ForeignKeyOnUpdateAction.Cascade => "ON UPDATE CASCADE",
                            ForeignKeyOnUpdateAction.SetNull => "ON UPDATE SET NULL",
                            ForeignKeyOnUpdateAction.SetDefault => "ON UPDATE SET DEFAULT",
                            _ => String.Empty,
                        };
                    }
                }


                /// <summary>
                /// Akce cizího klíče při smazání záznamu (read-only)
                /// </summary>
                public ForeignKeyOnDeleteAction OnDeleteAction
                {
                    get
                    {
                        Nullable<char> val =
                            Functions.GetNCharArg(
                                row: Data,
                                name: "confdeltype",
                                defaultValue: null);

                        if (val == null)
                        {
                            return ForeignKeyOnDeleteAction.Unknown;
                        }

                        return val switch
                        {
                            'a' => ForeignKeyOnDeleteAction.NoAction,
                            'r' => ForeignKeyOnDeleteAction.Restrict,
                            'c' => ForeignKeyOnDeleteAction.Cascade,
                            'n' => ForeignKeyOnDeleteAction.SetNull,
                            'd' => ForeignKeyOnDeleteAction.SetDefault,
                            _ => ForeignKeyOnDeleteAction.Unknown,
                        };
                    }
                }

                /// <summary>
                /// Akce cizího klíče při smazání záznamu - text (read-only)
                /// </summary>
                protected string OnDeleteActionText
                {
                    get
                    {
                        return OnDeleteAction switch
                        {
                            ForeignKeyOnDeleteAction.NoAction => "ON DELETE NO ACTION",
                            ForeignKeyOnDeleteAction.Restrict => "ON DELETE RESTRICT",
                            ForeignKeyOnDeleteAction.Cascade => "ON DELETE CASCADE",
                            ForeignKeyOnDeleteAction.SetNull => "ON DELETE SET NULL",
                            ForeignKeyOnDeleteAction.SetDefault => "ON DELETE SET DEFAULT",
                            _ => String.Empty,
                        };
                    }
                }


                /// <summary>
                /// Typ porovnání (read-only)
                /// </summary>
                public ForeignKeyMatchType MatchType
                {
                    get
                    {
                        Nullable<char> val =
                            Functions.GetNCharArg(
                                row: Data,
                                name: "confmatchtype",
                                defaultValue: null);

                        if (val == null)
                        {
                            return ForeignKeyMatchType.Unknown;
                        }

                        return val switch
                        {
                            'f' => ForeignKeyMatchType.Full,
                            'p' => ForeignKeyMatchType.Partial,
                            's' => ForeignKeyMatchType.Simple,
                            _ => ForeignKeyMatchType.Unknown,
                        };
                    }
                }

                /// <summary>
                /// Typ porovnání - text (read-only)
                /// </summary>
                protected string MatchTypeText
                {
                    get
                    {
                        return MatchType switch
                        {
                            ForeignKeyMatchType.Full => "MATCH FULL",
                            ForeignKeyMatchType.Partial => "MATCH PARTIAL",
                            ForeignKeyMatchType.Simple => "MATCH SIMPLE",
                            _ => String.Empty,
                        };
                    }
                }


                /// <summary>
                /// Seznam sloupců zahrnutých do omezení - identifikátory (read-only)
                /// </summary>
                public List<uint> ConstraintColumnsIds
                {
                    get
                    {
                        return
                            Functions.StringToUIntList(
                                text: Functions.GetStringArg(
                                    row: Data,
                                    name: "conkey",
                                    defaultValue: null),
                                separator: ';');
                    }
                }

                /// <summary>
                /// Seznam sloupců zahrnutých do omezení - objekty (read-only)
                /// </summary>
                public List<DBColumn> ConstraintColumns
                {
                    get
                    {
                        if (Table == null)
                        {
                            return [];
                        }

                        return
                            Table.Columns.Items
                                .Where(a => ConstraintColumnsIds.Contains(item: a.Id))
                                .ToList<DBColumn>();
                    }
                }

                /// <summary>
                /// Seznam sloupců zahrnutých do omezení - text (read-only)
                /// </summary>
                protected string ConstraintColumnsText
                {
                    get
                    {
                        return
                            ConstraintColumns.Count != 0
                                ? ConstraintColumns
                                    .Select(a => a.QName)
                                    .Aggregate((a, b) => $"{a}, {b}")
                                : String.Empty;
                    }
                }


                /// <summary>
                /// Seznam sloupců cizí tabulky - identifikátory (read-only)
                /// </summary>
                public List<uint> ForeignColumnsIds
                {
                    get
                    {
                        return
                            Functions.StringToUIntList(
                                text: Functions.GetStringArg(
                                    row: Data,
                                    name: "confkey",
                                    defaultValue: null),
                                separator: ';');
                    }
                }

                /// <summary>
                /// Seznam sloupců cizí tabulky - objekty (read-only)
                /// </summary>
                public List<DBColumn> ForeignColumns
                {
                    get
                    {
                        if (ForeignTable == null)
                        {
                            return [];
                        }

                        return
                            ForeignTable.Columns.Items
                                .Where(a => ForeignColumnsIds.Contains(item: a.Id))
                                .ToList<DBColumn>();
                    }
                }

                /// <summary>
                /// Seznam sloupců cizí tabulky - text (read-only)
                /// </summary>
                protected string ForeignColumnsText
                {
                    get
                    {
                        return
                            ForeignColumns.Count != 0
                                    ? ForeignColumns
                                        .Select(a => a.QName)
                                        .Aggregate((a, b) => $"{a}, {b}")
                                    : String.Empty;
                    }
                }


                /// <summary>
                /// Podmínka omezení (read-only)
                /// </summary>
                public string Condition
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "consrc",
                            defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Definice omezení (read-only)
                /// </summary>
                public string Definition
                {
                    get
                    {
                        return Functions.GetStringArg(
                            row: Data,
                            name: "condef",
                            defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Table.Schema.Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadané omezení je stejné
                /// </summary>
                /// <param name="obj">Zadané omezení</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBConstraint Type, return False
                    if (obj is not DBConstraint)
                    {
                        return false;
                    }

                    return
                        Id == ((DBConstraint)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }

            /// <summary>
            /// Typ omezení
            /// c,f,p,u,t,x
            /// </summary>
            public enum ConType
            {

                /// <summary>
                /// CheckConstraint
                /// </summary>
                CheckConstraint = (int)'c',

                /// <summary>
                /// ForeignKey
                /// </summary>
                ForeignKey = (int)'f',

                /// <summary>
                /// PrimaryKey
                /// </summary>
                PrimaryKey = (int)'p',

                /// <summary>
                /// UniqueConstraint
                /// </summary>
                UniqueConstraint = (int)'u',

                /// <summary>
                /// ContraintTrigger
                /// </summary>
                ContraintTrigger = (int)'t',

                /// <summary>
                /// ExclusionContraint
                /// </summary>
                ExclusionContraint = (int)'x',

                /// <summary>
                /// Unknown
                /// </summary>
                Unknown = (int)'0'

            };

            /// <summary>
            /// Foreign key update action
            /// a,r,c,n,d
            /// </summary>
            public enum ForeignKeyOnUpdateAction
            {

                /// <summary>
                /// NoAction
                /// </summary>
                NoAction = (int)'a',

                /// <summary>
                /// Restrict
                /// </summary>
                Restrict = (int)'r',

                /// <summary>
                /// Cascade
                /// </summary>
                Cascade = (int)'c',

                /// <summary>
                /// SetNull
                /// </summary>
                SetNull = (int)'n',

                /// <summary>
                /// SetDefault
                /// </summary>
                SetDefault = (int)'d',

                /// <summary>
                /// Unknown
                /// </summary>
                Unknown = (int)'0'

            };

            /// <summary>
            /// Foreign key deletion action
            /// a,r,c,n,d
            /// </summary>
            public enum ForeignKeyOnDeleteAction
            {

                /// <summary>
                /// NoAction
                /// </summary>
                NoAction = (int)'a',

                /// <summary>
                /// Restrict
                /// </summary>
                Restrict = (int)'r',

                /// <summary>
                /// Cascade
                /// </summary>
                Cascade = (int)'c',

                /// <summary>
                /// SetNull
                /// </summary>
                SetNull = (int)'n',

                /// <summary>
                /// SetDefault
                /// </summary>
                SetDefault = (int)'d',

                /// <summary>
                /// Unknown
                /// </summary>
                Unknown = (int)'0'

            };

            /// <summary>
            /// Foreign key match type
            /// f,p,s
            /// </summary>
            public enum ForeignKeyMatchType
            {

                /// <summary>
                /// Full
                /// </summary>
                Full = (int)'f',

                /// <summary>
                /// Partial
                /// </summary>
                Partial = (int)'p',

                /// <summary>
                /// Simple
                /// </summary>
                Simple = (int)'s',

                /// <summary>
                /// Unknown
                /// </summary>
                Unknown = (int)'0'

            };

        }
    }
}