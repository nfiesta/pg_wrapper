﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Uložená procedura
            /// </summary>
            public class DBStoredProcedure
                : IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam uložených procedur
                /// </summary>
                private DBStoredProcedureList storedProcedures;

                /// <summary>
                /// Metadata uložené procedury
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu uložené procedury
                /// </summary>
                /// <param name="storedProcedures">Seznam uložených procedur</param>
                /// <param name="data">Metadata uložené procedury</param>
                public DBStoredProcedure(DBStoredProcedureList storedProcedures, DataRow data)
                {
                    StoredProcedures = storedProcedures;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam uložených procedur (read-only)
                /// </summary>
                public DBStoredProcedureList StoredProcedures
                {
                    get
                    {
                        return storedProcedures;
                    }
                    private set
                    {
                        storedProcedures = value;
                    }
                }

                /// <summary>
                /// Metadata uložené procedury (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Identifikátor databázového schéma (read-only)
                /// </summary>
                public Nullable<uint> SchemaId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "pronamespace",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Databázové schéma uložené procedury (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return StoredProcedures.Schema;
                    }
                }


                /// <summary>
                /// Identifikátor uložené procedury (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "oid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Jméno uložené procedury (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "proname",
                                defaultValue: null)
                            ?? String.Empty;
                    }
                }

                /// <summary>
                /// Plné jméno uložené procedury, včetně datových typů vstupních argumentů (read-only)
                /// </summary>
                public string FullName
                {
                    get
                    {
                        return $"{Name}({ArgumentTypesNamesText})";
                    }
                }

                /// <summary>
                /// Jméno souboru pro uložení zdrojového kódu uložené procedury
                /// Musí být jedinečné v rámci jednoho databázového schéma
                /// Jméno souboru je shodné se jménem uložené procedury
                /// Pro přetížené procedury je jméno uložené procedury rozšířeno o identifikátor uložené procedury
                /// </summary>
                public string FileName
                {
                    get
                    {
                        IEnumerable<DBStoredProcedure> collection =
                            Schema.StoredProcedures.Items
                            .Where(a => a.Name == Name);

                        if (collection.Count() <= 1)
                        {
                            return $"{Name}";
                        }
                        else
                        {
                            return $"{Name}_{Id}";
                        }
                    }
                }


                /// <summary>
                /// Vlastník uložené procedury (read-only)
                /// </summary>
                public Nullable<uint> OwnerId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "proowner",
                                defaultValue: null);
                    }
                }


                /// <summary>
                /// Jazyk uložené procedury - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> LangId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "prolang",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Jazyk uložené procedury - název (read-only)
                /// </summary>
                public string LangName
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "prolangname",
                                defaultValue: null)
                            ?? String.Empty;
                    }
                }


                /// <summary>
                /// Typ uložené procedury (read-only)
                /// </summary>
                public ProKind ProKind
                {
                    get
                    {
                        Nullable<char> val =
                            Functions.GetNCharArg(
                                row: Data,
                                name: "prokind",
                                defaultValue: null);

                        if (val == null)
                        {
                            return ProKind.Unknown;
                        }

                        return val switch
                        {
                            'f' => ProKind.Function,
                            'p' => ProKind.Procedure,
                            'a' => ProKind.Aggregate,
                            'w' => ProKind.Window,
                            _ => ProKind.Unknown,
                        };
                    }
                }


                /// <summary>
                /// Uložená procedura je
                /// security definer - true,
                /// security invoker - false (read-only)
                /// </summary>
                public Nullable<bool> IsSecurityDefiner
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "prosecdef",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Uložená procedura je
                /// security definer - true,
                /// security invoker - false - text(read-only)
                /// </summary>
                protected string SecurityDefinerText
                {
                    get
                    {
                        return
                            (IsSecurityDefiner == null)
                                ? String.Empty
                                : (bool)IsSecurityDefiner
                                    ? "SECURITY DEFINER"
                                    : "SECURITY INVOKER";
                    }
                }


                /// <summary>
                /// Uložená procedura je strict
                /// </summary>
                public Nullable<bool> IsStrict
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "proisstrict",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Uložená procedura je strict - text
                /// </summary>
                protected string StrictText
                {
                    get
                    {
                        return
                            (IsSecurityDefiner == null)
                                ? String.Empty
                                : (bool)IsStrict
                                    ? "STRICT"
                                    : String.Empty;
                    }
                }


                /// <summary>
                /// Uložená procedura je trigger
                /// </summary>
                public bool IsTrigger
                {
                    get
                    {
                        return
                            FunctionResult.Equals(
                                value: "TRIGGER",
                                comparisonType: StringComparison.CurrentCultureIgnoreCase);
                    }
                }


                /// <summary>
                /// Uložená procedura vrací množinu záznamů
                /// </summary>
                public Nullable<bool> ReturnsSet
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "proisretset",
                                defaultValue: null);
                    }
                }


                /// <summary>
                /// Typ uložené procedury
                /// IMMUTABLE, STABLE, VOLATILE (read-only)
                /// </summary>
                public ProVolatile Volatile
                {
                    get
                    {
                        Nullable<char> val =
                            Functions.GetNCharArg(
                                row: Data,
                                name: "provolatile",
                                defaultValue: null);

                        if (val == null)
                        {
                            return ProVolatile.Unknown;
                        }

                        return val switch
                        {
                            'i' => ProVolatile.Immutable,
                            's' => ProVolatile.Stable,
                            'v' => ProVolatile.Volatile,
                            _ => ProVolatile.Unknown,
                        };
                    }
                }

                /// <summary>
                /// Typ uložené procedury
                /// IMMUTABLE, STABLE, VOLATILE  - text(read-only)
                /// </summary>
                public string VolatileText
                {
                    get
                    {
                        return Volatile switch
                        {
                            ProVolatile.Immutable => "IMMUTABLE",
                            ProVolatile.Stable => "STABLE",
                            ProVolatile.Volatile => "VOLATILE",
                            _ => String.Empty,
                        };
                    }
                }


                /// <summary>
                /// Návratový typ uložené procedury - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> ReturnTypeId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "prorettype",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Návratový typ uložené procedury - jméno (read-only)
                /// </summary>
                public string ReturnTypeName
                {
                    get
                    {
                        return GetTypeName(typeId: ReturnTypeId);
                    }
                }

                /// <summary>
                /// Typy vstupních argumentů uložené procedury - identifikátory (read-only)
                /// </summary>
                public List<uint> ArgumentTypesIds
                {
                    get
                    {
                        return
                            Functions.StringToUIntList(
                                text: Functions.GetStringArg(
                                    row: Data,
                                    name: "proargtypes",
                                    defaultValue: null),
                                separator: ';',
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Typy vstupních argumentů uložené procedury - jména (pole) (read-only)
                /// </summary>
                public List<string> ArgumentTypesNames
                {
                    get
                    {
                        if (ArgumentTypesIds == null)
                        {
                            return [];
                        }

                        return
                            ArgumentTypesIds
                            .Select(a => GetTypeName(typeId: a))
                            .ToList<string>();
                    }
                }

                /// <summary>
                /// Typy vstupních argumentů uložené procedury - jména (text) (read-only)
                /// </summary>
                public string ArgumentTypesNamesText
                {
                    get
                    {
                        return
                            ArgumentTypesNames.Count != 0
                                ? ArgumentTypesNames
                                    .Aggregate((a, b) => $"{a}, {b}")
                                : String.Empty;
                    }
                }


                /// <summary>
                /// Typy všech argumentů uložené procedury - identifikátory (read-only)
                /// </summary>
                public List<uint> AllArgumentTypesIds
                {
                    get
                    {
                        return
                            Functions.StringToUIntList(
                                text: Functions.GetStringArg(
                                    row: Data,
                                    name: "proallargtypes",
                                    defaultValue: null),
                                separator: ';',
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Typy všech argumentů uložené procedury - jména (pole) (read-only)
                /// </summary>
                public List<string> AllArgumentTypesNames
                {
                    get
                    {
                        if (AllArgumentTypesIds == null)
                        {
                            return [];
                        }

                        return
                            AllArgumentTypesIds
                            .Select(a => GetTypeName(typeId: a))
                            .ToList<string>();
                    }
                }

                /// <summary>
                /// Typy všech argumentů uložené procedury - jména (text) (read-only)
                /// </summary>
                public string AllArgumentTypesNamesText
                {
                    get
                    {
                        return
                            AllArgumentTypesNames.Count != 0
                                ? AllArgumentTypesNames
                                    .Aggregate((a, b) => $"{a}, {b}")
                                : String.Empty;
                    }
                }


                /// <summary>
                /// Režim argumentů uložené procedury - identifikátory (read-only)
                /// </summary>
                public List<char> ArgumentModesIds
                {
                    get
                    {
                        return
                            Functions.StringToCharList(
                                text: Functions.GetStringArg(
                                    row: Data,
                                    name: "proargmodes",
                                    defaultValue: null),
                                separator: ';',
                                defaultValue: '0');
                    }
                }

                /// <summary>
                /// Jména argumentů uložené procedury (read-only)
                /// </summary>
                public List<string> ArgumentNames
                {
                    get
                    {
                        return
                            Functions.StringToStringList(
                                text: Functions.GetStringArg(
                                    row: Data,
                                    name: "proargnames",
                                    defaultValue: String.Empty),
                                separator: ';');
                    }
                }


                /// <summary>
                /// Argumenty uložené procedury (read-only)
                /// </summary>
                public string FunctionArguments
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "function_arguments",
                                defaultValue: null)
                            ?? String.Empty;
                    }
                }

                /// <summary>
                /// Návratový typ uložené procedury (read-only)
                /// </summary>
                public string FunctionResult
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "function_result",
                                defaultValue: null)
                            ?? String.Empty;
                    }
                }

                /// <summary>
                /// Definice uložené procedury (read-only)
                /// </summary>
                public string FunctionDefinition
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "function_definition",
                                defaultValue: null)
                            ?? String.Empty;
                    }
                }

                /// <summary>
                /// Zdrojový kód uložené procedury (read-only)
                /// </summary>
                public string FunctionCode
                {
                    get
                    {
                        string strKey = "$function$";
                        int startIndex = FunctionDefinition.IndexOf(value: strKey) + strKey.Length;
                        int endIndex = FunctionDefinition.IndexOf(value: strKey, startIndex: startIndex);

                        return
                            FunctionDefinition[startIndex..endIndex];
                    }
                }


                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Schema.Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>()
                                ?? String.Empty;
                    }
                }

                /// <summary>
                /// Signatura uložené procedury (read-only)
                /// </summary>
                public string Signature
                {
                    get
                    {
                        return String.Concat(
                            $"{Schema.Name}.{Name}; ",
                            $"args: {FunctionArguments}; ",
                            $"returns: {FunctionResult}");
                    }
                }


                /// <summary>
                /// SQL definice uložené procedury (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        StringBuilder sb = new();

                        // Uložená procedura
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--{tab}{Name}");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}-- DROP FUNCTION IF EXISTS {Schema.Name}.{FullName} CASCADE;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{FunctionDefinition};");
                        sb.AppendLine();

                        // KOMENTÁŘE
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- comments");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON FUNCTION {Schema.Name}.{FullName}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");
                        sb.AppendLine();

                        // OPRÁVNĚNÍ (!TODO)
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- privileges");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER FUNCTION {Schema.Name}.{FullName}");
                        sb.AppendLine(value: $"{tab}{tab}OWNER TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT EXECUTE");
                        sb.AppendLine(value: $"{tab}{tab}ON FUNCTION {Schema.Name}.{FullName}");
                        sb.AppendLine(value: $"{tab}{tab}TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT EXECUTE");
                        sb.AppendLine(value: $"{tab}{tab}ON FUNCTION {Schema.Name}.{FullName}");
                        sb.AppendLine(value: $"{tab}{tab}TO usr_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT EXECUTE");
                        sb.AppendLine(value: $"{tab}{tab}ON FUNCTION {Schema.Name}.{FullName}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_mignil;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT EXECUTE");
                        sb.AppendLine(value: $"{tab}{tab}ON FUNCTION {Schema.Name}.{FullName}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_sanil;");
                        sb.AppendLine();

                        return sb.ToString();
                    }
                }


                /// <summary>
                /// Označení místa pro vložení definice uložené procedury (začátek) (read-only)
                /// </summary>
                public string MarkerBegin
                {
                    get
                    {
                        return
                            String.Concat(
                                $"--------------------------------------------------------------------------------;{Environment.NewLine}",
                                $"-- <function function_name = \"{Name}\" function_schema = \"{Schema.Name}\" src = \"functions/{Schema.Name}/{FileName}.sql\">{Environment.NewLine}");
                    }
                }

                /// <summary>
                /// Označení místa pro vložení definice uložené procedury (konec) (read-only)
                /// </summary>
                public static string MarkerEnd
                {
                    get
                    {
                        return $"-- </function>{Environment.NewLine}";
                    }
                }

                /// <summary>
                /// Označení místa pro vložení definice uložené procedury (read-only)
                /// </summary>
                public string Marker
                {
                    get
                    {
                        return String.Concat(MarkerBegin, MarkerEnd);
                    }
                }

                /// <summary>
                /// Schéma s objekty, na které se odkazuje definice uložené procedury (read-only)
                /// </summary>
                public List<DBSchema> ReferencedSchemas
                {
                    get
                    {
                        return
                            Schema.Catalog.Schemas.Items
                                .Where(a => a.Name != Schema.Name)
                                .Where(a => SqlDefinition.Contains(value: a.Name))
                                .ToList<DBSchema>();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaná uložená procedura je stejná
                /// </summary>
                /// <param name="obj">Zadaná uložená procedura</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBStoredProcedure Type, return False
                    if (obj is not DBStoredProcedure)
                    {
                        return false;
                    }

                    return
                        Id == ((DBStoredProcedure)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis uložené procedury
                /// </summary>
                /// <returns>Textový popis uložené procedury</returns>
                public override string ToString()
                {
                    return Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Stored procedure oid = {Id} signature = {Signature}.",
                        LanguageVersion.National => $"Uložená procedura oid = {Id} signature = {Signature}.",
                        _ => String.Empty,
                    };
                }

                /// <summary>
                /// Jméno datového typu
                /// </summary>
                /// <param name="typeId">Identifikační číslo datového typu</param>
                /// <returns>Vrací jméno datového typu</returns>
                private string GetTypeName(Nullable<uint> typeId)
                {
                    if (typeId == null)
                    {
                        return String.Empty;
                    }

                    DataRow rowType =
                        Schema.Catalog.PgType.AsEnumerable()
                            .Where(a => a.Field<Nullable<uint>>("oid") == (uint)typeId)
                            .FirstOrDefault<DataRow>();

                    if (rowType == null)
                    {
                        return String.Empty;
                    }

                    string typeName =
                            Functions.GetStringArg(
                                row: rowType,
                                name: "typname",
                                defaultValue: String.Empty);

                    uint schemaId =
                        Functions.GetUIntArg(
                                row: rowType,
                                name: "typnamespace",
                                defaultValue: 0);

                    DataRow rowSchema =
                        Schema.Catalog.PgNamespace.AsEnumerable()
                            .Where(a => a.Field<Nullable<uint>>("oid") == schemaId)
                            .FirstOrDefault<DataRow>();

                    if (rowSchema == null)
                    {
                        return typeName;
                    }

                    string schemaName =
                            Functions.GetStringArg(
                                row: rowSchema,
                                name: "nspname",
                                defaultValue: String.Empty);

                    if (String.IsNullOrEmpty(value: schemaName))
                    {
                        return typeName;
                    }

                    if (schemaName == "pg_catalog")
                    {
                        return typeName;
                    }

                    if (schemaName == "public")
                    {
                        return typeName;
                    }

                    return $"{schemaName}.{typeName}";
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam uložených procedur
            /// </summary>
            public class DBStoredProcedureList
            {

                #region Private Fields

                /// <summary>
                /// Databázové schéma
                /// </summary>
                private DBSchema schema;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu uložených procedur
                /// </summary>
                /// <param name="schema">Databázové schéma</param>
                public DBStoredProcedureList(DBSchema schema)
                {
                    Schema = schema;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Databázové schéma (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return schema;
                    }
                    private set
                    {
                        schema = value;
                    }
                }

                /// <summary>
                /// Data uložených procedur (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Schema.Catalog.PgProc
                            .Select($"pronamespace = {Schema.Id}");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgProc();
                        }
                    }
                }

                /// <summary>
                /// Seznam uložených procedur (read-only)
                /// </summary>
                public List<DBStoredProcedure> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBStoredProcedure(storedProcedures: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Uložená procedura podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor uložené procedury</param>
                /// <returns>Uložená procedura</returns>
                public DBStoredProcedure this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBStoredProcedure>();
                    }
                }

                /// <summary>
                /// Uložená procedura podle jména
                /// </summary>
                /// <param name="name">Jméno uložené procedury</param>
                /// <returns>Uložená procedura</returns>
                public DBStoredProcedure this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBStoredProcedure>();
                    }
                }

                #endregion Indexer

            }

        }

        /// <summary>
        /// Typ uložené procedury
        /// f,p,a,w
        /// </summary>
        public enum ProKind
        {

            /// <summary>
            /// Normal function
            /// </summary>
            Function = (int)'f',

            /// <summary>
            /// Procedure
            /// </summary>
            Procedure = (int)'p',

            /// <summary>
            /// Aggregate function
            /// </summary>
            Aggregate = (int)'a',

            /// <summary>
            /// Window function
            /// </summary>
            Window = (int)'w',

            /// <summary>
            /// Unknown
            /// </summary>
            Unknown = (int)'0'

        };

        /// <summary>
        /// Typ uložené procedury
        /// i,s,v
        /// </summary>
        public enum ProVolatile
        {

            /// <summary>
            /// Immutable
            /// </summary>
            Immutable = (int)'i',

            /// <summary>
            /// Stable
            /// </summary>
            Stable = (int)'s',

            /// <summary>
            /// PrimaryKey
            /// </summary>
            Volatile = (int)'v',

            /// <summary>
            /// Unknown
            /// </summary>
            Unknown = (int)'0'

        };

    }
}
