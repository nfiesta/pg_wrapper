﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Index
            /// </summary>
            public class DBIndex
                : IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam indexů
                /// </summary>
                private DBIndexList indexes;

                /// <summary>
                /// Metadata indexu
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu indexu
                /// </summary>
                /// <param name="indexes">Seznam indexů</param>
                /// <param name="data">Metadata indexu</param>
                public DBIndex(DBIndexList indexes, DataRow data)
                {
                    Indexes = indexes;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam indexů (read-only)
                /// </summary>
                public DBIndexList Indexes
                {
                    get
                    {
                        return indexes;
                    }
                    private set
                    {
                        indexes = value;
                    }
                }

                /// <summary>
                /// Metadata indexů (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Databázové schéma (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return Indexes.Schema;
                    }
                }


                /// <summary>
                /// Index - identifikační číslo (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "indexrelid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Index - jméno (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "indexrelname",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Přístupová metoda - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> AccessMethodId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "indexrelam",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Jméno přístupové metody (read-only)
                /// </summary>
                public string AccessMethodName
                {
                    get
                    {
                        if (AccessMethodId == null)
                        {
                            return String.Empty;
                        }

                        return
                            Schema.Catalog.PgAM.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("oid") == (uint)AccessMethodId)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "amname",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }


                /// <summary>
                /// Identifikační číslo indexované tabulky (read-only)
                /// </summary>
                public Nullable<uint> IndexTableId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "indrelid",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Jméno indexované tabulky (read-only)
                /// </summary>
                public string IndexTableName
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "indrelname",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Identifikační číslo schéma indexované tabulky (read-only)
                /// </summary>
                public Nullable<uint> IndexTableSchemaId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "indrelnamespaceoid",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Jméno schéma indexované tabulky (read-only)
                /// </summary>
                public string IndexTableSchemaName
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "indrelnamespacename",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Indexované tabulka - objekt (read-only)
                /// </summary>
                public DBTable IndexTable
                {
                    get
                    {
                        return
                            Indexes.Schema.Catalog.Schemas.Items
                                .SelectMany(a => a.Tables.Items)
                                .Where(a => a.Id == IndexTableId)
                                .FirstOrDefault<DBTable>();
                    }
                }


                /// <summary>
                /// Unique index (read-only)
                /// </summary>
                public Nullable<bool> IsUnique
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "indisunique",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Index reprezentuje primární klíč (read-only)
                /// </summary>
                public Nullable<bool> IsPrimary
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "indisprimary",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Kontrola unique je provedena ihned po insertu (read-only)
                /// </summary>
                public Nullable<bool> IsImmediate
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "indimmediate",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Index je platný pro dotazy (read-only)
                /// </summary>
                public Nullable<bool> IsValid
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "indisvalid",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Index je připravený pro insert (read-only)
                /// </summary>
                public Nullable<bool> IsReady
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "indisready",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Index nebyl smazán (read-only)
                /// </summary>
                public Nullable<bool> IsAlive
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "indislive",
                                defaultValue: null);
                    }
                }


                /// <summary>
                /// Seznam sloupců zahrnutých do indexu - identifikátory (read-only)
                /// </summary>
                public List<uint> IndexColumnsIds
                {
                    get
                    {
                        return
                            Functions.StringToUIntList(
                                text: Functions.GetStringArg(
                                    row: Data,
                                    name: "indkey",
                                    defaultValue: null),
                                separator: ';');
                    }
                }

                /// <summary>
                /// Seznam sloupců zahrnutých do indexu - objekty (read-only)
                /// </summary>
                public List<DBColumn> IndexColumns
                {
                    get
                    {
                        if (IndexTable == null)
                        {
                            return [];
                        }

                        return
                            IndexTable.Columns.Items
                                .Where(a => IndexColumnsIds.Contains(item: a.Id))
                                .ToList<DBColumn>();
                    }
                }

                /// <summary>
                /// Seznam sloupců zahrnutých do indexu - text (read-only)
                /// </summary>
                protected string IndexColumnsText
                {
                    get
                    {
                        return
                            IndexColumns.Count != 0
                                ? IndexColumns
                                    .Select(a => a.QName)
                                    .Aggregate((a, b) => $"{a}, {b}")
                                : String.Empty;
                    }
                }

                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Schema.Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }


                /// <summary>
                /// SQL definice indexu (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        if (IndexTable == null)
                        {
                            return String.Empty;
                        }

                        string strIndex =
                            (IsUnique == null)
                            ? "INDEX"
                            : (bool)IsUnique
                                ? "UNIQUE INDEX"
                                : "INDEX";

                        StringBuilder sb = new();

                        sb.AppendLine(value: $"{tab}--{Name}");
                        sb.AppendLine(value: $"{tab}DROP {strIndex} IF EXISTS {Schema.Name}.{Name};");
                        sb.AppendLine(value: $"{tab}CREATE {strIndex} {Name}");
                        sb.AppendLine(value: $"{tab}{tab}ON {IndexTableSchemaName}.{IndexTableName}");
                        sb.AppendLine(value: $"{tab}{tab}USING {AccessMethodName}({IndexColumnsText});");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON {strIndex} {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}IS '{Comment}';");

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaný index je stejný
                /// </summary>
                /// <param name="obj">Zadaný index</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBIndex Type, return False
                    if (obj is not DBIndex)
                    {
                        return false;
                    }

                    return
                        Id == ((DBIndex)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis indexu
                /// </summary>
                /// <returns>Textový popis indexu</returns>
                public override string ToString()
                {
                    return Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Index oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Index oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam indexů
            /// </summary>
            public class DBIndexList
            {

                #region Private Fields

                /// <summary>
                /// Databázové schéma
                /// </summary>
                private DBSchema schema;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu indexů
                /// </summary>
                /// <param name="schema">Databázové schéma</param>
                public DBIndexList(DBSchema schema)
                {
                    Schema = schema;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Databázové schéma (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return schema;
                    }
                    private set
                    {
                        schema = value;
                    }
                }

                /// <summary>
                /// Data indexů (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Schema.Catalog.PgIndex
                            .Select($"indexrelnamespaceoid = {Schema.Id}"); ;

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgClass();
                        }
                    }
                }

                /// <summary>
                /// Seznam indexů (read-only)
                /// </summary>
                public List<DBIndex> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBIndex(indexes: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Index podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor indexu</param>
                /// <returns>Index</returns>
                public DBIndex this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBIndex>();
                    }
                }

                /// <summary>
                /// Index podle jména
                /// </summary>
                /// <param name="name">Jméno indexu</param>
                /// <returns>Index</returns>
                public DBIndex this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBIndex>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}