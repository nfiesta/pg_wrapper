﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Tabulka
            /// </summary>
            public class DBTable
            : DBClass, IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam tabulek
                /// </summary>
                private DBTableList tables;

                /// <summary>
                /// Odstraní závislé objekty z SQL definice
                /// </summary>
                private bool removeDependency;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu tabulky
                /// </summary>
                /// <param name="tables">Seznam tabulek</param>
                /// <param name="data">Metadata tabulky</param>
                public DBTable(DBTableList tables, DataRow data)
                    : base(schema: tables.Schema, data: data)
                {
                    Tables = tables;
                    RemoveDependency = false;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam tabulek (read-only)
                /// </summary>
                public DBTableList Tables
                {
                    get
                    {
                        return tables;
                    }
                    private set
                    {
                        tables = value;
                    }
                }

                /// <summary>
                /// Seznam atributů (read-only)
                /// </summary>
                public DBColumnList Columns
                {
                    get
                    {
                        return new DBColumnList(table: this);
                    }
                }

                /// <summary>
                /// Seznam primárních klíčů (read-only)
                /// </summary>
                public DBPrimaryKeyList PrimaryKeys
                {
                    get
                    {
                        return new DBPrimaryKeyList(table: this);
                    }
                }

                /// <summary>
                /// Seznam cizích klíčů (read-only)
                /// </summary>
                public DBForeignKeyList ForeignKeys
                {
                    get
                    {
                        return new DBForeignKeyList(table: this);
                    }
                }

                /// <summary>
                /// Seznam unique omezení (read-only)
                /// </summary>
                public DBUniqueConstraintList UniqueConstraints
                {
                    get
                    {
                        return new DBUniqueConstraintList(table: this);
                    }
                }

                /// <summary>
                /// Seznam check omezení (read-only)
                /// </summary>
                public DBCheckConstraintList CheckConstraints
                {
                    get
                    {
                        return new DBCheckConstraintList(table: this);
                    }
                }

                /// <summary>
                /// Seznam indexů tabulky (read-only)
                /// </summary>
                public List<DBIndex> Indexes
                {
                    get
                    {
                        return
                            Tables.Schema.Indexes.Items
                                 .Where(a => a.IndexTableId == Id)
                                 .ToList<DBIndex>();
                    }
                }

                /// <summary>
                /// Seznam triggerů tabulky (read-only)
                /// </summary>
                public DBTriggerList Triggers
                {
                    get
                    {
                        return new DBTriggerList(table: this);
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Odstraní závislé objekty z SQL definice
                /// </summary>
                public bool RemoveDependency
                {
                    get
                    {
                        return removeDependency;
                    }
                    set
                    {
                        removeDependency = value;
                    }
                }

                /// <summary>
                /// SQL definice tabulky (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        string strColumns =
                            Columns.Items
                                .OrderBy(a => a.Id)
                                .Select(a => $"{tab}{tab}{a.SqlDefinition}")
                                .Aggregate((a, b) => $"{a},{Environment.NewLine}{b}");

                        string strColumnComments =
                            Columns.Items
                                .OrderBy(a => a.Id)
                                .Select(a => $"{a.SqlComment}")
                                .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                        string strColumnNotNullConstraints =
                            Columns.Items
                                .OrderBy(a => a.Id)
                                .Select(a => $"{a.SqlNotNullConstraint}")
                                .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                        StringBuilder sb = new();

                        // TABULKA
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--{tab}{Name}");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- {Comment}");
                        sb.AppendLine();
                        sb.AppendLine(value: $"--{tab[..^2]}SELECT * FROM {Schema.Name}.{Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}DROP TABLE IF EXISTS {Schema.Name}.{Name} CASCADE;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}CREATE TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}(");
                        sb.AppendLine(value: $"{strColumns}");
                        sb.AppendLine(value: $"{tab});");
                        sb.AppendLine();

                        // KOMENTÁŘE
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- comments");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}-- tabulka");
                        sb.AppendLine(value: $"{tab}COMMENT ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}-- atributy");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{strColumnComments}");

                        // NOT NULL OMEZENÍ
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- not null constraints");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{strColumnNotNullConstraints}");

                        // DEFAULTNÍ HODNOTY
                        if (Columns.Items.Where(a => a.HasDefault ?? false).Any())
                        {
                            string strColumnDefaultValues =
                                Columns.Items
                                    .Where(a => a.HasDefault ?? false)
                                    .OrderBy(a => a.Id)
                                    .Select(a => $"{a.SqlDefaultValue}")
                                    .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- default values");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{strColumnDefaultValues}");
                        }

                        // PRIMÁRNÍ KLÍČ
                        if (PrimaryKeys.Items.Count != 0)
                        {
                            string strPrimaryKeys =
                                PrimaryKeys.Items
                                    .OrderBy(a => a.Name)
                                    .Select(a => a.SqlDefinition)
                                    .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- primary key contraint");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{strPrimaryKeys}");
                        }

                        // UNIQUE OMEZENÍ
                        if (UniqueConstraints.Items.Count != 0)
                        {
                            string strUniqueConstraints =
                                UniqueConstraints.Items
                                    .OrderBy(a => a.Name)
                                    .Select(a => a.SqlDefinition)
                                    .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- unique contraints");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{strUniqueConstraints}");
                        }

                        // CIZÍ KLÍČE
                        if (ForeignKeys.Items.Count != 0)
                        {

                            IEnumerable<DBForeignKey> fkeys;
                            if (RemoveDependency)
                            {
                                fkeys = ForeignKeys.Items
                                    .Where(a => a.ForeignTable.Schema.Name == Schema.Name);
                            }
                            else
                            {
                                fkeys = ForeignKeys.Items;
                            }

                            string strForeignKeys;
                            if (fkeys.Any())
                            {
                                strForeignKeys = fkeys
                                        .OrderBy(a => a.Name)
                                        .Select(a => a.SqlDefinition)
                                        .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");
                            }
                            else
                            {
                                strForeignKeys = String.Empty;
                            }

                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- foreign key contraints");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{strForeignKeys}");
                        }

                        IEnumerable<DBIndex> indexes = Indexes
                                    .Where(a => !(a.IsPrimary ?? false))
                                    .Where(a => !(a.IsUnique ?? false));

                        if (indexes.Any())
                        {
                            string strIndexes = indexes
                                    .OrderBy(a => a.Name)
                                    .Select(a => a.SqlDefinition)
                                    .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- indexes");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{strIndexes}");
                        }

                        // CHECK OMEZENÍ
                        if (CheckConstraints.Items.Count != 0)
                        {
                            string strCheckConstraints =
                                CheckConstraints.Items
                                    .OrderBy(a => a.Name)
                                    .Select(a => a.SqlDefinition)
                                    .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- check contraints");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{strCheckConstraints}");
                        }

                        // TRIGGERY
                        if (Triggers.Items.Where(a => !(a.IsInternal ?? false)).Any())
                        {
                            string strTriggers =
                                Triggers.Items
                                    .Where(a => !(a.IsInternal ?? false))
                                    .OrderBy(a => a.Name)
                                    .Select(a => a.SqlDefinition)
                                    .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- triggers");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{strTriggers}");
                        }

                        // OPRÁVNĚNÍ (TODO)
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- access privileges");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}OWNER TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT SELECT");
                        sb.AppendLine(value: $"{tab}{tab}ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO usr_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_mignil;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_sanil;");
                        sb.AppendLine();

                        // DATA (pouze číselníky) (TODO)
                        if (Name.StartsWith(value: 'c'))
                        {
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- data");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{InsertCommand}");
                        }

                        // DUMP (ostatní tabulky a sekvence) (TODO)
                        else
                        {
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"-- dump");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                            sb.AppendLine(value: $"{tab}SELECT pg_catalog.pg_extension_config_dump('{Schema.Name}.{Name}', '');");
                        }

                        return sb.ToString();
                    }
                }

                /// <summary>
                /// SQL příkaz pro výběr všech dat z tabulky
                /// </summary>
                public string SelectCommand
                {
                    get
                    {
                        string strColumns =
                            Columns.Items
                                .Where(a => a.Name != "valid_from")
                                .Where(a => a.Name != "valid_until")
                                .OrderBy(a => a.Id)
                                .Select(a => a.Name)
                                .Aggregate((a, b) => $"{a}, {b}");

                        string strPKey = String.Empty;
                        if (PrimaryKeys.Items.FirstOrDefault<DBPrimaryKey>() != null)
                        {
                            if (PrimaryKeys.Items
                                .FirstOrDefault<DBPrimaryKey>()
                                .ConstraintColumns
                                    .FirstOrDefault<DBColumn>() != null)
                            {
                                strPKey =
                                   PrimaryKeys.Items
                                       .FirstOrDefault<DBPrimaryKey>()
                                       .ConstraintColumns
                                           .FirstOrDefault<DBColumn>()
                                           .Name;

                            }
                        }

                        if (String.IsNullOrEmpty(value: strPKey))
                        {
                            return String.Concat(
                                $"SELECT {strColumns}{Environment.NewLine}",
                                $"FROM {Schema.Name}.{Name};{Environment.NewLine}");
                        }
                        else
                        {
                            return String.Concat(
                                $"SELECT {strColumns}{Environment.NewLine}",
                                $"FROM {Schema.Name}.{Name}{Environment.NewLine}",
                                $"ORDER BY {strPKey};{Environment.NewLine}");
                        }
                    }
                }

                /// <summary>
                /// SQL příkaz pro vložení všech dat do tabulky
                /// </summary>
                public string InsertCommand
                {
                    get
                    {

                        string strColumns =
                            Columns.Items
                                .Where(a => a.Name != "valid_from")
                                .Where(a => a.Name != "valid_until")
                                .OrderBy(a => a.Id)
                                .Select(a => a.Name)
                                .Aggregate((a, b) => $"{a}, {b}");

                        DataTable data =
                            Tables.Schema.Catalog.Database.ExecuteQuery(
                                sqlCommand: SelectCommand);

                        StringBuilder sb = new();
                        sb.AppendLine(value: $"{tab}INSERT INTO {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}({strColumns})");
                        sb.AppendLine(value: $"{tab}VALUES");

                        int i = 0;
                        foreach (DataRow row in data.Rows)
                        {
                            i++;
                            string strRow = String.Empty;
                            string strVal = String.Empty;
                            int intVal;

                            int j = 0;
                            foreach (DBColumn col in
                                Columns.Items
                                    .Where(a => a.Name != "valid_from")
                                    .Where(a => a.Name != "valid_until")
                                    .OrderBy(a => a.Id))
                            {
                                j++;
                                switch (col.TypeName)
                                {
                                    case "int4":
                                        intVal = Functions.GetIntArg(row: row, name: col.Name);
                                        strRow = (j == 1)
                                            ? Functions.PrepIntArg(arg: intVal)
                                            : String.Concat(strRow, ", ", Functions.PrepIntArg(arg: intVal));
                                        break;

                                    default:
                                        strVal = Functions.GetStringArg(row: row, name: col.Name);
                                        strRow = (j == 1)
                                            ? Functions.PrepStringArg(arg: strVal)
                                            : String.Concat(strRow, ", ", Functions.PrepStringArg(arg: strVal));
                                        break;
                                }
                            }

                            if (i < data.Rows.Count)
                            {
                                sb.AppendLine(value: $"{tab}{tab}({strRow}),");
                            }
                            else
                            {
                                sb.AppendLine(value: $"{tab}{tab}({strRow});");
                            }
                        }

                        return sb.ToString();
                    }
                }

                /// <summary>
                /// Schéma s objekty, na které se odkazuje definice tabulky (read-only)
                /// </summary>
                public List<DBSchema> ReferencedSchemas
                {
                    get
                    {
                        return
                            ForeignKeys.Items
                                 .Where(a => a.ForeignTable.Schema.Name != Schema.Name)
                                 .Select(a => a.ForeignTable.Schema)
                                 .ToList<DBSchema>();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaná tabulka je stejná
                /// </summary>
                /// <param name="obj">Zadaná tabulka</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBTable Type, return False
                    if (obj is not DBTable)
                    {
                        return false;
                    }

                    return
                        Id == ((DBTable)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis tabulky
                /// </summary>
                /// <returns>Textový popis tabulky</returns>
                public override string ToString()
                {
                    return Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Table oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Tabulka oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam tabulek
            /// </summary>
            public class DBTableList
            {

                #region Private Fields

                /// <summary>
                /// Databázové schéma
                /// </summary>
                private DBSchema schema;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu tabulek
                /// </summary>
                /// <param name="schema">Databázové schéma</param>
                public DBTableList(DBSchema schema)
                {
                    Schema = schema;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Databázové schéma (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return schema;
                    }
                    private set
                    {
                        schema = value;
                    }
                }

                /// <summary>
                /// Data tabulek (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Schema.Catalog.PgClass
                            .Select($"relnamespace = {Schema.Id} AND relkind = 'r'");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgClass();
                        }
                    }
                }

                /// <summary>
                /// Seznam tabulek (read-only)
                /// </summary>
                public List<DBTable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBTable(tables: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Tabulka podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor tabulky</param>
                /// <returns>Tabulka</returns>
                public DBTable this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBTable>();
                    }
                }

                /// <summary>
                /// Tabulka podle jména
                /// </summary>
                /// <param name="name">Jméno tabulky</param>
                /// <returns>Tabulka</returns>
                public DBTable this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBTable>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}