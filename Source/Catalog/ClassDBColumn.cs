﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Atribut
            /// </summary>
            public class DBColumn
                : IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam atributů
                /// </summary>
                private DBColumnList columns;

                /// <summary>
                /// Metadata atributu
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu atributu
                /// </summary>
                /// <param name="columns">Seznam atributů</param>
                /// <param name="data">Metadata atributu</param>
                public DBColumn(DBColumnList columns, DataRow data)
                {
                    Columns = columns;
                    Data = data;
                }

                /// <summary>
                /// Konstruktor objektu atributu
                /// </summary>
                public DBColumn()
                {
                    Columns = null;

                    Data = DBCatalog.EmptyPgAttribute().NewRow();
                    Functions.SetUIntArg(row: Data, name: "attrelid", val: 0);
                    Functions.SetUIntArg(row: Data, name: "attnum", val: 0);
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam atributů (read-only)
                /// </summary>
                public DBColumnList Columns
                {
                    get
                    {
                        return columns;
                    }
                    private set
                    {
                        columns = value;
                    }
                }

                /// <summary>
                /// Metadata atributu (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Identifikátor tabulky (read-only)
                /// </summary>
                public uint TableId
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "attrelid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBClass Table
                {
                    get
                    {
                        return Columns.Table;
                    }
                }


                /// <summary>
                /// Identifikátor atributu (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "attnum",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Jméno atributu
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "attname",
                                defaultValue: String.Empty);
                    }
                    set
                    {
                        Functions.SetStringArg(
                            row: Data,
                            name: "attname",
                            val: value);
                    }
                }

                /// <summary>
                /// Jméno atributu
                /// (pokud se jedná a klíčové slovo jazyka pak je v uvozovkách)
                /// (read-only)
                /// </summary>
                public string QName
                {
                    get
                    {
                        if (PostgreSQLWrapper.SQLKeyWords.Contains(value: Name.ToLower()))
                        {
                            return $"\"{Name}\"";
                        }
                        else
                        {
                            return Name;
                        }
                    }
                }

                /// <summary>
                /// Datový typ - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> TypeId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "atttypid",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Jméno datového typu (read-only)
                /// </summary>
                public string TypeName
                {
                    get
                    {
                        if (TypeId == null)
                        {
                            return String.Empty;
                        }

                        return
                            Columns.Table.Schema.Catalog.PgType.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("oid") == (uint)TypeId)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "typname",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }

                /// <summary>
                /// Popis datového typu (read-only)
                /// </summary>
                public string TypeInfo
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "atttypinfo",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Délka atributu v bytech (read-only)
                /// </summary>
                public Nullable<short> Length
                {
                    get
                    {
                        return
                            Functions.GetNShortArg(
                                row: Data,
                                name: "attlen",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Délka pole,
                /// pokud atribut není pole pak 0 (read-only)
                /// </summary>
                public Nullable<short> Dimension
                {
                    get
                    {
                        return
                            Functions.GetNShortArg(
                                row: Data,
                                name: "attndims",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Atribut musí být vyplněn (read-only)
                /// </summary>
                public Nullable<bool> NotNull
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "attnotnull",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Atribut má defaultní hodnotu (read-only)
                /// </summary>
                public Nullable<bool> HasDefault
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "atthasdef",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Atribut byl smazán (read-only)
                /// </summary>
                public Nullable<bool> IsDropped
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "attisdropped",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Defaultní hodnota atributu (read-only)
                /// </summary>
                public string DefaultValue
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "attdef",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Table.Schema.Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Columns.Table.Id)
                                .Where(a => a.Field<Nullable<int>>("objsubid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }


                /// <summary>
                /// SQL definice atributu (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        int maxNameLength =
                            Columns.Items
                                .Max(a => a.Name.Length) + tab.Length;

                        return
                            String.Concat(
                                $"{QName.PadRight(totalWidth: maxNameLength, paddingChar: (char)32)} ",
                                $"{TypeInfo}");
                    }
                }

                /// <summary>
                /// SQL definice komentáře atributu (read-only)
                /// </summary>
                public string SqlComment
                {
                    get
                    {
                        if ((Table == null) || (Table.Schema == null))
                        {
                            return $"{tab}-- {Name}{Environment.NewLine}";
                        }

                        return
                            String.Concat(
                            $"{tab}-- {Name}{Environment.NewLine}",
                            $"{tab}COMMENT ON COLUMN {Table.Schema.Name}.{Table.Name}.{QName}{Environment.NewLine}",
                            $"{tab}{tab}IS '{Comment}';{Environment.NewLine}");
                    }
                }

                /// <summary>
                /// SQL definice not null omezení atributu (read-only)
                /// </summary>
                public string SqlNotNullConstraint
                {
                    get
                    {
                        if ((Table == null) || (Table.Schema == null))
                        {
                            return $"{tab}-- {Name}{Environment.NewLine}";
                        }

                        if (NotNull == null)
                        {
                            return $"{tab}-- {Name}{Environment.NewLine}";
                        }

                        if (!(bool)NotNull)
                        {
                            return $"{tab}-- {Name}{Environment.NewLine}";
                        }

                        return
                            String.Concat(
                                $"{tab}-- {Name}{Environment.NewLine}",
                                $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}{Environment.NewLine}",
                                $"{tab}{tab}ALTER COLUMN {QName}{Environment.NewLine}",
                                $"{tab}{tab}SET NOT NULL;{Environment.NewLine}");
                    }
                }

                /// <summary>
                /// SQL definice default hodnoty atributu (read-only)
                /// </summary>
                public string SqlDefaultValue
                {
                    get
                    {
                        if ((Table == null) || (Table.Schema == null))
                        {
                            return $"{tab}-- {Name}{Environment.NewLine}";
                        }

                        if (HasDefault == null)
                        {
                            return $"{tab}-- {Name}{Environment.NewLine}";
                        }

                        if (!(bool)HasDefault)
                        {
                            return $"{tab}-- {Name}{Environment.NewLine}";
                        }

                        return
                            String.Concat(
                                $"{tab}-- {Name}{Environment.NewLine}",
                                $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}{Environment.NewLine}",
                                $"{tab}{tab}ALTER COLUMN {QName}{Environment.NewLine}",
                                $"{tab}{tab}SET DEFAULT {DefaultValue};{Environment.NewLine}");
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaný atribut je stejný
                /// </summary>
                /// <param name="obj">Zadaný atribut</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBColumn Type, return False
                    if (obj is not DBColumn)
                    {
                        return false;
                    }

                    return
                        (TableId == ((DBColumn)obj).TableId) &&
                        (Id == ((DBColumn)obj).Id);
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        TableId.GetHashCode() ^ Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis atributu
                /// </summary>
                /// <returns>Textový popis atributu</returns>
                public override string ToString()
                {
                    return Table.Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Attribute oid = {TableId}-{Id} name = {Name}.",
                        LanguageVersion.National => $"Atribut oid = {TableId}-{Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam atributů
            /// </summary>
            public class DBColumnList
            {

                #region Private Fields

                /// <summary>
                /// Tabulka
                /// </summary>
                private DBClass table;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu atributů
                /// </summary>
                /// <param name="table">Tabulka</param>
                public DBColumnList(DBClass table)
                {
                    Table = table;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBClass Table
                {
                    get
                    {
                        return table;
                    }
                    private set
                    {
                        table = value;
                    }
                }

                /// <summary>
                /// Data atributů (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Table.Schema.Catalog.PgAttribute
                            .Select($"attrelid = {table.Id}");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgAttribute();
                        }
                    }
                }

                /// <summary>
                /// Seznam atributů (read-only)
                /// </summary>
                public List<DBColumn> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBColumn(columns: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Atribut podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor atributu</param>
                /// <returns>Atribut</returns>
                public DBColumn this[short id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBColumn>();
                    }
                }

                /// <summary>
                /// Atribut podle jména
                /// </summary>
                /// <param name="name">Jméno atributu</param>
                /// <returns>Atribut</returns>
                public DBColumn this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBColumn>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}