﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Sekvence
            /// </summary>
            public class DBSequence
            : DBClass, IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam sekvencí
                /// </summary>
                private DBSequenceList sequences;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu sekvence
                /// </summary>
                /// <param name="sequences">Seznam sekvencí</param>
                /// <param name="data">Metadata sekvence</param>
                public DBSequence(DBSequenceList sequences, DataRow data)
                    : base(schema: sequences.Schema, data: data)
                {
                    Sequences = sequences;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam sekvencí (read-only)
                /// </summary>
                public DBSequenceList Sequences
                {
                    get
                    {
                        return sequences;
                    }
                    private set
                    {
                        sequences = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Počátečení hodnota sekvence (read-only)
                /// </summary>
                public Nullable<long> StartValue
                {
                    get
                    {
                        return
                           Schema.Catalog.PgSequence.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("seqrelid") == Id)
                                .Select(a =>
                                    Functions.GetNLongArg(
                                        row: a,
                                        name: "seqstart",
                                        defaultValue: null))
                                .FirstOrDefault<Nullable<long>>();
                    }
                }

                /// <summary>
                /// Inkrement (read-only)
                /// </summary>
                public Nullable<long> Increment
                {
                    get
                    {
                        return
                            Schema.Catalog.PgSequence.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("seqrelid") == Id)
                                .Select(a =>
                                    Functions.GetNLongArg(
                                        row: a,
                                        name: "seqincrement",
                                        defaultValue: null))
                                .FirstOrDefault<Nullable<long>>();
                    }
                }

                /// <summary>
                /// Minimální hodnota sekvence (read-only)
                /// </summary>
                public Nullable<long> Minimum
                {
                    get
                    {
                        return
                            Schema.Catalog.PgSequence.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("seqrelid") == Id)
                                .Select(a =>
                                    Functions.GetNLongArg(
                                        row: a,
                                        name: "seqmin",
                                        defaultValue: null))
                                .FirstOrDefault<Nullable<long>>();
                    }
                }

                /// <summary>
                /// Maximální hodnota sekvence (read-only)
                /// </summary>
                public Nullable<long> Maximum
                {
                    get
                    {
                        return
                            Schema.Catalog.PgSequence.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("seqrelid") == Id)
                                .Select(a =>
                                    Functions.GetNLongArg(
                                        row: a,
                                        name: "seqmax",
                                        defaultValue: null))
                                .FirstOrDefault<Nullable<long>>();
                    }
                }

                /// <summary>
                /// Povoleno opakování sekvence (read-only)
                /// </summary>
                public Nullable<bool> Cycle
                {
                    get
                    {
                        return
                            Schema.Catalog.PgSequence.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("seqrelid") == Id)
                                .Select(a =>
                                    Functions.GetNBoolArg(
                                        row: a,
                                        name: "seqcycle",
                                        defaultValue: null))
                                .FirstOrDefault<Nullable<bool>>();
                    }
                }

                /// <summary>
                /// SQL definice sekvence (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        if (Increment == null)
                        {
                            return String.Empty;
                        }

                        string strIncrement = $"INCREMENT BY {Increment}";

                        string strMininum = (Minimum == null)
                            ? "NO MINVALUE"
                            : $"MINVALUE {Minimum}";

                        string strMaximum = (Maximum == null)
                            ? "NO MAXVALUE"
                            : $"MAXVALUE {Maximum}";

                        string strStart = (StartValue == null)
                            ? "START WITH 0"
                            : $"START WITH {StartValue}";

                        string strCycle = (Cycle == null)
                            ? "NO CYCLE" :
                            (bool)Cycle
                                ? "CYCLE"
                                : "NO CYCLE";

                        StringBuilder sb = new();

                        // Sekvence
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--{tab}{Name}");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- {Comment}");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}DROP SEQUENCE IF EXISTS {Schema.Name}.{Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}CREATE SEQUENCE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}{strIncrement}");
                        sb.AppendLine(value: $"{tab}{tab}{strMininum}");
                        sb.AppendLine(value: $"{tab}{tab}{strMaximum}");
                        sb.AppendLine(value: $"{tab}{tab}{strStart}");
                        sb.AppendLine(value: $"{tab}{tab}{strCycle};");
                        sb.AppendLine();

                        // KOMENTÁŘE
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- comments");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON SEQUENCE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");
                        sb.AppendLine();

                        // OPRÁVNĚNÍ (!TODO)
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- access privileges");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER SEQUENCE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}OWNER TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON SEQUENCE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT USAGE, SELECT");
                        sb.AppendLine(value: $"{tab}{tab}ON SEQUENCE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO usr_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON SEQUENCE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_mignil;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON SEQUENCE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_sanil;");
                        sb.AppendLine();

                        // DUMP
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- dump");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}SELECT pg_catalog.pg_extension_config_dump('{Schema.Name}.{Name}', '');");

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaná sekvence je stejná
                /// </summary>
                /// <param name="obj">Zadaná sekvence</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBSequence Type, return False
                    if (obj is not DBSequence)
                    {
                        return false;
                    }

                    return
                        Id == ((DBSequence)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis sekvence
                /// </summary>
                /// <returns>Textový popis sekvence</returns>
                public override string ToString()
                {
                    return Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Sequence oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Sekvence oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam sekvencí
            /// </summary>
            public class DBSequenceList
            {

                #region Private Fields

                /// <summary>
                ///  Databázové schéma
                /// </summary>
                private DBSchema schema;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu sekvencí
                /// </summary>
                /// <param name="schema">Databázové schéma</param>
                public DBSequenceList(DBSchema schema)
                {
                    Schema = schema;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Databázové schéma (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return schema;
                    }
                    private set
                    {
                        schema = value;
                    }
                }

                /// <summary>
                /// Data sekvencí (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Schema.Catalog.PgClass.
                            Select($"relnamespace = {Schema.Id} AND relkind = 'S'");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgClass();
                        }
                    }
                }

                /// <summary>
                /// Seznam sekvencí (read-only)
                /// </summary>
                public List<DBSequence> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBSequence(sequences: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Sekvence podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor sekvence</param>
                /// <returns>Sekvence</returns>
                public DBSequence this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBSequence>();
                    }
                }

                /// <summary>
                /// Sekvence podle jména
                /// </summary>
                /// <param name="name">Jméno sekvence</param>
                /// <returns>Sekvence</returns>
                public DBSequence this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBSequence>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}