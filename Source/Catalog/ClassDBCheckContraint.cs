﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Check omezení
            /// </summary>
            public class DBCheckConstraint
            : DBConstraint, IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam check omezení
                /// </summary>
                private DBCheckConstraintList checkConstraints;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu check omezení
                /// </summary>
                /// <param name="checkConstraints">Seznam check omezení</param>
                /// <param name="data">Metadata check omezení</param>
                public DBCheckConstraint(DBCheckConstraintList checkConstraints, DataRow data)
                    : base(table: checkConstraints.Table, data: data)
                {
                    CheckConstraints = checkConstraints;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam check omezení (read-only)
                /// </summary>
                public DBCheckConstraintList CheckConstraints
                {
                    get
                    {
                        return checkConstraints;
                    }
                    private set
                    {
                        checkConstraints = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// SQL definice check omezení (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        StringBuilder sb = new();

                        sb.AppendLine(value: $"{tab}-- {Name}");
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}DROP CONSTRAINT IF EXISTS {Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}ADD CONSTRAINT {Name}");
                        sb.AppendLine(value: $"{tab}{tab}CHECK {Condition}");
                        sb.AppendLine(value: $"{tab}{tab}{DeferrableText}");
                        sb.AppendLine(value: $"{tab}{tab}{DeferredText};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON CONSTRAINT {Name}");
                        sb.AppendLine(value: $"{tab}{tab}ON {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadané check omezení je stejné
                /// </summary>
                /// <param name="obj">Zadané check omezení</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBCheckConstraint Type, return False
                    if (obj is not DBCheckConstraint)
                    {
                        return false;
                    }

                    return
                        Id == ((DBCheckConstraint)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis objektu check omezení
                /// </summary>
                /// <returns>Textový popis objektu check omezení</returns>
                public override string ToString()
                {
                    return Table.Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Check constraint oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Check omezení oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam check omezení
            /// </summary>
            public class DBCheckConstraintList
            {

                #region Private Fields

                /// <summary>
                /// Tabulka
                /// </summary>
                private DBTable table;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu check omezení
                /// </summary>
                /// <param name="table">Tabulka</param>
                public DBCheckConstraintList(DBTable table)
                {
                    Table = table;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBTable Table
                {
                    get
                    {
                        return table;
                    }
                    private set
                    {
                        table = value;
                    }
                }

                /// <summary>
                /// Data check omezení (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Table.Schema.Catalog.PgConstraint
                            .Select(filterExpression: $"conrelid = {table.Id} AND contype = 'c'");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgConstraint();
                        }
                    }
                }

                /// <summary>
                /// Seznam check omezení (read-only)
                /// </summary>
                public List<DBCheckConstraint> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBCheckConstraint(checkConstraints: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Check omezení podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor check omezení</param>
                /// <returns>Check omezení</returns>
                public DBCheckConstraint this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBCheckConstraint>();
                    }
                }

                /// <summary>
                /// Check omezení podle jména
                /// </summary>
                /// <param name="name">Jméno check omezení</param>
                /// <returns>Check omezení</returns>
                public DBCheckConstraint this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBCheckConstraint>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}