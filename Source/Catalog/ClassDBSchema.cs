﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Databázové schéma
            /// </summary>
            public class DBSchema
                : IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam databázových schémat
                /// </summary>
                private DBSchemaList schemas;

                /// <summary>
                /// Metadata databázového schéma
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu databázového schéma
                /// </summary>
                /// <param name="schemas"> Seznam databázových schémat</param>
                /// <param name="data">Metadata databázového schéma</param>
                public DBSchema(DBSchemaList schemas, DataRow data)
                {
                    Schemas = schemas;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam databázových schémat (read-only)
                /// </summary>
                public DBSchemaList Schemas
                {
                    get
                    {
                        return schemas;
                    }
                    private set
                    {
                        schemas = value;
                    }
                }

                /// <summary>
                /// Metadata databázového schéma (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                /// <summary>
                /// Seznam tabulek (read-only)
                /// </summary>
                public DBTableList Tables
                {
                    get
                    {
                        return new DBTableList(schema: this);
                    }
                }

                /// <summary>
                /// Seznam indexů (read-only)
                /// </summary>
                public DBIndexList Indexes
                {
                    get
                    {
                        return new DBIndexList(schema: this);
                    }
                }

                /// <summary>
                /// Seznam sekvencí (read-only)
                /// </summary>
                public DBSequenceList Sequences
                {
                    get
                    {
                        return new DBSequenceList(schema: this);
                    }
                }

                /// <summary>
                /// Seznam pohledů (read-only)
                /// </summary>
                public DBViewList Views
                {
                    get
                    {
                        return new DBViewList(schema: this);
                    }
                }

                /// <summary>
                /// Seznam materializovaných pohledů (read-only)
                /// </summary>
                public DBMaterializedViewList MaterializedViews
                {
                    get
                    {
                        return new DBMaterializedViewList(schema: this);
                    }
                }

                /// <summary>
                /// Seznam cizích tabulek
                /// </summary>
                public DBForeignTableList ForeignTables
                {
                    get
                    {
                        return new DBForeignTableList(schema: this);
                    }
                }

                /// <summary>
                /// Seznam uložených procedur
                /// </summary>
                public DBStoredProcedureList StoredProcedures
                {
                    get
                    {
                        return new DBStoredProcedureList(schema: this);
                    }

                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Katalogy databáze (read-only)
                /// </summary>
                public DBCatalog Catalog
                {
                    get
                    {
                        return Schemas.Catalog;
                    }
                }

                /// <summary>
                /// Identifikátor databázového schéma (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "oid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Jméno databázového schéma (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "nspname",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Vlastník databázového schéma (read-only)
                /// </summary>
                public Nullable<uint> OwnerId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "nspowner",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Schemas.Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }

                /// <summary>
                /// SQL definice schématu (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        StringBuilder sb = new();

                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--{tab}Schema: {Name}");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- {Comment}");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}DROP SCHEMA IF EXISTS {Name} CASCADE;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}CREATE SCHEMA {Name};");
                        sb.AppendLine();

                        // KOMENTÁŘE
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- comments");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON SCHEMA {Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");
                        sb.AppendLine();

                        // OPRÁVNĚNÍ (!TODO)
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- access privileges");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER SCHEMA {Name}");
                        sb.AppendLine(value: $"{tab}{tab}OWNER TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON SCHEMA {Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT USAGE");
                        sb.AppendLine(value: $"{tab}{tab}ON SCHEMA {Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO usr_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON SCHEMA {Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_mignil;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON SCHEMA {Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_sanil;");
                        sb.AppendLine();

                        return sb.ToString();

                    }
                }


                /// <summary>
                /// Informace o závislostech objektů schématu na jiných schématech
                /// </summary>
                public string ReferenceListInfo
                {
                    get
                    {
                        StringBuilder sb = new();

                        foreach (DBTable table in Tables.Items.Where(a => a.ReferencedSchemas.Count != 0))
                        {
                            foreach (DBSchema schema in table.ReferencedSchemas)
                            {
                                sb.AppendLine(value: $"{table.Name} => {schema.Name}");
                            }
                        }

                        foreach (DBView view in Views.Items.Where(a => a.ReferencedSchemas.Count != 0))
                        {
                            foreach (DBSchema schema in view.ReferencedSchemas)
                            {
                                sb.AppendLine(value: $"{view.Name} => {schema.Name}");
                            }
                        }

                        foreach (DBMaterializedView view in MaterializedViews.Items.Where(a => a.ReferencedSchemas.Count != 0))
                        {
                            foreach (DBSchema schema in view.ReferencedSchemas)
                            {
                                sb.AppendLine(value: $"{view.Name} => {schema.Name}");
                            }
                        }

                        foreach (DBStoredProcedure sp in StoredProcedures.Items.Where(a => a.ReferencedSchemas.Count != 0))
                        {
                            foreach (DBSchema schema in sp.ReferencedSchemas)
                            {
                                sb.AppendLine(value: $"{sp.FullName} => {schema.Name}");
                            }
                        }

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadané databázové schéma je stejné
                /// </summary>
                /// <param name="obj">Zadané databázové schéma</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBSchema Type, return False
                    if (obj is not DBSchema)
                    {
                        return false;
                    }

                    return
                        Id == ((DBSchema)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis databázového schéma
                /// </summary>
                /// <returns>Textový popis databázového schéma</returns>
                public override string ToString()
                {
                    return Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Database schema oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Databázové schéma oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam databázových schémat
            /// </summary>
            public class DBSchemaList
            {

                #region Private Fields

                /// <summary>
                /// Katalogy databáze
                /// </summary>
                private DBCatalog catalog;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu databázových schémat
                /// </summary>
                /// <param name="catalog">Katalogy databáze</param>
                public DBSchemaList(DBCatalog catalog)
                {
                    Catalog = catalog;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Katalogy databáze (read-only)
                /// </summary>
                public DBCatalog Catalog
                {
                    get
                    {
                        return catalog;
                    }
                    private set
                    {
                        catalog = value;
                    }
                }

                /// <summary>
                /// Data databázových schémat (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Catalog.PgNamespace
                            .Select();

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgNamespace();
                        }
                    }
                }

                /// <summary>
                /// Seznam databázových schémat (read-only)
                /// </summary>
                public List<DBSchema> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBSchema(schemas: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Databázové schéma podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor databázového schéma</param>
                /// <returns>Databázové schéma</returns>
                public DBSchema this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBSchema>();
                    }
                }

                /// <summary>
                /// Databázové schéma podle jména
                /// </summary>
                /// <param name="name">Jméno databázového schéma</param>
                /// <returns>Databázové schéma</returns>
                public DBSchema this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBSchema>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}