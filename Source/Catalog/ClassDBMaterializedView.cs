﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Materializovaný pohled
            /// </summary>
            public class DBMaterializedView
            : DBClass, IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam materializovaných pohledů
                /// </summary>
                private DBMaterializedViewList materializedViews;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu materializovaného pohledu
                /// </summary>
                /// <param name="materializedViews">Seznam materializovaných pohledů</param>
                /// <param name="data">Metadata materializovaného pohledu</param>
                public DBMaterializedView(DBMaterializedViewList materializedViews, DataRow data)
                    : base(schema: materializedViews.Schema, data: data)
                {
                    MaterializedViews = materializedViews;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam materializovaných pohledů (read-only)
                /// </summary>
                public DBMaterializedViewList MaterializedViews
                {
                    get
                    {
                        return materializedViews;
                    }
                    private set
                    {
                        materializedViews = value;
                    }
                }

                /// <summary>
                /// Seznam atributů (read-only)
                /// </summary>
                public DBColumnList Columns
                {
                    get
                    {
                        return new DBColumnList(table: this);
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Definice materializovaného pohledu (read-only)
                /// </summary>
                public string ViewDefinition
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "view_definition",
                                defaultValue: null)
                            ?? String.Empty;
                    }
                }

                /// <summary>
                /// SQL definice materializovaného pohledu (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        StringBuilder sb = new();

                        string strColumnComments =
                            Columns.Items
                                .OrderBy(a => a.Id)
                                .Select(a => $"{a.SqlComment}")
                                .Aggregate((a, b) => $"{a}{Environment.NewLine}{b}");

                        // POHLED
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--{tab}{Name}");
                        sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"--{tab[..^2]}SELECT * FROM {Schema.Name}.{Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"--{tab[..^2]}REFRESH MATERIALIZED VIEW {Schema.Name}.{Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}DROP MATERIALIZED VIEW IF EXISTS {Schema.Name}.{Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}CREATE MATERIALIZED VIEW {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}AS");

                        if (ViewDefinition.EndsWith(value: ';'))
                        {
                            sb.AppendLine(value: $"{ViewDefinition}");
                        }
                        else
                        {
                            sb.AppendLine(value: $"{ViewDefinition};");
                        }

                        sb.AppendLine();

                        // KOMENTÁŘE
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- comments");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}-- pohled");
                        sb.AppendLine(value: $"{tab}COMMENT ON MATERIALIZED VIEW {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}-- atributy");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{strColumnComments}");

                        // OPRÁVNĚNÍ (TODO)
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine(value: $"-- access privileges");
                        sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}OWNER TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO adm_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT SELECT");
                        sb.AppendLine(value: $"{tab}{tab}ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO usr_nfi_data;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_mignil;");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}GRANT ALL");
                        sb.AppendLine(value: $"{tab}{tab}ON TABLE {Schema.Name}.{Name}");
                        sb.AppendLine(value: $"{tab}{tab}TO app_sanil;");
                        sb.AppendLine();

                        return sb.ToString();
                    }
                }

                /// <summary>
                /// Označení místa pro vložení definice pohledu (začátek) (read-only)
                /// </summary>
                public string MarkerBegin
                {
                    get
                    {
                        return
                            String.Concat(
                                $"--------------------------------------------------------------------------------;{Environment.NewLine}",
                                $"-- <view view_name = \"{Name}\" view_schema = \"{Schema.Name}\" src = \"views/{Schema.Name}/{Name}.sql\">{Environment.NewLine}");
                    }
                }

                /// <summary>
                /// Označení místa pro vložení definice pohledu (konec) (read-only)
                /// </summary>
                public static string MarkerEnd
                {
                    get
                    {
                        return $"-- </view>{Environment.NewLine}";
                    }
                }

                /// <summary>
                /// Označení místa pro vložení definice pohledu (read-only)
                /// </summary>
                public string Marker
                {
                    get
                    {
                        return String.Concat(MarkerBegin, MarkerEnd);
                    }
                }

                /// <summary>
                /// Schéma s objekty, na které se odkazuje definice pohledu (read-only)
                /// </summary>
                public List<DBSchema> ReferencedSchemas
                {
                    get
                    {
                        return
                            Schema.Catalog.Schemas.Items
                                .Where(a => a.Name != Schema.Name)
                                .Where(a => SqlDefinition.Contains(value: a.Name))
                                .ToList<DBSchema>();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaný materializovaný pohled je stejný
                /// </summary>
                /// <param name="obj">Zadaný materializovaný pohled</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBMaterializedView Type, return False
                    if (obj is not DBMaterializedView)
                    {
                        return false;
                    }

                    return
                        Id == ((DBMaterializedView)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis materializovaného pohledu
                /// </summary>
                /// <returns>Textový popis materializovaného pohledu</returns>
                public override string ToString()
                {
                    return Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Materialized view oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Materializovaný pohled oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam materializovaných pohledů
            /// </summary>
            public class DBMaterializedViewList
            {

                #region Private Fields

                /// <summary>
                /// Databázové schéma
                /// </summary>
                private DBSchema schema;

                #endregion Private Fields


                #region Contructor

                /// <summary>
                /// Konstruktor objektu seznamu materializovaných pohledů
                /// </summary>
                /// <param name="schema">Databázové schéma</param>
                public DBMaterializedViewList(DBSchema schema)
                {
                    Schema = schema;
                }

                #endregion Contructor


                #region Properties

                /// <summary>
                /// Databázové schéma (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return schema;
                    }
                    private set
                    {
                        schema = value;
                    }
                }

                /// <summary>
                /// Data materializovaných pohledů (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Schema.Catalog.PgClass
                            .Select($"relnamespace = {Schema.Id} AND relkind = 'm'");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgClass();
                        }
                    }
                }

                /// <summary>
                /// Seznam materializovaných pohledů (read-only)
                /// </summary>
                public List<DBMaterializedView> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBMaterializedView(materializedViews: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Materializovaný pohled podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor materializovaného pohledu</param>
                /// <returns>Materializovaný pohled</returns>
                public DBMaterializedView this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBMaterializedView>();
                    }
                }

                /// <summary>
                /// Materializovaný pohled podle jména
                /// </summary>
                /// <param name="name">Jméno materializovaného pohledu</param>
                /// <returns>Materializovaný pohled</returns>
                public DBMaterializedView this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBMaterializedView>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}
