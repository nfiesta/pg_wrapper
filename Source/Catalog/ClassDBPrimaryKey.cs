﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Primární klíč
            /// </summary>
            public class DBPrimaryKey
            : DBConstraint, IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam primárních klíčů
                /// </summary>
                private DBPrimaryKeyList primaryKeys;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu primárního klíče
                /// </summary>
                /// <param name="primaryKeys">Seznam primárních klíčů</param>
                /// <param name="data">Metadata primárního klíče</param>
                public DBPrimaryKey(DBPrimaryKeyList primaryKeys, DataRow data)
                    : base(table: primaryKeys.Table, data: data)
                {
                    PrimaryKeys = primaryKeys;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam primárních klíčů (read-only)
                /// </summary>
                public DBPrimaryKeyList PrimaryKeys
                {
                    get
                    {
                        return primaryKeys;
                    }
                    private set
                    {
                        primaryKeys = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// SQL definice primárního klíče (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        StringBuilder sb = new();

                        sb.AppendLine(value: $"{tab}-- {Name}");
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}DROP CONSTRAINT IF EXISTS {Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}ADD CONSTRAINT {Name}");
                        sb.AppendLine(value: $"{tab}{tab}PRIMARY KEY ({ConstraintColumnsText})");
                        sb.AppendLine(value: $"{tab}{tab}{DeferrableText}");
                        sb.AppendLine(value: $"{tab}{tab}{DeferredText};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON CONSTRAINT {Name}");
                        sb.AppendLine(value: $"{tab}{tab}ON {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaný primární klíč je stejný
                /// </summary>
                /// <param name="obj">Zadaný primární klíč</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBPrimaryKey Type, return False
                    if (obj is not DBPrimaryKey)
                    {
                        return false;
                    }

                    return
                        Id == ((DBPrimaryKey)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis objektu primárního klíče
                /// </summary>
                /// <returns>Textový popis objektu primárního klíče</returns>
                public override string ToString()
                {
                    return Table.Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Primary key oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Primární klíč oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam primárních klíčů
            /// </summary>
            public class DBPrimaryKeyList
            {

                #region Private Fields

                /// <summary>
                /// Tabulka
                /// </summary>
                private DBTable table;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu primárních klíčů
                /// </summary>
                /// <param name="table">Tabulka</param>
                public DBPrimaryKeyList(DBTable table)
                {
                    Table = table;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBTable Table
                {
                    get
                    {
                        return table;
                    }
                    private set
                    {
                        table = value;
                    }
                }

                /// <summary>
                /// Data primárních klíčů (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Table.Schema.Catalog.PgConstraint
                            .Select(filterExpression: $"conrelid = {table.Id} AND contype = 'p'");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgConstraint();
                        }
                    }
                }

                /// <summary>
                /// Seznam primárních klíčů (read-only)
                /// </summary>
                public List<DBPrimaryKey> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBPrimaryKey(primaryKeys: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Primární klíč podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor primárního klíče</param>
                /// <returns>Primární klíč</returns>
                public DBPrimaryKey this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBPrimaryKey>();
                    }
                }

                /// <summary>
                /// Primární klíč podle jména
                /// </summary>
                /// <param name="name">Jméno primárního klíče</param>
                /// <returns>Primární klíč</returns>
                public DBPrimaryKey this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBPrimaryKey>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}
