﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Cizí klíč
            /// </summary>
            public class DBForeignKey
            : DBConstraint, IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam cizích klíčů
                /// </summary>
                private DBForeignKeyList foreignKeys;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu cizího klíče
                /// </summary>
                /// <param name="foreignKeys">Seznam cizích klíčů</param>
                /// <param name="data">Metadata cizího klíče</param>
                public DBForeignKey(DBForeignKeyList foreignKeys, DataRow data)
                    : base(table: foreignKeys.Table, data: data)
                {
                    ForeignKeys = foreignKeys;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam cizích klíčů (read-only)
                /// </summary>
                public DBForeignKeyList ForeignKeys
                {
                    get
                    {
                        return foreignKeys;
                    }
                    private set
                    {
                        foreignKeys = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// SQL definice cizího klíče
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        StringBuilder sb = new();

                        sb.AppendLine(value: $"{tab}-- {Name}");
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}DROP CONSTRAINT IF EXISTS {Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}ADD CONSTRAINT {Name}");
                        sb.AppendLine(value: $"{tab}{tab}FOREIGN KEY ({ConstraintColumnsText})");
                        sb.AppendLine(value: $"{tab}{tab}REFERENCES {ForeignTable.Schema.Name}.{ForeignTable.Name}({ForeignColumnsText})");
                        sb.AppendLine(value: $"{tab}{tab}{MatchTypeText}");
                        sb.AppendLine(value: $"{tab}{tab}{OnUpdateActionText}");
                        sb.AppendLine(value: $"{tab}{tab}{OnDeleteActionText}");
                        sb.AppendLine(value: $"{tab}{tab}{DeferrableText}");
                        sb.AppendLine(value: $"{tab}{tab}{DeferredText};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON CONSTRAINT {Name}");
                        sb.AppendLine(value: $"{tab}{tab}ON {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaný cizí klíč je stejný
                /// </summary>
                /// <param name="obj">Zadaný cizí klíč</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBForeignKey Type, return False
                    if (obj is not DBForeignKey)
                    {
                        return false;
                    }

                    return
                        Id == ((DBForeignKey)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis objektu cizího klíče
                /// </summary>
                /// <returns>Textový popis objektu cizího klíče</returns>
                public override string ToString()
                {
                    return Table.Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Foreign key oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Cizí klíč oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam cizích klíčů
            /// </summary>
            public class DBForeignKeyList
            {

                #region Private Fields

                /// <summary>
                /// Tabulka
                /// </summary>
                private DBTable table;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu cizích klíčů
                /// </summary>
                /// <param name="table">Tabulka</param>
                public DBForeignKeyList(DBTable table)
                {
                    Table = table;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBTable Table
                {
                    get
                    {
                        return table;
                    }
                    private set
                    {
                        table = value;
                    }
                }

                /// <summary>
                /// Data cizích klíčů (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Table.Schema.Catalog.PgConstraint
                            .Select($"conrelid = {table.Id} AND contype = 'f'");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgConstraint();
                        }
                    }
                }

                /// <summary>
                /// Seznam cizích klíčů (read-only)
                /// </summary>
                public List<DBForeignKey> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBForeignKey(foreignKeys: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Cizí klíč podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor cizího klíče</param>
                /// <returns>Cizí klíč</returns>
                public DBForeignKey this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBForeignKey>();
                    }
                }

                /// <summary>
                /// Cizí klíč podle jména
                /// </summary>
                /// <param name="name">Jméno cizího klíče</param>
                /// <returns>Cizí klíč</returns>
                public DBForeignKey this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBForeignKey>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}