﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Cizí tabulka
            /// </summary>
            public class DBForeignTable
            : DBClass, IDBObject
            {

                #region Private Fields

                /// <summary>
                /// Seznam cizích tabulek
                /// </summary>
                private DBForeignTableList foreignTables;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu cizí tabulky
                /// </summary>
                /// <param name="foreignTables">Seznam cizích tabulek</param>
                /// <param name="data">Metadata cizí tabulky</param>
                public DBForeignTable(DBForeignTableList foreignTables, DataRow data)
                    : base(schema: foreignTables.Schema, data: data)
                {
                    ForeignTables = foreignTables;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam cizích tabulek (read-only)
                /// </summary>
                public DBForeignTableList ForeignTables
                {
                    get
                    {
                        return foreignTables;
                    }
                    private set
                    {
                        foreignTables = value;
                    }
                }

                /// <summary>
                /// Seznam atributů (read-only)
                /// </summary>
                public DBColumnList Columns
                {
                    get
                    {
                        return new DBColumnList(table: this);
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// SQL definice cizí tabulky (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        return String.Empty;
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaná cizí tabulka je stejná
                /// </summary>
                /// <param name="obj">Zadaná cizí tabulka</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBForeignTable Type, return False
                    if (obj is not DBForeignTable)
                    {
                        return false;
                    }

                    return
                        Id == ((DBForeignTable)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis cizí tabulky
                /// </summary>
                /// <returns>Textový popis cizí tabulky</returns>
                public override string ToString()
                {
                    return Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Foreign table oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Cizí tabulka oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam cizích tabulek
            /// </summary>
            public class DBForeignTableList
            {

                #region Private Fields

                /// <summary>
                /// Databázové schéma
                /// </summary>
                private DBSchema schema;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu cizích tabulek
                /// </summary>
                /// <param name="schema">Databázové schéma</param>
                public DBForeignTableList(DBSchema schema)
                {
                    Schema = schema;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Databázové schéma (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return schema;
                    }
                    private set
                    {
                        schema = value;
                    }
                }

                /// <summary>
                /// Data cizích tabulek (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Schema.Catalog.PgClass
                            .Select($"relnamespace = {Schema.Id} AND relkind = 'f'");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgClass();
                        }
                    }
                }

                /// <summary>
                /// Seznam cizích tabulek (read-only)
                /// </summary>
                public List<DBForeignTable> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBForeignTable(foreignTables: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Cizí tabulka podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor cizí tabulky</param>
                /// <returns>Cizí tabulka</returns>
                public DBForeignTable this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBForeignTable>();
                    }
                }

                /// <summary>
                /// Cizí tabulka podle jména
                /// </summary>
                /// <param name="name">Jméno cizí tabulky</param>
                /// <returns>Cizí tabulka</returns>
                public DBForeignTable this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBForeignTable>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}
