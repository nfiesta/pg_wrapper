﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Role
            /// </summary>
            public class DBRole
                : IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam rolí
                /// </summary>
                private DBRoleList roles;

                /// <summary>
                /// Metadata rolí
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu role
                /// </summary>
                /// <param name="roles">Seznam rolí</param>
                /// <param name="data">Metadata rolí</param>
                public DBRole(DBRoleList roles, DataRow data)
                {
                    Roles = roles;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam rolí (read-only)
                /// </summary>
                public DBRoleList Roles
                {
                    get
                    {
                        return roles;
                    }
                    private set
                    {
                        roles = value;
                    }
                }

                /// <summary>
                /// Metadata rolí (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Katalogy databáze (read-only)
                /// </summary>
                public DBCatalog Catalog
                {
                    get
                    {
                        return Roles.Catalog;
                    }
                }

                /// <summary>
                /// Identifikátor role (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "oid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Jméno role (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "rolname",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Role má oprávnění super uživatele (read-only)
                /// </summary>
                public Nullable<bool> SuperUser
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "rolsuper",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Role dědí oprávnění skupin, kterých je členem (read-only)
                /// </summary>
                public Nullable<bool> Inherit
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "rolinherit",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Role může vytvářet další role (read-only)
                /// </summary>
                public Nullable<bool> CreateRole
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "rolcreaterole",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Role může vytvářet databáze (read-only)
                /// </summary>
                public Nullable<bool> CreateDb
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "rolcreatedb",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Role se může přihlásit (read-only)
                /// </summary>
                public Nullable<bool> CanLogin
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "rolcanlogin",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Role může iniciovat replikační připojení a vytvářet a rušit replikační sloty (read-only)
                /// </summary>
                public Nullable<bool> Replication
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "rolreplication",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Role obchází všechny zásady zabezpečení na úrovni řádků (read-only)
                /// </summary>
                public Nullable<bool> ByPassRLS
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "rolbypassrls",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Maximální počet připojení, které může role souběžně vytvořit (read-only)
                /// </summary>
                public Nullable<int> ConnectionLimit
                {
                    get
                    {
                        return
                            Functions.GetNIntArg(
                                row: Data,
                                name: "rolconnlimit",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }

                /// <summary>
                /// SQL definice role (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        if (String.IsNullOrEmpty(value: Name))
                        {
                            return String.Empty;
                        }

                        StringBuilder sb = new();

                        if (CanLogin ?? false)
                        {
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                            sb.AppendLine(value: $"--{tab}Login role: {Name}");
                            sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                        }
                        else
                        {
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                            sb.AppendLine(value: $"--{tab}Group role: {Name}");
                            sb.AppendLine(value: $"--||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||--;");
                            sb.AppendLine(value: $"--------------------------------------------------------------------------------;");
                            sb.AppendLine();
                        }

                        sb.AppendLine(value: $"{tab}-- DROP ROLE {Name};");

                        sb.AppendLine();

                        sb.AppendLine(value: $"{tab}CREATE ROLE {Name} WITH");

                        if (SuperUser != null)
                        {
                            sb.AppendLine(value: (bool)SuperUser ? $"{tab}{tab}SUPERUSER" : $"{tab}{tab}NOSUPERUSER");
                        }

                        if (CreateDb != null)
                        {
                            sb.AppendLine(value: (bool)CreateDb ? $"{tab}{tab}CREATEDB" : $"{tab}{tab}NOCREATEDB");
                        }

                        if (CreateRole != null)
                        {
                            sb.AppendLine(value: (bool)CreateRole ? $"{tab}{tab}CREATEROLE" : $"{tab}{tab}NOCREATEROLE");
                        }

                        if (Inherit != null)
                        {
                            sb.AppendLine(value: (bool)Inherit ? $"{tab}{tab}INHERIT" : $"{tab}{tab}NOINHERIT");
                        }

                        if (CanLogin != null)
                        {
                            sb.AppendLine(value: (bool)CanLogin ? $"{tab}{tab}LOGIN" : $"{tab}{tab}NOLOGIN");
                        }

                        if (Replication != null)
                        {
                            sb.AppendLine(value: (bool)Replication ? $"{tab}{tab}REPLICATION" : $"{tab}{tab}NOREPLICATION");
                        }


                        if (CanLogin ?? false)
                        {
                            if (ByPassRLS != null)
                            {
                                sb.AppendLine(value: (bool)ByPassRLS ? $"{tab}{tab}BYPASSRLS" : $"{tab}{tab}NOBYPASSRLS");
                            }

                            if (ConnectionLimit != null)
                            {
                                sb.AppendLine(value: $"{tab}CONNECTION LIMIT {ConnectionLimit};");
                            }
                            else
                            {
                                sb.AppendLine(value: $"{tab}CONNECTION LIMIT -1;");
                            }
                        }
                        else
                        {
                            if (ByPassRLS != null)
                            {
                                sb.AppendLine(value: (bool)ByPassRLS ? $"{tab}{tab}BYPASSRLS;" : $"{tab}{tab}NOBYPASSRLS;");
                            }
                        }

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaná role je stejná
                /// </summary>
                /// <param name="obj">Zadaná role</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBRole Type, return False
                    if (obj is not DBRole)
                    {
                        return false;
                    }

                    return
                        Id == ((DBRole)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis role
                /// </summary>
                /// <returns>Textový popis role</returns>
                public override string ToString()
                {
                    return Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Role oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Role oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam rolí
            /// </summary>
            public class DBRoleList
            {

                #region Private Fields

                /// <summary>
                /// Katalogy databáze
                /// </summary>
                private DBCatalog catalog;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu rolí
                /// </summary>
                /// <param name="catalog">Katalogy databáze</param>
                public DBRoleList(DBCatalog catalog)
                {
                    Catalog = catalog;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Katalogy databáze (read-only)
                /// </summary>
                public DBCatalog Catalog
                {
                    get
                    {
                        return catalog;
                    }
                    private set
                    {
                        catalog = value;
                    }
                }

                /// <summary>
                /// Data rolí (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Catalog.PgAuthId
                            .Select();

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgNamespace();
                        }
                    }
                }

                /// <summary>
                /// Seznam rolí (read-only)
                /// </summary>
                public List<DBRole> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBRole(roles: this, data: a))];
                    }
                }

                /// <summary>
                /// Seznam přihlašovacích rolí (read-only)
                /// </summary>
                public List<DBRole> LoginRoles
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable()
                            .Select(a => new DBRole(roles: this, data: a))
                            .Where(a => a.CanLogin ?? false)];
                    }
                }

                /// <summary>
                /// Seznam skupinových rolí (read-only)
                /// </summary>
                public List<DBRole> GroupRoles
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable()
                            .Select(a => new DBRole(roles: this, data: a))
                            .Where(a => !(a.CanLogin ?? false))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Role podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor role</param>
                /// <returns>Role</returns>
                public DBRole this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBRole>();
                    }
                }

                /// <summary>
                /// Role podle jména
                /// </summary>
                /// <param name="name">Jméno role</param>
                /// <returns>Role</returns>
                public DBRole this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBRole>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}
