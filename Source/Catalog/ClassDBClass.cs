﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Data;
using System.Linq;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Relace
            /// </summary>
            public abstract class DBClass
            {

                #region Private Fields

                /// <summary>
                /// Databázové schéma
                /// </summary>
                private DBSchema schema;

                /// <summary>
                /// Metadata relace
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu relace
                /// </summary>
                /// <param name="schema">Databázové schéma</param>
                /// <param name="data">Metadata relace</param>
                public DBClass(DBSchema schema, DataRow data)
                {
                    Schema = schema;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Databázové schéma (read-only)
                /// </summary>
                public DBSchema Schema
                {
                    get
                    {
                        return schema;
                    }
                    private set
                    {
                        schema = value;
                    }
                }

                /// <summary>
                /// Metadata relace (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Identifikátor relace (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "oid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Jméno relace (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "relname",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Celé jméno relace včetně schéma (read-only)
                /// </summary>
                public string FullName
                {
                    get
                    {
                        return $"{Schema.Name}.{Name}";
                    }
                }

                /// <summary>
                /// Databázové schéma - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> SchemaId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "relnamespace",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Vlastník tabulky - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> OwnerId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "nspowner",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Přístupová metoda - identifikátor (read-only)
                /// </summary>
                public Nullable<uint> AccessMethodId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "relam",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Jméno přístupové metody (read-only)
                /// </summary>
                public string AccessMethodName
                {
                    get
                    {
                        if (AccessMethodId == null)
                        {
                            return String.Empty;
                        }

                        return
                            Schema.Catalog.PgAM.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("oid") == (uint)AccessMethodId)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "amname",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }

                /// <summary>
                /// Typ relace (read-only)
                /// </summary>
                public RelKind RelKind
                {
                    get
                    {
                        Nullable<char> val =
                            Functions.GetNCharArg(
                                row: Data,
                                name: "relkind",
                                defaultValue: null);

                        if (val == null)
                        {
                            return RelKind.Unknown;
                        }

                        return val switch
                        {
                            'r' => RelKind.Table,
                            'i' => RelKind.Index,
                            'S' => RelKind.Sequence,
                            'v' => RelKind.View,
                            'm' => RelKind.MaterializedView,
                            'c' => RelKind.CompositeType,
                            't' => RelKind.ToastTable,
                            'f' => RelKind.ForeignTable,
                            _ => RelKind.Unknown,
                        };
                    }
                }

                /// <summary>
                /// Relace má indexy (read-only)
                /// </summary>
                public Nullable<bool> HasIndex
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "relhasindex",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Relace má triggery (read-only)
                /// </summary>
                public Nullable<bool> HasTriggers
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "relhastriggers",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Počet atributů relace (read-only)
                /// </summary>
                public Nullable<short> AttributesNumber
                {
                    get
                    {
                        return
                            Functions.GetNShortArg(
                                row: Data,
                                name: "relnatts",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Počet check omezení relace (read-only)
                /// </summary>
                public Nullable<short> CheckNumber
                {
                    get
                    {
                        return
                            Functions.GetNShortArg(
                                row: Data,
                                name: "relchecks",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Schema.Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaná relace je stejná
                /// </summary>
                /// <param name="obj">Zadaná relace</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBClass Type, return False
                    if (obj is not DBClass)
                    {
                        return false;
                    }

                    return
                        Id == ((DBClass)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                #endregion Methods

            }

            /// <summary>
            /// Typ relace
            /// r,i,S,v,m,c,t,f
            /// </summary>
            public enum RelKind
            {

                /// <summary>
                /// Tabulka
                /// </summary>
                Table = (int)'r',

                /// <summary>
                /// Sekvence
                /// </summary>
                Sequence = (int)'S',

                /// <summary>
                /// Index
                /// </summary>
                Index = (int)'i',

                /// <summary>
                /// Pohled
                /// </summary>
                View = (int)'v',

                /// <summary>
                /// Materializovaný pohled
                /// </summary>
                MaterializedView = (int)'m',

                /// <summary>
                /// Cizí tabulka
                /// </summary>
                ForeignTable = (int)'f',

                /// <summary>
                /// Složený typ
                /// </summary>
                CompositeType = (int)'c',

                /// <summary>
                /// Toast tabulka
                /// </summary>
                ToastTable = (int)'t',

                /// <summary>
                /// Rozdělená tabulka
                /// </summary>
                PartitionedTable = (int)'p',

                /// <summary>
                /// Rozdělený index
                /// </summary>
                PartitionedIndex = (int)'I',

                /// <summary>
                /// Není definováno
                /// </summary>
                Unknown = (int)'0'

            };

        }
    }
}