﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Rozhraní pro objekt z katalogů
            /// </summary>
            public interface IDBObject
            {

                /// <summary>
                /// Identifikátor (read-only)
                /// </summary>
                uint Id { get; }


                /// <summary>
                /// Jméno (read-only)
                /// </summary>
                string Name { get; }


                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                string Comment { get; }


                /// <summary>
                /// SQL definice atributu (read-only)
                /// </summary>
                string SqlDefinition { get; }

            }

        }
    }
}
