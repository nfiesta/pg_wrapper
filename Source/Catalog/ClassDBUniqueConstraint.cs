﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Unique omezení
            /// </summary>
            public class DBUniqueConstraint
            : DBConstraint, IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam unique omezení
                /// </summary>
                private DBUniqueConstraintList uniqueConstraints;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu unique omezení
                /// </summary>
                /// <param name="uniqueConstraints">Seznam unique omezení</param>
                /// <param name="data">Metadata unique omezení</param>
                public DBUniqueConstraint(DBUniqueConstraintList uniqueConstraints, DataRow data)
                    : base(table: uniqueConstraints.Table, data: data)
                {
                    UniqueConstraints = uniqueConstraints;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam unique omezení (read-only)
                /// </summary>
                public DBUniqueConstraintList UniqueConstraints
                {
                    get
                    {
                        return uniqueConstraints;
                    }
                    private set
                    {
                        uniqueConstraints = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// SQL definice unique omezení (read-only)
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        StringBuilder sb = new();

                        sb.AppendLine(value: $"{tab}-- {Name}");
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}DROP CONSTRAINT IF EXISTS {Name};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}ALTER TABLE {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}ADD CONSTRAINT {Name}");
                        sb.AppendLine(value: $"{tab}{tab}UNIQUE ({ConstraintColumnsText})");
                        sb.AppendLine(value: $"{tab}{tab}{DeferrableText}");
                        sb.AppendLine(value: $"{tab}{tab}{DeferredText};");
                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON CONSTRAINT {Name}");
                        sb.AppendLine(value: $"{tab}{tab}ON {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadané unique omezení je stejné
                /// </summary>
                /// <param name="obj">Zadané unique omezení</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBUniqueConstraint Type, return False
                    if (obj is not DBUniqueConstraint)
                    {
                        return false;
                    }

                    return
                        Id == ((DBUniqueConstraint)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis objektu unique omezení
                /// </summary>
                /// <returns>Textový popis objektu unique omezení</returns>
                public override string ToString()
                {
                    return Table.Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Unique constraint oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Unique omezení oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam unique omezení
            /// </summary>
            public class DBUniqueConstraintList
            {

                #region Private Fields

                /// <summary>
                /// Tabulka
                /// </summary>
                private DBTable table;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu unique omezení
                /// </summary>
                /// <param name="table">Tabulka</param>
                public DBUniqueConstraintList(DBTable table)
                {
                    Table = table;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBTable Table
                {
                    get
                    {
                        return table;
                    }
                    private set
                    {
                        table = value;
                    }
                }

                /// <summary>
                /// Data unique omezení (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Table.Schema.Catalog.PgConstraint
                            .Select(filterExpression: $"conrelid = {table.Id} AND contype = 'u'");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgConstraint();
                        }
                    }
                }

                /// <summary>
                /// Seznam unique omezení (read-only)
                /// </summary>
                public List<DBUniqueConstraint> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBUniqueConstraint(uniqueConstraints: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Unique omezení podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor unique omezení</param>
                /// <returns>Unique omezení</returns>
                public DBUniqueConstraint this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBUniqueConstraint>();
                    }
                }

                /// <summary>
                /// Unique omezení podle jména
                /// </summary>
                /// <param name="name">Jméno unique omezení</param>
                /// <returns>Unique omezení</returns>
                public DBUniqueConstraint this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBUniqueConstraint>();
                    }
                }

                #endregion Indexer

            }

        }
    }
}
