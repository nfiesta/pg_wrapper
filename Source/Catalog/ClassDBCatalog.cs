﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using Npgsql;
using System;
using System.Data;
using System.Windows.Forms;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Katalogy databáze
            /// </summary>
            public class DBCatalog
            {

                #region Private Fields

                /// <summary>
                /// Připojení k databázi PostgreSQL
                /// </summary>
                private PostgreSQLWrapper database;

                /// <summary>
                /// Číslo verze databázového serveru PostgreSQL
                /// </summary>
                private Nullable<int> serverVersionNum;

                /// <summary>
                /// Jsou načtena data katalogu?
                /// </summary>
                private bool initialized;

                /// <summary>
                /// Katalog databázových extenzí
                /// </summary>
                private DataTable pgExtension;

                /// <summary>
                /// Katalog schémat
                /// </summary>
                private DataTable pgNamespace;

                /// <summary>
                /// Katalog tabulek
                /// </summary>
                private DataTable pgClass;

                /// <summary>
                /// Katalog sekvencí
                /// </summary>
                private DataTable pgSequence;

                /// <summary>
                /// Katalog indexů
                /// </summary>
                private DataTable pgIndex;

                /// <summary>
                /// Katalog atributů
                /// </summary>
                private DataTable pgAttribute;

                /// <summary>
                /// Katalog defaultních hodnot atributů
                /// </summary>
                private DataTable pgAttrDef;

                /// <summary>
                /// Katalog typů
                /// </summary>
                private DataTable pgType;

                /// <summary>
                /// Katalog přístupových metod
                /// </summary>
                private DataTable pgAM;

                /// <summary>
                /// Katalog jazyků
                /// </summary>
                private DataTable pgLanguage;

                /// <summary>
                /// Katalog omezení
                /// </summary>
                private DataTable pgConstraint;

                /// <summary>
                /// Katalog triggerů
                /// </summary>
                private DataTable pgTrigger;

                /// <summary>
                /// Katalog popisků
                /// </summary>
                private DataTable pgDescription;

                /// <summary>
                /// Katalog rolí
                /// </summary>
                private DataTable pgAuthId;

                /// <summary>
                /// Katalog uložených procedur
                /// </summary>
                private DataTable pgProc;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu katalogů databáze
                /// </summary>
                /// <param name="database">Připojení k databázi</param>
                public DBCatalog(PostgreSQLWrapper database)
                {
                    Database = database;
                    ServerVersionNum = null;

                    PgExtension = EmptyPgExtension();
                    PgNamespace = EmptyPgNamespace();
                    PgClass = EmptyPgClass();
                    PgSequence = EmptyPgSequence();
                    PgIndex = EmptyPgIndex();
                    PgAttribute = EmptyPgAttribute();
                    PgAttrDef = EmptyPgAttrDef();
                    PgType = EmptyPgType();
                    PgAM = EmptyPgAM();
                    pgLanguage = EmptyPgLanguage();
                    PgConstraint = EmptyPgConstraint();
                    PgTrigger = EmptyPgTrigger();
                    PgDescription = EmptyPgDescription();
                    PgAuthId = EmptyPgAuthid();
                    PgProc = EmptyPgProc();

                    Initialized = false;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Připojení k databázi PostgreSQL (read-only)
                /// </summary>
                public PostgreSQLWrapper Database
                {
                    get
                    {
                        return database;
                    }
                    private set
                    {
                        database = value;
                    }
                }

                /// <summary>
                /// Číslo verze databázového serveru PostgreSQL
                /// </summary>
                public Nullable<int> ServerVersionNum
                {
                    get
                    {
                        return serverVersionNum;
                    }
                    set
                    {
                        serverVersionNum = value;
                    }
                }

                /// <summary>
                /// Jsou načtena data katalogu?
                /// </summary>
                public bool Initialized
                {
                    get
                    {
                        return initialized;
                    }
                    set
                    {
                        initialized = value;
                    }
                }

                /// <summary>
                /// Jméno databáze (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return database.Database;
                    }
                }

                /// <summary>
                /// Seznam databázových extenzí (read-only)
                /// </summary>
                public DBExtensionList Extensions
                {
                    get
                    {
                        return new DBExtensionList(catalog: this);
                    }
                }

                /// <summary>
                /// Seznam databázových schémat (read-only)
                /// </summary>
                public DBSchemaList Schemas
                {
                    get
                    {
                        return new DBSchemaList(catalog: this);
                    }
                }

                /// <summary>
                /// Seznam rolí (read-only)
                /// </summary>
                public DBRoleList Roles
                {
                    get
                    {
                        return new DBRoleList(catalog: this);
                    }
                }

                /// <summary>
                /// Katalog databázových extenzí (read-only)
                /// </summary>
                public DataTable PgExtension
                {
                    get
                    {
                        return
                            pgExtension ??
                            EmptyPgExtension();
                    }
                    private set
                    {
                        pgExtension = value;
                    }
                }

                /// <summary>
                /// Katalog schémat (read-only)
                /// </summary>
                public DataTable PgNamespace
                {
                    get
                    {
                        return
                            pgNamespace ??
                            EmptyPgNamespace();
                    }
                    private set
                    {
                        pgNamespace = value;
                    }
                }

                /// <summary>
                /// Katalog tabulek (read-only)
                /// </summary>
                public DataTable PgClass
                {
                    get
                    {
                        return
                            pgClass ??
                            EmptyPgClass();
                    }
                    private set
                    {
                        pgClass = value;
                    }
                }

                /// <summary>
                /// Katalog sekvencí (read-only)
                /// </summary>
                public DataTable PgSequence
                {
                    get
                    {
                        return
                            pgSequence ??
                            EmptyPgSequence();
                    }
                    private set
                    {
                        pgSequence = value;
                    }
                }

                /// <summary>
                /// Katalog indexů (read-only)
                /// </summary>
                public DataTable PgIndex
                {
                    get
                    {
                        return
                            pgIndex ??
                            EmptyPgIndex();
                    }
                    private set
                    {
                        pgIndex = value;
                    }
                }

                /// <summary>
                /// Katalog atributů (read-only)
                /// </summary>
                public DataTable PgAttribute
                {
                    get
                    {
                        return
                            pgAttribute
                            ?? EmptyPgAttribute();
                    }
                    private set
                    {
                        pgAttribute = value;
                    }
                }

                /// <summary>
                /// Katalog defaultních hodnot atributů (read-only)
                /// </summary>
                public DataTable PgAttrDef
                {
                    get
                    {
                        return
                            pgAttrDef
                            ?? EmptyPgAttrDef();
                    }
                    private set
                    {
                        pgAttrDef = value;
                    }
                }

                /// <summary>
                /// Katalog typů (read-only)
                /// </summary>
                public DataTable PgType
                {
                    get
                    {
                        return
                            pgType ??
                            EmptyPgType();
                    }
                    private set
                    {
                        pgType = value;
                    }
                }

                /// <summary>
                /// Katalog přístupových metod (read-only)
                /// </summary>
                public DataTable PgAM
                {
                    get
                    {
                        return
                            pgAM ??
                            EmptyPgAM();
                    }
                    private set
                    {
                        pgAM = value;
                    }
                }

                /// <summary>
                /// Katalog jazyků (read-only)
                /// </summary>
                public DataTable PgLanguage
                {
                    get
                    {
                        return
                            pgLanguage ??
                            EmptyPgLanguage();
                    }
                    private set
                    {
                        pgLanguage = value;
                    }
                }


                /// <summary>
                /// Katalog omezení (read-only)
                /// </summary>
                public DataTable PgConstraint
                {
                    get
                    {
                        return
                            pgConstraint ??
                            EmptyPgConstraint();
                    }
                    private set
                    {
                        pgConstraint = value;
                    }
                }

                /// <summary>
                /// Katalog triggerů (read-only)
                /// </summary>
                public DataTable PgTrigger
                {
                    get
                    {
                        return
                            pgTrigger ??
                            EmptyPgTrigger();
                    }
                    private set
                    {
                        pgTrigger = value;
                    }
                }

                /// <summary>
                /// Katalog popisků (read-only)
                /// </summary>
                public DataTable PgDescription
                {
                    get
                    {
                        return
                            pgDescription ??
                            EmptyPgDescription();
                    }
                    private set
                    {
                        pgDescription = value;
                    }
                }

                /// <summary>
                /// Katalog rolí (read-only)
                /// </summary>
                public DataTable PgAuthId
                {
                    get
                    {
                        return
                            pgAuthId ??
                            EmptyPgAuthid();
                    }
                    private set
                    {
                        pgAuthId = value;
                    }
                }

                /// <summary>
                /// Katalog uložených procedur (read-only)
                /// </summary>
                public DataTable PgProc
                {
                    get
                    {
                        return
                            pgProc ??
                            EmptyPgProc();
                    }
                    private set
                    {
                        pgProc = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// SQL příkaz pro výběr seznamu databázových extenzí (read-only)
                /// </summary>
                private static string SqlPgExtension
                {
                    get
                    {
                        // * oid                oid
                        // Row identifier

                        // * extname            name
                        // Name of the extension

                        // * extowner           oid (references pg_authid.oid)
                        // Owner of the extension

                        // * extnamespace       oid (references pg_namespace.oid)
                        // Schema containing the extension's exported objects

                        // * extrelocatable     bool
                        // True if extension can be relocated to another schema

                        // * extversion         text
                        // Version name for the extension

                        // extconfig            oid[] (references pg_class.oid)
                        // Array of regclass OIDs for the extension's configuration table(s), or NULL if none

                        // extcondition         text[]
                        // Array of WHERE - clause filter conditions for the extension's configuration table(s), or NULL if none

                        return String.Concat(
                            $"SELECT ",
                            $"               a.oid                               AS oid, ",
                            $"               a.extname                           AS extname, ",
                            $"               a.extowner                          AS extowner, ",
                            $"               a.extnamespace                      AS extnamespace, ",
                            $"               a.extrelocatable                    AS extrelocatable, ",
                            $"               a.extversion                        AS extversion ",
                            $"FROM           pg_extension                        AS a; ");
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu databázových extenzí
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu databázových extenzí</returns>
                public static DataTable EmptyPgExtension()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "extname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "extowner", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "extnamespace", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "extrelocatable", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "extversion", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu databázových schémat (read-only)
                /// </summary>
                private static string SqlPgNamespace
                {
                    get
                    {
                        // * oid                      oid
                        // Row identifier

                        // * nspname                  name
                        // Name of the namespace

                        // * nspowner                 oid (references pg_authid.oid)
                        // Owner of the namespace

                        //  nspacl                  aclitem[]
                        //  Access privileges

                        return String.Concat(
                            $"SELECT ",
                            $"               a.oid                               AS oid, ",
                            $"               a.nspname                           AS nspname, ",
                            $"               a.nspowner                          AS nspowner ",
                            $"FROM           pg_namespace                        AS a ",
                            $"WHERE          a.nspname <> 'information_schema' ",
                            $"AND            a.nspname !~ E'^pg_'; ");
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu databázových schémat
                /// </summary>
                /// <returns> Prázdná datová tabulka seznamu databázových schémat</returns>
                public static DataTable EmptyPgNamespace()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "nspname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "nspowner", DataType = Type.GetType(typeName: "System.UInt32") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu tabulek (read-only)
                /// </summary>
                private static string SqlPgClass
                {
                    get
                    {
                        // * oid                    oid
                        // Row identifier

                        // * relname                name
                        // Name of the table, index, view, etc.

                        // * relnamespace           oid (references pg_namespace.oid)
                        // The OID of the namespace that contains this relation

                        // reltype                  oid (references pg_type.oid)
                        // The OID of the data type that corresponds to this table's row type, if any;
                        // zero for indexes, sequences, and toast tables, which have no pg_type entry

                        // reloftype                oid (references pg_type.oid)
                        // For typed tables, the OID of the underlying composite type; zero for all other relations

                        // * relowner               oid (references pg_authid.oid)
                        // Owner of the relation

                        // * relam                  oid (references pg_am.oid)
                        // If this is a table or an index, the access method used (heap, B-tree, hash, etc.);
                        // otherwise zero (zero occurs for sequences, as well as relations without storage, such as views)

                        // relfilenode              oid
                        // Name of the on-disk file of this relation;
                        // zero means this is a “mapped” relation whose disk file name is determined by low-level state

                        // reltablespace            oid (references pg_tablespace.oid)
                        // The tablespace in which this relation is stored. If zero, the database's default tablespace is implied.
                        // (Not meaningful if the relation has no on-disk file.)

                        // relpages                 int4
                        // Size of the on-disk representation of this table in pages (of size BLCKSZ).
                        // This is only an estimate used by the planner.
                        // It is updated by VACUUM, ANALYZE, and a few DDL commands such as CREATE INDEX.

                        // reltuples                float4
                        // Number of live rows in the table. This is only an estimate used by the planner.
                        // It is updated by VACUUM, ANALYZE, and a few DDL commands such as CREATE INDEX. If the table has never yet been vacuumed or analyzed, reltuples contains -1 indicating that the row count is unknown.

                        // relallvisible            int4
                        // Number of pages that are marked all-visible in the table's visibility map.
                        // This is only an estimate used by the planner.
                        // It is updated by VACUUM, ANALYZE, and a few DDL commands such as CREATE INDEX.

                        // reltoastrelid            oid (references pg_class.oid)
                        // OID of the TOAST table associated with this table, zero if none.
                        // The TOAST table stores large attributes “out of line” in a secondary table.

                        // * relhasindex            bool
                        // True if this is a table and it has (or recently had) any indexes

                        // relisshared              bool
                        // True if this table is shared across all databases in the cluster.
                        // Only certain system catalogs (such as pg_database) are shared.

                        // relpersistence           char
                        // p = permanent table/sequence, u = unlogged table/sequence, t = temporary table/sequence

                        // * relkind                char
                        // r = ordinary table, i = index, S = sequence, t = TOAST table, v = view, m = materialized view,
                        // c = composite type, f = foreign table, p = partitioned table, I = partitioned index

                        // * relnatts               int2
                        // Number of user columns in the relation (system columns not counted).
                        // There must be this many corresponding entries in pg_attribute.
                        // See also pg_attribute.attnum.

                        // * relchecks              int2
                        // Number of CHECK constraints on the table; see pg_constraint catalog

                        // relhasrules              bool
                        // True if table has (or once had) rules; see pg_rewrite catalog

                        // * relhastriggers         bool
                        // True if table has (or once had) triggers; see pg_trigger catalog

                        // relhassubclass           bool
                        // True if table or index has (or once had) any inheritance children or partitions

                        // relrowsecurity           bool
                        // True if table has row-level security enabled; see pg_policy catalog

                        // relforcerowsecurity      bool
                        // True if row-level security (when enabled)
                        // will also apply to table owner; see pg_policy catalog

                        // relispopulated           bool
                        // True if relation is populated
                        // (this is true for all relations other than some materialized views)

                        // relreplident             char
                        // Columns used to form “replica identity” for rows:
                        // d = default (primary key, if any), n = nothing,
                        // f = all columns, i = index with indisreplident set
                        // (same as nothing if the index used has been dropped)

                        // relispartition           bool
                        // True if table or index is a partition

                        // relrewrite               oid (references pg_class.oid)
                        // For new relations being written during a DDL operation that requires a table rewrite,
                        // this contains the OID of the original relation; otherwise zero.
                        // That state is only visible internally;
                        // this field should never contain anything other than zero for a user-visible relation.

                        // relfrozenxid             xid
                        // All transaction IDs before this one have been replaced with a permanent(“frozen”) transaction ID in this table.
                        // This is used to track whether the table needs to be vacuumed in order to prevent transaction ID wraparound
                        // or to allow pg_xact to be shrunk.Zero(InvalidTransactionId) if the relation is not a table.

                        // relminmxid               xid
                        // All multixact IDs before this one have been replaced by a transaction ID in this table.
                        // This is used to track whether the table needs to be vacuumed in order to prevent multixact ID wraparound
                        // or to allow pg_multixact to be shrunk.Zero(InvalidMultiXactId) if the relation is not a table.

                        // relacl                   aclitem[]
                        // Access privileges

                        // reloptions               text[]
                        // Access - method - specific options, as “keyword = value” strings

                        // relpartbound             pg_node_tree
                        // If table is a partition (see relispartition), internal representation of the partition bound

                        return String.Concat(
                            $"SELECT ",
                            $"    a.oid                                                     AS oid, ",
                            $"    a.relname                                                 AS relname, ",
                            $"    a.relnamespace                                            AS relnamespace, ",
                            $"    a.relowner                                                AS relowner, ",
                            $"    a.relam                                                   AS relam, ",
                            $"    a.relhasindex                                             AS relhasindex, ",
                            $"    a.relkind                                                 AS relkind, ",
                            $"    a.relnatts                                                AS relnatts, ",
                            $"    a.relchecks                                               AS relchecks, ",
                            $"    a.relhastriggers                                          AS relhastriggers, ",
                            $"    CASE WHEN (a.relkind = 'v') OR (a.relkind = 'm') ",
                            $"    THEN pg_get_viewdef(a.oid)",
                            $"    ELSE NULL END                                             AS view_definition ",
                            $"FROM           pg_class                                       AS a; "
                        );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu tabulek
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu tabulek</returns>
                public static DataTable EmptyPgClass()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relnamespace", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relowner", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relam", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relhasindex", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relkind", DataType = Type.GetType(typeName: "System.Char") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relnatts", DataType = Type.GetType(typeName: "System.Int16") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relchecks", DataType = Type.GetType(typeName: "System.Int16") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "relhastriggers", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "view_definition", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu sekvencí (read-only)
                /// </summary>
                private string SqlPgSequence
                {
                    get
                    {
                        // * seqrelid                   oid (references pg_class.oid)
                        // The OID of the pg_class entry for this sequence

                        // seqtypid                     oid (references pg_type.oid)
                        // Data type of the sequence

                        // * seqstart                   int8
                        // Start value of the sequence

                        // * seqincrement               int8
                        // Increment value of the sequence

                        // * seqmin                     int8
                        // Minimum value of the sequence

                        // * seqmax                     int8
                        // Maximum value of the sequence

                        // seqcache                     int8
                        // Cache size of the sequence

                        // * seqcycle                   bool
                        // Whether the sequence cycles

                        if (ServerVersionNum == null)
                        {
                            return null;
                        }

                        if (ServerVersionNum < 100000)
                        {
                            return String.Concat(
                                $"SELECT ",
                                $"               a.oid                                                       AS seqrelid, ",
                                $"               s.start_value                                               AS seqstart, ",
                                $"               s.increment                                                 AS seqincrement, ",
                                $"               s.minimum_value                                             AS seqmin, ",
                                $"               s.maximum_value                                             AS seqmax, ",
                                $"               CASE WHEN s.cycle_option = 'NO' THEN false ELSE true END    AS seqcycle ",
                                $"FROM           information_schema.sequences                                AS s ",
                                $"LEFT JOIN ( ",
                                $"SELECT c.oid, nsp.nspname, c.relname FROM pg_catalog.pg_class AS c ",
                                $"LEFT JOIN pg_catalog.pg_namespace AS nsp ON (nsp.oid = c.relnamespace) ",
                                $") AS a ON (a.nspname, a.relname) = (s.sequence_schema, s.sequence_name); ");
                        }

                        return String.Concat(
                        $"SELECT ",
                        $"               a.seqrelid                  AS seqrelid, ",
                        $"               a.seqstart                  AS seqstart, ",
                        $"               a.seqincrement              AS seqincrement, ",
                        $"               a.seqmin                    AS seqmin, ",
                        $"               a.seqmax                    AS seqmax, ",
                        $"               a.seqcycle                  AS seqcycle ",
                        $"FROM           pg_sequence                 AS a; "
                    );

                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu sekvencí
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu sekvencí</returns>
                public static DataTable EmptyPgSequence()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "seqrelid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "seqstart", DataType = Type.GetType(typeName: "System.Int64") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "seqincrement", DataType = Type.GetType(typeName: "System.Int64") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "seqmin", DataType = Type.GetType(typeName: "System.Int64") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "seqmax", DataType = Type.GetType(typeName: "System.Int64") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "seqcycle", DataType = Type.GetType(typeName: "System.Boolean") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr indexů (read-only)
                /// </summary>
                private static string SqlPgIndex
                {
                    get
                    {
                        // * indexrelid                 oid (references pg_class.oid)
                        // The OID of the pg_class entry for this index

                        // * indrelid                   oid (references pg_class.oid)
                        // The OID of the pg_class entry for the table this index is for

                        // indnatts                     int2
                        // The total number of columns in the index(duplicates pg_class.relnatts);
                        // this number includes both key and included attributes

                        // indnkeyatts                  int2
                        // The number of key columns in the index, not counting any included columns,
                        // which are merely stored and do not participate in the index semantics
                        // Version 11+

                        // * indisunique                bool
                        // If true, this is a unique index

                        // indnullsnotdistinct          bool
                        // This value is only used for unique indexes.
                        // If false, this unique index will consider null values distinct (so the index can contain multiple null values in a column,
                        // the default PostgreSQL behavior).
                        // If it is true, it will consider null values to be equal (so the index can only contain one null value in a column).

                        // * indisprimary               bool
                        // If true, this index represents the primary key of the table (indisunique should always be true when this is true)

                        // indisexclusion               bool
                        // If true, this index supports an exclusion constraint

                        // * indimmediate               bool
                        // If true, the uniqueness check is enforced immediately on insertion (irrelevant if indisunique is not true)

                        // indisclustered               bool
                        // If true, the table was last clustered on this index

                        // * indisvalid                 bool
                        // If true, the index is currently valid for queries.
                        // False means the index is possibly incomplete: it must still be modified by INSERT / UPDATE operations,
                        // but it cannot safely be used for queries.If it is unique, the uniqueness property is not guaranteed true either.

                        // indcheckxmin                 bool
                        // If true, queries must not use the index until the xmin of this pg_index row is below their TransactionXmin event horizon,
                        // because the table may contain broken HOT chains with incompatible rows that they can see

                        // * indisready                   bool
                        // If true, the index is currently ready for inserts.
                        // False means the index must be ignored by INSERT/UPDATE operations.

                        // * indislive                    bool
                        // If false, the index is in process of being dropped,
                        // and should be ignored for all purposes (including HOT-safety decisions)

                        // indisreplident               bool
                        // If true this index has been chosen as “replica identity”
                        // using ALTER TABLE...REPLICA IDENTITY USING INDEX...

                        // * indkey                       int2vector (references pg_attribute.attnum)
                        // This is an array of indnatts values that indicate which table columns this index indexes.
                        // For example, a value of 1 3 would mean that the first and the third table columns make up the index entries.
                        // Key columns come before non-key (included) columns.
                        // A zero in this array indicates that the corresponding index attribute is an expression over the table columns,
                        // rather than a simple column reference.

                        // indcollation                 oidvector (references pg_collation.oid)
                        // For each column in the index key (indnkeyatts values),
                        // this contains the OID of the collation to use for the index, or zero if the column is not of a collatable data type.

                        // indclass                     oidvector (references pg_opclass.oid)
                        // For each column in the index key (indnkeyatts values),
                        // this contains the OID of the operator class to use.See pg_opclass for details.

                        // indoption                    int2vector
                        // This is an array of indnkeyatts values that store per-column flag bits.
                        // The meaning of the bits is defined by the index's access method.

                        // indexprs                     pg_node_tree
                        // Expression trees(in nodeToString() representation) for index attributes that are not simple column references.
                        // This is a list with one element for each zero entry in indkey.Null if all index attributes are simple references.

                        // indpred                      pg_node_tree
                        // Expression tree (in nodeToString() representation) for partial index predicate.
                        // Null if not a partial index.

                        return String.Concat(
                            $"SELECT ",
                            $"              a.indexrelid                                AS indexrelid, ",
                            $"              b.relnamespace                              AS indexrelnamespaceoid, ",
                            $"              b.relname                                   AS indexrelname, ",
                            $"              b.relam                                     AS indexrelam, ",

                            $"              a.indrelid                                  AS indrelid, ",
                            $"              c.relnamespace                              AS indrelnamespaceoid, ",
                            $"              d.nspname                                   AS indrelnamespacename, ",
                            $"              c.relname                                   AS indrelname, ",

                            $"              a.indisunique                               AS indisunique, ",
                            $"              a.indisprimary                              AS indisprimary, ",
                            $"              a.indimmediate                              AS indimmediate, ",
                            $"              a.indisvalid                                AS indisvalid, ",
                            $"              a.indisready                                AS indisready, ",
                            $"              a.indislive                                 AS indislive, ",
                            $"              array_to_string(a.indkey, ';', 'NULL')      AS indkey ",
                            $"FROM          pg_catalog.pg_index                         AS a ",
                            $"LEFT JOIN     pg_catalog.pg_class                         AS b ON (a.indexrelid = b.oid) ",
                            $"LEFT JOIN     pg_catalog.pg_class                         AS c ON (a.indrelid = c.oid) ",
                            $"LEFT JOIN     pg_catalog.pg_namespace                     AS d ON (c.relnamespace = d.oid); "
                         );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu indexů
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu indexů</returns>
                public static DataTable EmptyPgIndex()
                {
                    DataTable result = new();

                    result.Columns.Add(column: new DataColumn() { ColumnName = "indexrelid", DataType = Type.GetType("System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indexrelnamespaceoid", DataType = Type.GetType("System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indexrelname", DataType = Type.GetType("System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indexrelam", DataType = Type.GetType("System.UInt32") });

                    result.Columns.Add(column: new DataColumn() { ColumnName = "indrelid", DataType = Type.GetType("System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indrelnamespaceoid", DataType = Type.GetType("System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indrelnamespacename", DataType = Type.GetType("System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indrelname", DataType = Type.GetType("System.String") });

                    result.Columns.Add(column: new DataColumn() { ColumnName = "indisunique", DataType = Type.GetType("System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indisprimary", DataType = Type.GetType("System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indimmediate", DataType = Type.GetType("System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indisvalid", DataType = Type.GetType("System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indisready", DataType = Type.GetType("System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indislive", DataType = Type.GetType("System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "indkey", DataType = Type.GetType("System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu atributů tabulky (read-only)
                /// </summary>
                private static string SqlPgAttribute
                {
                    get
                    {
                        // * attrelid                   oid (references pg_class.oid)
                        // The table this column belongs to

                        // * attname                    name
                        // The column name

                        // * atttypid                   oid (references pg_type.oid)
                        // The data type of this column(zero for a dropped column)

                        // * attlen                     int2
                        // A copy of pg_type.typlen of this column's type

                        // * attnum                     int2
                        // The number of the column.Ordinary columns are numbered from 1 up.System columns, such as ctid, have(arbitrary) negative numbers.

                        // attcacheoff                  int4
                        // Always - 1 in storage, but when loaded into a row descriptor in memory this might be updated to cache the offset of the attribute within the row

                        // atttypmod                    int4
                        // atttypmod records type-specific data supplied at table creation time(for example, the maximum length of a varchar column).
                        // It is passed to type - specific input functions and length coercion functions. The value will generally be - 1 for types that do not need atttypmod.

                        // * attndims                   int2
                        // Number of dimensions, if the column is an array type; otherwise 0.
                        // (Presently, the number of dimensions of an array is not enforced, so any nonzero value effectively means “it's an array”.)

                        // attbyval                     bool
                        // A copy of pg_type.typbyval of this column's type

                        // attalign                     char
                        // A copy of pg_type.typalign of this column's type

                        // attstorage                   char
                        // Normally a copy of pg_type.typstorage of this column's type. For TOAST-able data types,
                        // this can be altered after column creation to control storage policy.

                        // attcompression               char
                        // The current compression method of the column.Typically this is '\0' to specify use of the current default setting(see default_toast_compression).
                        // Otherwise, 'p' selects pglz compression, while 'l' selects LZ4 compression.However, this field is ignored whenever attstorage does not allow compression.

                        // * attnotnull                 bool
                        // This represents a not - null constraint.

                        // * atthasdef                  bool
                        // This column has a default expression or generation expression, in which case there will be a corresponding entry
                        // in the pg_attrdef catalog that actually defines the expression.
                        // (Check attgenerated to determine whether this is a default or a generation expression.)

                        // atthasmissing                bool
                        // This column has a value which is used where the column is entirely missing from the row,
                        // as happens when a column is added with a non-volatile DEFAULT value after the row is created.
                        // The actual value used is stored in the attmissingval column.

                        // attidentity                  char
                        // If a zero byte(''), then not an identity column. Otherwise, a = generated always, d = generated by default.

                        // attgenerated                 char
                        // If a zero byte(''), then not a generated column.Otherwise, s = stored. (Other values might be added in the future.)

                        // * attisdropped               bool
                        // This column has been dropped and is no longer valid.
                        // A dropped column is still physically present in the table,
                        // but is ignored by the parser and so cannot be accessed via SQL.

                        // attislocal                   bool
                        // This column is defined locally in the relation. Note that a column can be locally defined and inherited simultaneously.

                        // attinhcount                  int2
                        // The number of direct ancestors this column has. A column with a nonzero number of ancestors cannot be dropped nor renamed.

                        // attstattarget                int2
                        // attstattarget controls the level of detail of statistics accumulated for this column by ANALYZE.
                        // A zero value indicates that no statistics should be collected.A negative value says to use the system default statistics target.
                        // The exact meaning of positive values is data type - dependent.For scalar data types,
                        // attstattarget is both the target number of “most common values” to collect, and the target number of histogram bins to create.

                        // attcollation                 oid (references pg_collation.oid)
                        // The defined collation of the column, or zero if the column is not of a collatable data type

                        // attacl                       aclitem[]
                        // Column - level access privileges, if any have been granted specifically on this column

                        // attoptions                   text[]
                        // Attribute - level options, as “keyword = value” strings

                        // attfdwoptions                text[]
                        // Attribute - level foreign data wrapper options, as “keyword = value” strings

                        // attmissingval                anyarray
                        // This column has a one element array containing the value used when the column is entirely missing from the row,
                        // as happens when the column is added with a non-volatile DEFAULT value after the row is created.
                        // The value is only used when atthasmissing is true.If there is no value the column is null.

                        return String.Concat(
                        $"SELECT ",
                        $"           a.attrelid                                AS attrelid, ",
                        $"           a.attname                                 AS attname, ",
                        $"           a.atttypid                                AS atttypid, ",
                        $"           a.attlen                                  AS attlen, ",
                        $"           a.attnum                                  AS attnum, ",
                        $"           a.attndims                                AS attndims, ",
                        $"           a.attnotnull                              AS attnotnull, ",
                        $"           a.atthasdef                               AS atthasdef, ",
                        $"           a.attisdropped                            AS attisdropped, ",
                        $"           format_type(a.atttypid, a.atttypmod)      AS atttypinfo, ",
                        $"           pg_get_expr(d.adbin, d.adrelid)           AS attdef ",
                        $"FROM       pg_attribute                              AS a ",
                        $"LEFT JOIN  pg_attrdef                                AS d ",
                        $"ON (a.attrelid, a.attnum) = (d.adrelid, d.adnum) ",
                        $"WHERE     (a.attnum > 0) ",       //systémové sloupce tabulky mají hodnotu menší než 0
                        $"AND NOT   (a.attisdropped); "     //sloupec nebyl smazán
                        );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu atributů
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu atributů</returns>
                public static DataTable EmptyPgAttribute()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "attrelid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "attname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "atttypid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "attlen", DataType = Type.GetType(typeName: "System.Int16") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "attnum", DataType = Type.GetType(typeName: "System.Int16") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "attndims", DataType = Type.GetType(typeName: "System.Int16") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "attnotnull", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "atthasdef", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "attisdropped", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "atttypinfo", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "attdef", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu defaultních hodnot atributů tabulky (read-only)
                /// </summary>
                private static string SqlPgAttrDef
                {
                    get
                    {
                        // * oid                        oid
                        // Row identifier

                        // * adrelid                    oid (references pg_class.oid)
                        // The table this column belongs to

                        // * adnum                      int2 (references pg_attribute.attnum)
                        // The number of the column

                        // * adbin                      pg_node_tree
                        // The column default value, in nodeToString() representation.
                        // Use pg_get_expr(adbin, adrelid) to convert it to an SQL expression.

                        return String.Concat(
                        $"SELECT ",
                        $"           a.oid                                     AS oid, ",
                        $"           a.adrelid                                 AS adrelid, ",
                        $"           a.adnum                                   AS adnum, ",
                        $"           a.adbin::text                             AS adbin, ",
                        $"           pg_get_expr(a.adbin, a.adrelid)           AS adbinexpr ",
                        $"FROM       pg_attrdef                                AS a;"
                        );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu defaultních hodnot atributů tabulky
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu defaultních hodnot atributů tabulky</returns>
                public static DataTable EmptyPgAttrDef()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "adrelid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "adnum", DataType = Type.GetType(typeName: "System.Int16") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "adbin", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "adbinexpr", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu datových typů (read-only)
                /// </summary>
                private static string SqlPgType
                {
                    get
                    {
                        // * oid                        oid
                        // Row identifier

                        // * typname                    name
                        // Data type name

                        // * typnamespace               oid (references pg_namespace.oid)
                        // The OID of the namespace that contains this type

                        // * typowner                   oid (references pg_authid.oid)
                        // Owner of the type

                        // typlen                       int2
                        // For a fixed-size type, typlen is the number of bytes in the internal representation of the type.
                        // But for a variable-length type, typlen is negative.
                        // -1 indicates a “varlena” type (one that has a length word),
                        // -2 indicates a null-terminated C string.

                        // typbyval                     bool
                        // typbyval determines whether internal routines pass a value of this type by value or by reference.
                        // typbyval had better be false if typlen is not 1, 2, or 4 (or 8 on machines where Datum is 8 bytes).
                        // Variable-length types are always passed by reference.
                        // Note that typbyval can be false even if the length would allow pass-by-value.

                        // typtype                      char
                        // typtype is b for a base type, c for a composite type (e.g., a table's row type), d for a domain,
                        // e for an enum type, p for a pseudo-type, r for a range type, or m for a multirange type.
                        // See also typrelid and typbasetype.

                        // typcategory                  char
                        // typcategory is an arbitrary classification of data types that is used by the parser to determine which implicit casts should be “preferred”.

                        // typispreferred               bool
                        // True if the type is a preferred cast target within its typcategory

                        // typisdefined                 bool
                        // True if the type is defined, false if this is a placeholder entry for a not-yet-defined type.
                        // When typisdefined is false, nothing except the type name, namespace, and OID can be relied on.

                        // typdelim                     char
                        // Character that separates two values of this type when parsing array input.
                        // Note that the delimiter is associated with the array element data type, not the array data type.

                        // typrelid                     oid (references pg_class.oid)
                        // If this is a composite type (see typtype), then this column points to the pg_class entry that defines the corresponding table.
                        // (For a free-standing composite type, the pg_class entry doesn't really represent a table,
                        // but it is needed anyway for the type's pg_attribute entries to link to.) Zero for non-composite types.

                        // typsubscript                 regproc (references pg_proc.oid)
                        // Subscripting handler function's OID, or zero if this type doesn't support subscripting.
                        // Types that are “true” array types have typsubscript = array_subscript_handler,
                        // but other types may have other handler functions to implement specialized subscripting behavior.

                        // typelem                      oid (references pg_type.oid)
                        // If typelem is not zero then it identifies another row in pg_type, defining the type yielded by subscripting.
                        // This should be zero if typsubscript is zero.
                        // However, it can be zero when typsubscript isn't zero, if the handler doesn't need typelem to determine the subscripting result type.
                        // Note that a typelem dependency is considered to imply physical containment of the element type in this type;
                        // so DDL changes on the element type might be restricted by the presence of this type.

                        // typarray                     oid (references pg_type.oid)
                        // If typarray is not zero then it identifies another row in pg_type, which is the “true” array type having this type as element

                        // typinput                     regproc (references pg_proc.oid)
                        // Input conversion function (text format)

                        // typoutput                    regproc (references pg_proc.oid)
                        // Output conversion function (text format)

                        // typreceive                   regproc (references pg_proc.oid)
                        // Input conversion function (binary format), or zero if none

                        // typsend                      regproc (references pg_proc.oid)
                        // Output conversion function (binary format), or zero if none

                        // typmodin                     regproc (references pg_proc.oid)
                        // Type modifier input function, or zero if type does not support modifiers

                        // typmodout                    regproc (references pg_proc.oid)
                        // Type modifier output function, or zero to use the standard format

                        // typanalyze                   regproc (references pg_proc.oid)
                        // Custom ANALYZE function, or zero to use the standard function

                        // typalign                     char
                        // typalign is the alignment required when storing a value of this type.
                        // It applies to storage on disk as well as most representations of the value inside PostgreSQL.
                        // When multiple values are stored consecutively, such as in the representation of a complete row on disk,
                        // padding is inserted before a datum of this type so that it begins on the specified boundary.
                        // The alignment reference is the beginning of the first datum in the sequence. Possible values are:
                        //    c = char alignment, i.e., no alignment needed.
                        //    s = short alignment (2 bytes on most machines).
                        //    i = int alignment (4 bytes on most machines).
                        //    d = double alignment (8 bytes on many machines, but by no means all).

                        // typstorage                   char
                        // typstorage tells for varlena types (those with typlen = -1) if the type is prepared
                        // for toasting and what the default strategy for attributes of this type should be.
                        // Possible values are:
                        //    p (plain): Values must always be stored plain (non-varlena types always use this value).
                        //    e (external): Values can be stored in a secondary “TOAST” relation (if relation has one, see pg_class.reltoastrelid).
                        //    m (main): Values can be compressed and stored inline.
                        //    x (extended): Values can be compressed and/or moved to a secondary relation.
                        // x is the usual choice for toast-able types. Note that m values can also be moved out to secondary storage,
                        // but only as a last resort (e and x values are moved first).

                        // typnotnull                   bool
                        // typnotnull represents a not-null constraint on a type. Used for domains only.

                        // typbasetype                  oid (references pg_type.oid)
                        // If this is a domain (see typtype), then typbasetype identifies the type that this one is based on.
                        // Zero if this type is not a domain.

                        // typtypmod                    int4
                        // Domains use typtypmod to record the typmod to be applied to their base type (-1 if base type does not use a typmod).
                        // -1 if this type is not a domain.

                        // typndims                     int4
                        // typndims is the number of array dimensions for a domain over an array (that is, typbasetype is an array type).
                        // Zero for types other than domains over array types.

                        // typcollation                 oid (references pg_collation.oid)
                        // typcollation specifies the collation of the type. If the type does not support collations, this will be zero.
                        // A base type that supports collations will have a nonzero value here, typically DEFAULT_COLLATION_OID.
                        // A domain over a collatable type can have a collation OID different from its base type's, if one was specified for the domain.

                        // typdefaultbin                pg_node_tree
                        // If typdefaultbin is not null, it is the nodeToString() representation of a default expression for the type. This is only used for domains.

                        // typdefault                   text
                        // typdefault is null if the type has no associated default value.
                        // If typdefaultbin is not null, typdefault must contain a human-readable version of the default expression represented by typdefaultbin.
                        // If typdefaultbin is null and typdefault is not, then typdefault is the external representation of the type's default value,
                        // which can be fed to the type's input converter to produce a constant.

                        // typacl                       aclitem []
                        // Access privileges;

                        return String.Concat(
                            $"SELECT ",
                            $"              a.oid                                       AS oid, ",
                            $"              a.typname                                   AS typname, ",
                            $"              a.typnamespace                              AS typnamespace, ",
                            $"              a.typowner                                  AS typowner ",
                            $"FROM          pg_type                                     AS a; "
                         );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu typů
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu typů</returns>
                public static DataTable EmptyPgType()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "typname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "typnamespace", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "typowner", DataType = Type.GetType(typeName: "System.UInt32") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr přístupových metod (read-only)
                /// </summary>
                private static string SqlPgAM
                {
                    get
                    {
                        // oid                          oid
                        // Row identifier

                        // amname                       name
                        // Name of the access method

                        // amhandler                    regproc (references pg_proc.oid)
                        // OID of a handler function that is responsible for supplying information about the access method

                        // amtype                       char
                        // t = table (including materialized views), i = index.

                        return String.Concat(
                            $"SELECT ",
                            $"              a.oid                                   AS oid, ",
                            $"              a.amname                                AS amname, ",
                            $"              a.amtype                                AS amtype ",
                            $"FROM          pg_am                                   AS a; ");
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu přístupových metod
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu přístupových metod</returns>
                public static DataTable EmptyPgAM()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "amname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "amtype", DataType = Type.GetType(typeName: "System.Char") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr jazyků (read-only)
                /// </summary>
                private static string SqlPgLanguage
                {
                    get
                    {
                        // * oid                            oid
                        // Row identifier

                        // * lanname                        name
                        // Name of the language

                        // lanowner                         oid (references pg_authid.oid)
                        // Owner of the language

                        // lanispl                          bool
                        // This is false for internal languages (such as SQL) and true for user - defined languages.Currently,
                        // pg_dump still uses this to determine which languages need to be dumped,
                        // but this might be replaced by a different mechanism in the future.

                        // lanpltrusted                     bool
                        // True if this is a trusted language,
                        // which means that it is believed not to grant access to anything outside the normal SQL execution environment.
                        // Only superusers can create functions in untrusted languages.

                        // lanplcallfoid                    oid (references pg_proc.oid)
                        // For noninternal languages this references the language handler,
                        // which is a special function that is responsible for executing all functions that are written in the particular language.
                        // Zero for internal languages.

                        // laninline                        oid (references pg_proc.oid)
                        // This references a function that is responsible for executing “inline” anonymous code blocks(DO blocks).
                        // Zero if inline blocks are not supported.

                        // lanvalidator                     oid (references pg_proc.oid)
                        // This references a language validator function that is responsible for checking the syntax and validity of new functions
                        // when they are created. Zero if no validator is provided.

                        // lanacl                           aclitem[]
                        // Access privileges;

                        return String.Concat(
                            $"SELECT ",
                            $"              a.oid                                   AS oid, ",
                            $"              a.lanname                               AS lanname ",
                            $"FROM          pg_language                             AS a; ");
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu jazyků
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu jazyků</returns>
                public static DataTable EmptyPgLanguage()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "lanname", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu omezení
                /// </summary>
                private string SqlPgConstraint
                {
                    get
                    {
                        // * oid                        oid
                        // Row identifier

                        // * conname                    name
                        // Constraint name(not necessarily unique!)

                        // * connamespace               oid (references pg_namespace.oid)
                        // The OID of the namespace that contains this constraint

                        // * contype                    char
                        // c = check constraint, f = foreign key constraint, p = primary key constraint,
                        // u = unique constraint, t = constraint trigger, x = exclusion constraint

                        // * condeferrable              bool
                        // Is the constraint deferrable?

                        // * condeferred                bool
                        // Is the constraint deferred by default?

                        // * convalidated               bool
                        // Has the constraint been validated? Currently, can be false only for foreign keys and CHECK constraints

                        // * conrelid                   oid (references pg_class.oid)
                        // The table this constraint is on; zero if not a table constraint

                        // contypid                     oid (references pg_type.oid)
                        // The domain this constraint is on; zero if not a domain constraint

                        // * conindid                   oid (references pg_class.oid)
                        // The index supporting this constraint, if it's a unique, primary key, foreign key, or exclusion constraint; else zero

                        // conparentid                  oid (references pg_constraint.oid)
                        // The corresponding constraint of the parent partitioned table, if this is a constraint on a partition; else zero

                        // * confrelid                  oid (references pg_class.oid)
                        // If a foreign key, the referenced table; else zero

                        // * confupdtype                char
                        // Foreign key update action code:
                        // a = no action, r = restrict, c = cascade, n = set null, d = set default

                        // * confdeltype                char
                        // Foreign key deletion action code:
                        // a = no action, r = restrict, c = cascade, n = set null, d = set default

                        // * confmatchtype              char
                        // Foreign key match type:
                        // f = full, p = partial, s = simple

                        // conislocal                   bool
                        // This constraint is defined locally for the relation.
                        // Note that a constraint can be locally defined and inherited simultaneously.

                        // coninhcount                  int2
                        // The number of direct inheritance ancestors this constraint has.
                        // A constraint with a nonzero number of ancestors cannot be dropped nor renamed.,

                        // connoinherit                 bool
                        // This constraint is defined locally for the relation. It is a non - inheritable constraint.

                        // * conkey                     int2[] (references pg_attribute.attnum)
                        // If a table constraint(including foreign keys, but not constraint triggers), list of the constrained columns

                        // * confkey                    int2[] (references pg_attribute.attnum)
                        // If a foreign key, list of the referenced columns

                        // conpfeqop                    oid[] (references pg_operator.oid)
                        // If a foreign key, list of the equality operators for PK = FK comparisons

                        // conppeqop                    oid[] (references pg_operator.oid)
                        // If a foreign key, list of the equality operators for PK = PK comparisons

                        // conffeqop                    oid[] (references pg_operator.oid)
                        // If a foreign key, list of the equality operators for FK = FK comparisons

                        // confdelsetcols               int2[] (references pg_attribute.attnum)
                        // If a foreign key with a SET NULL or SET DEFAULT delete action, the columns that will be updated.
                        // If null, all of the referencing columns will be updated.

                        // conexclop                    oid[] (references pg_operator.oid)
                        // If an exclusion constraint, list of the per - column exclusion operators

                        // conbin                       pg_node_tree
                        // If a check constraint, an internal representation of the expression.
                        // (It's recommended to use pg_get_constraintdef() to extract the definition of a check constraint.)

                        // * consrc 	                    text
                        // If a check constraint, a human-readable representation of the expression
                        // (attribute is obsolete since version 12 )

                        if (ServerVersionNum == null)
                        {
                            return null;
                        }

                        if (ServerVersionNum < 120000)
                        {
                            return String.Concat(
                                $"SELECT ",
                                $"              a.oid                                       AS oid, ",
                                $"              a.conname                                   AS conname, ",
                                $"              a.connamespace                              AS connamespace, ",
                                $"              a.contype                                   AS contype, ",
                                $"              a.condeferrable                             AS condeferrable, ",
                                $"              a.condeferred                               AS condeferred, ",
                                $"              a.convalidated                              AS convalidated, ",
                                $"              a.conrelid                                  AS conrelid, ",
                                $"              a.conindid                                  AS conindid, ",
                                $"              a.confrelid                                 AS confrelid, ",
                                $"              a.confupdtype                               AS confupdtype, ",
                                $"              a.confdeltype                               AS confdeltype, ",
                                $"              a.confmatchtype                             AS confmatchtype, ",
                                $"              array_to_string(a.conkey, ';', 'NULL')      AS conkey, ",
                                $"              array_to_string(a.confkey, ';', 'NULL')     AS confkey, ",
                                $"              a.consrc                                    AS consrc, ",
                                $"              pg_get_constraintdef(a.oid)                 AS condef ",
                                $"FROM          pg_constraint                               AS a; "
                                );
                        }

                        return String.Concat(
                            $"SELECT ",
                            $"              a.oid                                       AS oid, ",
                            $"              a.conname                                   AS conname, ",
                            $"              a.connamespace                              AS connamespace, ",
                            $"              a.contype                                   AS contype, ",
                            $"              a.condeferrable                             AS condeferrable, ",
                            $"              a.condeferred                               AS condeferred, ",
                            $"              a.convalidated                              AS convalidated, ",
                            $"              a.conrelid                                  AS conrelid, ",
                            $"              a.conindid                                  AS conindid, ",
                            $"              a.confrelid                                 AS confrelid, ",
                            $"              a.confupdtype                               AS confupdtype, ",
                            $"              a.confdeltype                               AS confdeltype, ",
                            $"              a.confmatchtype                             AS confmatchtype, ",
                            $"              array_to_string(a.conkey, ';', 'NULL')      AS conkey, ",
                            $"              array_to_string(a.confkey, ';', 'NULL')     AS confkey, ",
                            $"              pg_get_expr(a.conbin, a.conrelid)           AS consrc, ",
                            $"              pg_get_constraintdef(a.oid)                 AS condef ",
                            $"FROM          pg_constraint                               AS a; "
                            );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu omezení
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu omezení</returns>
                public static DataTable EmptyPgConstraint()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "conname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "connamespace", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "contype", DataType = Type.GetType(typeName: "System.Char") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "condeferrable", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "condeferred", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "convalidated", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "conrelid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "conindid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "confrelid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "confupdtype", DataType = Type.GetType(typeName: "System.Char") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "confdeltype", DataType = Type.GetType(typeName: "System.Char") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "confmatchtype", DataType = Type.GetType(typeName: "System.Char") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "conkey", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "confkey", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "consrc", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "condef", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu triggerů
                /// </summary>
                private static string SqlPgTrigger
                {
                    get
                    {
                        // * oid                        oid
                        // Row identifier

                        // * tgrelid                    oid (references pg_class.oid)
                        // The table this trigger is on

                        // tgparentid                   oid (references pg_trigger.oid)
                        // Parent trigger that this trigger is cloned from
                        // (this happens when partitions are created or attached to a partitioned table);
                        // zero if not a clone

                        // * tgname                     name
                        // Trigger name (must be unique among triggers of same table)

                        // * tgfoid                     oid (references pg_proc.oid)
                        // The function to be called

                        // * tgtype                     int2
                        // Bit mask identifying trigger firing conditions

                        // tgenabled                    char
                        // Controls in which session_replication_role modes the trigger fires.
                        // O = trigger fires in “origin” and “local” modes,
                        // D = trigger is disabled,
                        // R = trigger fires in “replica” mode,
                        // A = trigger fires always.

                        // * tgisinternal               bool
                        // True if trigger is internally generated
                        // (usually, to enforce the constraint identified by tgconstraint)

                        // tgconstrrelid                oid (references pg_class.oid)
                        // The table referenced by a referential integrity constraint
                        // (zero if trigger is not for a referential integrity constraint)

                        // tgconstrindid                oid (references pg_class.oid)
                        // The index supporting a unique, primary key, referential integrity,
                        // or exclusion constraint
                        // (zero if trigger is not for one of these types of constraint)

                        // * tgconstraint               oid (references pg_constraint.oid)
                        // The pg_constraint entry associated with the trigger
                        // (zero if trigger is not for a constraint)

                        // * tgdeferrable               bool
                        // True if constraint trigger is deferrable

                        // * tginitdeferred             bool
                        // True if constraint trigger is initially deferred

                        // tgnargs                      int2
                        // Number of argument strings passed to trigger function

                        // * tgattr                     int2vector (references pg_attribute.attnum)
                        // Column numbers, if trigger is column - specific; otherwise an empty array

                        // tgargs                       bytea
                        // Argument strings to pass to trigger, each NULL - terminated

                        // tgqual                       pg_node_tree
                        // Expression tree(in nodeToString() representation)
                        // for the trigger's WHEN condition, or null if none

                        // tgoldtable                   name
                        // REFERENCING clause name for OLD TABLE, or null if none

                        // tgnewtable                   name
                        // REFERENCING clause name for NEW TABLE, or null if none

                        return String.Concat(
                            $"SELECT ",
                            $"    a.oid                                                 AS oid, ",
                            $"    a.tgname                                              AS tgname, ",
                            $"    a.tgrelid                                             AS tgrelid, ",
                            $"    a.tgfoid                                              AS tgfoid, ",
                            $"    a.tgconstraint                                        AS tgconstraint, ",
                            $"    a.tgisinternal                                        AS tgisinternal, ",
                            $"    a.tgdeferrable                                        AS tgdeferrable, ",
                            $"    a.tginitdeferred                                      AS tginitdeferred, ",
                            $"    array_to_string(a.tgattr, ';', 'NULL')                AS tgattr, ",
                            $"    a.tgtype                                              AS tgtype, ",
                            $"    (a.tgtype::int::bit(7) & b'0000001')::int = 0         AS tgisstatement, ",
                            $"    (a.tgtype::int::bit(7) & b'0000010')::int = 2         AS tgisbefore, ",
                            $"    (a.tgtype::int::bit(7) & b'0000100')::int = 4         AS tgisinsert, ",
                            $"    (a.tgtype::int::bit(7) & b'0001000')::int = 8         AS tgisdelete, ",
                            $"    (a.tgtype::int::bit(7) & b'0010000')::int = 16        AS tgisupdate, ",
                            $"    (a.tgtype::int::bit(7) & b'0100000')::int = 32        AS tgistruncate, ",
                            $"    (a.tgtype::int::bit(7) & b'1000000')::int = 64        AS tgisinstead, ",
                            $"    pg_get_triggerdef(a.oid)                              AS tgdef ",
                            $"FROM          pg_trigger                                  AS a; "
                            );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu triggerů
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu triggerů</returns>
                public static DataTable EmptyPgTrigger()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgrelid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgfoid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgconstraint", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgisinternal", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgdeferrable", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tginitdeferred", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgattr", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgtype", DataType = Type.GetType(typeName: "System.Int16") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgisstatement", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgisbefore", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgisinsert", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgisdelete", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgisupdate", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgistruncate", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgisinstead", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "tgdef", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu popisků objektů
                /// </summary>
                private static string SqlPgDescription
                {
                    get
                    {
                        // * objoid                     oid (references any OID column)
                        // The OID of the object this description pertains to

                        // * classoid                   oid (references pg_class.oid)
                        // The OID of the system catalog this object appears in

                        // * objsubid                   int4
                        // For a comment on a table column, this is the column number
                        // (the objoid and classoid refer to the table itself).
                        // For all other object types, this column is zero.

                        // * description                text
                        // Arbitrary text that serves as the description of this object

                        return String.Concat(
                            $"SELECT ",
                            $"              a.objoid                                    AS objoid, ",
                            $"              a.classoid                                  AS classoid, ",
                            $"              a.objsubid                                  AS objsubid, ",
                            $"              a.description                               AS description ",
                            $"FROM          pg_description                              AS a; "
                         );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu popisků objektů
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu popisků objektů</returns>
                public static DataTable EmptyPgDescription()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "objoid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "classoid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "objsubid", DataType = Type.GetType(typeName: "System.Int32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "description", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu rolí
                /// </summary>
                private static string SqlPgAuthid
                {
                    get
                    {
                        // * oid                            oid
                        // Row identifier

                        // * rolname                        name
                        // Role name

                        // * rolsuper                       bool
                        // Role has superuser privileges

                        // * rolinherit                     bool
                        // Role automatically inherits privileges of roles it is a member of

                        // * rolcreaterole                  bool
                        // Role can create more roles

                        // * rolcreatedb                    bool
                        // Role can create databases

                        // * rolcanlogin                    bool
                        // Role can log in. That is, this role can be given as the initial session authorization identifier.

                        // * rolreplication                 bool
                        // Role is a replication role.A replication role can initiate replication connections and create and drop replication slots.

                        // * rolbypassrls                     bool
                        // Role bypasses every row-level security policy

                        // * rolconnlimit                   int4
                        // For roles that can log in, this sets maximum number of concurrent connections this role can make. -1 means no limit.

                        // rolpassword                      text
                        // Password (possibly encrypted); null if none.The format depends on the form of encryption used.

                        // rolvaliduntil                    timestamptz
                        // Password expiry time(only used for password authentication); null if no expiration


                        // Do katalogu pg_authid nemám oprávnění přístupu:
                        //return String.Concat(
                        //    $"SELECT ",
                        //    $"              a.oid                                       AS oid, ",
                        //    $"              a.rolname                                   AS rolname, ",
                        //    $"              a.rolsuper                                  AS rolsuper, ",
                        //    $"              a.rolinherit                                AS rolinherit, ",
                        //    $"              a.rolcreaterole                             AS rolcreaterole, ",
                        //    $"              a.rolcreatedb                               AS rolcreatedb, ",
                        //    $"              a.rolcanlogin                               AS rolcanlogin, ",
                        //    $"              a.rolreplication                            AS rolreplication, ",
                        //    $"              a.rolbypassrls                              AS rolbypassrls, ",
                        //    $"              a.rolconnlimit                              AS rolconnlimit ",
                        //    $"FROM          pg_authid                                   AS a; "
                        // );

                        // Ale do systémového pohledu pg_roles přístup mám:
                        return String.Concat(
                            $"SELECT ",
                            $"              a.oid                                       AS oid, ",
                            $"              a.rolname                                   AS rolname, ",
                            $"              a.rolsuper                                  AS rolsuper, ",
                            $"              a.rolinherit                                AS rolinherit, ",
                            $"              a.rolcreaterole                             AS rolcreaterole, ",
                            $"              a.rolcreatedb                               AS rolcreatedb, ",
                            $"              a.rolcanlogin                               AS rolcanlogin, ",
                            $"              a.rolreplication                            AS rolreplication, ",
                            $"              a.rolbypassrls                              AS rolbypassrls, ",
                            $"              a.rolconnlimit                              AS rolconnlimit ",
                            $"FROM          pg_roles                                    AS a; "
                         );
                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu rolí
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu rolí</returns>
                public static DataTable EmptyPgAuthid()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolsuper", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolinherit", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolcreaterole", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolcreatedb", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolcanlogin", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolreplication", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolbypassrls", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "rolconnlimit", DataType = Type.GetType(typeName: "System.Int32") });
                    return result;
                }


                /// <summary>
                /// SQL příkaz pro výběr seznamu uložených procedur (read-only)
                /// </summary>
                private string SqlPgProc
                {
                    get
                    {
                        // * oid                                 oid
                        // Row identifier

                        // * proname                             name
                        // Name of the function

                        // * pronamespace                        oid(references pg_namespace.oid)
                        // The OID of the namespace that contains this function

                        // * proowner                           oid(references pg_authid.oid)
                        // Owner of the function

                        // * prolang                            oid (references pg_language.oid)
                        // Implementation language or call interface of this function

                        // procost                              float4
                        // Estimated execution cost (in units of cpu_operator_cost);
                        // if proretset, this is cost per row returned

                        // prorows                              float4
                        // Estimated number of result rows (zero if not proretset)

                        // provariadic                         oid (references pg_type.oid)
                        // Data type of the variadic array parameter's elements,
                        // or zero if the function does not have a variadic parameter

                        // prosupport                           regproc (references pg_proc.oid)
                        // Planner support function for this function (see Section 38.11), or zero if none

                        // * prokind                            char
                        // f for a normal function,
                        // p for a procedure,
                        // a for an aggregate function,
                        // or w for a window function
                        // version 11+

                        // * prosecdef                          bool
                        // Function is a security definer (i.e., a “setuid” function)

                        // proleakproof                         bool
                        // The function has no side effects.
                        // No information about the arguments is conveyed except via the return value.
                        // Any function that might throw an error depending on the values
                        // of its arguments is not leak-proof.

                        // * proisstrict                        bool
                        // Function returns null if any call argument is null.
                        // In that case the function won't actually be called at all.
                        // Functions that are not “strict” must be prepared to handle null inputs.

                        // * proretset                          bool
                        // Function returns a set (i.e., multiple values of the specified data type)

                        // * provolatile                        char
                        // provolatile tells whether the function's result depends only on its input arguments,
                        // or is affected by outside factors.
                        // It is i for “immutable” functions, which always deliver the same result for the same inputs.
                        // It is s for “stable” functions, whose results (for fixed inputs) do not change within a scan.
                        // It is v for “volatile” functions, whose results might change at any time.
                        // (Use v also for functions with side-effects, so that calls to them cannot get optimized away.)

                        // proparallel                          char
                        // proparallel tells whether the function can be safely run in parallel mode.
                        // It is s for functions which are safe to run in parallel mode without restriction.
                        // It is r for functions which can be run in parallel mode,
                        // but their execution is restricted to the parallel group leader;
                        // parallel worker processes cannot invoke these functions.
                        // It is u for functions which are unsafe in parallel mode;
                        // the presence of such a function forces a serial execution plan.

                        // pronargs                             int2
                        // Number of input arguments

                        // pronargdefaults                      int2
                        // Number of arguments that have defaults

                        // * prorettype                         oid (references pg_type.oid)
                        // Data type of the return value

                        // * proargtypes                        oidvector (references pg_type.oid)
                        // An array of the data types of the function arguments.
                        // This includes only input arguments(including INOUT and VARIADIC arguments),
                        // and thus represents the call signature of the function.

                        // * proallargtypes                     oid[] (references pg_type.oid)
                        // An array of the data types of the function arguments.
                        // This includes all arguments(including OUT and INOUT arguments);
                        // however, if all the arguments are IN arguments,
                        // this field will be null.
                        // Note that subscripting is 1 - based, whereas for historical reasons proargtypes is
                        // subscripted from 0.

                        // * proargmodes                        char[]
                        // An array of the modes of the function arguments,
                        // encoded as i for IN arguments, o for OUT arguments,
                        // b for INOUT arguments, v for VARIADIC arguments, t for TABLE arguments.
                        // If all the arguments are IN arguments, this field will be null.
                        // Note that subscripts correspond to positions of proallargtypes not proargtypes.

                        // * proargnames                        text[]
                        // An array of the names of the function arguments.
                        // Arguments without a name are set to empty strings in the array.
                        // If none of the arguments have a name, this field will be null.
                        // Note that subscripts correspond to positions of proallargtypes not proargtypes.

                        // proargdefaults                       pg_node_tree
                        // Expression trees(in nodeToString() representation) for default values.
                        // This is a list with pronargdefaults elements,
                        // corresponding to the last N input arguments(i.e., the last N proargtypes positions).
                        // If none of the arguments have defaults, this field will be null.

                        // protrftypes                          oid[](references pg_type.oid)
                        // An array of the argument / result data type(s) for which
                        // to apply transforms(from the function's TRANSFORM clause).
                        // Null if none.

                        // prosrc                               text
                        // This tells the function handler how to invoke the function.
                        // It might be the actual source code of the function for interpreted languages, a link symbol,
                        // a file name, or just about anything else,
                        // depending on the implementation language / call convention.

                        // probin                               text
                        // Additional information about how to invoke the function.
                        // Again, the interpretation is language - specific.

                        // prosqlbody                           pg_node_tree
                        // Pre - parsed SQL function body.
                        // This is used for SQL - language functions when the body is given in
                        // SQL - standard notation rather than as a string literal.It's null in other cases.

                        // proconfig                            text[]
                        // Function's local settings for run-time configuration variables

                        // proacl                               aclitem[]
                        // Access privileges;

                        if (ServerVersionNum == null)
                        {
                            return null;
                        }

                        if (ServerVersionNum < 110000)
                        {
                            return String.Concat(
                            $"SELECT ",
                            $"     a.oid                                            AS oid, ",
                            $"     a.proname                                        AS proname, ",
                            $"     a.pronamespace                                   AS pronamespace, ",
                            $"     a.proowner                                       AS proowner, ",
                            $"     a.prolang                                        AS prolang, ",
                            $"     b.lanname                                        AS prolangname, ",
                            $"     '0'                                              AS prokind, ",
                            $"     a.prosecdef                                      AS prosecdef, ",
                            $"     a.proisstrict                                    AS proisstrict, ",
                            $"     a.proretset                                      AS proretset, ",
                            $"     a.provolatile                                    AS provolatile, ",
                            $"     a.prorettype                                     AS prorettype, ",
                            $"     array_to_string(a.proargtypes, ';', 'NULL')      AS proargtypes, ",
                            $"     array_to_string(a.proallargtypes, ';', 'NULL')   AS proallargtypes, ",
                            $"     array_to_string(a.proargmodes, ';', 'NULL')      AS proargmodes, ",
                            $"     array_to_string(a.proargnames, ';', 'NULL')      AS proargnames, ",
                            $"     pg_catalog.pg_get_function_arguments(a.oid)      AS function_arguments, ",
                            $"     pg_catalog.pg_get_function_result(a.oid)         AS function_result, ",
                            $"     CASE WHEN b.lanname = 'internal' ",
                            $"     THEN a.prosrc ",
                            $"     ELSE pg_get_functiondef(a.oid) END               AS function_definition ",
                            $"FROM pg_proc                                          AS a ",
                            $"LEFT JOIN pg_language                                 AS b ON (a.prolang = b.oid) "
                            );
                        }

                        return String.Concat(
                            $"SELECT ",
                            $"     a.oid                                            AS oid, ",
                            $"     a.proname                                        AS proname, ",
                            $"     a.pronamespace                                   AS pronamespace, ",
                            $"     a.proowner                                       AS proowner, ",
                            $"     a.prolang                                        AS prolang, ",
                            $"     b.lanname                                        AS prolangname, ",
                            $"     a.prokind                                        AS prokind, ",
                            $"     a.prosecdef                                      AS prosecdef, ",
                            $"     a.proisstrict                                    AS proisstrict, ",
                            $"     a.proretset                                      AS proretset, ",
                            $"     a.provolatile                                    AS provolatile, ",
                            $"     a.prorettype                                     AS prorettype, ",
                            $"     array_to_string(a.proargtypes, ';', 'NULL')      AS proargtypes, ",
                            $"     array_to_string(a.proallargtypes, ';', 'NULL')   AS proallargtypes, ",
                            $"     array_to_string(a.proargmodes, ';', 'NULL')      AS proargmodes, ",
                            $"     array_to_string(a.proargnames, ';', 'NULL')      AS proargnames, ",
                            $"     pg_catalog.pg_get_function_arguments(a.oid)      AS function_arguments, ",
                            $"     pg_catalog.pg_get_function_result(a.oid)         AS function_result, ",
                            $"     CASE WHEN b.lanname = 'internal' ",
                            $"     THEN a.prosrc ",
                            $"     ELSE pg_get_functiondef(a.oid) END               AS function_definition ",
                            $"FROM pg_proc                                          AS a ",
                            $"LEFT JOIN pg_language                                 AS b ON (a.prolang = b.oid) "
                            );

                    }
                }

                /// <summary>
                /// Připraví prázdnou datovou tabulku seznamu uložených procedur
                /// </summary>
                /// <returns>Prázdná datová tabulka seznamu uložených procedur</returns>
                public static DataTable EmptyPgProc()
                {
                    DataTable result = new();
                    result.Columns.Add(column: new DataColumn() { ColumnName = "oid", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "proname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "pronamespace", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "proowner", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "prolang", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "prolangname", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "prokind", DataType = Type.GetType(typeName: "System.Char") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "prosecdef", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "proisstrict", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "proretset", DataType = Type.GetType(typeName: "System.Boolean") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "provolatile", DataType = Type.GetType(typeName: "System.Char") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "prorettype", DataType = Type.GetType(typeName: "System.UInt32") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "proargtypes", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "proallargtypes", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "proargmodes", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "proargnames", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "function_arguments", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "function_result", DataType = Type.GetType(typeName: "System.String") });
                    result.Columns.Add(column: new DataColumn() { ColumnName = "function_definition", DataType = Type.GetType(typeName: "System.String") });
                    return result;
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Načtení dat z databáze do datové tabulky
                /// </summary>
                /// <param name="command">Příkaz pro výběr dat pro načtení do datové tabulky</param>
                /// <returns>Datová tabulka s načtenými daty z databáze</returns>
                private static DataTable LoadDataTable(NpgsqlCommand command)
                {
                    DataTable result = new();
                    NpgsqlDataAdapter adapter = new(selectCommand: command);
                    adapter.Fill(dataTable: result);
                    return result;
                }

                /// <summary>
                /// Načtení katalogů databáze, pokud ještě načtené nebyly
                /// </summary>
                public void Load()
                {
                    if (!Initialized)
                    {
                        ReLoad();
                    }
                }

                /// <summary>
                /// Znovu načtení katalogů databáze
                /// </summary>
                public void ReLoad()
                {
                    if (Database?.Initialized ?? false)
                    {
                        try
                        {
                            if (Database.Connection.State != ConnectionState.Open)
                            {
                                Database.Connection.Open();
                            }

                            string strServerVersionNum =
                                Database.ExecuteScalar(sqlCommand: "SHOW server_version_num;");
                            if (!String.IsNullOrEmpty(value: strServerVersionNum))
                            {
                                if (Int32.TryParse(s: strServerVersionNum, result: out int svn))
                                {
                                    ServerVersionNum = svn;
                                }
                                else
                                {
                                    ServerVersionNum = null;
                                }
                            }
                            else
                            {
                                ServerVersionNum = null;
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgExtension))
                            {
                                NpgsqlCommand cmdPgExtension =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgExtension
                                };
                                PgExtension = LoadDataTable(command: cmdPgExtension);
                            }
                            else
                            {
                                PgExtension = EmptyPgExtension();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgNamespace))
                            {
                                NpgsqlCommand cmdPgNamespace =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgNamespace
                                };
                                PgNamespace = LoadDataTable(command: cmdPgNamespace);
                            }
                            else
                            {
                                PgNamespace = EmptyPgNamespace();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgClass))
                            {
                                NpgsqlCommand cmdPgClass =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgClass
                                };
                                PgClass = LoadDataTable(command: cmdPgClass);
                            }
                            else
                            {
                                PgClass = EmptyPgClass();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgSequence))
                            {
                                NpgsqlCommand cmdPgSequence =
                                    new()
                                    {
                                        Connection = Database.Connection,
                                        CommandText = SqlPgSequence
                                    };
                                PgSequence = LoadDataTable(command: cmdPgSequence);
                            }
                            else
                            {
                                PgSequence = EmptyPgSequence();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgIndex))
                            {
                                NpgsqlCommand cmdPgIndex =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgIndex
                                };
                                PgIndex = LoadDataTable(command: cmdPgIndex);
                            }
                            else
                            {
                                PgIndex = EmptyPgIndex();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgAttribute))
                            {
                                NpgsqlCommand cmdPgAttribute =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgAttribute
                                };
                                PgAttribute = LoadDataTable(command: cmdPgAttribute);
                            }
                            else
                            {
                                PgAttribute = EmptyPgAttribute();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgAttrDef))
                            {
                                NpgsqlCommand cmdPgAttrDef =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgAttrDef
                                };
                                PgAttrDef = LoadDataTable(command: cmdPgAttrDef);
                            }
                            else
                            {
                                PgAttrDef = EmptyPgAttrDef();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgType))
                            {
                                NpgsqlCommand cmdPgType =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgType
                                };
                                PgType = LoadDataTable(command: cmdPgType);
                            }
                            else
                            {
                                PgType = EmptyPgType();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgAM))
                            {
                                NpgsqlCommand cmdPgAM =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgAM
                                };
                                PgAM = LoadDataTable(command: cmdPgAM);
                            }
                            else
                            {
                                PgAM = EmptyPgAM();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgLanguage))
                            {
                                NpgsqlCommand cmdPgLanguage =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgLanguage
                                };
                                PgLanguage = LoadDataTable(command: cmdPgLanguage);
                            }
                            else
                            {
                                PgLanguage = EmptyPgLanguage();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgConstraint))
                            {
                                NpgsqlCommand cmdPgConstraint =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgConstraint
                                };
                                PgConstraint = LoadDataTable(command: cmdPgConstraint);
                            }
                            else
                            {
                                PgConstraint = EmptyPgConstraint();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgTrigger))
                            {
                                NpgsqlCommand cmdPgTrigger =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgTrigger
                                };
                                PgTrigger = LoadDataTable(command: cmdPgTrigger);
                            }
                            else
                            {
                                PgTrigger = EmptyPgTrigger();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgDescription))
                            {
                                NpgsqlCommand cmdPgDescription =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgDescription
                                };
                                PgDescription = LoadDataTable(command: cmdPgDescription);
                            }
                            else
                            {
                                PgDescription = EmptyPgDescription();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgAuthid))
                            {
                                NpgsqlCommand cmdPgAuthid =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgAuthid
                                };
                                PgAuthId = LoadDataTable(command: cmdPgAuthid);
                            }
                            else
                            {
                                PgAuthId = EmptyPgAuthid();
                            }

                            if (!String.IsNullOrEmpty(value: SqlPgProc))
                            {
                                NpgsqlCommand cmdPgProc =
                                new()
                                {
                                    Connection = Database.Connection,
                                    CommandText = SqlPgProc
                                };
                                PgProc = LoadDataTable(command: cmdPgProc);
                            }
                            else
                            {
                                PgProc = EmptyPgProc();
                            }

                            Initialized = true;
                        }

                        catch (Exception e)
                        {
                            MessageBox.Show(
                                text: e.Message,
                                caption: String.Empty,
                                buttons: MessageBoxButtons.OK,
                                icon: MessageBoxIcon.Information);
                            Initialized = false;
                        }

                        finally
                        {
                            if (!Database.StayConnected)
                            {
                                Database.Connection.Close();
                            }
                        }
                    }
                    else
                    {
                        Initialized = false;
                    }
                }

                #endregion Methods

            }

        }
    }
}