﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace ZaJi
{
    namespace PostgreSQL
    {
        namespace Catalog
        {

            /// <summary>
            /// Trigger
            /// </summary>
            public class DBTrigger
                : IDBObject
            {

                #region Constants

                /// <summary>
                /// Tabulátor
                /// </summary>
                private const string tab = "    ";

                #endregion Constants


                #region Private Fields

                /// <summary>
                /// Seznam triggerů
                /// </summary>
                private DBTriggerList triggers;

                /// <summary>
                /// Metadata triggeru
                /// </summary>
                private DataRow data;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu triggeru
                /// </summary>
                /// <param name="triggers">Seznam triggerů</param>
                /// <param name="data">Metadata triggeru</param>
                public DBTrigger(DBTriggerList triggers, DataRow data)
                {
                    Triggers = triggers;
                    Data = data;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Seznam triggerů (read-only)
                /// </summary>
                public DBTriggerList Triggers
                {
                    get
                    {
                        return triggers;
                    }
                    private set
                    {
                        triggers = value;
                    }
                }

                /// <summary>
                /// Metadata triggeru (read-only)
                /// </summary>
                public DataRow Data
                {
                    get
                    {
                        return data;
                    }
                    private set
                    {
                        data = value;
                    }
                }

                #endregion Properties


                #region Derived Properties

                /// <summary>
                /// Identifikátor tabulky (read-only)
                /// </summary>
                public uint TableId
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "tgrelid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBClass Table
                {
                    get
                    {
                        return Triggers.Table;
                    }
                }


                /// <summary>
                /// Identifikátor triggeru (read-only)
                /// </summary>
                public uint Id
                {
                    get
                    {
                        return
                            Functions.GetUIntArg(
                                row: Data,
                                name: "oid",
                                defaultValue: 0);
                    }
                }

                /// <summary>
                /// Jméno triggeru (read-only)
                /// </summary>
                public string Name
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "tgname",
                                defaultValue: null)
                            ?? String.Empty;
                    }
                }


                /// <summary>
                /// Identifikátor uložené procedury (read-only)
                /// </summary>
                public Nullable<uint> StoredProcId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "tgfoid",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Uložená procedura - objekt (read-only)
                /// </summary>
                public DBStoredProcedure StoredProc
                {
                    get
                    {
                        if (StoredProcId == null)
                        {
                            return null;
                        }

                        return Table.Schema.StoredProcedures[(uint)StoredProcId];
                    }
                }


                /// <summary>
                /// Identifikátor omezení (read-only)
                /// </summary>
                public Nullable<uint> ConstraintId
                {
                    get
                    {
                        return
                            Functions.GetNUIntArg(
                                row: Data,
                                name: "tgconstraint",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Constraint trigger (read-only)
                /// </summary>
                public bool IsConstraint
                {
                    get
                    {
                        if (ConstraintId == null)
                        {
                            return false;
                        }

                        return
                            Table.Schema.Catalog.PgConstraint.AsEnumerable()
                            .Where(a => a.Field<uint>("oid") == ConstraintId)
                            .Any();
                    }
                }


                /// <summary>
                /// Systémový trigger (read-only)
                /// </summary>
                public Nullable<bool> IsInternal
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgisinternal",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Trigger je odložitelný (read-only)
                /// </summary>
                public Nullable<bool> Deferrable
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgdeferrable",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Trigger je odložitelný - text (read-only)
                /// </summary>
                protected string DeferrableText
                {
                    get
                    {
                        return
                            (Deferrable == null)
                                ? String.Empty
                                : (bool)Deferrable
                                    ? "DEFERRABLE"
                                    : "NOT DEFERRABLE";
                    }
                }


                /// <summary>
                /// Trigger je odložen (read-only)
                /// </summary>
                public Nullable<bool> Deferred
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tginitdeferred",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Trigger je odložen - text (read-only)
                /// </summary>
                protected string DeferredText
                {
                    get
                    {
                        return
                            (Deferrable == null)
                                ? String.Empty
                                : (bool)Deferred
                                    ? "INITIALLY DEFERRED"
                                    : "INITIALLY IMMEDIATE";
                    }
                }


                /// <summary>
                /// Seznam sloupců u triggerů spouštěných pouze při update sloupce - identifikátory (read-only)
                /// </summary>
                public List<uint> TriggerColumnsIds
                {
                    get
                    {
                        return
                            [.. Functions.StringToUIntList(
                                text: Functions.GetStringArg(
                                    row: Data,
                                    name: "tgattr",
                                    defaultValue: null),
                                separator: ';')
                            .OrderBy(a => a)];
                    }
                }

                /// <summary>
                /// Seznam sloupců u triggerů spouštěných pouze při update sloupce - objekty (read-only)
                /// </summary>
                public List<DBColumn> TriggerColumns
                {
                    get
                    {
                        if (Table == null)
                        {
                            return [];
                        }

                        if (Table is not DBTable)
                        {
                            return [];
                        }

                        return
                            [.. ((DBTable)Table).Columns.Items
                                .Where(a => TriggerColumnsIds.Contains(item: a.Id))
                                .OrderBy(a => a.Id)];
                    }
                }

                /// <summary>
                /// Seznam sloupců u triggerů spouštěných pouze při update sloupce - text (read-only)
                /// </summary>
                protected string TriggerColumnsText
                {
                    get
                    {
                        if (!(IsUpdate ?? false))
                        {
                            return String.Empty;
                        }

                        if (TriggerColumns.Count == 0)
                        {
                            return String.Empty;
                        }

                        return
                            TriggerColumns
                                .Select(a => a.Name)
                                .Aggregate((a, b) => $"{a}, {b}");
                    }
                }


                /// <summary>
                /// Bitová maska určující typ triggeru (read-only)
                /// </summary>
                public Nullable<short> TriggerType
                {
                    get
                    {
                        return
                            Functions.GetNShortArg(
                                row: Data,
                                name: "tgtype",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Rozsah triggeru
                /// true: statement,
                /// false: each row (read-only)
                /// </summary>
                public Nullable<bool> IsStatement
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgisstatement",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Doba spuštění triggeru
                /// true: before action,
                /// false: after action (read-only)
                /// </summary>
                public Nullable<bool> IsBefore
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgisbefore",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Insert trigger (read-only)
                /// </summary>
                public Nullable<bool> IsInsert
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgisinsert",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Delete trigger (read-only)
                /// </summary>
                public Nullable<bool> IsDelete
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgisdelete",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Update trigger (read-only)
                /// </summary>
                public Nullable<bool> IsUpdate
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgisupdate",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Truncate trigger (read-only)
                /// </summary>
                public Nullable<bool> IsTruncate
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgistruncate",
                                defaultValue: null);
                    }
                }

                /// <summary>
                /// Instead trigger (read-only)
                /// </summary>
                public Nullable<bool> IsInstead
                {
                    get
                    {
                        return
                            Functions.GetNBoolArg(
                                row: Data,
                                name: "tgisinstead",
                                defaultValue: null);
                    }
                }


                /// <summary>
                /// Rozsah triggeru (read-only)
                /// </summary>
                public TriggerScope TriggerScope
                {
                    get
                    {
                        return
                            (IsStatement ?? false)
                                ? TriggerScope.Statement
                                : TriggerScope.Row;
                    }
                }

                /// <summary>
                /// Rozsah triggeru - sql text(read-only)
                /// </summary>
                public string TriggerScopeText
                {
                    get
                    {
                        return TriggerScope switch
                        {
                            TriggerScope.Statement => "FOR EACH STATEMENT",
                            TriggerScope.Row => "FOR EACH ROW",
                            _ => String.Empty,
                        };
                    }
                }


                /// <summary>
                /// Spuštění triggeru (read-only)
                /// </summary>
                public TriggerTiming TriggerTiming
                {
                    get
                    {
                        if (IsInstead ?? false)
                        {
                            return TriggerTiming.Instead;
                        }
                        else if (IsBefore == null)
                        {
                            return TriggerTiming.Unknown;
                        }
                        else if (IsBefore ?? false)
                        {
                            return TriggerTiming.Before;
                        }
                        else
                        {
                            return TriggerTiming.After;
                        }
                    }
                }

                /// <summary>
                /// Spuštění triggeru - sql text (read-only)
                /// </summary>
                public string TriggerTimingText
                {
                    get
                    {
                        return TriggerTiming switch
                        {
                            TriggerTiming.Before => "BEFORE",
                            TriggerTiming.After => "AFTER",
                            TriggerTiming.Instead => "INSTEAD OF",
                            _ => String.Empty,
                        };
                    }
                }


                /// <summary>
                /// Akce spouštějící trigger (read-only)
                /// </summary>
                public List<TriggerAction> TriggerActions
                {
                    get
                    {
                        List<TriggerAction> result =
                            [];

                        if (IsInsert ?? false)
                        {
                            result.Add(TriggerAction.Insert);
                        }

                        if (IsDelete ?? false)
                        {
                            result.Add(TriggerAction.Delete);
                        }

                        if (IsUpdate ?? false)
                        {
                            result.Add(TriggerAction.Update);
                        }

                        if (IsTruncate ?? false)
                        {
                            result.Add(TriggerAction.Truncate);
                        }

                        return result;
                    }
                }

                /// <summary>
                /// Akce spouštějící trigger (read-only)
                /// </summary>
                public string TriggerActionsText
                {
                    get
                    {
                        string result = String.Empty;

                        if (TriggerActions == null)
                        {
                            return result;
                        }

                        if (TriggerActions.Count == 0)
                        {
                            return result;
                        }

                        foreach (TriggerAction action in TriggerActions)
                        {
                            if (String.IsNullOrEmpty(value: result))
                            {
                                switch (action)
                                {
                                    case TriggerAction.Insert:
                                        result = "INSERT";
                                        break;
                                    case TriggerAction.Delete:
                                        result = "DELETE";
                                        break;
                                    case TriggerAction.Update:
                                        result = String.IsNullOrEmpty(value: TriggerColumnsText)
                                            ? "UPDATE"
                                            : $"UPDATE OF {TriggerColumnsText}";
                                        break;
                                    case TriggerAction.Truncate:
                                        result = "TRUNCATE";
                                        break;
                                }
                            }
                            else
                            {
                                switch (action)
                                {
                                    case TriggerAction.Insert:
                                        result = String.Concat(result, " OR INSERT");
                                        break;
                                    case TriggerAction.Delete:
                                        result = String.Concat(result, " OR DELETE");
                                        break;
                                    case TriggerAction.Update:
                                        result = String.Concat(result,
                                            String.IsNullOrEmpty(value: TriggerColumnsText)
                                            ? " OR UPDATE"
                                            : $" OR UPDATE OF {TriggerColumnsText}"
                                        );
                                        break;
                                    case TriggerAction.Truncate:
                                        result = String.Concat(result, " OR TRUNCATE");
                                        break;
                                }
                            }
                        }

                        return result;
                    }
                }

                /// <summary>
                /// Definice triggeru (read-only)
                /// </summary>
                public string Definition
                {
                    get
                    {
                        return
                            Functions.GetStringArg(
                                row: Data,
                                name: "tgdef",
                                defaultValue: String.Empty);
                    }
                }

                /// <summary>
                /// Popis objektu (read-only)
                /// </summary>
                public string Comment
                {
                    get
                    {
                        return
                            Table.Schema.Catalog.PgDescription.AsEnumerable()
                                .Where(a => a.Field<Nullable<uint>>("objoid") == Id)
                                .Select(a =>
                                    Functions.GetStringArg(
                                        row: a,
                                        name: "description",
                                        defaultValue: String.Empty))
                                .FirstOrDefault<string>();
                    }
                }

                /// <summary>
                /// SQL definice triggeru
                /// </summary>
                public string SqlDefinition
                {
                    get
                    {
                        StringBuilder sb = new();

                        sb.AppendLine(value: $"{tab}-- {Name}");

                        if (Table == null)
                        {
                            return sb.ToString();
                        }

                        if (StoredProc == null)
                        {
                            return sb.ToString();
                        }

                        if (Table.Schema == null)
                        {
                            return sb.ToString();
                        }

                        if (StoredProc.Schema == null)
                        {
                            return sb.ToString();
                        }

                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}-- DROP TRIGGER IF EXISTS {Name}");
                        sb.AppendLine(value: $"{tab}-- {tab}ON {Table.Schema.Name}.{Table.Name};");
                        sb.AppendLine();

                        if (!IsConstraint)
                        {
                            sb.AppendLine(value: $"{tab}CREATE OR REPLACE TRIGGER {Name}");
                            sb.AppendLine(value: $"{tab}{tab}{TriggerTimingText} {TriggerActionsText}");
                            sb.AppendLine(value: $"{tab}{tab}ON {Table.Schema.Name}.{Table.Name}");
                            sb.AppendLine(value: $"{tab}{tab}{TriggerScopeText}");
                            sb.AppendLine(value: $"{tab}{tab}EXECUTE PROCEDURE");
                            sb.AppendLine(value: $"{tab}{tab}{tab}{StoredProc.Schema.Name}.{StoredProc.Name}();");
                        }
                        else
                        {
                            sb.AppendLine(value: $"{tab}CREATE OR REPLACE CONSTRAINT TRIGGER {Name}");
                            sb.AppendLine(value: $"{tab}{tab}{TriggerTimingText} {TriggerActionsText}");
                            sb.AppendLine(value: $"{tab}{tab}ON {Table.Schema.Name}.{Table.Name}");
                            sb.AppendLine(value: $"{tab}{tab}{DeferrableText}");
                            sb.AppendLine(value: $"{tab}{tab}{DeferredText}");
                            sb.AppendLine(value: $"{tab}{tab}{TriggerScopeText}");
                            sb.AppendLine(value: $"{tab}{tab}EXECUTE PROCEDURE");
                            sb.AppendLine(value: $"{tab}{tab}{tab}{StoredProc.Schema.Name}.{StoredProc.Name}();");
                        }

                        sb.AppendLine();
                        sb.AppendLine(value: $"{tab}COMMENT ON TRIGGER {Name}");
                        sb.AppendLine(value: $"{tab}{tab}ON {Table.Schema.Name}.{Table.Name}");
                        sb.AppendLine(value: $"{tab}{tab}IS '{Comment}';");

                        return sb.ToString();
                    }
                }

                #endregion Derived Properties


                #region Methods

                /// <summary>
                /// Určuje zda zadaný trigger je stejný
                /// </summary>
                /// <param name="obj">Zadaný trigger</param>
                /// <returns>true/false</returns>
                public override bool Equals(object obj)
                {
                    // If the passed object is null, return False
                    if (obj == null)
                    {
                        return false;
                    }

                    // If the passed object is not DBTrigger Type, return False
                    if (obj is not DBTrigger)
                    {
                        return false;
                    }

                    return
                        Id == ((DBTrigger)obj).Id;
                }

                /// <summary>
                /// Vrací hash code
                /// </summary>
                /// <returns>Hash code</returns>
                public override int GetHashCode()
                {
                    return
                        Id.GetHashCode();
                }

                /// <summary>
                /// Textový popis triggeru
                /// </summary>
                /// <returns>Textový popis triggeru</returns>
                public override string ToString()
                {
                    return Table.Schema.Catalog.Database.Setting.LanguageVersion switch
                    {
                        LanguageVersion.International => $"Trigger oid = {Id} name = {Name}.",
                        LanguageVersion.National => $"Trigger oid = {Id} name = {Name}.",
                        _ => String.Empty,
                    };
                }

                #endregion Methods

            }


            /// <summary>
            /// Seznam triggerů
            /// </summary>
            public class DBTriggerList
            {

                #region Private Fields

                /// <summary>
                /// Tabulka
                /// </summary>
                private DBTable table;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu seznamu triggerů
                /// </summary>
                /// <param name="table">Tabulka</param>
                public DBTriggerList(DBTable table)
                {
                    Table = table;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Tabulka (read-only)
                /// </summary>
                public DBTable Table
                {
                    get
                    {
                        return table;
                    }
                    private set
                    {
                        table = value;
                    }
                }

                /// <summary>
                /// Data triggerů (read-only)
                /// </summary>
                public DataTable Data
                {
                    get
                    {
                        DataRow[] rows = Table.Schema.Catalog.PgTrigger
                            .Select($"tgrelid = {table.Id}");

                        if (rows.Length != 0)
                        {
                            return rows.CopyToDataTable<DataRow>();
                        }
                        else
                        {
                            return DBCatalog.EmptyPgTrigger();
                        }
                    }
                }

                /// <summary>
                /// Seznam triggerů (read-only)
                /// </summary>
                public List<DBTrigger> Items
                {
                    get
                    {
                        return
                            [.. Data.AsEnumerable().Select(a => new DBTrigger(triggers: this, data: a))];
                    }
                }

                #endregion Properties


                #region Indexer

                /// <summary>
                /// Trigger podle identifikátoru
                /// </summary>
                /// <param name="id">Identifikátor triggeru</param>
                /// <returns>Trigger</returns>
                public DBTrigger this[uint id]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Id == id)
                            .FirstOrDefault<DBTrigger>();
                    }
                }

                /// <summary>
                /// Trigger podle jména
                /// </summary>
                /// <param name="name">Jméno triggeru</param>
                /// <returns>Trigger</returns>
                public DBTrigger this[string name]
                {
                    get
                    {
                        return
                            Items
                            .Where(a => a.Name == name)
                            .FirstOrDefault<DBTrigger>();
                    }
                }

                #endregion Indexer

            }

        }

        /// <summary>
        /// Rozsah triggeru
        /// </summary>
        public enum TriggerScope
        {
            /// <summary>
            /// "Row" trigger
            /// </summary>
            Row = (int)'r',

            /// <summary>
            /// "Statement" trigger
            /// </summary>
            Statement = (int)'s',

            /// <summary>
            /// Unknown
            /// </summary>
            Unknown = (int)'0'
        };

        /// <summary>
        /// Spuštění triggeru
        /// </summary>
        public enum TriggerTiming
        {
            /// <summary>
            /// "Before" trigger
            /// </summary>
            Before = (int)'b',

            /// <summary>
            /// "After" trigger
            /// </summary>
            After = (int)'a',

            /// <summary>
            /// "Instead of" trigger
            /// </summary>
            Instead = (int)'i',

            /// <summary>
            /// Unknown
            /// </summary>
            Unknown = (int)'0'
        };

        /// <summary>
        /// Akce spouštějící trigger
        /// </summary>
        public enum TriggerAction
        {
            /// <summary>
            /// "On Insert" trigger
            /// </summary>
            Insert = (int)'i',

            /// <summary>
            /// "On Delete" trigger
            /// </summary>
            Delete = (int)'d',

            /// <summary>
            /// "On Update" trigger
            /// </summary>
            Update = (int)'u',

            /// <summary>
            /// "On Truncate" trigger
            /// </summary>
            Truncate = (int)'t',

            /// <summary>
            /// Unknown
            /// </summary>
            Unknown = (int)'0'
        };

    }
}
