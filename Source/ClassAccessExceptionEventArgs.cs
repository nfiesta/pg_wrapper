﻿//
// Copyright 2020, 2025 NLI
//
// Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL(the "Licence");
// You may not use this work except in compliance with the Licence.
// You may obtain a copy of the Licence at:
//
// https://joinup.ec.europa.eu/software/page/eupl
//
// Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the Licence for the specific language governing permissions and limitations under the Licence.
//

using System;
using System.Data.OleDb;

namespace ZaJi
{
    namespace Access
    {

        /// <summary>
        /// Připojení k databázi Access
        /// </summary>
        public partial class AccessWrapper
        {

            /// <summary>
            /// Zpráva o chybě při zpracování SQL příkazu
            /// </summary>
            public class ExceptionEventArgs : EventArgs
            {

                #region Private Fields

                /// <summary>
                /// Identifikační číslo
                /// </summary>
                private int id;

                /// <summary>
                /// Jméno funkce nebo metody
                /// </summary>
                private string functionName;

                /// <summary>
                /// Nadpis pro zobrazení v MessageBox
                /// </summary>
                private string messageBoxCaption;

                /// <summary>
                /// Zpráva pro zobrazení v MessageBox
                /// </summary>
                private string messageBoxText;

                /// <summary>
                /// Objekt výjimky
                /// </summary>
                private Exception exceptionObject;

                /// <summary>
                /// Příkaz, který způsobil výjimku
                /// </summary>
                private OleDbCommand commandObject;

                #endregion Private Fields


                #region Constructor

                /// <summary>
                /// Konstruktor objektu
                /// </summary>
                public ExceptionEventArgs()
                {
                    Id = 0;
                    FunctionName = null;
                    MessageBoxCaption = null;
                    MessageBoxText = null;
                    ExceptionObject = null;
                    CommandObject = null;
                }

                #endregion Constructor


                #region Properties

                /// <summary>
                /// Identifikační číslo
                /// </summary>
                public int Id
                {
                    get
                    {
                        return id;
                    }
                    set
                    {
                        id = value;
                    }
                }

                /// <summary>
                /// Jméno funkce nebo metody
                /// </summary>
                public string FunctionName
                {
                    get
                    {
                        return functionName ?? String.Empty;
                    }
                    set
                    {
                        functionName = value ?? String.Empty;
                    }
                }

                /// <summary>
                /// Nadpis pro zobrazení v MessageBox
                /// </summary>
                public string MessageBoxCaption
                {
                    get
                    {
                        return messageBoxCaption ?? String.Empty;
                    }
                    set
                    {
                        messageBoxCaption = value ?? String.Empty;
                    }
                }

                /// <summary>
                /// Zpráva pro zobrazení v MessageBox
                /// </summary>
                public string MessageBoxText
                {
                    get
                    {
                        return messageBoxText ?? String.Empty;
                    }
                    set
                    {
                        messageBoxText = value ?? String.Empty;
                    }
                }

                /// <summary>
                /// Objekt výjimky
                /// </summary>
                public Exception ExceptionObject
                {
                    get
                    {

                        return exceptionObject;
                    }
                    set
                    {
                        exceptionObject = value;
                    }
                }

                /// <summary>
                /// Příkaz, který způsobil výjimku
                /// </summary>
                public OleDbCommand CommandObject
                {
                    get
                    {
                        return commandObject;
                    }
                    set
                    {
                        commandObject = value;
                    }
                }

                #endregion Properties


                #region Methods

                /// <summary>
                /// Vrací textový řetězec reprezentující objekt
                /// </summary>
                /// <returns>Vrací textový řetězec reprezentující objekt</returns>
                public override string ToString()
                {
                    return $"{FunctionName}: {ExceptionObject.Message}";
                }

                #endregion Methods

            }

        }

    }
}